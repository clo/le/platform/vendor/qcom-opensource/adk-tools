"""
* Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are
* met:
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of The Linux Foundation nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
* ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
* BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
* BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

import os
import shutil
import logging
from collections import OrderedDict
from .generator.parser import JsonParser
from .generator.renderer import Renderer
from .generator.utils import getConstStringAttr

logger = logging.getLogger('adk_code_generator')
logger.setLevel(logging.INFO)

# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# create file handler which logs even debug messages
fh = logging.FileHandler('adk_code_generator.log')
fh.setLevel(logging.INFO)
fh.setFormatter(formatter)
logger.addHandler(fh)

# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
ch.setFormatter(formatter)
logger.addHandler(ch)


def generate_attr_strings(out, types, user_def):
    """
    Generate string var names for only member types
    :param types: Types dict
    :param out: Output dict
    :return: None
    """
    for type_name, type_info in types.items():
        for member in type_info.members.keys():
            if member not in out:
                out[getConstStringAttr(member)] = member
    for prop, prop_info in user_def["properties"].items():
        if prop not in out.values():
            out[getConstStringAttr(prop)] = prop

    for meth, method_info in user_def["methods"].items():
        if meth not in out.values():
            out[getConstStringAttr(meth)] = meth

    for sig, sig_info in user_def["signals"].items():
        if sig not in out.values():
            out[getConstStringAttr(sig)] = sig

def generate(user_json="user.json", out_dir="out", cache_enabled="OFF"):
    """
    Generate interface handlers and attribute classes
    :param user_json: Location of user json files
    :param out_dir: The output directory
    :return: success status of the generation
    """
    success = True
    try:
        if not os.path.exists(user_json):
            logger.error("Invalid file specified")
            success &= False
            return success

        if not os.path.exists(out_dir):
            os.mkdir(os.path.realpath(out_dir))

        template_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), "templates")

        if not os.path.exists(template_dir):
            logger.error("Templates not found")
            success &= False
            return success

        p = JsonParser(user_json=user_json, repo_json=os.path.join(os.path.dirname(os.path.abspath(__file__)), "repo", "repo.json"))
        user_def = p.parse()
        user_def = OrderedDict(sorted(user_def.items(), key=lambda x: x[0]))
        sorted_types = p.sort_type_dependencies()

        attr_strings = OrderedDict()
        generate_attr_strings(attr_strings, sorted_types, user_def)

        renderer = Renderer(user_def=user_def, adk_types=sorted_types, attr_strings=attr_strings)

        if cache_enabled=="ON":
            for in_f in ["interface_handlers.cc.jinja2", "interface_handlers.h.jinja2",
                         "attribute_classes.h.jinja2",
                         "adk_system_interface.cc.jinja2",
                         "adk_system_interface.h.jinja2",
                         "attribute_strings.h.jinja2"]:
                try:
                    logger.debug("Rendering %s ... " % in_f)
                    renderer.render(in_file=os.path.join(template_dir, in_f), out_dir=out_dir)
                except Exception as e:
                    logger.error("Failed to process %s with exception %s" % (in_f, str(e)))
                    success &= False
                    import traceback
                    traceback.print_exc()
                    return success
        else:
            for in_f in ["attribute_classes.h.jinja2",
                         "adk_system_interface.cc.jinja2",
                         "adk_system_interface.h.jinja2",
                         "attribute_strings.h.jinja2"]:
                try:
                    logger.debug("Rendering %s ... " % in_f)
                    renderer.render(in_file=os.path.join(template_dir, in_f), out_dir=out_dir)
                except Exception as e:
                    logger.error("Failed to process %s with exception %s" % (in_f, str(e)))
                    success &= False
                    import traceback
                    traceback.print_exc()
                    return success
            try:
                logger.debug("Rendering interface_handlers.cc ...")
                renderer.render(os.path.join(template_dir, "interface_handlers_no_cache.cc.jinja2"), "interface_handlers.cc", out_dir)
            except Exception as e:
                logger.error("Failed to process %s with exception %s" % ("interface_handlers_no_cache.cc.jinja2", str(e)))
                success &= False
                import traceback
                traceback.print_exc()
                return success

            try:
                logger.debug("Rendering interface_handlers.h ...")
                renderer.render(os.path.join(template_dir, "interface_handlers_no_cache.h.jinja2"), "interface_handlers.h", out_dir)
            except Exception as e:
                logger.error("Failed to process %s with exception %s" % ("interface_handlers_no_cache.h.jinja2", str(e)))
                success &= False
                import traceback
                traceback.print_exc()
                return success

        try:
            shutil.copy2(os.path.join(template_dir, "CMakeLists.txt"), out_dir)
        except Exception as e:
            success &= False
            logger.error("Failed to copy CMakeLists.txt to out dir with ex %s" % str(e))
            return success

        for in_f in ["dbus.h.jinja2", "dbus.cc.jinja2", "dbus_strings.h.jinja2"]:
            try:
                if not os.path.exists(os.path.join(out_dir, "internal_api")):
                    os.mkdir(os.path.join(out_dir, "internal_api"))
                logger.debug("Rendering %s ... " % in_f)
                renderer.render(in_file=os.path.join(template_dir, in_f), out_dir=os.path.join(out_dir, "internal_api"))
            except Exception as e:
                logger.error("Failed to process %s with exception %s" % (in_f, str(e)))
                success &= False
                import traceback
                traceback.print_exc()
                return success

        try:
            shutil.copyfile(os.path.join(template_dir, "CMakeLists_internal_api.txt"),
                            os.path.join(out_dir, "internal_api", "CMakeLists.txt"))
        except Exception as e:
            success &= False
            logger.error("Failed to copy CMakeLists.txt to out dir with ex %s" % str(e))
            return success

    except Exception as ex:
        logger.error("Error occured %s" % str(ex))
        success &= False
        return success
    return success

def main():
    import argparse
    parser = argparse.ArgumentParser(description='ADK interface Code generator')
    parser.add_argument("--user-json",
                        dest="user_json",
                        action="store",
                        default="UserInterfaces.json",
                        help="json file with user interface definitions")

    parser.add_argument("--out-dir",
                        dest="out_dir",
                        action="store",
                        default="out",
                        help="Output directory")

    parser.add_argument("--debug",
                        dest="debug",
                        action="store_true",
                        default=False,
                        help="Enable debug logs")

    parser.add_argument("--cache",
                        dest="cache_enabled",
                        action="store",
                        default="OFF",
                        help="ON/OFF Select if the generated code needs to cache the properties read from ipc connections.")

    args = parser.parse_args()
    if args.debug:
        logger.setLevel(logging.DEBUG)

    success = generate(user_json=args.user_json, out_dir=args.out_dir, cache_enabled=args.cache_enabled)
    if not success:
        logger.info("*********Failed to generate code **********")
        logger.info("deleting out directory %s" %args.out_dir)
        import shutil
        shutil.rmtree(args.out_dir)
    else:
        logger.info("*********Code generation successful**********")
        logger.info("code generated in %s" % args.out_dir)

if __name__ == "__main__":
    main()
