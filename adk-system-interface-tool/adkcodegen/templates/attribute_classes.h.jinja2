/*
 * Copyright (c) 2019-2021, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of The Linux Foundation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef AUTOGEN_ATTRIBUTE_CLASSES_H_
#define AUTOGEN_ATTRIBUTE_CLASSES_H_

#include <string>
#include <unordered_set>
#include <vector>
#include "attribute_strings.h"

namespace adk {

enum ErrorCode {
    ADK_SI_OK,
    ADK_SI_ERROR_FAILED,
    ADK_SI_ERROR_NO_SERVICE,
    ADK_SI_ERROR_NO_OBJECT,
    ADK_SI_ERROR_TIMEOUT
};

{% for type_name, type_info in adk_types.items() -%}

class {{type_name|upper_camel_case}} {
{% if type_info.description %}
/*
 {{type_info.description.strip()}}
*/
{% endif %}

 public:
    {{type_name|upper_camel_case}}() = default;
    ~{{type_name|upper_camel_case}}() = default;

    {% for member, member_info in type_info.members.items() %}
    {{member_info.type_descriptor.complete_cpp_type()}} get{{member|upper_camel_case}}() { return {{member|lower_snake_case}}_; }

    void set{{member|upper_camel_case}}(const {{member_info.type_descriptor.complete_cpp_type()}} &{{member|lower_snake_case}}) {
        {{member|lower_snake_case}}_ = {{member|lower_snake_case}};
        changed_properties_.emplace({{get_const_string_attr(member)}});
    }
    {% endfor %}

 public:
    void clearChangedProperties() { changed_properties_.clear(); }
    std::unordered_set<std::string> getChangedProperties() { return changed_properties_; }

 private:
    {% for member, member_info in type_info.members.items() %}
    {{member_info.type_descriptor.complete_cpp_type()}} {{member|lower_snake_case}}_;
    {% endfor %}

    std::unordered_set<std::string> changed_properties_;
};

{% endfor %}

class PropertiesAttr {
 public:
    PropertiesAttr() = default;
    ~PropertiesAttr() = default;

    {% for prop_name, prop_info in user_def["properties"].items() %}
    {{prop_info.type_desc.complete_cpp_type()}} get{{prop_name|upper_camel_case}}() {
        return {{prop_name|lower_snake_case}}_;
    }
    void set{{prop_name|upper_camel_case}}({{prop_info.type_desc.complete_cpp_type()}} {{prop_name|lower_snake_case}}) {
        {{prop_name|lower_snake_case}}_ = {{prop_name|lower_snake_case}};
        changed_properties_.emplace({{get_const_string_attr(prop_name)}});
    }
    {% endfor %}

    void clearChangedProperties() { changed_properties_.clear(); }
    std::unordered_set<std::string> getChangedProperties() { return changed_properties_; }

 private:
    {% for prop_name, prop_info in user_def["properties"].items() %}
    {{prop_info.type_desc.complete_cpp_type()}} {{prop_name|lower_snake_case}}_;
    {% endfor %}

    std::unordered_set<std::string> changed_properties_;
};

{% for method, method_info in user_def["methods"].items() %}
{% if method_info.in_args.len() > 1 %}
class {{method|upper_camel_case}}In {
 public:
    {{method|upper_camel_case}}In() = default;
    ~{{method|upper_camel_case}}In() = default;

    {% for in_arg, in_arg_info in method_info.in_args.items() %}
    {{in_arg_info.complete_cpp_type()}} get{{in_arg|upper_camel_case}}() { return {{in_arg|lower_snake_case}}_; }
    void set{{in_arg|upper_camel_case}}(const {{in_arg_info.complete_cpp_type()}} &{{in_arg|lower_snake_case}}) {
       {{in_arg|lower_snake_case}}_ = {{in_arg|lower_snake_case}};
    }

    {% endfor %}

 private:
    {% for in_arg, in_arg_info in method_info.in_args.items() %}
    {{in_arg_info.complete_cpp_type()}} {{in_arg|lower_snake_case}}_;
    {% endfor %}
};
{% endif %}

{% if method_info.out_args.len() > 1 %}
class {{method|upper_camel_case}}Out {
 public:
    {{method|upper_camel_case}}Out() = default;
    ~{{method|upper_camel_case}}Out() = default;

    {% for out_arg, out_arg_info in method_info.out_args.items() %}
    {{out_arg_info.complete_cpp_type()}} get{{out_arg|upper_camel_case}}() { return {{out_arg|lower_snake_case}}_; }
    void set{{out_arg|upper_camel_case}}(const {{out_arg_info.complete_cpp_type()}} &{{out_arg|lower_snake_case}}) {
       {{out_arg|lower_snake_case}}_ = {{out_arg|lower_snake_case}};
    }

    {% endfor %}

 private:
    {% for out_arg, out_arg_info in method_info.out_args.items() %}
    {{out_arg_info.complete_cpp_type()}} {{out_arg|lower_snake_case}}_;
    {% endfor %}
};
{% endif %}
{% endfor %}

{% for signal, signal_info in user_def["signals"].items() %}
{% if signal_info.args.len() > 1 %}
class {{signal|upper_camel_case}}SigOut {
 public:
    {{signal|upper_camel_case}}SigOut() = default;
    ~{{signal|upper_camel_case}}SigOut() = default;

    {% for arg, arg_info in signal_info.args.items() %}
    {{arg_info.complete_cpp_type()}} get{{arg|upper_camel_case}}() { return {{arg|lower_snake_case}}_; }
    void set{{arg|upper_camel_case}}(const {{arg_info.complete_cpp_type()}} &{{arg|lower_snake_case}}) {
       {{arg|lower_snake_case}}_ = {{arg|lower_snake_case}};
    }
    {% endfor %}

 private:
    {% for arg, arg_info in signal_info.args.items() %}
    {{arg_info.complete_cpp_type()}} {{arg|lower_snake_case}}_;
    {% endfor %}
};
{% endif %}
{% endfor %}

}  // namespace adk
#endif  //  AUTOGEN_ATTRIBUTE_CLASSES_H_
