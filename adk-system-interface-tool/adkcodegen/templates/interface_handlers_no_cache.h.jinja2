
/*
 * Copyright (c) 2019-2021, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of The Linux Foundation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef AUTOGEN_INTERNAL_API_INTERFACE_HANDLERS_H_
#define AUTOGEN_INTERNAL_API_INTERFACE_HANDLERS_H_

#include <gio/gio.h>
#include <glib.h>
#include <functional>
#include <memory>
#include <mutex>
#include <string>
#include <tuple>
#include <unordered_map>
#include <vector>

// #include "adk_ipc.h"
#include "attribute_classes.h"
#include "internal_api/dbus.h"

namespace adk {
class PropertiesHandler {
 public:
    explicit PropertiesHandler(Dbus *dbus);
    ~PropertiesHandler() = default;

    {% for prop, prop_info in user_def["properties"].items() %}
    {% if prop_info.bustype == "session" %}
    std::tuple<ErrorCode, {{prop_info.type_desc.complete_cpp_type()}}> read{{prop|upper_camel_case}}(
        {% if has_path_ids(prop_info) %}
        const std::string &obj_id
        {% endif %}
    );

    {% if prop_info.is_writable == 1 %}
    ErrorCode update{{prop|upper_camel_case}}(
        {{prop_info.type_desc.complete_cpp_type()}} {{prop|lower_snake_case}}{% if has_path_ids(prop_info) %},
        const std::string & obj_id{% endif %});

    {% endif %}
    {% endif %}
    {% endfor %}


    // obj_path and set of strings that suggest the changed properties.
    void setPropertiesChangedListener(
        const std::function<void(PropertiesAttr, const std::string &)> &listener) {
        properties_changed_listeners_.emplace_back(listener);
    }

    void setObjIdMapChangedListener(
        const std::function<void(const std::unordered_map<std::string, std::unordered_set<std::string>>)> &listener) {
        obj_id_map_changed_listeners_.emplace_back(listener);
    }

 private:
    void propertiesChangedCb(const std::string &obj_path,
        const std::string &iface_name, GVariantIter *iter);

    {% for iface in get_ifaces(user_def["properties"]) %}
    PropertiesAttr process{{get_const_string_def(iface=iface)[1:]|upper_camel_case}}Response(
        const std::string& obj_path, GVariantIter *iter);
    {% endfor %}

 private:
    Dbus *dbus_;
    std::mutex mutex_;

    std::vector<std::function<void(PropertiesAttr, const std::string &)>> properties_changed_listeners_;
    std::vector<std::function<void(const std::unordered_map<std::string, std::unordered_set<std::string>>)>>
        obj_id_map_changed_listeners_;

    std::unordered_map<std::string, std::unordered_set<std::string>> obj_id_map_;
};

class MethodsHandler {
 public:
    explicit MethodsHandler(Dbus *dbus);
    ~MethodsHandler() = default;

    void setObjIdMapChangedListener(
        const std::function<void(const std::unordered_map<std::string, std::unordered_set<std::string>>)> &listener) {
        obj_id_map_changed_listeners_.emplace_back(listener);
    }
    {% for method, method_info in user_def["methods"].items() %}
    {% if method_info.out_args.len() == 1 %}
    std::tuple<ErrorCode, {{method_info.out_args.at(0).complete_cpp_type()}}>
    {% elif method_info.out_args.len() > 1 %}
    std::tuple<ErrorCode, {{method|upper_camel_case}}Out>
    {% else %}
    ErrorCode
    {% endif %}
    {{method|lower_camel_case}}(
        {% if method_info.in_args.len() == 1 %}
        {{method_info.in_args.at(0).complete_cpp_type()}} in_arg
        {% elif method_info.in_args.len() > 1 %}
        {{method|upper_camel_case}}In in_arg
        {% endif %}
        {% if has_path_ids(method_info)%}{% if method_info.in_args.len() > 0 %},{% endif %}
        const std::string &obj_id
        {% endif %});
    {% endfor %}
 private:
    void interfacesAddedCb(const std::string &obj_path,
        const std::string &iface_name, GVariantIter *iter);

 private:
    Dbus *dbus_;
    std::mutex mutex_;

    std::vector<std::function<void(const std::unordered_map<std::string, std::unordered_set<std::string>>)>>
        obj_id_map_changed_listeners_;

    std::unordered_map<std::string, std::unordered_set<std::string>> obj_id_map_;
};

class SignalsHandler {
 public:
    explicit SignalsHandler(Dbus *dbus);
    ~SignalsHandler() = default;

    {% for sig, sig_info in user_def["signals"].items() %}
    void set{{sig|upper_camel_case}}Listener(
        std::function<void(
            {% if sig_info.args.len() == 1 %}
            const {{sig_info.args.at(0).complete_cpp_type()}} &,
            {% elif sig_info.args.len() > 1 %}
            {{sig|upper_camel_case}}SigOut,
            {% endif %}
            const std::string &
        )> listener) {
        {{sig|lower_snake_case}}_listeners_.emplace_back(listener);
    }
    {% endfor %}

 private:

    {% for sig, sig_info in user_def["signals"].items() %}
    void call{{sig|upper_camel_case}}Listeners(const std::string& obj_path, GVariant *response);
    {% endfor %}

 private:
    Dbus *dbus_;
    std::mutex mutex_;

    {% for sig, sig_info in user_def["signals"].items() %}
    std::vector<std::function<void(
        {% if sig_info.args.len() == 1 %}
        const {{sig_info.args.at(0).complete_cpp_type()}} &,
        {% elif sig_info.args.len() > 1 %}
        {{sig|upper_camel_case}}SigOut,
        {% endif %}
        const std::string &)>> {{sig|lower_snake_case}}_listeners_;
    {% endfor %}
};
}  // namespace adk

#endif  // AUTOGEN_INTERNAL_API_INTERFACE_HANDLERS_H_
