"""
* Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are
* met:
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of The Linux Foundation nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
* ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
* BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
* BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""


import json
import pprint
import re
import logging
from collections import OrderedDict
from types import SimpleNamespace

FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(format=FORMAT)

logger = logging.getLogger("CreateRepository")
logger.setLevel(logging.INFO)

class DBusTypes:

    TYPE_MAP = {
        "INT16": "int16_t",
        "UINT16": "uint16_t",
        "INT32": "int32_t",
        "UINT32": "uint32_t",
        "INT64": "int64_t",
        "UINT64": "uint64_t",
        "DOUBLE": "double",
        "BYTE": "uint8_t",
        "STRING": "std::string",
        "SIGNATURE": "std::string",
        "OBJECT_PATH": "std::string",
        "BOOLEAN": "bool",
        "ARRAY": "vector",
        "STRUCT": "Struct",
        "DICT_ENTRY": "unordered_map",
        "VARIANT": "Struct"
    }

    FMT_MAP = {
        "n": "int16_t",
        "q": "uint16_t",
        "i": "int32_t",
        "u": "uint32_t",
        "x": "int64_t",
        "t": "uint64_t",
        "d": "double",
        "y": "uint8_t",
        "s": "std::string",
        "g": "std::string",
        "o": "std::string",
        "b": "bool",
        "a": "vector",
        "()": "Struct",
        "{}": "unordered_map",
        "v": "Struct"
    }

    CPP_NATIVE_TYPES = list(FMT_MAP.values())

    ALLOWED_FMT_CHAR = "".join(list(FMT_MAP.keys()))

    @staticmethod
    def is_supported(fmt):
        for c in fmt:
            if c not in DBusTypes.ALLOWED_FMT_CHAR:
                return False
        return True

AdkTypes = OrderedDict()

class TypeDescriptor:
    """
    Helper info for all types
    Dump should not be required as each type uses different properties of this
    descriptor
    """

    def __init__(self, cpp_type, dbus_fmt=None,
                 is_array=False, is_dict=False, key_type=None, is_struct=None):
        # cpp_type should be some basic type or adk type without any vector or unordered_map
        # if complete cpp_type is required, use complete_cpp_type function
        # cpp_type resolution usually happens in ElemType parse_dbus_fmt function
        # or in DbusXmlParse parse function for AdkTypes
        self.cpp_type = cpp_type
        if dbus_fmt:
            self.dbus_fmt = dbus_fmt
        else:
            self.dbus_fmt = ""
        self.is_array = is_array
        self.is_dict = is_dict
        self.key_type = key_type
        self.is_struct = is_struct

    def complete_cpp_type(self):
        logger.debug("TypeDescriptor: complete_cpp_type")
        if self.is_array:
            if self.is_dict:
                return "std::unordered_map<%s, %s>" % (self.key_type, self.cpp_type)
            return "std::vector<%s>" % self.cpp_type
        else:
            if self.is_dict:
                return "std::pair<%s, %s>" % (self.key_type, self.cpp_type)
        return self.cpp_type

    def __repr__(self):
        repr = {}
        for k in ["is_array", "is_dict", "key_type", "cpp_type", "dbus_fmt", "is_struct"]:
            if k.startswith("__") or hasattr(k, "__call__"):
                continue
            repr[k] = getattr(self, k)
        return pprint.pformat(repr, indent=4)

    def dump(self):
        dump_dict = {"cpp_type": self.cpp_type, "dbus_fmt": self.dbus_fmt,
               "is_array": self.is_array, "is_dict": self.is_dict,
               "key_type": self.key_type, "is_struct": self.is_struct,
               "complete_cpp_type": self.complete_cpp_type()
               }
        return OrderedDict(sorted(dump_dict.items(), key=lambda x: x[0]))


class BaseType:
    """
    Base type to identify all types with
    """

    def __init__(self):
        self.type_descriptor = None
        self.description = ""
        self.resolved = False
        self.members = {}
        self.dependents = []

    @staticmethod
    def compute_dbus_fmt(type_inst):
        """
        Compute dbus format string based on the member types.
        This takes all the member types as structs
        and any overrides should be taken care after type creation
        :param type_inst: Type instance
        :return: DBus format of the Type arg
        """
        logger.debug("BaseType: compute_dbus_fmt")
        assert isinstance(type_inst, BaseType) and type_inst.resolved, "Invalid type for compute_dbus_fmt"
        if type_inst.type_descriptor.dbus_fmt:
            return type_inst.type_descriptor.dbus_fmt
        fmt = ""
        for key in type_inst.order:
            mem_info = type_inst.members[key]
            assert mem_info.resolved, "Invalid type for compute_dbus_fmt"
            logger.debug("compute_dbus_fmt member_name %s" % key)
            logger.debug("compute_dbus_fmt of mem_info cpp_type %s" % mem_info.type_descriptor.cpp_type)
            if ElemType.is_adk_type(mem_info):
                fmt += + "#"+ BaseType.compute_dbus_fmt(AdkTypes[mem_info.type_descriptor.cpp_type]) + "#"
            else:
                fmt += mem_info.type_descriptor.dbus_fmt
        return fmt


    @staticmethod
    def resolve_deps(type_inst):
        """
        Resolve the dependencies between AdkTypes
        """

        logger.debug("BaseType: resolve_deps")
        assert isinstance(type_inst, AdkType), "Invalid Type for dependency resolution"

        if type_inst.resolved:
            for mem_key, mem_info in type_inst.members.items():
                if not mem_info.resolved:
                    if isinstance(AdkTypes[mem_info.type_descriptor.cpp_type], UnResolvedType):
                        logger.debug("Can't resolve %s" % type_inst.type_descriptor.cpp_type)

            for dep in type_inst.dependents:
                assert isinstance(dep, AdkType), "Invalid dependency Type"
                if_any = True
                for dep_mem_key, dep_mem_info in dep.members.items():
                    if dep_mem_info.type_descriptor.cpp_type == type_inst.type_descriptor.cpp_type:
                        dep_mem_info.resolved = True
                    if_any &= dep_mem_info.resolved

                if if_any:
                    dep.resolved = True
                    BaseType.resolve_deps(dep)
                    if dep.type_descriptor.dbus_fmt == "":
                        dep.type_descriptor.dbus_fmt = BaseType.compute_dbus_fmt(dep)
                    logger.debug("Resolved %s" % dep.type_descriptor.cpp_type)

            type_inst.type_descriptor.dbus_fmt = BaseType.compute_dbus_fmt(type_inst)

            logger.debug("Resolved %s" % type_inst.type_descriptor.cpp_type)

    def add_dependent(self, dep):
        logger.debug("BaseType: add_dependent")
        assert isinstance(dep, AdkType), "Trying to add Invalid type as dependent"
        if dep.type_descriptor.cpp_type not in [_.type_descriptor.cpp_type for _ in self.dependents]:
            self.dependents.append(dep)

    def __repr__(self):
        repr = {}
        for k in ["type_descriptor", "dependents", "members", "resolved"]:
            if k.startswith("__") or hasattr(k, "__call__"):
                continue
            if k == "dependents":
                repr[k] = [_.cpp_type for _ in getattr(self, k)]
            elif k == "members":
                repr[k] = {m: t.type_descriptor for m, t in getattr(self, k).items()}
            else:
                try:
                    repr[k] = getattr(self, k)
                except:
                    pass
        return pprint.pformat(repr, indent=4)


class UnResolvedType(BaseType):
    """
    AdkType for which complete definition has not been found.
    """
    def __init__(self, cpp_type, dependents=None, description=None):
        logger.debug("Could not resolve_deps %s for now" % cpp_type)
        self.cpp_type = cpp_type
        if not dependents:
            self.dependents = []
        else:
            self.dependents = dependents
        self.resolved = False
        self.type_descriptor = None
        self.members = {}
        self.description = description
        self.order = []

    def dump(self):
        desc_dump = None
        if self.type_descriptor:
            desc_dump = self.type_descriptor.dump()
        logger.debug("AdkType members dump in order %s" % self.members)
        dump_dict = {"desc": desc_dump, "dependent_types": [_.type_descriptor.cpp_type for _ in self.dependents],
                "members": {n : _.dump() for n, _ in self.members.items()},
                "order": [n for n in self.order], "resolved": self.resolved,
                "description": self.description
                }
        return OrderedDict(sorted(dump_dict.items(), key=lambda x: x[0]))


class AdkType(BaseType):
    """
    Type for all AdkType identifications
    """

    def __init__(self, type_descriptor, dependents=None, members=None, description=None):
        self.type_descriptor = type_descriptor
        if not dependents:
            self.dependents = []
        else:
            # Make sure each dependent is AdkType
            self.dependents = dependents

        # resolved becomes true only at the end of creation and
        # when all the dependents are also resolved
        self.resolved = False
        self.members = {}
        if members:
            self.members = members

        self.description = description
        self.order = []

    def add_member(self, name, member):
        """
        Add member to the AdkType
        :param name: name of the member
        :param member: Member type to add
        """
        logger.debug("AdkType: add_member %s" % name)
        if name in self.members:
            logger.debug("%s already added" % name)
            return

        if isinstance(member, ElemType):
            self.members[name] = member
            self.resolved &= member.resolved
            logger.debug("Adding member %s" %name)

    @staticmethod
    def Create(cpp_type=None, members=None, description=None, prefix=""):
        """
        Create AdkType based on cpp_type and members
        :param cpp_type: name of the AdkType
        :param members: attr members of AdkType
        :param description: type description of the AdkType
        :param prefix: namespace prefix of the AdkType
        :return: AdkType object
        """
        logger.debug("AdkType: Create")
        new_type = None
        if members:
            if len(members) > 0:
                type_desc = TypeDescriptor(cpp_type=cpp_type)
                if type_desc.cpp_type not in DBusTypes.CPP_NATIVE_TYPES:
                    type_desc.cpp_type = prefix + type_desc.cpp_type
                new_type = AdkType(type_desc, description=description)
                if new_type.type_descriptor.cpp_type in AdkTypes and isinstance(AdkTypes[new_type.type_descriptor.cpp_type], UnResolvedType):
                    new_type.dependents = AdkTypes[new_type.type_descriptor.cpp_type].dependents
                elif new_type.type_descriptor.cpp_type not in AdkTypes:
                    AdkTypes[new_type.type_descriptor.cpp_type] = new_type

                mem = OrderedDict()
                mem = members
                new_type.order = mem.keys()
                resolved = True
                fmt = ""
                for name, mem_info in mem.items():
                    member_type = ElemType.Create(dbus_fmt=mem_info["cpp_type"], description=mem_info["description"])
                    if member_type.type_descriptor.cpp_type in AdkTypes:
                        logger.info("Member type entered %s in AdkTypes" % name)
                        if isinstance(AdkTypes[member_type.type_descriptor.cpp_type], UnResolvedType) or not AdkTypes[member_type.type_descriptor.cpp_type].resolved:
                            AdkTypes[member_type.type_descriptor.cpp_type].add_dependent(new_type)
                            resolved = False
                        else:
                            fmt += "#" + member_type.type_descriptor.dbus_fmt + "#"
                            resolved = True
                    else:
                        if member_type.type_descriptor.cpp_type not in DBusTypes.CPP_NATIVE_TYPES:
                            logger.info("Member type %s entered creation of unresolved type" % name)
                            AdkTypes[member_type.type_descriptor.cpp_type] = UnResolvedType(member_type.type_descriptor.cpp_type)
                            AdkTypes[member_type.type_descriptor.cpp_type].add_dependent(new_type)
                            resolved = False
                        else:
                            fmt += member_type.type_descriptor.dbus_fmt
                            resolved = True
                    new_type.add_member(name, member_type)
                    mem_info[name] = member_type

                new_type.resolved = resolved
                if new_type.resolved:
                    BaseType.resolve_deps(new_type)
                    BaseType.compute_dbus_fmt(new_type)

        logger.debug("AdkType: Create members at the end of create %s", new_type.members.keys())
        return new_type

    def dump(self):
        desc_dump = None
        members_dump = OrderedDict()
        if self.type_descriptor:
            desc_dump = self.type_descriptor.dump()
        if self.members is not None:
            for mem in self.order:
                members_dump[mem] = self.members[mem].dump()
        dump_dict = {"desc": desc_dump, "dependent_types": [_.type_descriptor.cpp_type for _ in self.dependents],
                "members": members_dump,
                "order": [n for n in self.order], "resolved": self.resolved,
                "description": self.description
                }
        return OrderedDict(sorted(dump_dict.items(), key=lambda x: x[0]))


class ElemType(BaseType):
    """
    Base type for all properties, method and signal args and AdkType members
    """
    def __init__(self, descriptor, description=""):
        self.type_descriptor = descriptor
        self.description = description

    @staticmethod
    def is_adk_type(type_inst):
        if type_inst.type_descriptor.cpp_type in AdkTypes:
            return True
        return False

    @staticmethod
    def parse_dbus_fmt(dbus_fmt):
        """
        Parse dbus type format
        :param dbus_fmt: dbus format of the element type
        :return: TypeDescriptor object
        """
        logger.debug("ElemType: parse_dbus_fmt")
        is_array = False
        is_dict = False
        key_type = None
        cpp_type = None
        is_struct = None

        if dbus_fmt.startswith("{") or dbus_fmt.startswith("a{"):
            is_dict = True
            idx = dbus_fmt.index("{")
            key_type = DBusTypes.FMT_MAP[dbus_fmt[idx + 1]]
            if dbus_fmt[idx + 2] == "(":
                is_struct = True
                if dbus_fmt[idx + 3] == "#":
                    cpp_type = dbus_fmt[idx + 4:-3]
                    assert dbus_fmt[-3] == "#" and dbus_fmt[-2] == ")" and dbus_fmt[-1] == "}" "Invalid format"
                else:
                    cpp_type = dbus_fmt[idx + 3:-2]
                    assert dbus_fmt[-2] == ")" and dbus_fmt[-1] == "}", "Invalid format"
            else:
                cpp_type = DBusTypes.FMT_MAP[dbus_fmt[idx + 2]]
                assert dbus_fmt[idx + 3] == "}", "Invalid format"
            if dbus_fmt.startswith("a{"):
                is_array = True

        elif dbus_fmt.startswith("("):
            is_struct = True
            if dbus_fmt[1] == "#":
                cpp_type = dbus_fmt[2:-2]
                assert dbus_fmt[-2] == "#" and dbus_fmt[-1] == ")" , "Invalid format"
            else:
                cpp_type = dbus_fmt[1:-1]
                assert dbus_fmt[-1] == ")", "Invalid format"

        elif dbus_fmt.startswith("/"):
            cpp_type = dbus_fmt

        elif dbus_fmt.startswith("a("):
            is_struct = True
            if dbus_fmt[2] == "#":
                cpp_type = dbus_fmt[3:-2]
                assert dbus_fmt[-2] == "#" and dbus_fmt[-1] == ")" , "Invalid format"
            else:
                cpp_type = dbus_fmt[2:-1]
                assert dbus_fmt[-1] == ")", "Invalid format"
            is_array = True

        elif dbus_fmt.startswith("a"):
            cpp_type = DBusTypes.FMT_MAP[dbus_fmt[1:]]
            is_array = True

        elif dbus_fmt.startswith("#"):
            cpp_type = dbus_fmt[1:-1]
            assert dbus_fmt[-1] == "#", "Invalid format"

        elif dbus_fmt not in DBusTypes.FMT_MAP:
            cpp_type = dbus_fmt
        else:
            cpp_type = DBusTypes.FMT_MAP[dbus_fmt]

        return TypeDescriptor(cpp_type=cpp_type, dbus_fmt=dbus_fmt,
                              is_array=is_array, is_dict=is_dict,
                              key_type=key_type, is_struct=is_struct)

    @staticmethod
    def Create(dbus_fmt=None, description="", prefix=""):
        """
        Create ElemType identifier based on Dbus format
        :param dbus_fmt: the complete dbus_fmt received from parsed attr
        :param description: ElemType description
        :return: ElemType object
        """
        logger.debug("ElemType: Create")
        new_type = None
        if dbus_fmt:
            type_desc = ElemType.parse_dbus_fmt(dbus_fmt)
            if type_desc.cpp_type not in DBusTypes.CPP_NATIVE_TYPES:
                type_desc.cpp_type = prefix + type_desc.cpp_type
                logger.debug("New type : %s" % type_desc.cpp_type)
            new_type = ElemType(type_desc, description)
            if new_type.type_descriptor.cpp_type in DBusTypes.CPP_NATIVE_TYPES:
                new_type.resolved = True
            elif ElemType.is_adk_type(new_type):
                if not AdkTypes[new_type.type_descriptor.cpp_type].resolved or isinstance(AdkTypes[new_type.type_descriptor.cpp_type], UnResolvedType):
                    new_type.resolved = False
                else:
                    new_type.resolved = True
            else:
                AdkTypes[new_type.type_descriptor.cpp_type] = AdkType.Create(new_type.type_descriptor.cpp_type)
                new_type.resolved = False

        return new_type

    def dump(self):
        desc_dump = None
        if self.type_descriptor:
            desc_dump = self.type_descriptor.dump()
        dump_dict = {"desc": desc_dump, "resolved": self.resolved,
                "description": self.description
                }
        return OrderedDict(sorted(dump_dict.items(), key=lambda x: x[0]))

class DBusService:
    """
    Dbus info Hierarchy starts here
    """

    def __init__(self, name, root_object="/"):
        self.name = name
        self.objects = {}

        # Find a use later
        # self.add_object(root_object)

    def add_object(self, obj):
        if isinstance(obj, DBusService.Object):
            if obj.path not in self.objects:
                self.objects[obj.path] = obj
                self.objects[obj.path].service = self
            else:
                logger.debug("Object %s already added" % obj.path)

    def __repr__(self):
        repr = {}
        for k in ["name", "objects"]:
            repr[k] = getattr(self, k)

        return pprint.pformat(repr, indent=4)

    def dump(self):
        out = OrderedDict()
        out["key"] = KeyGen(self.name)
        for path, obj in self.objects.items():
            out[path] = obj.dump()
        return out

    class Object:
        """
        Dbus object info
        """
        def __init__(self, path, *interfaces, **kwargs):
            self.path = path
            self.path_ids = []
            self.service = kwargs.get("service", None)
            self.interfaces = {}
            for iface in interfaces:
                self.add_interface(iface)
            self.set_path_var()


        def set_path_var(self):
            for path_var in re.finditer(r'\'.*?\'', self.path):
                self.path_ids.append(path_var.group().strip("'").strip("'"))


        def add_interface(self, iface):
            if not isinstance(iface, DBusService.Interface):
                logger.error("Invalid arg %s" % str(iface))
            else:
                if iface.name not in self.interfaces:
                    self.interfaces[iface.name] = iface
                    self.interfaces[iface.name].obj = self
                else:
                    logger.debug("Interface %s already added" % iface.name)

        def get_interface(self, iface_name):
            return self.interfaces.get(iface_name)

        def __repr__(self):
            repr = {}
            for k in ["path", "interfaces", "path_ids"]:
                repr[k] = getattr(self, k)
            return pprint.pformat(repr, indent=4)

        def dump(self):
            out = OrderedDict()
            if len(path_ids) > 0:
                out[path_ids] = self.path_ids
            for name, iface in self.interfaces.items():
                out[name] = iface.dump()
            return out

    class Interface:
        """
        Dbus interface info
        """
        def __init__(self, name, obj=None, properties=None, methods=None, signals=None):
            self.name = name
            self.obj = obj
            self.properties = {}
            self.methods = {}
            self.signals = {}

            if properties:
                for prop in properties:
                    if isinstance(prop, DBusService.Property):
                        self.add_property(prop)

            if methods:
                for method in methods:
                    if isinstance(method, DBusService.Method):
                        self.add_method(method)

            if signals:
                for signal in signals:
                    if isinstance(signal, DBusService.Signal):
                        self.add_signal(signal)

        def add_property(self, prop):
            if isinstance(prop, DBusService.Property):
                if prop.name not in self.properties:
                    self.properties[prop.name] = prop
                    self.properties[prop.name].iface = self
                    logger.info("Added new property %s" % prop.name)
                    return True
                else:
                    logger.error("Property %s already added" % prop.name)
            return False

        def add_signal(self, signal):
            if isinstance(signal, DBusService.Signal):
                if signal.name not in self.signals:
                    self.signals[signal.name] = signal
                    self.signals[signal.name].iface = self
                    logger.info("Added new signal %s" % signal.name)
                    return True
                else:
                    logger.error("Signal %s already added" % signal.name)
            return False

        def add_method(self, method):
            if isinstance(method, DBusService.Method):
                if method.name not in self.methods:
                    self.methods[method.name] = method
                    self.methods[method.name].iface = self
                    logger.info("Added new method %s" % method.name)
                    return True
                else:
                    logger.error("Method %s already added" % method.name)
            return False

        def __repr__(self):
            repr = {}
            for k in ["methods"]:
                repr[k] = getattr(self, k).keys()
            return pprint.pformat(repr, indent=4)

        def dump(self):
            out = OrderedDict()
            out["Properties"] = OrderedDict()
            out["Methods"] = OrderedDict()
            out["Signals"] = OrderedDict()
            for name, prop in self.properties.items():
                out["Properties"][name] = prop.dump()

            for name, method in self.methods.items():
                out["Methods"][name] = method.dump()

            for name, signal in self.signals.items():
                out["Signals"][name] = signal.dump()

            return out

    class Property:
        """
        Dbus property class
        """

        def __init__(self, name, iface=None, type_desc=None, access="r", bustype="session", description=None):
            self.name = name
            self.iface = iface
            self.type_desc = type_desc
            self.access = access
            self.bustype = bustype
            self.description = description

        @property
        def is_writable(self):
            if "w" in self.access.lower():
                return 1
            return 0

        def __repr__(self):
            repr = {}
            for k in ["name", "iface", "type_desc", "access", "description"]:
                v = getattr(self, k)
                # Below is to avoid infinite recursion when logger.debuging
                if isinstance(v, DBusService.Interface):
                    v = v.name
                repr[k] = v
            return pprint.pformat(repr, indent=4)

        def dump(self):
            type_desc_dump = None
            if isinstance(self.type_desc, TypeDescriptor):
                type_desc_dump = self.type_desc.dump()
            dump_dict = {
                "name": self.name,
                "type_info": type_desc_dump,
                "access": self.access,
                "bustype": self.bustype,
                "description": self.description
            }
            return OrderedDict(sorted(dump_dict.items(), key=lambda x: x[0]))

    class Method:
        """
        Dbus Method info
        """

        def __init__(self, name, iface=None, in_args=None, out_args=None, in_args_dbus_fmt=None, out_args_dbus_fmt=None,
                     bustype="session", description=None):
            self.name = name
            self.iface = iface
            self.in_args = OrderedDict()
            self.out_args = OrderedDict()
            self.bustype = bustype
            self.description = description
            self.in_args_dbus_fmt = ""
            self.out_args_dbus_fmt = ""

            if in_args_dbus_fmt:
                self.in_args_dbus_fmt = in_args_dbus_fmt
            if out_args_dbus_fmt:
                self.out_args_dbus_fmt = out_args_dbus_fmt

            if in_args:
                for arg in in_args:
                    assert isinstance(list(arg.values())[0], TypeDescriptor), "Invalid Argument type"
                    self.in_args.update(arg)

            if out_args:
                for arg in out_args:
                    assert isinstance(list(arg.values())[0], TypeDescriptor), "Invalid Argument type"
                    self.out_args.update(arg)

        def __repr__(self):
            repr = {}
            for k in ["name", "iface", "in_args", "out_args", "description"]:
                v = getattr(self, k)
                # Below is to avoid infinite recursion when logger.debuging
                if isinstance(v, DBusService.Interface):
                    v = v.name
                repr[k] = v
            return pprint.pformat(repr, indent=4)

        def dump(self):
            dump_dict = {
                "name": self.name,
                "in_args": OrderedDict((k, {"type_info": v.dump()}) for k, v in self.in_args.items()),
                "in_order": list(self.in_args.keys()),
                "out_args": OrderedDict((k, {"type_info": v.dump()}) for k, v in self.out_args.items()),
                "out_order": list(self.out_args.keys()),
                "bustype" : self.bustype,
                "description": self.description
            }
            return OrderedDict(sorted(dump_dict.items(), key=lambda x: x[0]))

    class Signal:
        """
        Dbus Signal info
        """
        def __init__(self, name, iface=None, associated_property=None, args=None,
                     bustype="session", description=None):
            self.name = name
            self.iface = iface
            self.associated_property = associated_property
            self.args = OrderedDict()
            self.bustype = bustype
            self.description = description

            if args:
                for arg in args:
                    assert isinstance(list(arg.values())[0], TypeDescriptor), "Invalid Argument type"
                    self.args.update(arg)

        def __repr__(self):
            repr = {}
            for k in ["name", "iface", "associated_property", "args", "description"]:
                v = getattr(self, k)
                # Below is to avoid infinite recursion when logger.debuging
                if isinstance(v, DBusService.Interface):
                    v = v.name
                repr[k] = v
            return pprint.pformat(repr, indent=4)

        def dump(self):
            dump_dict = {
                "name": self.name,
                "args": OrderedDict((k, {"type_info": v.dump()}) for k, v in self.args.items()),
                "order": list(self.args.keys()),
                "bustype": self.bustype,
                "description": self.description
            }
            return OrderedDict(sorted(dump_dict.items(), key=lambda x: x[0]))


class DbusXmlParser:
    """
    Parses xmls that can be stored in a structured repository
    """
    DEFAULT_XMLNS = "http://adkcodegen/dbus_interface"
    ADK_TYPE_XMLNS = "http://adkcodegen/dbus_interface/adk_types"

    def __init__(self, *files):
        self.to_parse = list(*files)
        self.parsed_files = []
        # parsed_data contains all the xml hierarchy in understandable dictionary format
        self.parsed_data = {}
        self.ctx = SimpleNamespace()
        self.clear_context()

    def clear_context(self):
        self.ctx.current_file = None
        self.ctx.root = None
        self.ctx.namespace = ""
        self.ctx.bustype = "session"

    def parse_property(self, prop_elem):
        prop_name, prop_type = self.parse_attr(prop_elem)
        new_type = ElemType.Create(prop_type, prop_elem.get("description"))

        if prop_elem.get("override_fmt"):
            new_type.type_descriptor.dbus_fmt = prop_elem.get("override_fmt")
            if not ElemType.is_adk_type(new_type):
                if new_type.type_descriptor.dbus_fmt.startswith("a"):
                    new_type.type_descriptor.is_array = True
                else:
                    new_type.type_descriptor.is_array = False
                if "{" in new_type.type_descriptor.dbus_fmt and "}" in new_type.type_descriptor.dbus_fmt:
                    new_type.type_descriptor.is_dict = True

        if prop_name in AdkTypes and AdkTypes[prop_name].cpp_type != new_type.cpp_type:
            logger.debug("Conflicting types found for same property %s .. Ignoring this instance" % prop_name)
            return

        if ElemType.is_adk_type(new_type):
            AdkType.resolve_deps(AdkTypes[new_type.type_descriptor.cpp_type])

        return DBusService.Property(prop_name,
                                    type_desc=new_type.type_descriptor,
                                    access=prop_elem.get("access"),
                                    description=prop_elem.get("description"))


    def parse_method(self, method_elem):
        args = {"in": [], "out": []}
        method_name = method_elem.get("name")

        for arg in method_elem.findall("{%s}arg" % DbusXmlParser.DEFAULT_XMLNS):
            arg_name, arg_type = self.parse_attr(arg)
            arg_type = ElemType.Create(dbus_fmt=arg_type)

            if ElemType.is_adk_type(arg_type):
                AdkType.resolve_deps(AdkTypes[arg_type.type_descriptor.cpp_type])

            if arg.get("override_fmt"):
                arg_type.type_descriptor.dbus_fmt = arg.get("override_fmt")
            if not ElemType.is_adk_type(arg_type):
                if arg_type.type_descriptor.dbus_fmt.startswith("a"):
                    arg_type.type_descriptor.is_array = True
                else:
                    arg_type.type_descriptor.is_array = False
                if "{" in arg_type.type_descriptor.dbus_fmt and "}" in arg_type.type_descriptor.dbus_fmt:
                    arg_type.type_descriptor.is_dict = True

            args[arg.get("direction")].append({arg_name: arg_type.type_descriptor})

        return DBusService.Method(name=method_name, in_args=args["in"], out_args=args["out"],
                                  description=method_elem.get("description"))

    def parse_signal(self, signal_elem):
        args = []
        sig_name = signal_elem.get("name")

        for arg in signal_elem.findall("{%s}arg" % DbusXmlParser.DEFAULT_XMLNS):
            arg_name, arg_type = self.parse_attr(arg)

            arg_type = ElemType.Create(dbus_fmt=arg_type)

            if ElemType.is_adk_type(arg_type):
                AdkType.resolve_deps(AdkTypes[arg_type.type_descriptor.cpp_type])

            if arg.get("override_fmt"):
                arg_type.type_descriptor.dbus_fmt = arg.get("override_fmt")
                if not ElemType.is_adk_type(arg_type):
                    if arg_type.type_descriptor.dbus_fmt.startswith("a"):
                        arg_type.type_descriptor.is_array = True
                    else:
                        arg_type.type_descriptor.is_array = False
                    if "{" in arg_type.type_descriptor.dbus_fmt and "}" in arg_type.type_descriptor.dbus_fmt:
                        arg_type.type_descriptor.is_dict = True

            args.append({arg_name: arg_type.type_descriptor})

        return DBusService.Signal(name=sig_name, args=args,
                                  description=signal_elem.get("description"))


    def parse_attr(self, attr_elem, **namespace):
        """
        parses any atomic element like property, arg or attr
        :param attr_elem: xml element for the atomic item
        :param namespace: xml namespace associated with the element
        :return: attr name and full type
        """
        _name = attr_elem.get("name")
        _type = attr_elem.get("type")
        if not namespace:
            namespace = {"adk": DbusXmlParser.ADK_TYPE_XMLNS}

        _fulltype = ""
        start_pos = 0
        for xpath in re.finditer(r'#[\.]?/.*?#', _type):
            _fulltype += _type[start_pos:xpath.start()]
            logger.debug("Search for %s " % xpath.group().strip("(").strip(")"))
            try:
                if xpath.group().startswith("#."):
                    # Use relative path
                    _fulltype += "#" + self.ctx.namespace + attr_elem.find(xpath.group().strip("#").strip("#"), namespace).get("name") + "#"
                else:
                    _fulltype += "#" + self.ctx.namespace + self.ctx.root.find("./" + xpath.group().strip("#").strip("#"), namespace).get("name") + "#"
            except Exception as e:
                _fulltype = re.search(r'@name=[\'"](.*?)[\'"]', _type).group(1)
                logger.debug("Using %s as unresolved type for now" % _fulltype)
                return _name, _fulltype
            start_pos = xpath.end()
        if start_pos > 0:
            logger.debug("fulltype %s" %_fulltype)
            return _name, _fulltype + _type[start_pos:]
        else:
            return _name, _type

    def parse(self, file="", key_discovered_cb=None):
        """
        XML parsing
        :param file: File name to parse
        """
        import xml.etree.ElementTree as ET
        if file not in self.parsed_files and file not in self.to_parse:
            self.to_parse.append(file)

        for f in self.to_parse:
            if len(f.strip()) == 0:
                continue
            logger.info("PARSE %s" % f)
            self.ctx.current_file = f
            try:
                xml = ET.parse(f)
                root = xml.getroot()
                logger.debug(root.tag)
                if root.tag != "{%s}node" % DbusXmlParser.DEFAULT_XMLNS:
                    logger.debug("Incorrect root node")
                    continue
                self.ctx.root = root
                self.ctx.namespace = root.get("namespace")
                if self.ctx.namespace:
                    self.ctx.namespace = self.ctx.namespace[0].upper() + self.ctx.namespace[1:]
                else:
                    self.ctx.namespace = ""
                self.ctx.bustype = root.get("bustype")
                if self.ctx.bustype:
                    self.ctx.bustype = self.ctx.bustype.lower()
                else:
                    self.ctx.bustype = "session"

                for adk_type in root.findall("adk:types/adk:type", {"adk": DbusXmlParser.ADK_TYPE_XMLNS}):
                    adk_type_name = adk_type.get("name")
                    attrs = OrderedDict()
                    for attr in adk_type.findall("adk:attr", {"adk": DbusXmlParser.ADK_TYPE_XMLNS}):
                        attr_name, attr_type = self.parse_attr(attr)
                        attrs[attr_name] = {"cpp_type": attr_type, "description": attr.get("description", None)}

                    if len(attrs) > 0:
                        logger.debug("parsed AdkType attrs %s" %  attrs)
                        AdkTypes[self.ctx.namespace + adk_type_name] = AdkType.Create(cpp_type=adk_type_name,
                                                                                      members=attrs,
                                                                                      description=adk_type.get("description"),
                                                                                      prefix=self.ctx.namespace)
                    else:
                        logger.info("No members to create an AdkType")
                    AdkType.resolve_deps(AdkTypes[self.ctx.namespace + adk_type_name])

                if root.get("name") not in self.parsed_data:
                    self.parsed_data[root.get("name")] = DBusService(root.get("name"))

                for x_obj in root.findall("{%s}object" % DbusXmlParser.DEFAULT_XMLNS):
                    obj = DBusService.Object(x_obj.get("path"), service=self.parsed_data[root.get("name")])
                    self.parsed_data[root.get("name")].add_object(obj)

                    for interface in x_obj.findall("{%s}interface" % DbusXmlParser.DEFAULT_XMLNS):
                        iface = DBusService.Interface(interface.get("name"), obj=obj)

                        for ch in interface.getchildren():
                            if ch.tag == "{%s}property" % DbusXmlParser.DEFAULT_XMLNS:
                                new_prop = self.parse_property(ch)
                                new_prop.bustype = self.ctx.bustype
                                if iface.add_property(new_prop):
                                    if key_discovered_cb:
                                        key_discovered_cb(new_prop)
                            elif ch.tag == "{%s}signal" % DbusXmlParser.DEFAULT_XMLNS:
                                new_sig = self.parse_signal(ch)
                                new_sig.bustype = self.ctx.bustype
                                if iface.add_signal(new_sig):
                                    if key_discovered_cb:
                                        key_discovered_cb(new_sig)
                            elif ch.tag == "{%s}method" % DbusXmlParser.DEFAULT_XMLNS:
                                new_method = self.parse_method(ch)
                                new_method.bustype = self.ctx.bustype
                                if iface.add_method(new_method):
                                    if key_discovered_cb:
                                        key_discovered_cb(new_method)

                        obj.add_interface(iface)
                    self.parsed_data[root.get("name")].add_object(obj)

            except Exception as e:
                logger.error("FAILED to parse %s with error %s" % (f, str(e)))


def main():
    import os
    import argparse
    from .utils import generate_key as KeyGen

    data = OrderedDict()
    data["properties"] = OrderedDict()
    data["methods"] = OrderedDict()
    data["signals"] = OrderedDict()

    def dump_item_info(item):
        item_dump = item.dump()
        item_dump.update({"svc": item.iface.obj.service.name,
                "obj_path": item.iface.obj.path,
                "iface": item.iface.name})
        if len(item.iface.obj.path_ids) > 0:
            item_dump.update({"path_ids": item.iface.obj.path_ids})
        return OrderedDict(sorted(item_dump.items(), key=lambda x: x[0]))

    def key_discovered(item):
        if isinstance(item, DBusService.Property):
            key = KeyGen(item.iface.obj.service.name, iface=item.iface.name,
                         prop=item.name)
            data["properties"][key] = dump_item_info(item)

        elif isinstance(item, DBusService.Method):
            key = KeyGen(item.iface.obj.service.name, iface=item.iface.name,
                         method=item.name)
            data["methods"][key] = dump_item_info(item)

        elif isinstance(item, DBusService.Signal):
            key = KeyGen(item.iface.obj.service.name, iface=item.iface.name,
                         sig=item.name)
            data["signals"][key] = dump_item_info(item)

    parser = argparse.ArgumentParser(description='ADK interface Repository creator')

    parser.add_argument("--xml-dir",
                        dest="xml_path",
                        action="store",
                        default="xmls",
                        help="Location of xmls with dbus interface definitions")

    parser.add_argument("--debug",
                        dest="debug",
                        action="store_true",
                        default=False,
                        help="Enable debug logs")

    args = parser.parse_args()
    if args.debug:
        logger.setLevel(logging.DEBUG)

    xml_parser = DbusXmlParser([os.path.join(args.xml_path, x) for x in os.listdir(args.xml_path)])
    xml_parser.parse(key_discovered_cb=key_discovered)
    dump_dict = OrderedDict()
    type_dict = {k : v.dump() for k,v in AdkTypes.items()}
    dump_dict["Types"] = OrderedDict(sorted(type_dict.items(), key=lambda x: x[0]))
    logger.debug("*************")
    dump_dict["dbus_info"] = data
    repo_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), "..", "repo")
    f = open(os.path.join(repo_dir, "repo.json"), "w")
    f.write(json.dumps(dump_dict, indent=4))
    f.close()
    logger.info("**** Adk System Interface repo updated. Please generate the code with the new repository and update the sample app ****")


if __name__ == "__main__":
    main()

