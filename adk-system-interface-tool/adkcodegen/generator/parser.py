"""
* Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are
* met:
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of The Linux Foundation nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
* ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
* BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
* BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""


import sys
import re
import logging
import json
import pprint
from collections import OrderedDict

from .create_repo import DBusTypes, AdkType, ElemType, TypeDescriptor

FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(format=FORMAT)

logger = logging.getLogger("adk_code_generator")
logger.setLevel(logging.INFO)

AdkTypes = {}


class CustomOrderedDict(OrderedDict):
    """
    An OrderDict wrapper class to help compensate jinja dict limitations
    """
    def len(self):
        return len(self)

    def at(self, idx):
        return self[list(self.keys())[idx]]

    def keys(self):
        return list(super().keys())


class JsonParser:
    """
    Parse input json files that would then get used for rendering Jinja templates
    """

    def __init__(self, user_json=None, repo_json=None):
        """
        Initialize with input json files
        :param user_json: Json file provided by the user with required key selections
        :param repo_json: internal repository of all the adk settings
        """
        self.user_json_file = user_json
        self.repo_json_file = repo_json
        self.user_def = OrderedDict()
        self.user_def["properties"] = OrderedDict()
        self.user_def["methods"] = OrderedDict()
        self.user_def["signals"] = OrderedDict()

    def build_adk_types(self, json_dict):
        """
        Build AdkTypes
        :param json_dict: Json dictionary from repo.json
        """
        for type_name, type_info in json_dict.items():
            if type_name in AdkTypes:
                logger.debug("%s already found .. Ignoring this instance .. check" % tp)
                continue
            try:
                members = OrderedDict()
                for mem_name in type_info["order"]:
                    mem_info = type_info["members"][mem_name]
                    mem_type_desc = TypeDescriptor(mem_info["desc"]["cpp_type"],
                                                   mem_info["desc"]["dbus_fmt"],
                                                   mem_info["desc"]["is_array"],
                                                   mem_info["desc"]["is_dict"],
                                                   mem_info["desc"]["key_type"])
                    members[mem_name] = ElemType(descriptor=mem_type_desc,
                                                 description=mem_info["description"])
                adk_type_desc = TypeDescriptor(type_info["desc"]["cpp_type"],
                                               type_info["desc"]["dbus_fmt"],
                                               type_info["desc"]["is_array"],
                                               type_info["desc"]["is_dict"],
                                               type_info["desc"]["key_type"])
                AdkTypes[type_name] = AdkType(adk_type_desc, members=members, description=type_info["description"])

            except Exception as e:
                logger.error("Failed to get type %s" % e)

    def sort_type_dependencies(self):
        """
        Perform a kind of topological Sort AdkTypes, such that dependencies get to be declared first
        :return: Ordered Dictionary of sorted types
        """
        def sort_dep(tp, out):
            if tp.type_descriptor.cpp_type in out:
                return
            for mem, mem_info in tp.members.items():
                if ElemType.is_adk_type(mem_info):
                    if mem_info.type_descriptor.cpp_type in out:
                        continue
                    sort_dep(AdkTypes[mem_info.type_descriptor.cpp_type], out)
                    if mem_info.type_descriptor.cpp_type not in out:
                        out[mem_info.type_descriptor.cpp_type] = AdkTypes[mem_info.type_descriptor.cpp_type]
            if tp.type_descriptor.cpp_type in out:
                logger.debug("Cyclic dependency detected")
            out[tp.type_descriptor.cpp_type] = tp

        used_types = []
        for meth, method_info in self.user_def["methods"].items():
            for arg in method_info.in_args.values():
                if arg.cpp_type in AdkTypes and arg.cpp_type not in used_types:
                    used_types.append(AdkTypes[arg.cpp_type])
            for arg in method_info.out_args.values():
                if arg.cpp_type in AdkTypes and arg.cpp_type not in used_types:
                    used_types.append(AdkTypes[arg.cpp_type])
        for sig, sig_info in self.user_def["signals"].items():
            for arg in sig_info.args.values():
                if arg.cpp_type in AdkTypes and arg.cpp_type not in used_types:
                    used_types.append(AdkTypes[arg.cpp_type])
        for prop, prop_info in self.user_def["properties"].items():
            if prop_info.type_desc.cpp_type in AdkTypes and prop_info.type_desc.cpp_type not in used_types:
                used_types.append(AdkTypes[prop_info.type_desc.cpp_type])

        sorted_types = OrderedDict()
        for tp in used_types:
            sort_dep(tp, sorted_types)

        return sorted_types

    def parse(self):
        with open(self.user_json_file, encoding='utf-8', errors='ignore') as json_data:
            app_dict = json.load(json_data, strict=False, object_pairs_hook=OrderedDict)

        with open(self.repo_json_file, encoding='utf-8', errors='ignore') as json_data:
            adk_map_json_dict = json.load(json_data, strict=False, object_pairs_hook=OrderedDict)

        self.build_adk_types(adk_map_json_dict["Types"])

        if "properties" in app_dict.keys():
            for prop_name, prop_key in app_dict["properties"].items():
                if prop_key not in adk_map_json_dict["dbus_info"]["properties"]:
                    logger.info("Not generating for %s::%s, as no such entry is present in adk-meta-data provided" %(prop_name, prop_key))
                    continue
                prop_info = adk_map_json_dict["dbus_info"]["properties"][prop_key]
                self.user_def["properties"][prop_name] = Property(**prop_info)

        if "methods" in app_dict.keys():
            for method_name, method_key in app_dict["methods"].items():
                if method_key not in adk_map_json_dict["dbus_info"]["methods"]:
                    logger.info("Not generating for %s::%s, as no such entry is present in adk-meta-data provided" %(method_name, method_key))
                    continue
                method_info = adk_map_json_dict["dbus_info"]["methods"][method_key]
                self.user_def["methods"][method_name] = Method(**method_info)

        if "signals" in app_dict.keys():
            for signal_name, signal_key in app_dict["signals"].items():
                if signal_key not in adk_map_json_dict["dbus_info"]["signals"]:
                    logger.info("Not generating for %s::%s, as no such entry is present in adk-meta-data provided" %(signal_name, signal_key))
                    continue
                signal_info = adk_map_json_dict["dbus_info"]["signals"][signal_key]
                self.user_def["signals"][signal_name] = Signal(**signal_info)

        return self.user_def

class Property:
    """
    adk_map property class
    """
    def __init__(self, **kwargs):
        for k in ("name", "svc", "iface", "obj_path", "access", "bustype", "description"):
            setattr(self, k, kwargs[k])
        self.type_desc = TypeDescriptor(kwargs["type_info"]["cpp_type"],
                                        kwargs["type_info"]["dbus_fmt"],
                                        kwargs["type_info"]["is_array"],
                                        kwargs["type_info"]["is_dict"],
                                        kwargs["type_info"]["key_type"])

        if "path_ids" in kwargs:
            self.path_ids = kwargs["path_ids"]

    @property
    def is_writable(self):
        if "write" in self.access.lower():
            return True
        return False

    def __repr__(self):
        repr = {}
        for k in dir(self):
            if not k.startswith("__"):
                repr[k] = getattr(self, k)
        return pprint.pformat(repr, indent=4)

class Method:
    """
    adk_map method class
    """

    def __init__(self, **kwargs):
        for k in ("name", "svc", "iface", "obj_path", "description",
                  "in_order", "out_order", "bustype"):
            setattr(self, k, kwargs[k])
        self.in_args = CustomOrderedDict()
        self.out_args = CustomOrderedDict()
        for in_arg, in_arg_info in kwargs["in_args"].items():
            self.in_args[in_arg] = TypeDescriptor(in_arg_info["type_info"]["cpp_type"],
                                                  in_arg_info["type_info"]["dbus_fmt"],
                                                  in_arg_info["type_info"]["is_array"],
                                                  in_arg_info["type_info"]["is_dict"],
                                                  in_arg_info["type_info"]["key_type"])

        for out_arg, out_arg_info in kwargs["out_args"].items():
            self.out_args[out_arg] = TypeDescriptor(out_arg_info["type_info"]["cpp_type"],
                                                    out_arg_info["type_info"]["dbus_fmt"],
                                                    out_arg_info["type_info"]["is_array"],
                                                    out_arg_info["type_info"]["is_dict"],
                                                    out_arg_info["type_info"]["key_type"])
        self.in_arg_dbus_fmt = ""
        self.out_arg_dbus_fmt = ""
        for in_arg in self.in_order:
            self.in_arg_dbus_fmt += self.in_args[in_arg].dbus_fmt
        for out_arg in self.out_order:
            self.out_arg_dbus_fmt += self.out_args[out_arg].dbus_fmt

        if "path_ids" in kwargs:
            self.path_ids = kwargs["path_ids"]

    def __repr__(self):
        repr = {}
        for k in dir(self):
            if not k.startswith("__"):
                repr[k] = getattr(self, k)
        return pprint.pformat(repr, indent=4)

class Signal:
    """
    adk_map signal class
    """

    def __init__(self, **kwargs):
        for k in ("name", "svc", "iface", "obj_path", "description",
                  "order", "bustype"):
            setattr(self, k, kwargs[k])
        self.args = CustomOrderedDict()
        for arg, arg_info in kwargs["args"].items():
            self.args[arg] = TypeDescriptor(arg_info["type_info"]["cpp_type"],
                                            arg_info["type_info"]["dbus_fmt"],
                                            arg_info["type_info"]["is_array"],
                                            arg_info["type_info"]["is_dict"],
                                            arg_info["type_info"]["key_type"])
        self.arg_dbus_fmt = ""
        for arg in self.order:
            self.arg_dbus_fmt += self.args[arg].dbus_fmt

        if "path_ids" in kwargs:
            self.path_ids = kwargs["path_ids"]

    def __repr__(self):
        repr = {}
        for k in dir(self):
            if not k.startswith("__"):
                repr[k] = getattr(self, k)
        return pprint.pformat(repr, indent=4)
