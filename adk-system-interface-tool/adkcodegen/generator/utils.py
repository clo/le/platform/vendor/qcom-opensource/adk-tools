"""
* Copyright (c) 2019, The Linux Foundation. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are
* met:
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of The Linux Foundation nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
* ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
* BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
* BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""


import re


def getConstStringDef(svc=None, obj_path=None, iface=None, prop=None):
    """
    Used in creating const string ids out of golden strings, especially in context of dbus.
    This methood basically takes last 2 fields seperated by a . or / and constructs an id
    that can be used to refer to, in the generated code, the actual string provided.
    :param svc: Service name
    :param obj_path: Object Path
    :param iface: Interface name
    :param prop: Property name
    :return:
    """
    if svc:
        return re.sub('[^A-Za-z0-9]+', '', "k" + "".join([x.title() for x in svc.split(".")[-2:]]) + "Service")
    elif obj_path:
        return re.sub('[^A-Za-z0-9]+', '', "k" + "".join([x.title() for x in obj_path.split("/")[-2:]]) + "ObjPath")
    elif iface:
        return re.sub('[^A-Za-z0-9]+', '', "k" + "".join([x.title() for x in iface.split(".")[-2:]]) + "Iface")
    elif prop:
        return re.sub('[^A-Za-z0-9]+', '', "k" + "".join([x.title() for x in prop.split(".")[-2:]]))


def getConstStringAttr(attr):
    """
    Generic id generator used for any names like properties etc.
    :param attr: Value of the attribute, a string
    :return: an id resembling the string provided, but with special characters removed that can be used
             in c++ code.
    """
    return re.sub('[^A-Za-z0-9]+', "", "k" + "".join([x[0].upper() + x[1:] for x in attr.split("_")]))


def generate_key(svc=None, obj_path=None, iface=None, prop=None, sig=None, method=None):
    """
    Generate key from dbus property/method/signals.
    :param svc: Service name
    :param obj_path: Object path
    :param iface: Interface name
    :param prop: Property name
    :param sig: Signal name
    :param method: Methos name
    :return:
    """

    def GetKey(key, gen_map):
        for k, v in gen_map.items():
            key = key.replace(k, v)
        return key

    key = ""

    if svc:
        key += "#S@" + getConstStringDef(svc=svc)[1:-7] + "#"

    if obj_path:
        assert svc, "Can't generate key without service name"
        key += "@@#O@" + getConstStringDef(obj_path=obj_path)[1:-7] + "#"

    if iface:
        assert svc, "Can't generate key without service name"
        key += "@@#I@" + getConstStringDef(iface=iface)[1:-5] + "#"

    if prop:
        assert svc and iface, "Can't generate key without service name and interface name"
        key += "P#" + getConstStringDef(prop=prop)[1:] + "#"
        return key

    if sig:
        assert svc and iface, "Can't generate key without service name and interface name"
        key += "S#" + sig + "#"
        return key

    if method:
        assert svc and iface, "Can't generate key without service name and interface name"
        key += "M#" + method + "#"
        return key

    return key
