"""
* Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are
* met:
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of The Linux Foundation nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
* ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
* BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
* BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

import json
import pprint
import re
import os
import logging
import itertools
from collections import OrderedDict

from .create_repo import TypeDescriptor, AdkType, DBusTypes, ElemType

FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(format=FORMAT)

logger = logging.getLogger("SearchRepository")
logger.setLevel(logging.INFO)


class UserInterfaceCreator:
    def __init__(self, repo_json="repo.json"):

        assert os.path.exists(repo_json), "Repo not found"
        with open(repo_json, encoding='utf-8', errors='ignore') as json_data:
            self.repo = json.load(json_data, strict=False, object_pairs_hook=OrderedDict)

        if os.path.exists("user.json"):
            with open("user.json", encoding='utf-8', errors='ignore') as json_data:
                self.selected_keys = json.load(json_data, strict=False, object_pairs_hook=OrderedDict)
        else:
            self.selected_keys = OrderedDict()
            self.selected_keys["properties"] = OrderedDict()
            self.selected_keys["methods"] = OrderedDict()
            self.selected_keys["signals"] = OrderedDict()

    @staticmethod
    def process_lookup_result(dbus_info):
        out = OrderedDict()
        for item_name, item_info in dbus_info.items():
            out[item_name] = item_info

        return out

    @staticmethod
    def property_dump(prop_info, prop_key):
        keys = ("name", "access", "description", "obj_path")
        out = OrderedDict((k, prop_info[k]) for k in keys)
        out["type"] = prop_info["type_info"]["complete_cpp_type"]
        if "path_ids" in prop_info:
            out["dynamic_obj_path_part"] = prop_info["path_ids"]
        out["adk_map_key"] = prop_key
        return out

    @staticmethod
    def method_dump(meth_info, meth_key):
        keys = ("name", "out_order", "in_order", "description", "obj_path")
        out = OrderedDict((k, meth_info[k]) for k in keys)
        out["in_args"] = OrderedDict()
        out["out_args"] = OrderedDict()
        for arg, arg_info in meth_info["in_args"].items():
            if arg not in out["in_args"]:
                out["in_args"][arg] = {}
            out["in_args"][arg]["type"] = arg_info["type_info"]["complete_cpp_type"]
        for arg, arg_info in meth_info["out_args"].items():
            if arg not in out["out_args"]:
                out["out_args"][arg] = {}
            out["out_args"][arg]["type"] = arg_info["type_info"]["complete_cpp_type"]
        if "path_ids" in meth_info:
            out["dynamic_obj_path_part"] = meth_info["path_ids"]
        out["adk_map_key"] = meth_key
        return out

    @staticmethod
    def signal_dump(sig_info, sig_key):
        keys = ("name", "order", "description", "obj_path")
        out = OrderedDict((k, sig_info[k]) for k in keys)
        out["args"] = OrderedDict()
        for arg, arg_info in sig_info["args"].items():
            if arg not in out["args"]:
                out["args"][arg] = {}
            out["args"][arg]["type"] = arg_info["type_info"]["complete_cpp_type"]
        if "path_ids" in sig_info:
            out["dynamic_obj_path_part"] = sig_info["path_ids"]
        out["adk_map_key"] = sig_key
        return out

    def lookup_property(self, prop=None, exact=False):
        props_found = OrderedDict()
        if prop:
            for repo_k, k_info in self.repo["dbus_info"]["properties"].items():
                if (not exact and (prop.lower() in k_info["name"].lower())) or (
                        exact and (prop.lower() == k_info["name"].lower())):
                    if repo_k not in props_found:
                        props_found[repo_k] = {}
                    props_found[repo_k] = self.property_dump(k_info, repo_k)

        return self.process_lookup_result(props_found)

    def find_property(self, prop):
        return self.lookup_property(prop, exact=True)

    def lookup_method(self, meth=None, exact=False):
        methods_found = OrderedDict()
        if meth:
            for repo_k, k_info in self.repo["dbus_info"]["methods"].items():
                if (not exact and (meth.lower() in k_info["name"].lower())) or (
                        exact and (meth.lower() == k_info["name"].lower())):
                    if repo_k not in methods_found:
                        methods_found[repo_k] = {}
                    methods_found[repo_k] = self.method_dump(k_info, repo_k)
        return self.process_lookup_result(methods_found)

    def find_method(self, meth):
        return self.lookup_method(meth, exact=True)

    def lookup_signal(self, sig=None, exact=False):
        signals_found = OrderedDict()
        if sig:
            for repo_k, k_info in self.repo["dbus_info"]["signals"].items():
                if (not exact and (sig.lower() in k_info["name"].lower())) or (
                        exact and (sig.lower() == k_info["name"].lower())):
                    if repo_k not in signals_found:
                        signals_found[repo_k] = {}
                    signals_found[repo_k] = self.signal_dump(k_info, repo_k)
        return self.process_lookup_result(signals_found)

    def find_signal(self, sig):
        return self.lookup_signal(sig, exact=True)

    def print_type_info(self, type_name=None):
        out = OrderedDict()
        if type_name and type_name in self.repo["Types"]:
            out = OrderedDict((k, self.repo["Types"][type_name][k]) for k in ("resolved", "order", "description", "members"))
            for mem_name, mem_info in self.repo["Types"][type_name]["members"].items():
                out["members"][mem_name]["type"] = mem_info["desc"]["complete_cpp_type"]

        return out

    def list_all_keys(self):
        key_dump = OrderedDict()
        key_dump["properties"] = [self.property_dump(prop_info, prop) for prop, prop_info in self.repo["dbus_info"]["properties"].items()]
        key_dump["methods"] = [self.method_dump(method_info, method) for method, method_info in self.repo["dbus_info"]["methods"].items()]
        key_dump["signals"] = [self.signal_dump(signal_info, signal) for signal, signal_info in self.repo["dbus_info"]["signals"].items()]
        return key_dump

    def select_key(self, key=None, name=None):
        if key and name:
            if key in self.repo["dbus_info"]["properties"]:
                if not name in self.selected_keys["properties"]:
                    self.selected_keys["properties"][name] = key
                    print("Added key ", self.selected_keys["properties"][name])
                else:
                    print(name," already exists")
            elif key in self.repo["dbus_info"]["methods"]:
                if not name in self.selected_keys["methods"]:
                    self.selected_keys["methods"][name] = key
                    print("Added key ", self.selected_keys["methods"][name])
                else:
                    print(name," already exists")
            elif key in self.repo["dbus_info"]["signals"]:
                if not name in self.selected_keys["signals"]:
                    self.selected_keys["signals"][name] = key
                    print("Added key ", self.selected_keys["signals"][name])
                else:
                    print(name," already exists")
            else:
                print("Invalid key selected")

    def delete_key(self, name=None, elem=None):
        if name and elem:
            if elem.lower() == "properties":
                if name in self.selected_keys["properties"]:
                    del self.selected_keys["properties"][name]
                else:
                    print(name, "not in selected properties")
            elif elem.lower() == "methods":
                if name in self.selected_keys["methods"]:
                    del self.selected_keys["methods"][name]
                else:
                    print(name, "not in selected methods")
            elif elem.lower() == "signals":
                if name in self.selected_keys["signals"]:
                    del self.selected_keys["signals"][name]
                else:
                    print(name, "not in selected signals")

    def delete_all_keys(self, choice=None):
        if choice:
            if choice.lower() == "y":
                self.selected_keys["properties"].clear()
                self.selected_keys["methods"].clear()
                self.selected_keys["signals"].clear()
                print("Cleared all selected keys")

    def display_json(self):
        self.write_output(self.selected_keys)

    def write_output(self, out):
        print(json.dumps(out, indent=4))
        print("\n" + "*" * 150)

    def menu(self):
        options = """
        0. Exit
        1. Find property
        2. Search Property containing substring
        3. Find method
        4. Search method containing substring
        5. Find Signal
        6. Search signal containing a substring
        7. Get Complex type info
        8. List All keys
        9. Select keys for code generation
        10. Display JSON with selected keys
        11. Delete Selected keys
        12. Delete all Selected keys
        """

        while True:
            print(options)
            try:
                option = int(input("Enter your choice : "))
            except Exception as e:
                print("Invalid Input .. Try again")
                continue

            if option == 0:
                break
            elif option == 1:
                prop = input("Enter property to search : ")
                self.write_output(self.find_property(prop))
            elif option == 2:
                prop = input("Enter word in property to lookup : ")
                self.write_output(self.lookup_property(prop))
            elif option == 3:
                method = input("Enter method to search : ")
                self.write_output(self.find_method(method))
            elif option == 4:
                method = input("Enter word in method to lookup : ")
                self.write_output(self.lookup_method(method))
            elif option == 5:
                sig = input("Enter signal to search : ")
                self.write_output(self.find_signal(sig))
            elif option == 6:
                sig = input("Enter word in method to lookup : ")
                self.write_output(self.lookup_signal(sig))
            elif option == 7:
                type = input("Enter the type : ")
                self.write_output(self.print_type_info(type))
            elif option == 8:
                self.write_output(self.list_all_keys())
            elif option == 9:
                print("Editing the existing user.json. Please backup the file if required")
                key = input("Enter the selected key : ")
                name = input("Enter the user defined name for the key : ")
                self.select_key(key, name)
                self.display_json()
            elif option == 10:
                self.display_json()
            elif option == 11:
                print("Editing the existing user.json. Please backup the file if required")
                name = input("Enter the name of the item to be deleted : ")
                elem = input("Enter the handler in which you want the name to be deleted : ")
                self.delete_key(name, elem)
                self.display_json()
            elif option == 12:
                print("All selected keys will be deleted in user.json file. Please backup the file if required")
                choice = input("Continue? Y/N : ")
                self.delete_all_keys(choice)
                self.display_json()
            else:
                print("Invalid choice .. Try again")

def main():
    ui = UserInterfaceCreator(os.path.join(os.path.dirname(os.path.abspath(__file__)), "..", "repo", "repo.json"))
    ui.menu()
    f = open("user.json", "w")
    f.write(json.dumps(ui.selected_keys, indent=4))
    f.close()


if __name__ == "__main__":
    main()
