"""
* Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are
* met:
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of The Linux Foundation nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
* ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
* BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
* BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""


import os
import sys
import re
import logging
from subprocess import run
from jinja2 import Template, Environment, FileSystemLoader, contextfunction
from types import SimpleNamespace
from .utils import getConstStringDef, getConstStringAttr

logger = logging.getLogger('adk_code_generator')

class Filters:
    """
    Jinja filters to format names
    """
    @staticmethod
    def lower_snake_case(x):
        return re.sub('([a-z0-9])([A-Z])', r'\1_\2', x).lower().replace(".", "_").replace("/", "_")

    @staticmethod
    def upper_camel_case(x):
        return "".join([_[0].upper() + _[1:] for _ in re.split("_|\\.", x)])

    @staticmethod
    def lower_camel_case(x):
        l_string = "".join([_[0].upper() + _[1:] for _ in re.split("_|\\.", x)])
        return l_string[0].lower() + l_string[1:]

    @staticmethod
    def extract_module_submodule(x):
        return "".join([_.title() for _ in x.split(".")[-2:]])

    @staticmethod
    def remove_namespace_from_type(x):
        return x.rsplit("::", 1)[-1]

@contextfunction
def get_const_string_def(context, **kwargs):
    return getConstStringDef(**kwargs)

@contextfunction
def get_const_string_attr(context, attr):
    return getConstStringAttr(attr)

@contextfunction
def is_adk_type(context, adk_types, cpp_type):
    if cpp_type in adk_types:
        return 1
    return 0

@contextfunction
def has_path_ids(context, item):
        return hasattr(item, 'path_ids')

@contextfunction
def get_all_dbus_ifaces(context, user_def):
    """
    Get all dbus interfaces
    :param context: Jinja caller context
    :return: List of all dbus interfaces
    """
    ifaces = []
    for prop, prop_info in user_def["properties"].items():
        if prop_info.iface not in ifaces:
            ifaces.append(prop_info.iface)
    for meth, meth_info in user_def["methods"].items():
        if meth_info.iface not in ifaces:
            ifaces.append(meth_info.iface)
    for sig, sig_info in user_def["signals"].items():
        if sig_info.iface not in ifaces:
            ifaces.append(sig_info.iface)

    return ifaces

@contextfunction
def get_all_dbus_svc(context, user_def):
    """
    Get all dbus services
    :param context: Jinja caller context
    :return: List of all dbus services
    """
    svc = []
    for prop, prop_info in user_def["properties"].items():
        if prop_info.svc not in svc:
            svc.append(prop_info.svc)
    for meth, meth_info in user_def["methods"].items():
        if meth_info.svc not in svc:
            svc.append(meth_info.svc)
    for sig, sig_info in user_def["signals"].items():
        if sig_info.svc not in svc:
            svc.append(sig_info.svc)

    return svc

@contextfunction
def get_ifaces(context, iface_def):
    """
    get interface names for each element in iface_def
    :param context: Jinja caller context
    :param iface_def: dictionary with iface
    :return: List of interfaces
    """
    ifaces = []
    for iface, iface_info in iface_def.items():
        if iface_info.iface not in ifaces:
            ifaces.append(iface_info.iface)

    return ifaces

@contextfunction
def get_dyn_ifaces(context, iface_def):
    ifaces = []
    for iface, iface_info in iface_def.items():
        if iface_info.iface not in ifaces and hasattr(iface_info, 'path_ids'):
            ifaces.append(iface_info.iface)

    return ifaces

@contextfunction
def get_services(context, svc_def):
    """
    get service names for each element in svc_def
    :param context: Jinja caller context
    :param svc_def: dictionary with service
    :return: List of services
    """
    svcs = []
    for svc, svc_info in svc_def.items():
        if svc_info.svc not in svcs:
            svcs.append(svc_info.svc)

    return svcs

@contextfunction
def get_ipc_ifaces(context, iface_def):
    """
    get the interface hierarchy from the iface definition items
    :param context: Jinja caller context
    :param iface_def: dictionary with iface
    :return: list of dbus interface hierarchy
    """
    ifaces = []
    for iface, iface_info in iface_def.items():
        try:
            if not hasattr(iface_info, 'path_ids') and iface_info.bustype == "session":
                iface = SimpleNamespace()
                iface.name = iface_info.iface
                iface.svc = iface_info.svc
                iface.obj_path = iface_info.obj_path
                if iface not in ifaces:
                    ifaces.append(iface)
        except Exception as e:
            logger.error("Failed to get interfaces from %s" %iface_info.name)
    return ifaces

@contextfunction
def get_static_dbus_obj_paths(context, user_def):
    """
    get object paths list for the pre-defined ones
    :param context: Jinja caller context
    :param user_def: dictionary with user_definition
    :return: list of predefined object paths
    """

    obj_paths = []
    for prop, prop_info in user_def["properties"].items():
        if prop_info.obj_path not in obj_paths:
            if not hasattr(prop_info, 'path_ids'):
                obj_paths.append(prop_info.obj_path)
    for meth, meth_info in user_def["methods"].items():
        if meth_info.obj_path not in obj_paths:
            if not hasattr(meth_info, 'path_ids'):
                obj_paths.append(meth_info.obj_path)
    for sig, sig_info in user_def["signals"].items():
        if sig_info.obj_path not in obj_paths:
            if not hasattr(sig_info, 'path_ids'):
                obj_paths.append(sig_info.obj_path)

    return obj_paths

@contextfunction
def get_g_variant_type_api(context, type_name):
    """
    Get corresponding g_variant_get_* functiont be used for a given type.
    :param context: Jinja caller context
    :param type_name: c++ type
    :return: Appropriate g_variant_get_* function for the given type
    """
    api_map = {
        "int16_t": "g_variant_get_int16",
        "uint16_t": "g_variant_get_uint16",
        "int32_t": "g_variant_get_int32",
        "uint32_t": "g_variant_get_uint32",
        "int64_t": "g_variant_get_int64",
        "uint64_t": "g_variant_get_uint64",
        "double": "g_variant_get_double",
        "std::string": "g_variant_get_string",
        "bool": "g_variant_get_boolean"
    }
    return api_map[type_name]

@contextfunction
def get_garbage_value(context, type_name):
    """
    Get the corresponding default garbage values for each type
    :param context: Jinja caller context
    :param type_name: c++ type
    :return: Appropriate garbage value
    """

    type_map = {
        "int16_t": "0",
        "uint16_t": "0",
        "int32_t": "0",
        "uint32_t": "0",
        "int64_t": "0",
        "uint64_t": "0",
        "double": "0",
        "std::string": "\"\"",
        "bool": "false"
    }

    if type_name in type_map:
        return type_map[type_name]
    return "{}"

class Renderer:
    """
    Renders Jinja templates
    """

    def __init__(self, **data):
        self.data = data

    def render(self, in_file=None, out_file=None, out_dir="."):
        """
        Jinja renderer
        :param in_file: Template file
        :param out_dir: Output directory
        :return:
        """
        if not in_file:
            logger.info("Nothing to render .. ")
            return

        env = Environment(loader=FileSystemLoader(os.path.dirname(os.path.realpath(in_file))), trim_blocks=True, lstrip_blocks=False)
        env.filters["lower_snake_case"] = Filters.lower_snake_case
        env.filters["upper_camel_case"] = Filters.upper_camel_case
        env.filters["lower_camel_case"] = Filters.lower_camel_case
        env.filters["extract_module_submodule"] = Filters.extract_module_submodule
        env.filters["remove_namespace_from_type"] = Filters.remove_namespace_from_type
        env.globals["get_all_dbus_ifaces"] = get_all_dbus_ifaces
        env.globals["get_all_dbus_svc"] = get_all_dbus_svc
        env.globals["get_ifaces"] = get_ifaces
        env.globals["is_adk_type"] = is_adk_type
        env.globals["get_garbage_value"] = get_garbage_value
        env.globals["get_g_variant_type_api"] = get_g_variant_type_api
        env.globals["get_const_string_def"] = get_const_string_def
        env.globals["get_const_string_attr"] = get_const_string_attr
        env.globals["get_static_dbus_obj_paths"] = get_static_dbus_obj_paths
        env.globals["has_path_ids"] = has_path_ids
        env.globals["get_ipc_ifaces"] = get_ipc_ifaces
        env.globals["get_services"] = get_services
        env.globals["get_dyn_ifaces"] = get_dyn_ifaces

        if out_file:
            out_f = os.path.join(out_dir, os.path.basename(out_file))
        else:
            out_f = os.path.join(out_dir, os.path.basename(in_file).rstrip(".jinja2"))
        tmp = env.get_template(os.path.basename(in_file))
        f = open(out_f, "w")
        f.write(tmp.render(**self.data))
        f.close()
        self.format_file_with_clang(out_f)
        f = open(out_f, "a")
        f.write("\n")
        f.close()

    @staticmethod
    def format_file_with_clang(outfile):
        """
        Use clang-format to align code as per Google standards
        :param outfile: File to format
        :return:
        """

        # Taking local clang format as the -style=file doesn't work as expected from python script
        clang_format_params = "{Language: Cpp, BasedOnStyle: Google, AccessModifierOffset: -3, " \
            "AlignAfterOpenBracket: DontAlign, AlignOperands: false, " \
            "AllowShortFunctionsOnASingleLine: InlineOnly, " \
            "AllowShortIfStatementsOnASingleLine: false, " \
            "AllowShortLoopsOnASingleLine: false, " \
            "BreakBeforeBinaryOperators: NonAssignment, " \
            "BreakConstructorInitializersBeforeComma: true, " \
            "BreakStringLiterals: false, ColumnLimit: 0, " \
            "IndentPPDirectives: AfterHash, PointerAlignment: Right, " \
            "Standard: Cpp11, IndentWidth: 4, TabWidth: 4}"

        try:
            if sys.platform.startswith("win"):
                run([r"clang-format.exe", "-i", "-verbose", "-style="+str(clang_format_params), outfile])
            if sys.platform.startswith("linux"):
                run([r"clang-format", "-i", "-verbose", "-style="+str(clang_format_params), outfile])
        except Exception as e:
            logger.info("Clang-formatting not applied ..")
