import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="adkcodegen",
    version="1.0",
    author="QTI",
    description="ADK interface code generator",
    long_description=long_description,
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    entry_points={
            'console_scripts': [
                r'adk-codegen = adkcodegen.main:main',
                r'create-repo = adkcodegen.generator.create_repo:main',
                r'adk-find = adkcodegen.generator.search_repo:main'
            ]
        },
    install_requires=["jinja2>=2.11.0,<2.11.3", "markupsafe==1.1.1"],
    package_data={"adkcodegen": ["repo/repo.json", "templates/*"]}
)
