# adk-system-interface-tool

Tool that generates c++ code which provides IPC agnostic APIs that any user agent
should call to read and update the user settings

### Dependencies ###
* Python3.5 is preferred, not validated against other versions.
* The tool checks for Jinja2 module. If not present the tool tries to install
  the module.
* The tool checks for clang-format(>=5). If not present the generated code is
  not formatted according to clang-formatting

### Set up the environment ###
```
cd WORKSPACE_PATH/vendor/qcom/opensource/adk-tools/adk-system-interface-tool
virtualenv -p python3.5 venv
source venv/bin/activate
```

**NOTE: All the properties and interfaces mentioned in the xmls of this project
are still under development and may change in future releases**

### Installations ###
The tool once installed provides three utilitiles
1. Create-repo - for generating internal repo.json that is used as a master
  database to expose user settings and adk-map keys
2. adk-find - A menu app that exposes the interfaces, properties, methods, signals,
  their types and keys
3. adk-codegen - Tool to generate c++ code from JSON input and Jinja2 templates.

```
python3.5 setup.py install
```

### Usage instructions ###
* create-repo

```
create-repo --xml-dir <xml_dir_path>
```
**Note:** The utility should be used only if there is a new xml added to the pool
of xmls and new adk-map have to be generated

``` create-repo --help``` can be used for more information on create-repo

* adk-find menu
```
adk-find
```

Following are the options provided in the menu app which are self-explanatory
```
0. Exit
1. List Services
2. List Interfaces
3. Find Property
4. Search Property containing word
5. List Properties of an Interface
6. Find Method
7. Search method containing word
8. List Methods of an Interface
9. Find Signal
10. Seach signal containing word
11. List Signals of an Interface
12. Get type info
13. List all keys
```

* adk-codegen code generation

```
adk-codegen --user-json <custom_json_file>
```
The <custom_json_file> is the json file with adk-map keys and property/method/signal
groupings in a format mandated for the tool. The json file should be written by
the user to match their requirements

--out-dir is optional and if not provided by the user the tool generates code
in the out folder of current directory.

```adk-codegen --help ``` can be used for more information to the adk-codegen
utility

### Current Support ###
* The tool currently supports all basic types for properties Read and Update.
* Arrays of basic types for properties Read and Update.
* Parsing complex types like array of basic types, a{sv}, array of a{sv},
  recursive a{sv}
* InterfacesAdded and PropertiesChanged signals for all services are subscribed
  for and the responses are parsed.

### Limitations ###
* The tool currently takes a single user JSON file as input. All the properties,
  methods and signals mappings required should be provided in that single file.
* Parsing of complex structures like (ssxquua{si}ns) is not supported.
* ListEffects method output is also treated similar to a structure even though
  it is a{ss} and is not supported in the current tool
* Signal matching and parsing for signals other than InterfacesAdded and
  PropertiesChanged is not supported.
* Encoding any type of complex structures other than array of basic types is not
  supported.

A sample code is provided in this project and all the instructions are in the
README.md file of the code-sample folder.
