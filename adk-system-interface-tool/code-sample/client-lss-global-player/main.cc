/*
 * Copyright (c) 2019-2021, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of The Linux Foundation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <cassert>
#include <chrono>
#include <csignal>
#include <cstring>
#include <functional>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <thread>

#include "autogen/adk_system_interface.h"
#include "autogen/attribute_classes.h"
#include "autogen/attribute_strings.h"

#include <iostream>

using namespace adk;
using namespace std;

bool startAdkSystemInterface() {
    g_print("StartAdkSystemInterface\n");

    // ignore SIGPIPE
    struct sigaction action;
    action.sa_handler = SIG_IGN;
    sigemptyset(&action.sa_mask);
    action.sa_flags = 0;
    sigaction(SIGPIPE, &action, nullptr);

    // block signals we will want to "sigwait" on.
    // (needs to be set before we create any thread or the app will be killed
    // if we receive another a second signal while processing the first)
    sigset_t sig_set;
    sigemptyset(&sig_set);
    sigaddset(&sig_set, SIGHUP);
    sigaddset(&sig_set, SIGINT);
    sigaddset(&sig_set, SIGTERM);
    pthread_sigmask(SIG_BLOCK, &sig_set, nullptr);

    AdkSystemInterface &adk_si = AdkSystemInterface::getInstance();

    if (!adk_si.init("adk_system_interface_sample")) {
        return false;
    }

    /* Get property, method & signal handlers */
    std::shared_ptr<PropertiesHandler> properties_handler = adk_si.getPropertiesHandler();
    std::shared_ptr<MethodsHandler> methods_handler = adk_si.getMethodsHandler();
    std::shared_ptr<SignalsHandler> signals_handler = adk_si.getSignalsHandler();
    bool decoder_restart = false;

    g_print("\n\n----------------- Initialize ----------------\n\n");
    g_print("registering listener for Detected format signal\n");
    signals_handler->setLSSDetectedFormatListener(
        [decoder_restart, &properties_handler, &methods_handler](
        const std::string &format_string, const std::string &obj_path) -> void {
        g_print("\n--->received DetectedFormat signal from %s as %s\n",
                obj_path.c_str(), format_string.c_str());
        ErrorCode err_code;
        std::string current_source;
        std::tie(err_code, current_source) = properties_handler->readCurrentSource();
        if (err_code == ADK_SI_OK) {
            g_print("\n--->read current source %s\n", current_source.c_str());
        } else {
            g_print("\n--->Failed to read current source %d\n", err_code);
        }

        uint32_t out_sample_freq;
        std::tie(err_code, out_sample_freq) = properties_handler->readLSSOutputsamplingfrequency();
        if (err_code == ADK_SI_OK) {
            g_print("\n--->read out_sample_freq %d\n", out_sample_freq);
        } else {
            g_print("\n--->Failed to read out_sample_freq %d\n", err_code);
        }

        std::string detected_format;
        std::tie(err_code, detected_format) = properties_handler->readLSSReadDetectedformat();
        if (err_code == ADK_SI_OK) {
            g_print("\n--->read detected format %s\n", detected_format.c_str());
        } else {
            g_print("\n--->Failed to read detected format %d\n", err_code);
        }

        if ((detected_format == "DDP") || (detected_format == "DDP ATMOS") || (detected_format == "DDP CH") ||
            (detected_format == "DD") || (detected_format == "DD CH") || (detected_format == "DD ATMOS")){
            uint32_t dec_sample_freq;
            std::tie(err_code, dec_sample_freq) = properties_handler->readLSSDecSamplFreq();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read dec_sample_freq %d\n", dec_sample_freq);
            } else {
                g_print("\n--->Failed to read dec_sample_freq %d\n", err_code);
            }

            std::string ddp_stream_type;
            std::tie(err_code, ddp_stream_type) = properties_handler->readLSSDDPStreamType();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read ddp_stream_type %s\n", ddp_stream_type.c_str());
            } else {
                g_print("\n--->Failed to read ddp_stream_type %d\n", err_code);
            }

            int32_t ddp_surround_ex;
            std::tie(err_code, ddp_surround_ex) = properties_handler->readLSSDDPSurroundex();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read ddp_surround_ex %d\n", ddp_surround_ex);
            } else {
                g_print("\n--->Failed to read ddp_surround_ex %d\n", err_code);
            }

            int32_t ddp_dual_mono;
            std::tie(err_code, ddp_dual_mono) = properties_handler->readLSSDDPDualMono();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read ddp_dual_mono %d\n", ddp_dual_mono);
            } else {
                g_print("\n--->Failed to read ddp_dual_mono %d\n", err_code);
            }
            int32_t update_ddp_dual_mono = 2;
            err_code = properties_handler->updateLSSDDPDualMono(update_ddp_dual_mono);
            if (err_code == ADK_SI_OK) {
                g_print("\n--->updated ddp_dual_mono %d\n", update_ddp_dual_mono);
            } else {
                g_print("\n--->Failed to update ddp_dual_mono %d\n", err_code);
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
            std::tie(err_code, ddp_dual_mono) = properties_handler->readLSSDDPDualMono();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read ddp_dual_mono %d\n", ddp_dual_mono);
            } else {
                g_print("\n--->Failed to read ddp_dual_mono %d\n", err_code);
            }

            int32_t ddp_karaoke;
            std::tie(err_code, ddp_karaoke) = properties_handler->readLSSDDPKaraoke();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read ddp_karaoke %d\n", ddp_karaoke);
            } else {
                g_print("\n--->Failed to read ddp_karaoke %d\n", err_code);
            }

            int32_t ddp_center_downmix_level;
            std::tie(err_code, ddp_center_downmix_level) = properties_handler->readLSSDDPCenterDownmixLevel();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read ddp_center_downmix_level %d\n", ddp_center_downmix_level);
            } else {
                g_print("\n--->Failed to read ddp_center_downmix_level %d\n", err_code);
            }

            int32_t ddp_surround_downmix_level;
            std::tie(err_code, ddp_surround_downmix_level) = properties_handler->readLSSDDPSurroundDownmixLevel();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read ddp_surround_downmix_level %d\n", ddp_surround_downmix_level);
            } else {
                g_print("\n--->Failed to read ddp_surround_downmix_level %d\n", err_code);
            }

            int32_t ddp_diag_norm;
            std::tie(err_code, ddp_diag_norm) = properties_handler->readLSSDDPDialoguenormalization();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read ddp_diag_norm %d\n", ddp_diag_norm);
            } else {
                g_print("\n--->Failed to read ddp_diag_norm %d\n", err_code);
            }

            int32_t ddp_audio_coding_mode;
            std::tie(err_code, ddp_audio_coding_mode) = properties_handler->readLSSDDPAudioCodingMode();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read ddp_audio_coding_mode %d\n", ddp_audio_coding_mode);
            } else {
                g_print("\n--->Failed to read ddp_audio_coding_mode %d\n", err_code);
            }

            int32_t ddp_lfe;
            std::tie(err_code, ddp_lfe) = properties_handler->readLSSDDPLfe();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read ddp_lfe %d\n", ddp_lfe);
            } else {
                g_print("\n--->Failed to read ddp_lfe %d\n", err_code);
            }

            uint32_t ddp_custom_ch_map;
            std::tie(err_code, ddp_custom_ch_map) = properties_handler->readLSSDDPCustomChMap();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read ddp_custom_ch_map %d\n", ddp_custom_ch_map);
            } else {
                g_print("\n--->Failed to read ddp_custom_ch_map %d\n", err_code);
            }

            int32_t ddp_bit_rate;
            std::tie(err_code, ddp_bit_rate) = properties_handler->readLSSDDPBitrate();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read ddp_bit_rate %d\n", ddp_bit_rate);
            } else {
                g_print("\n--->Failed to read ddp_bit_rate %d\n", err_code);
            }

            std::vector<int32_t> ddp_drc_mode;
            std::tie(err_code, ddp_drc_mode) = properties_handler->readLSSDDPDrcmode();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read ddp_drc_mode\n");
                for (auto &it : ddp_drc_mode) {
                    g_print(" %d \n", it);
                }
            } else {
                g_print("\n--->Failed to read ddp_drc_mode %d\n", err_code);
            }
            std::vector<int32_t> valued{1,2,3};
            err_code = properties_handler->updateLSSDDPDrcmode(valued);
            if (err_code == ADK_SI_OK) {
                g_print("\n--->updated ddp_drc_mode\n");
            } else {
                g_print("\n--->Failed to update ddp_drc_mode %d\n", err_code);
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
            std::tie(err_code, ddp_drc_mode) = properties_handler->readLSSDDPDrcmode();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read ddp_drc_mode\n");
                for (auto &it : ddp_drc_mode) {
                    g_print(" %d \n", it);
                }
            } else {
                g_print("\n--->Failed to read ddp_drc_mode %d\n", err_code);
            }

            int32_t ddp_dec_ch_config;
            std::tie(err_code, ddp_dec_ch_config) = properties_handler->readLSSDDPDecChConfig();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read ddp_dec_ch_config %d\n", ddp_dec_ch_config);
            } else {
                g_print("\n--->Failed to read ddp_dec_ch_config %d\n", err_code);
            }
            int32_t update_ddp_dec_ch_config = 2;
            err_code = properties_handler->updateLSSDDPDecChConfig(update_ddp_dec_ch_config);
            if (err_code == ADK_SI_OK) {
                g_print("\n--->updated ddp_dec_ch_config %d\n", update_ddp_dec_ch_config);
            } else {
                g_print("\n--->Failed to update ddp_dec_ch_config %d\n", err_code);
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
            std::tie(err_code, ddp_dec_ch_config) = properties_handler->readLSSDDPDecChConfig();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read ddp_dec_ch_config %d\n", ddp_dec_ch_config);
            } else {
                g_print("\n--->Failed to read ddp_dec_ch_config %d\n", err_code);
            }
        } else if (detected_format == "PCM") {
            uint32_t pcm_sample_rate;
            std::tie(err_code, pcm_sample_rate) = properties_handler->readLSSPCMsamplingRate();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read pcm_sample_rate %d\n", pcm_sample_rate);
            } else {
                g_print("\n--->Failed to read pcm_sample_rate %d\n", err_code);
            }

            std::string pcm_ch_config;
            std::tie(err_code, pcm_ch_config) = properties_handler->readLSSPCMChConfig();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read pcm_ch_config %s\n", pcm_ch_config.c_str());
            } else {
                g_print("\n--->Failed to read pcm_ch_config %d\n", err_code);
            }
        } else if (detected_format == "AAC") {
            uint32_t dec_sample_freq;
            std::tie(err_code, dec_sample_freq) = properties_handler->readLSSDecSamplFreq();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read dec_sample_freq %d\n", dec_sample_freq);
            } else {
                g_print("\n--->Failed to read dec_sample_freq %d\n", err_code);
            }

            /* Read current channel mode */
            int32_t current_channelmode;
            std::tie(err_code, current_channelmode) = properties_handler->readLSSAacDualMonoChannelMode();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read Dual Mono channel mode as %d\n", current_channelmode);
            } else {
                g_print("\n--->Failed to read Dual Mono channel mode %d\n", err_code);
            }

            /* change to new channel mode */
            int32_t new_channelmode = 0;
            if (0 == current_channelmode) {
                new_channelmode = 1;
            } else if (1 == current_channelmode) {
                new_channelmode = 2;
            }
            err_code = properties_handler->updateLSSAacDualMonoChannelMode(new_channelmode);
            if (err_code == ADK_SI_OK) {
                g_print("\n--->Set Dual Mono channel mode to %d\n", new_channelmode);
            } else {
                g_print("\n--->Failed to set Dual Mono channel mode %d\n", err_code);
            }

            /* Read updated channel mode */
            int32_t updated_channelmode;
            std::tie(err_code, updated_channelmode) = properties_handler->readLSSAacDualMonoChannelMode();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read updated Dual Mono channel mode as %d\n", updated_channelmode);
            } else {
                g_print("\n--->Failed to read Dual Mono channel mode %d\n", err_code);
            }

            /* Check for test case validation */
            if (updated_channelmode == new_channelmode) {
                g_print("\n--->Successfully verified Dual mono signal, set & get\n");
            } else {
                g_print("\n--->Failed to verify Dual mono signal, set & get\n");
            }
        } else if (detected_format == "DSD") {
            uint32_t dec_sample_freq;
            std::tie(err_code, dec_sample_freq) = properties_handler->readLSSDecSamplFreq();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read dec_sample_freq %d\n", dec_sample_freq);
            } else {
                g_print("\n--->Failed to read dec_sample_freq %d\n", err_code);
            }

            int32_t dsd_ch_config;
            std::tie(err_code, dsd_ch_config) = properties_handler->readLSSDSDChConfig();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read dsd_ch_config %d\n", dsd_ch_config);
            } else {
                g_print("\n--->Failed to read dsd_ch_config %d\n", err_code);
            }

            uint32_t dsd_sample_rate;
            std::tie(err_code, dsd_sample_rate) = properties_handler->readLSSDSDSamplingrate();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read dsd_sample_rate %d\n", dsd_sample_rate);
            } else {
                g_print("\n--->Failed to read dsd_sample_rate %d\n", err_code);
            }

            uint32_t dsd_type;
            std::tie(err_code, dsd_type) = properties_handler->readLSSDSDType();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read dsd_type %d\n", dsd_type);
            } else {
                g_print("\n--->Failed to read dsd_type %d\n", err_code);
            }

            std::string dsd_path = "dummydsdpath";
            err_code = methods_handler->lSSDSDPath(dsd_path);
            if (err_code == ADK_SI_OK) {
               g_print("lSSDSDPath method call success\n");
            } else {
               g_printerr("lSSDSDPath method call failed : %d\n", err_code);
            }
        } else if ((detected_format == "DTS") || (detected_format == "DTSX") || (detected_format == "DTS CD")) {
            uint32_t dec_sample_freq;
            std::tie(err_code, dec_sample_freq) = properties_handler->readLSSDecSamplFreq();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read dec_sample_freq %d\n", dec_sample_freq);
            } else {
                g_print("\n--->Failed to read dec_sample_freq %d\n", err_code);
            }

            int32_t dts_bit_rate;
            std::tie(err_code, dts_bit_rate) = properties_handler->readLSSDTSBitrate();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read dts_bit_rate %d\n", dts_bit_rate);
            } else {
                g_print("\n--->Failed to read dts_bit_rate %d\n", err_code);
            }

            int32_t dts_es_matrix_flag;
            std::tie(err_code, dts_es_matrix_flag) = properties_handler->readLSSDTSEsMatrixFlag();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read dts_es_matrix_flag %d\n", dts_es_matrix_flag);
            } else {
                g_print("\n--->Failed to read dts_es_matrix_flag %d\n", err_code);
            }

            int32_t dtsx_ch_flag;
            std::tie(err_code, dtsx_ch_flag) = properties_handler->readLSSDTSXchFlag();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read dtsx_ch_flag %d\n", dtsx_ch_flag);
            } else {
                g_print("\n--->Failed to read dtsx_ch_flag %d\n", err_code);
            }

            int32_t dtsx_96_flag;
            std::tie(err_code, dtsx_96_flag) = properties_handler->readLSSDTSX96Flag();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read dtsx_96_flag %d\n", dtsx_96_flag);
            } else {
                g_print("\n--->Failed to read dtsx_96_flag %d\n", err_code);
            }

            int32_t dts_diag_norm;
            std::tie(err_code, dts_diag_norm) = properties_handler->readLSSDTSDialogueNormalization();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read dts_diag_norm %d\n", dts_diag_norm);
            } else {
                g_print("\n--->Failed to read dts_diag_norm %d\n", err_code);
            }

            int32_t dts_extn_mask;
            std::tie(err_code, dts_extn_mask) = properties_handler->readLSSDTSExtensionMask();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read dts_extn_mask %d\n", dts_extn_mask);
            } else {
                g_print("\n--->Failed to read dts_extn_mask %d\n", err_code);
            }

            uint32_t dts_prog_ch_mask;
            std::tie(err_code, dts_prog_ch_mask) = properties_handler->readLSSDTSProgChMask();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read dts_prog_ch_mask %d\n", dts_prog_ch_mask);
            } else {
                g_print("\n--->Failed to read dts_prog_ch_mask %d\n", err_code);
            }

            int32_t dtsx_11_sample_rate;
            std::tie(err_code, dtsx_11_sample_rate) = properties_handler->readLSSDTSXllsamplerate();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read dtsx_11_sample_rate %d\n", dtsx_11_sample_rate);
            } else {
                g_print("\n--->Failed to read dtsx_11_sample_rate %d\n", err_code);
            }

            int32_t dts_embedded_downmix_select;
            std::tie(err_code, dts_embedded_downmix_select) = properties_handler->readLSSDTSEmbeddeddownmixselect();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read dts_embedded_downmix_select %d\n", dts_embedded_downmix_select);
            } else {
                g_print("\n--->Failed to read dts_embedded_downmix_select %d\n", err_code);
            }

            uint32_t dts_sample_rate;
            std::tie(err_code, dts_sample_rate) = properties_handler->readLSSDTSSamplerate();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read dts_sample_rate %d\n", dts_sample_rate);
            } else {
                g_print("\n--->Failed to read dts_sample_rate %d\n", err_code);
            }

            int32_t dts_stream_type;
            std::tie(err_code, dts_stream_type) = properties_handler->readLSSDTSStreamType();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read dts_stream_type %d\n", dts_stream_type);
            } else {
                g_print("\n--->Failed to read dts_stream_type %d\n", err_code);
            }

            int32_t dts_decode_info;
            std::tie(err_code, dts_decode_info) = properties_handler->readLSSDTSDecodeInfo();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read dts_decode_info %d\n", dts_decode_info);
            } else {
                g_print("\n--->Failed to read dts_decode_info %d\n", err_code);
            }

            int32_t dts_core_bit_rate;
            std::tie(err_code, dts_core_bit_rate) = properties_handler->readLSSDTSCoreBitRate();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read dts_core_bit_rate %d\n", dts_core_bit_rate);
            } else {
                g_print("\n--->Failed to read dts_core_bit_rate %d\n", err_code);
            }

            int32_t dts_object_num;
            std::tie(err_code, dts_object_num) = properties_handler->readLSSDTSObjectNum();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read dts_object_num %d\n", dts_object_num);
            } else {
                g_print("\n--->Failed to read dts_object_num %d\n", err_code);
            }

            int32_t dts_diag_control_avail;
            std::tie(err_code, dts_diag_control_avail) = properties_handler->readLSSDTSDialogueControlAvailability();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read dts_diag_control_avail %d\n", dts_diag_norm);
            } else {
                g_print("\n--->Failed to read dts_diag_control_avail %d\n", err_code);
            }

            int32_t dts_analog_comp_gain;
            std::tie(err_code, dts_analog_comp_gain) = properties_handler->readLSSDTSAnalogCompensationGain();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read dts_analog_comp_gain %d\n", dts_analog_comp_gain);
            } else {
                g_print("\n--->Failed to read dts_analog_comp_gain %d\n", err_code);
            }

            int32_t dtsx_drc_percent;
            std::tie(err_code, dtsx_drc_percent) = properties_handler->readLSSDtsxDrcpercent();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read dtsx_drc_percent %d\n", dtsx_drc_percent);
            } else {
                g_print("\n--->Failed to read dtsx_drc_percent %d\n", err_code);
            }
            int32_t update_dtsx_drc_percent = 2;
            err_code = properties_handler->updateLSSDtsxDrcpercent(update_dtsx_drc_percent);
            if (err_code == ADK_SI_OK) {
                g_print("\n--->updated dtsx_drc_percent %d\n", update_dtsx_drc_percent);
            } else {
                g_print("\n--->Failed to update dtsx_drc_percent %d\n", err_code);
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
            std::tie(err_code, dtsx_drc_percent) = properties_handler->readLSSDtsxDrcpercent();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read dtsx_drc_percent %d\n", dtsx_drc_percent);
            } else {
                g_print("\n--->Failed to read dtsx_drc_percent %d\n", err_code);
            }

            int32_t dtsx_spkr_out;
            std::tie(err_code, dtsx_spkr_out) = properties_handler->readLSSDTSXSpkrout();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read dtsx_spkr_out %d\n", dtsx_spkr_out);
            } else {
                g_print("\n--->Failed to read dtsx_spkr_out %d\n", err_code);
            }
            int32_t update_dtsx_spkr_out = 2;
            err_code = properties_handler->updateLSSDTSXSpkrout(update_dtsx_spkr_out);
            if (err_code == ADK_SI_OK) {
                g_print("\n--->update dtsx_spkr_out %d\n", update_dtsx_spkr_out);
            } else {
                g_print("\n--->Failed to update dtsx_spkr_out %d\n", err_code);
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
            std::tie(err_code, dtsx_spkr_out) = properties_handler->readLSSDTSXSpkrout();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read dtsx_spkr_out %d\n", dtsx_spkr_out);
            } else {
                g_print("\n--->Failed to read dtsx_spkr_out %d\n", err_code);
            }

            int32_t dtsx_set_diag_control;
            std::tie(err_code, dtsx_set_diag_control) = properties_handler->readLSSDTSXSetDialogcontrol();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read dtsx_set_diag_control %d\n", dtsx_set_diag_control);
            } else {
                g_print("\n--->Failed to read dtsx_set_diag_control %d\n", err_code);
            }
            int32_t update_dtsx_set_diag_control = 2;
            err_code = properties_handler->updateLSSDTSXSetDialogcontrol(update_dtsx_set_diag_control);
            if (err_code == ADK_SI_OK) {
                g_print("\n--->update dtsx_set_diag_control %d\n", update_dtsx_set_diag_control);
            } else {
                g_print("\n--->Failed to update dtsx_set_diag_control %d\n", err_code);
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
            std::tie(err_code, dtsx_set_diag_control) = properties_handler->readLSSDTSXSetDialogcontrol();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read dtsx_set_diag_control %d\n", dtsx_set_diag_control);
            } else {
                g_print("\n--->Failed to read dtsx_set_diag_control %d\n", err_code);
            }

            int32_t dtsx_get_diag_control;
            std::tie(err_code, dtsx_get_diag_control) = properties_handler->readLSSDTSXGetDialogcontrol();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read dtsx_get_diag_control %d\n", dtsx_get_diag_control);
            } else {
                g_print("\n--->Failed to read dtsx_get_diag_control %d\n", err_code);
            }

            int32_t dtsx_enable_direct_mode;
            std::tie(err_code, dtsx_enable_direct_mode) = properties_handler->readLSSDTSXEnableDirectmode();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read dtsx_enable_direct_mode %d\n", dtsx_enable_direct_mode);
            } else {
                g_print("\n--->Failed to read dtsx_enable_direct_mode %d\n", err_code);
            }
            int32_t update_dtsx_enable_direct_mode = (dtsx_enable_direct_mode == 1)? 0 : 1;
            err_code = properties_handler->updateLSSDTSXEnableDirectmode(update_dtsx_enable_direct_mode);
            if (err_code == ADK_SI_OK) {
                g_print("\n--->update dtsx_enable_direct_mode %d\n", update_dtsx_enable_direct_mode);
            } else {
                g_print("\n--->Failed to update dtsx_enable_direct_mode %d\n", err_code);
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
            std::tie(err_code, dtsx_enable_direct_mode) = properties_handler->readLSSDTSXEnableDirectmode();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read dtsx_enable_direct_mode %d\n", dtsx_enable_direct_mode);
            } else {
                g_print("\n--->Failed to read dtsx_enable_direct_mode %d\n", err_code);
            }

            int32_t dtsx_disable_direct_mode;
            std::tie(err_code, dtsx_disable_direct_mode) = properties_handler->readLSSDTSXDisableDirectmode();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read dtsx_disable_direct_mode %d\n", dtsx_disable_direct_mode);
            } else {
                g_print("\n--->Failed to read dtsx_disable_direct_mode %d\n", err_code);
            }
            int32_t update_dtsx_disable_direct_mode = (dtsx_disable_direct_mode == 1)? 0 : 1;
            err_code = properties_handler->updateLSSDTSXDisableDirectmode(update_dtsx_disable_direct_mode);
            if (err_code == ADK_SI_OK) {
                g_print("\n--->update dtsx_disable_direct_mode %d\n", update_dtsx_disable_direct_mode);
            } else {
                g_print("\n--->Failed to update dtsx_disable_direct_mode %d\n", err_code);
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
            std::tie(err_code, dtsx_disable_direct_mode) = properties_handler->readLSSDTSXDisableDirectmode();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read dtsx_disable_direct_mode %d\n", dtsx_disable_direct_mode);
            } else {
                g_print("\n--->Failed to read dtsx_disable_direct_mode %d\n", err_code);
            }

            int32_t dtsx_enable_blind_parma;
            std::tie(err_code, dtsx_enable_blind_parma) = properties_handler->readLSSDTSXEnableBlindparma();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read dtsx_enable_blind_parma %d\n", dtsx_enable_blind_parma);
            } else {
                g_print("\n--->Failed to read dtsx_enable_blind_parma %d\n", err_code);
            }
            int32_t update_dtsx_enable_blind_parma = (dtsx_enable_blind_parma == 1)? 0 : 1;
            err_code = properties_handler->updateLSSDTSXEnableBlindparma(update_dtsx_enable_blind_parma);
            if (err_code == ADK_SI_OK) {
                g_print("\n--->update dtsx_enable_blind_parma %d\n", update_dtsx_enable_blind_parma);
            } else {
                g_print("\n--->Failed to update dtsx_enable_blind_parma %d\n", err_code);
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
            std::tie(err_code, dtsx_enable_blind_parma) = properties_handler->readLSSDTSXEnableBlindparma();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read dtsx_enable_blind_parma %d\n", dtsx_enable_blind_parma);
            } else {
                g_print("\n--->Failed to read dtsx_enable_blind_parma %d\n", err_code);
            }

            int32_t dtsx_disable_blind_parma;
            std::tie(err_code, dtsx_disable_blind_parma) = properties_handler->readLSSDTSXDisableBlindparma();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read dtsx_disable_blind_parma %d\n", dtsx_disable_blind_parma);
            } else {
                g_print("\n--->Failed to read dtsx_disable_blind_parma %d\n", err_code);
            }
            int32_t update_dtsx_disable_blind_parma = (dtsx_disable_blind_parma == 1)? 0 : 1;
            err_code = properties_handler->updateLSSDTSXDisableBlindparma(update_dtsx_disable_blind_parma);
            if (err_code == ADK_SI_OK) {
                g_print("\n--->update dtsx_disable_blind_parma %d\n", update_dtsx_disable_blind_parma);
            } else {
                g_print("\n--->Failed to update dtsx_disable_blind_parma %d\n", err_code);
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
            std::tie(err_code, dtsx_disable_blind_parma) = properties_handler->readLSSDTSXDisableBlindparma();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read dtsx_disable_blind_parma %d\n", dtsx_disable_blind_parma);
            } else {
                g_print("\n--->Failed to read dtsx_disable_blind_parma %d\n", err_code);
            }

            int32_t dtsx_es_matrix;
            std::tie(err_code, dtsx_es_matrix) = properties_handler->readLSSDTSXEsMatrix();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read dtsx_es_matrix %d\n", dtsx_es_matrix);
            } else {
                g_print("\n--->Failed to read dtsx_es_matrix %d\n", err_code);
            }
            int32_t update_dtsx_es_matrix = (dtsx_es_matrix == 1)? 0 : 1;
            err_code = properties_handler->updateLSSDTSXEsMatrix(update_dtsx_es_matrix);
            if (err_code == ADK_SI_OK) {
                g_print("\n--->update dtsx_es_matrix %d\n", update_dtsx_es_matrix);
            } else {
                g_print("\n--->Failed to update dtsx_es_matrix %d\n", err_code);
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
            std::tie(err_code, dtsx_es_matrix) = properties_handler->readLSSDTSXEsMatrix();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read dtsx_es_matrix %d\n", dtsx_es_matrix);
            } else {
                g_print("\n--->Failed to read dtsx_es_matrix %d\n", err_code);
            }

            int32_t dtsx_enable_analog_compensation;
            std::tie(err_code, dtsx_enable_analog_compensation) = properties_handler->readLSSDTSXEnableAnalogCompensation();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read dtsx_enable_analog_compensation %d\n", dtsx_enable_analog_compensation);
            } else {
                g_print("\n--->Failed to read dtsx_enable_analog_compensation %d\n", err_code);
            }
            int32_t update_dtsx_enable_analog_compensation = (dtsx_enable_analog_compensation == 1)? 0 : 1;
            err_code = properties_handler->updateLSSDTSXEnableAnalogCompensation(update_dtsx_enable_analog_compensation);
            if (err_code == ADK_SI_OK) {
                g_print("\n--->update dtsx_enable_analog_compensation %d\n", update_dtsx_enable_analog_compensation);
            } else {
                g_print("\n--->Failed to update dtsx_enable_analog_compensation %d\n", err_code);
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
            std::tie(err_code, dtsx_enable_analog_compensation) = properties_handler->readLSSDTSXEnableAnalogCompensation();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read dtsx_enable_analog_compensation %d\n", dtsx_enable_analog_compensation);
            } else {
                g_print("\n--->Failed to read dtsx_enable_analog_compensation %d\n", err_code);
            }

            int32_t dtsx_set_analog_comp_max_user_gain;
            std::tie(err_code, dtsx_set_analog_comp_max_user_gain) = properties_handler->readLSSDTSXSetAnalogcompmaxUsergain();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read dtsx_set_analog_comp_max_user_gain %d\n", dtsx_set_analog_comp_max_user_gain);
            } else {
                g_print("\n--->Failed to read dtsx_set_analog_comp_max_user_gain %d\n", err_code);
            }
            int32_t update_dtsx_set_analog_comp_max_user_gain = 2;
            err_code = properties_handler->updateLSSDTSXSetAnalogcompmaxUsergain(update_dtsx_set_analog_comp_max_user_gain);
            if (err_code == ADK_SI_OK) {
                g_print("\n--->update dtsx_set_analog_comp_max_user_gain %d\n", update_dtsx_set_analog_comp_max_user_gain);
            } else {
                g_print("\n--->Failed to update dtsx_set_analog_comp_max_user_gain %d\n", err_code);
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
            std::tie(err_code, dtsx_set_analog_comp_max_user_gain) = properties_handler->readLSSDTSXSetAnalogcompmaxUsergain();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read dtsx_set_analog_comp_max_user_gain %d\n", dtsx_set_analog_comp_max_user_gain);
            } else {
                g_print("\n--->Failed to read dtsx_set_analog_comp_max_user_gain %d\n", err_code);
            }

            if (decoder_restart) {
                /* check for decoder restart */
                std::string decoder_name = "DTS";
                err_code = methods_handler->lSSDecodeRestart(decoder_name);
                if (err_code == ADK_SI_OK) {
                   g_print("lSSDecodeRestart method call success for %s\n", decoder_name.c_str());
                } else {
                   g_printerr("lSSDecodeRestart method call for %s failed : %d\n", decoder_name.c_str(), err_code);
                }
                std::this_thread::sleep_for(std::chrono::milliseconds(5000));
            }
        } else if ((detected_format == "MAT CH") || (detected_format == "MAT ATMOS")) {
            uint32_t dec_sample_freq;
            std::tie(err_code, dec_sample_freq) = properties_handler->readLSSDecSamplFreq();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read dec_sample_freq %d\n", dec_sample_freq);
            } else {
                g_print("\n--->Failed to read dec_sample_freq %d\n", err_code);
            }

            int32_t mat_stream_type;
            std::tie(err_code, mat_stream_type) = properties_handler->readLSSMATStreamtype();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read mat_stream_type %d\n", mat_stream_type);
            } else {
                g_print("\n--->Failed to read mat_stream_type %d\n", err_code);
            }

            std::vector<int32_t> mat_drc_mode;
            std::tie(err_code, mat_drc_mode) = properties_handler->readLSSMATDrcmode();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read mat_drc_mode\n");
                for (auto &it : mat_drc_mode) {
                    g_print(" %d \n", it);
                }
            } else {
                g_print("\n--->Failed to read mat_drc_mode %d\n", err_code);
            }
            std::vector<int32_t> valued{1,2,3};
            err_code = properties_handler->updateLSSMATDrcmode(valued);
            if (err_code == ADK_SI_OK) {
                g_print("\n--->updated mat_drc_mode\n");
            } else {
                g_print("\n--->Failed to update mat_drc_mode %d\n", err_code);
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
            std::tie(err_code, mat_drc_mode) = properties_handler->readLSSMATDrcmode();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read mat_drc_mode\n");
                for (auto &it : mat_drc_mode) {
                    g_print(" %d \n", it);
                }
            } else {
                g_print("\n--->Failed to read mat_drc_mode %d\n", err_code);
            }
        } else if ((detected_format == "THD CH") || (detected_format == "THD ATMOS")) {
            uint32_t dec_sample_freq;
            std::tie(err_code, dec_sample_freq) = properties_handler->readLSSDecSamplFreq();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read dec_sample_freq %d\n", dec_sample_freq);
            } else {
                g_print("\n--->Failed to read dec_sample_freq %d\n", err_code);
            }

            std::string thd_stream_type;
            std::tie(err_code, thd_stream_type) = properties_handler->readLSSTHDStreamtype();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read thd_stream_type %s\n", thd_stream_type.c_str());
            } else {
                g_print("\n--->Failed to read thd_stream_type %d\n", err_code);
            }

            int32_t thd_gain_required;
            std::tie(err_code, thd_gain_required) = properties_handler->readLSSTHDGainRequired();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read thd_gain_required %d\n", thd_gain_required);
            } else {
                g_print("\n--->Failed to read thd_gain_required %d\n", err_code);
            }

            uint32_t thd_fbb_ch;
            std::tie(err_code, thd_fbb_ch) = properties_handler->readLSSTHDFbbch();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read thd_fbb_ch %d\n", thd_fbb_ch);
            } else {
                g_print("\n--->Failed to read thd_fbb_ch %d\n", err_code);
            }

            uint32_t thd_fba_ch;
            std::tie(err_code, thd_fba_ch) = properties_handler->readLSSTHDFbach();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read thd_fba_ch %d\n", thd_fba_ch);
            } else {
                g_print("\n--->Failed to read thd_fba_ch %d\n", err_code);
            }

            int32_t thd_fba_surround_ch;
            std::tie(err_code, thd_fba_surround_ch) = properties_handler->readLSSTHDFbaSurroundCh();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read thd_fba_surround_ch %d\n", thd_fba_surround_ch);
            } else {
                g_print("\n--->Failed to read thd_fba_surround_ch %d\n", err_code);
            }

            int32_t thd_fba_diag_norm;
            std::tie(err_code, thd_fba_diag_norm) = properties_handler->readLSSTHDFbaDialogueNormalization();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read thd_fba_diag_norm %d\n", thd_fba_diag_norm);
            } else {
                g_print("\n--->Failed to read thd_fba_diag_norm %d\n", err_code);
            }

            uint32_t thd_true_hdsync;
            std::tie(err_code, thd_true_hdsync) = properties_handler->readLSSTHDTrueHdSync();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read thd_true_hdsync %d\n", thd_true_hdsync);
            } else {
                g_print("\n--->Failed to read thd_true_hdsync %d\n", err_code);
            }

            std::vector<int32_t> thd_drc_mode;
            std::tie(err_code, thd_drc_mode) = properties_handler->readLSSTHDDrcmode();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read thd_drc_mode\n");
                for (auto &it : thd_drc_mode) {
                    g_print(" %d \n", it);
                }
            } else {
                g_print("\n--->Failed to read thd_drc_mode %d\n", err_code);
            }
            std::vector<int32_t> valued{1,2,3};
            err_code = properties_handler->updateLSSTHDDrcmode(valued);
            if (err_code == ADK_SI_OK) {
                g_print("\n--->updated thd_drc_mode\n");
            } else {
                g_print("\n--->Failed to update thd_drc_mode %d\n", err_code);
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
            std::tie(err_code, thd_drc_mode) = properties_handler->readLSSTHDDrcmode();
            if (err_code == ADK_SI_OK) {
                g_print("\n--->read newer thd_drc_mode\n");
                for (auto &it : thd_drc_mode) {
                    g_print(" %d \n", it);
                }
            } else {
                g_print("\n--->Failed to read newer thd_drc_mode %d\n", err_code);
            }
        } else {
            g_print("\n--->Dectected formant %s is not handled\n", detected_format.c_str());
        }
    });

    g_print("registering listener for DualMonoAAC signal\n");
    signals_handler->setLSSDualMonoAACListener(
        [&properties_handler, &methods_handler](
        const std::string &obj_path) -> void {
        g_print("\n--->received DualMonoAAC signal from %s\n",
                obj_path.c_str());
        ErrorCode err_code;
        /* Read current channel mode */
        int32_t current_channelmode;
        std::tie(err_code, current_channelmode) = properties_handler->readLSSAacDualMonoChannelMode();
        if (err_code == ADK_SI_OK) {
            g_print("\n--->read Dual Mono channel mode as %d\n", current_channelmode);
        } else {
            g_print("\n--->Failed to read Dual Mono channel mode %d\n", err_code);
        }

        /* change to new channel mode */
        int32_t new_channelmode = 0;
        if (0 == current_channelmode) {
            new_channelmode = 1;
        } else if (1 == current_channelmode) {
            new_channelmode = 2;
        }
        err_code = properties_handler->updateLSSAacDualMonoChannelMode(new_channelmode);
        if (err_code == ADK_SI_OK) {
            g_print("\n--->Set Dual Mono channel mode to %d\n", new_channelmode);
        } else {
            g_print("\n--->Failed to set Dual Mono channel mode %d\n", err_code);
        }

        /* Read updated channel mode */
        int32_t updated_channelmode;
        std::tie(err_code, updated_channelmode) = properties_handler->readLSSAacDualMonoChannelMode();
        if (err_code == ADK_SI_OK) {
            g_print("\n--->read updated Dual Mono channel mode as %d\n", updated_channelmode);
        } else {
            g_print("\n--->Failed to read Dual Mono channel mode %d\n", err_code);
        }

        /* Check for test case validation */
        if (updated_channelmode == new_channelmode) {
            g_print("\n--->Successfully verified Dual mono signal, set & get\n");
        } else {
            g_print("\n--->Failed to verify Dual mono signal, set & get\n");
        }
    });

    g_print("registering listener for properties changed signal\n");
    properties_handler->setPropertiesChangedListener(
        [](PropertiesAttr prop_attr, const std::string &obj_path) -> void {
        std::unordered_set<std::string> changed_properties = prop_attr.getChangedProperties();
        /* Stream config interface related */
        if (changed_properties.find(std::string(adk::kLSSDecChConfig)) != changed_properties.end()) {
            int32_t lssdec_ch_config = prop_attr.getLSSDecChConfig();
            g_print("\n--->lssdec_ch_config changed to %d\n", lssdec_ch_config);
        }
        /* DTS or DTSX interface related */
        if (changed_properties.find(std::string(adk::kLSSDtsxDrcpercent)) != changed_properties.end()) {
            int32_t lssdtsx_drcpercent = prop_attr.getLSSDtsxDrcpercent();
            g_print("\n--->lssdtsx_drcpercent changed to %d\n", lssdtsx_drcpercent);
        }
        if (changed_properties.find(std::string(adk::kLSSDTSXSpkrout)) != changed_properties.end()) {
            int32_t lssdtsxspkrout = prop_attr.getLSSDTSXSpkrout();
            g_print("\n--->lssdtsxspkrout changed to %d\n", lssdtsxspkrout);
        }
        if (changed_properties.find(std::string(adk::kLSSDTSXSetDialogcontrol)) != changed_properties.end()) {
            int32_t lssdtsxset_dialogcontrol = prop_attr.getLSSDTSXSetDialogcontrol();
            g_print("\n--->lssdtsxset_dialogcontrol changed to %d\n", lssdtsxset_dialogcontrol);
        }
        if (changed_properties.find(std::string(adk::kLSSDTSXEnableDirectmode)) != changed_properties.end()) {
            int32_t lssdtsxenable_directmode = prop_attr.getLSSDTSXEnableDirectmode();
            g_print("\n--->lssdtsxenable_directmode changed to %d\n", lssdtsxenable_directmode);
        }
        if (changed_properties.find(std::string(adk::kLSSDTSXDisableDirectmode)) != changed_properties.end()) {
            int32_t lssdtsxdisable_directmode = prop_attr.getLSSDTSXDisableDirectmode();
            g_print("\n--->lssdtsxdisable_directmode changed to %d\n", lssdtsxdisable_directmode);
        }
        if (changed_properties.find(std::string(adk::kLSSDTSXEnableBlindparma)) != changed_properties.end()) {
            int32_t lssdtsxenable_blindparma = prop_attr.getLSSDTSXEnableBlindparma();
            g_print("\n--->lssdtsxenable_blindparma changed to %d\n", lssdtsxenable_blindparma);
        }
        if (changed_properties.find(std::string(adk::kLSSDTSXDisableBlindparma)) != changed_properties.end()) {
            int32_t lssdtsxdisable_blindparma = prop_attr.getLSSDTSXDisableBlindparma();
            g_print("\n--->lssdtsxdisable_blindparma changed to %d\n", lssdtsxdisable_blindparma);
        }
        if (changed_properties.find(std::string(adk::kLSSDTSXEsMatrix)) != changed_properties.end()) {
            int32_t lssdtsxes_matrix = prop_attr.getLSSDTSXEsMatrix();
            g_print("\n--->lssdtsxes_matrix changed to %d\n", lssdtsxes_matrix);
        }
        if (changed_properties.find(std::string(adk::kLSSDTSXEnableAnalogCompensation)) != changed_properties.end()) {
            int32_t lssdtsxenable_analog_compensation = prop_attr.getLSSDTSXEnableAnalogCompensation();
            g_print("\n--->lssdtsxenable_analog_compensation changed to %d\n", lssdtsxenable_analog_compensation);
        }
        if (changed_properties.find(std::string(adk::kLSSDTSXSetAnalogcompmaxUsergain)) != changed_properties.end()) {
            int32_t lssdtsxset_analogcompmax_usergain = prop_attr.getLSSDTSXSetAnalogcompmaxUsergain();
            g_print("\n--->lssdtsxset_analogcompmax_usergain changed to %d\n", lssdtsxset_analogcompmax_usergain);
        }
        /* DD or DDP interface related */
        if (changed_properties.find(std::string(adk::kLSSDDPDualMono)) != changed_properties.end()) {
            int32_t lssddpdual_mono = prop_attr.getLSSDDPDualMono();
            g_print("\n--->lssddpdual_mono changed to %d\n", lssddpdual_mono);
        }
        if (changed_properties.find(std::string(adk::kLSSDDPDrcmode)) != changed_properties.end()) {
            std::vector<int32_t> lssddpdrcmode = prop_attr.getLSSDDPDrcmode();
            g_print("\n--->lssddpdrcmode changed to\n");
            for (auto &it : lssddpdrcmode) {
                g_print(" %d \n", it);
            }
        }
        if (changed_properties.find(std::string(adk::kLSSDDPDecChConfig)) != changed_properties.end()) {
            int32_t lssddpdec_ch_config = prop_attr.getLSSDDPDecChConfig();
            g_print("\n--->lssddpdec_ch_config changed to %d\n", lssddpdec_ch_config);
        }
        /* MAT interface related */
        if (changed_properties.find(std::string(adk::kLSSMATDrcmode)) != changed_properties.end()) {
            std::vector<int32_t> lssmatdrcmode = prop_attr.getLSSMATDrcmode();
            g_print("\n--->lssmatdrcmode changed to\n");
            for (auto &it : lssmatdrcmode) {
                g_print(" %d \n", it);
            }
        }
        /* THD interface related */
        if (changed_properties.find(std::string(adk::kLSSTHDDrcmode)) != changed_properties.end()) {
            std::vector<int32_t> lssthddrcmode = prop_attr.getLSSTHDDrcmode();
            g_print("\n--->lssthddrcmode changed to\n");
            for (auto &it : lssthddrcmode) {
                g_print(" %d \n", it);
            }
        }
        /* AAC interface related */
        if (changed_properties.find(std::string(adk::kLSSAacDualMonoChannelMode)) != changed_properties.end()) {
            int32_t lssaac_dual_mono_channel_mode = prop_attr.getLSSAacDualMonoChannelMode();
            g_print("\n--->lssaac_dual_mono_channel_mode changed to %d\n", lssaac_dual_mono_channel_mode);
        }
        /* others, if any */
        if (changed_properties.find(std::string(adk::kCurrentSource)) != changed_properties.end()) {
            std::string current_source = prop_attr.getCurrentSource();
            g_print("\n--->Current source property changed %s\n", current_source.c_str());
        }
    });

    ErrorCode err_code;
    std::string current_source;
    std::tie(err_code, current_source) = properties_handler->readCurrentSource();
    if (err_code == ADK_SI_OK) {
        g_print("\n--->read current source %s\n", current_source.c_str());
    } else {
        g_print("\n--->Failed to read current source %d\n", err_code);
    }
    g_print("\n\n----------------- START ----------------\n\n");

    /* Run mainloop */
    siginfo_t sig_info;
    int sig;
    do {
        int tries = 0;
        do {
            sig = sigwaitinfo(&sig_set, &sig_info);
            if (sig < 0) {
                if (errno == EINTR) {
                    // spurious wakeup, ignore
                    continue;
                }
                g_print("Error on sigwaitinfo errno %d %s, retrying \n", errno,
                    strerror(errno));
                tries++;
                std::this_thread::sleep_for(std::chrono::seconds(1));
            } else {
                break;
            }
        } while (tries < 5);
        if (sig < 0) {
            g_print("Error waiting signal, exiting \n");
            return false;
        }
    } while ((sig != SIGTERM) && (sig != SIGINT) && (sig != SIGHUP));
    g_print("exit signal wait \n");

    return true;
}

int main() {
    return startAdkSystemInterface() ? 0 : 1;
}
