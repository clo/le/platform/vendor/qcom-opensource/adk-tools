/*
 * Copyright (c) 2019-2021, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of The Linux Foundation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef AUTOGEN_INTERNAL_API_DBUS_STRINGS_H_
#define AUTOGEN_INTERNAL_API_DBUS_STRINGS_H_

namespace adk {
namespace dbus {
// dbus service destinations
const char kFreedesktopDBusService[] = "org.freedesktop.DBus";
const char kAdkLivesourceserviceService[] = "com.qualcomm.qti.adk.livesourceservice";
const char kAdkAudiomanagerService[] = "com.qualcomm.qti.adk.audiomanager";

// dbus mandatory object paths
const char kFreedesktopDBusObjPath[] = "/org/freedesktop/DBus";
const char kFreedesktopDBusObjectManagerObjPath[] = "/";
const char kLivesourceserviceLssglobalplayernameObjPath[] = "/com/qualcomm/qti/adk/livesourceservice/LSSGlobalPlayerName";
const char kEffects109Ddea4D91C4969A2346Fbe2Ad0Ef3AObjPath[] = "/com/qualcomm/qti/adk/audiomanager/effects/109ddea4d91c4969a2346fbe2ad0ef3a";
const char kAudiomanagerDeviceControllerObjPath[] = "/com/qualcomm/qti/adk/audiomanager/device_controller";

// dbus interfaces
const char kFreedesktopDBusIface[] = "org.freedesktop.DBus";
const char kFreedesktopDBusPropertiesIface[] = "org.freedesktop.DBus.Properties";
const char kFreedesktopDBusObjectManagerIface[] = "org.freedesktop.DBus.ObjectManager";
const char kLivesourceserviceStreamIface[] = "com.qualcomm.qti.adk.livesourceservice.stream";
const char kLivesourceserviceDtsxIface[] = "com.qualcomm.qti.adk.livesourceservice.DTSX";
const char kLivesourceserviceDdpIface[] = "com.qualcomm.qti.adk.livesourceservice.DDP";
const char kLivesourceserviceMatIface[] = "com.qualcomm.qti.adk.livesourceservice.MAT";
const char kLivesourceserviceThdIface[] = "com.qualcomm.qti.adk.livesourceservice.THD";
const char kLivesourceserviceDsdIface[] = "com.qualcomm.qti.adk.livesourceservice.DSD";
const char kLivesourceserviceAacIface[] = "com.qualcomm.qti.adk.livesourceservice.AAC";
const char kLivesourceservicePcmIface[] = "com.qualcomm.qti.adk.livesourceservice.PCM";
const char kAudiomanagerDapIface[] = "com.qualcomm.qti.adk.audiomanager.dap";
const char kAudiomanagerDeviceControllerIface[] = "com.qualcomm.qti.adk.audiomanager.device_controller";

}  // namespace dbus
}  // namespace adk
#endif  // AUTOGEN_INTERNAL_API_DBUS_STRINGS_H_

