
/*
 * Copyright (c) 2019-2021, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of The Linux Foundation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef AUTOGEN_INTERNAL_API_INTERFACE_HANDLERS_H_
#define AUTOGEN_INTERNAL_API_INTERFACE_HANDLERS_H_

#include <gio/gio.h>
#include <glib.h>
#include <functional>
#include <memory>
#include <mutex>
#include <string>
#include <tuple>
#include <unordered_map>
#include <vector>

// #include "adk_ipc.h"
#include "attribute_classes.h"
#include "internal_api/dbus.h"

namespace adk {
class PropertiesHandler {
 public:
    explicit PropertiesHandler(Dbus *dbus);
    ~PropertiesHandler() = default;

            std::tuple<ErrorCode, uint32_t> readLSSDecSamplFreq(
            );

                    std::tuple<ErrorCode, int32_t> readLSSDecChConfig(
            );

        ErrorCode updateLSSDecChConfig(
        int32_t lssdec_ch_config);

                    std::tuple<ErrorCode, uint32_t> readLSSOutputsamplingfrequency(
            );

                    std::tuple<ErrorCode, std::string> readLSSReadDetectedformat(
            );

                    std::tuple<ErrorCode, int32_t> readLSSDTSBitrate(
            );

                    std::tuple<ErrorCode, int32_t> readLSSDTSEsMatrixFlag(
            );

                    std::tuple<ErrorCode, int32_t> readLSSDTSXchFlag(
            );

                    std::tuple<ErrorCode, int32_t> readLSSDTSX96Flag(
            );

                    std::tuple<ErrorCode, int32_t> readLSSDTSDialogueNormalization(
            );

                    std::tuple<ErrorCode, int32_t> readLSSDTSExtensionMask(
            );

                    std::tuple<ErrorCode, uint32_t> readLSSDTSProgChMask(
            );

                    std::tuple<ErrorCode, int32_t> readLSSDTSXllsamplerate(
            );

                    std::tuple<ErrorCode, int32_t> readLSSDTSEmbeddeddownmixselect(
            );

                    std::tuple<ErrorCode, uint32_t> readLSSDTSSamplerate(
            );

                    std::tuple<ErrorCode, int32_t> readLSSDTSStreamType(
            );

                    std::tuple<ErrorCode, int32_t> readLSSDTSDecodeInfo(
            );

                    std::tuple<ErrorCode, int32_t> readLSSDTSCoreBitRate(
            );

                    std::tuple<ErrorCode, int32_t> readLSSDTSObjectNum(
            );

                    std::tuple<ErrorCode, int32_t> readLSSDTSDialogueControlAvailability(
            );

                    std::tuple<ErrorCode, int32_t> readLSSDTSAnalogCompensationGain(
            );

                    std::tuple<ErrorCode, int32_t> readLSSDtsxDrcpercent(
            );

        ErrorCode updateLSSDtsxDrcpercent(
        int32_t lssdtsx_drcpercent);

                    std::tuple<ErrorCode, int32_t> readLSSDTSXSpkrout(
            );

        ErrorCode updateLSSDTSXSpkrout(
        int32_t lssdtsxspkrout);

                    std::tuple<ErrorCode, int32_t> readLSSDTSXSetDialogcontrol(
            );

        ErrorCode updateLSSDTSXSetDialogcontrol(
        int32_t lssdtsxset_dialogcontrol);

                    std::tuple<ErrorCode, int32_t> readLSSDTSXGetDialogcontrol(
            );

                    std::tuple<ErrorCode, int32_t> readLSSDTSXEnableDirectmode(
            );

        ErrorCode updateLSSDTSXEnableDirectmode(
        int32_t lssdtsxenable_directmode);

                    std::tuple<ErrorCode, int32_t> readLSSDTSXDisableDirectmode(
            );

        ErrorCode updateLSSDTSXDisableDirectmode(
        int32_t lssdtsxdisable_directmode);

                    std::tuple<ErrorCode, int32_t> readLSSDTSXEnableBlindparma(
            );

        ErrorCode updateLSSDTSXEnableBlindparma(
        int32_t lssdtsxenable_blindparma);

                    std::tuple<ErrorCode, int32_t> readLSSDTSXDisableBlindparma(
            );

        ErrorCode updateLSSDTSXDisableBlindparma(
        int32_t lssdtsxdisable_blindparma);

                    std::tuple<ErrorCode, int32_t> readLSSDTSXEsMatrix(
            );

        ErrorCode updateLSSDTSXEsMatrix(
        int32_t lssdtsxes_matrix);

                    std::tuple<ErrorCode, int32_t> readLSSDTSXEnableAnalogCompensation(
            );

        ErrorCode updateLSSDTSXEnableAnalogCompensation(
        int32_t lssdtsxenable_analog_compensation);

                    std::tuple<ErrorCode, int32_t> readLSSDTSXSetAnalogcompmaxUsergain(
            );

        ErrorCode updateLSSDTSXSetAnalogcompmaxUsergain(
        int32_t lssdtsxset_analogcompmax_usergain);

                    std::tuple<ErrorCode, std::string> readLSSDDPStreamType(
            );

                    std::tuple<ErrorCode, int32_t> readLSSDDPSurroundex(
            );

                    std::tuple<ErrorCode, int32_t> readLSSDDPDualMono(
            );

        ErrorCode updateLSSDDPDualMono(
        int32_t lssddpdual_mono);

                    std::tuple<ErrorCode, int32_t> readLSSDDPKaraoke(
            );

                    std::tuple<ErrorCode, int32_t> readLSSDDPCenterDownmixLevel(
            );

                    std::tuple<ErrorCode, int32_t> readLSSDDPSurroundDownmixLevel(
            );

                    std::tuple<ErrorCode, int32_t> readLSSDDPDialoguenormalization(
            );

                    std::tuple<ErrorCode, int32_t> readLSSDDPAudioCodingMode(
            );

                    std::tuple<ErrorCode, int32_t> readLSSDDPLfe(
            );

                    std::tuple<ErrorCode, uint32_t> readLSSDDPCustomChMap(
            );

                    std::tuple<ErrorCode, int32_t> readLSSDDPBitrate(
            );

                    std::tuple<ErrorCode, std::vector<int32_t>> readLSSDDPDrcmode(
            );

        ErrorCode updateLSSDDPDrcmode(
        std::vector<int32_t> lssddpdrcmode);

                    std::tuple<ErrorCode, int32_t> readLSSDDPDecChConfig(
            );

        ErrorCode updateLSSDDPDecChConfig(
        int32_t lssddpdec_ch_config);

                    std::tuple<ErrorCode, int32_t> readLSSMATStreamtype(
            );

                    std::tuple<ErrorCode, std::vector<int32_t>> readLSSMATDrcmode(
            );

        ErrorCode updateLSSMATDrcmode(
        std::vector<int32_t> lssmatdrcmode);

                    std::tuple<ErrorCode, std::string> readLSSTHDStreamtype(
            );

                    std::tuple<ErrorCode, int32_t> readLSSTHDGainRequired(
            );

                    std::tuple<ErrorCode, uint32_t> readLSSTHDFbbch(
            );

                    std::tuple<ErrorCode, uint32_t> readLSSTHDFbach(
            );

                    std::tuple<ErrorCode, int32_t> readLSSTHDFbaSurroundCh(
            );

                    std::tuple<ErrorCode, int32_t> readLSSTHDFbaDialogueNormalization(
            );

                    std::tuple<ErrorCode, uint32_t> readLSSTHDTrueHdSync(
            );

                    std::tuple<ErrorCode, std::vector<int32_t>> readLSSTHDDrcmode(
            );

        ErrorCode updateLSSTHDDrcmode(
        std::vector<int32_t> lssthddrcmode);

                    std::tuple<ErrorCode, int32_t> readLSSDSDChConfig(
            );

                    std::tuple<ErrorCode, uint32_t> readLSSDSDSamplingrate(
            );

                    std::tuple<ErrorCode, uint32_t> readLSSDSDType(
            );

                    std::tuple<ErrorCode, int32_t> readLSSAacDualMonoChannelMode(
            );

        ErrorCode updateLSSAacDualMonoChannelMode(
        int32_t lssaac_dual_mono_channel_mode);

                    std::tuple<ErrorCode, std::string> readLSSPCMChConfig(
            );

                    std::tuple<ErrorCode, uint32_t> readLSSPCMsamplingRate(
            );

                    std::tuple<ErrorCode, bool> readLSSDAPDlbsurroundmode(
            );

        ErrorCode updateLSSDAPDlbsurroundmode(
        bool lssdapdlbsurroundmode);

                    std::tuple<ErrorCode, bool> readLSSDlbsurroundcenterspread(
            );

        ErrorCode updateLSSDlbsurroundcenterspread(
        bool lssdlbsurroundcenterspread);

                    std::tuple<ErrorCode, std::string> readCurrentSource(
            );

            

    // obj_path and set of strings that suggest the changed properties.
    void setPropertiesChangedListener(
        const std::function<void(PropertiesAttr, const std::string &)> &listener) {
        properties_changed_listeners_.emplace_back(listener);
    }

    void setObjIdMapChangedListener(
        const std::function<void(const std::unordered_map<std::string, std::unordered_set<std::string>>)> &listener) {
        obj_id_map_changed_listeners_.emplace_back(listener);
    }

 private:
    void propertiesChangedCb(const std::string &obj_path,
        const std::string &iface_name, GVariantIter *iter);

        PropertiesAttr processLivesourceserviceStreamIfaceResponse(
        const std::string& obj_path, GVariantIter *iter);
        PropertiesAttr processLivesourceserviceDtsxIfaceResponse(
        const std::string& obj_path, GVariantIter *iter);
        PropertiesAttr processLivesourceserviceDdpIfaceResponse(
        const std::string& obj_path, GVariantIter *iter);
        PropertiesAttr processLivesourceserviceMatIfaceResponse(
        const std::string& obj_path, GVariantIter *iter);
        PropertiesAttr processLivesourceserviceThdIfaceResponse(
        const std::string& obj_path, GVariantIter *iter);
        PropertiesAttr processLivesourceserviceDsdIfaceResponse(
        const std::string& obj_path, GVariantIter *iter);
        PropertiesAttr processLivesourceserviceAacIfaceResponse(
        const std::string& obj_path, GVariantIter *iter);
        PropertiesAttr processLivesourceservicePcmIfaceResponse(
        const std::string& obj_path, GVariantIter *iter);
        PropertiesAttr processAudiomanagerDapIfaceResponse(
        const std::string& obj_path, GVariantIter *iter);
        PropertiesAttr processAudiomanagerDeviceControllerIfaceResponse(
        const std::string& obj_path, GVariantIter *iter);
    
 private:
    Dbus *dbus_;
    std::mutex mutex_;

    std::vector<std::function<void(PropertiesAttr, const std::string &)>> properties_changed_listeners_;
    std::vector<std::function<void(const std::unordered_map<std::string, std::unordered_set<std::string>>)>>
        obj_id_map_changed_listeners_;

    std::unordered_map<std::string, std::unordered_set<std::string>> obj_id_map_;
};

class MethodsHandler {
 public:
    explicit MethodsHandler(Dbus *dbus);
    ~MethodsHandler() = default;

    void setObjIdMapChangedListener(
        const std::function<void(const std::unordered_map<std::string, std::unordered_set<std::string>>)> &listener) {
        obj_id_map_changed_listeners_.emplace_back(listener);
    }
            ErrorCode
        lSSDecodeRestart(
                std::string in_arg
                );
            ErrorCode
        lSSDSDPath(
                std::string in_arg
                );
     private:
    void interfacesAddedCb(const std::string &obj_path,
        const std::string &iface_name, GVariantIter *iter);

 private:
    Dbus *dbus_;
    std::mutex mutex_;

    std::vector<std::function<void(const std::unordered_map<std::string, std::unordered_set<std::string>>)>>
        obj_id_map_changed_listeners_;

    std::unordered_map<std::string, std::unordered_set<std::string>> obj_id_map_;
};

class SignalsHandler {
 public:
    explicit SignalsHandler(Dbus *dbus);
    ~SignalsHandler() = default;

        void setLSSDetectedFormatListener(
        std::function<void(
                        const std::string &,
                        const std::string &
        )> listener) {
        lssdetected_format_listeners_.emplace_back(listener);
    }
        void setLSSDualMonoAACListener(
        std::function<void(
                        const std::string &
        )> listener) {
        lssdual_mono_aac_listeners_.emplace_back(listener);
    }
    
 private:

        void callLSSDetectedFormatListeners(const std::string& obj_path, GVariant *response);
        void callLSSDualMonoAACListeners(const std::string& obj_path, GVariant *response);
    
 private:
    Dbus *dbus_;
    std::mutex mutex_;

        std::vector<std::function<void(
                const std::string &,
                const std::string &)>> lssdetected_format_listeners_;
        std::vector<std::function<void(
                const std::string &)>> lssdual_mono_aac_listeners_;
    };
}  // namespace adk

#endif  // AUTOGEN_INTERNAL_API_INTERFACE_HANDLERS_H_
