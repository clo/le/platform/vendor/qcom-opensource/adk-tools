/*
 * Copyright (c) 2019-2021, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of The Linux Foundation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef AUTOGEN_ATTRIBUTE_CLASSES_H_
#define AUTOGEN_ATTRIBUTE_CLASSES_H_

#include <string>
#include <unordered_set>
#include <vector>
#include "attribute_strings.h"

namespace adk {

enum ErrorCode {
    ADK_SI_OK,
    ADK_SI_ERROR_FAILED,
    ADK_SI_ERROR_NO_SERVICE,
    ADK_SI_ERROR_NO_OBJECT,
    ADK_SI_ERROR_TIMEOUT
};


class PropertiesAttr {
 public:
    PropertiesAttr() = default;
    ~PropertiesAttr() = default;

        uint32_t getLSSDecSamplFreq() {
        return lssdec_sampl_freq_;
    }
    void setLSSDecSamplFreq(uint32_t lssdec_sampl_freq) {
        lssdec_sampl_freq_ = lssdec_sampl_freq;
        changed_properties_.emplace(kLSSDecSamplFreq);
    }
        int32_t getLSSDecChConfig() {
        return lssdec_ch_config_;
    }
    void setLSSDecChConfig(int32_t lssdec_ch_config) {
        lssdec_ch_config_ = lssdec_ch_config;
        changed_properties_.emplace(kLSSDecChConfig);
    }
        uint32_t getLSSOutputsamplingfrequency() {
        return lssoutputsamplingfrequency_;
    }
    void setLSSOutputsamplingfrequency(uint32_t lssoutputsamplingfrequency) {
        lssoutputsamplingfrequency_ = lssoutputsamplingfrequency;
        changed_properties_.emplace(kLSSOutputsamplingfrequency);
    }
        std::string getLSSReadDetectedformat() {
        return lssread_detectedformat_;
    }
    void setLSSReadDetectedformat(std::string lssread_detectedformat) {
        lssread_detectedformat_ = lssread_detectedformat;
        changed_properties_.emplace(kLSSReadDetectedformat);
    }
        int32_t getLSSDTSBitrate() {
        return lssdtsbitrate_;
    }
    void setLSSDTSBitrate(int32_t lssdtsbitrate) {
        lssdtsbitrate_ = lssdtsbitrate;
        changed_properties_.emplace(kLSSDTSBitrate);
    }
        int32_t getLSSDTSEsMatrixFlag() {
        return lssdtses_matrix_flag_;
    }
    void setLSSDTSEsMatrixFlag(int32_t lssdtses_matrix_flag) {
        lssdtses_matrix_flag_ = lssdtses_matrix_flag;
        changed_properties_.emplace(kLSSDTSEsMatrixFlag);
    }
        int32_t getLSSDTSXchFlag() {
        return lssdtsxch_flag_;
    }
    void setLSSDTSXchFlag(int32_t lssdtsxch_flag) {
        lssdtsxch_flag_ = lssdtsxch_flag;
        changed_properties_.emplace(kLSSDTSXchFlag);
    }
        int32_t getLSSDTSX96Flag() {
        return lssdtsx96_flag_;
    }
    void setLSSDTSX96Flag(int32_t lssdtsx96_flag) {
        lssdtsx96_flag_ = lssdtsx96_flag;
        changed_properties_.emplace(kLSSDTSX96Flag);
    }
        int32_t getLSSDTSDialogueNormalization() {
        return lssdtsdialogue_normalization_;
    }
    void setLSSDTSDialogueNormalization(int32_t lssdtsdialogue_normalization) {
        lssdtsdialogue_normalization_ = lssdtsdialogue_normalization;
        changed_properties_.emplace(kLSSDTSDialogueNormalization);
    }
        int32_t getLSSDTSExtensionMask() {
        return lssdtsextension_mask_;
    }
    void setLSSDTSExtensionMask(int32_t lssdtsextension_mask) {
        lssdtsextension_mask_ = lssdtsextension_mask;
        changed_properties_.emplace(kLSSDTSExtensionMask);
    }
        uint32_t getLSSDTSProgChMask() {
        return lssdtsprog_ch_mask_;
    }
    void setLSSDTSProgChMask(uint32_t lssdtsprog_ch_mask) {
        lssdtsprog_ch_mask_ = lssdtsprog_ch_mask;
        changed_properties_.emplace(kLSSDTSProgChMask);
    }
        int32_t getLSSDTSXllsamplerate() {
        return lssdtsxllsamplerate_;
    }
    void setLSSDTSXllsamplerate(int32_t lssdtsxllsamplerate) {
        lssdtsxllsamplerate_ = lssdtsxllsamplerate;
        changed_properties_.emplace(kLSSDTSXllsamplerate);
    }
        int32_t getLSSDTSEmbeddeddownmixselect() {
        return lssdtsembeddeddownmixselect_;
    }
    void setLSSDTSEmbeddeddownmixselect(int32_t lssdtsembeddeddownmixselect) {
        lssdtsembeddeddownmixselect_ = lssdtsembeddeddownmixselect;
        changed_properties_.emplace(kLSSDTSEmbeddeddownmixselect);
    }
        uint32_t getLSSDTSSamplerate() {
        return lssdtssamplerate_;
    }
    void setLSSDTSSamplerate(uint32_t lssdtssamplerate) {
        lssdtssamplerate_ = lssdtssamplerate;
        changed_properties_.emplace(kLSSDTSSamplerate);
    }
        int32_t getLSSDTSStreamType() {
        return lssdtsstream_type_;
    }
    void setLSSDTSStreamType(int32_t lssdtsstream_type) {
        lssdtsstream_type_ = lssdtsstream_type;
        changed_properties_.emplace(kLSSDTSStreamType);
    }
        int32_t getLSSDTSDecodeInfo() {
        return lssdtsdecode_info_;
    }
    void setLSSDTSDecodeInfo(int32_t lssdtsdecode_info) {
        lssdtsdecode_info_ = lssdtsdecode_info;
        changed_properties_.emplace(kLSSDTSDecodeInfo);
    }
        int32_t getLSSDTSCoreBitRate() {
        return lssdtscore_bit_rate_;
    }
    void setLSSDTSCoreBitRate(int32_t lssdtscore_bit_rate) {
        lssdtscore_bit_rate_ = lssdtscore_bit_rate;
        changed_properties_.emplace(kLSSDTSCoreBitRate);
    }
        int32_t getLSSDTSObjectNum() {
        return lssdtsobject_num_;
    }
    void setLSSDTSObjectNum(int32_t lssdtsobject_num) {
        lssdtsobject_num_ = lssdtsobject_num;
        changed_properties_.emplace(kLSSDTSObjectNum);
    }
        int32_t getLSSDTSDialogueControlAvailability() {
        return lssdtsdialogue_control_availability_;
    }
    void setLSSDTSDialogueControlAvailability(int32_t lssdtsdialogue_control_availability) {
        lssdtsdialogue_control_availability_ = lssdtsdialogue_control_availability;
        changed_properties_.emplace(kLSSDTSDialogueControlAvailability);
    }
        int32_t getLSSDTSAnalogCompensationGain() {
        return lssdtsanalog_compensation_gain_;
    }
    void setLSSDTSAnalogCompensationGain(int32_t lssdtsanalog_compensation_gain) {
        lssdtsanalog_compensation_gain_ = lssdtsanalog_compensation_gain;
        changed_properties_.emplace(kLSSDTSAnalogCompensationGain);
    }
        int32_t getLSSDtsxDrcpercent() {
        return lssdtsx_drcpercent_;
    }
    void setLSSDtsxDrcpercent(int32_t lssdtsx_drcpercent) {
        lssdtsx_drcpercent_ = lssdtsx_drcpercent;
        changed_properties_.emplace(kLSSDtsxDrcpercent);
    }
        int32_t getLSSDTSXSpkrout() {
        return lssdtsxspkrout_;
    }
    void setLSSDTSXSpkrout(int32_t lssdtsxspkrout) {
        lssdtsxspkrout_ = lssdtsxspkrout;
        changed_properties_.emplace(kLSSDTSXSpkrout);
    }
        int32_t getLSSDTSXSetDialogcontrol() {
        return lssdtsxset_dialogcontrol_;
    }
    void setLSSDTSXSetDialogcontrol(int32_t lssdtsxset_dialogcontrol) {
        lssdtsxset_dialogcontrol_ = lssdtsxset_dialogcontrol;
        changed_properties_.emplace(kLSSDTSXSetDialogcontrol);
    }
        int32_t getLSSDTSXGetDialogcontrol() {
        return lssdtsxget_dialogcontrol_;
    }
    void setLSSDTSXGetDialogcontrol(int32_t lssdtsxget_dialogcontrol) {
        lssdtsxget_dialogcontrol_ = lssdtsxget_dialogcontrol;
        changed_properties_.emplace(kLSSDTSXGetDialogcontrol);
    }
        int32_t getLSSDTSXEnableDirectmode() {
        return lssdtsxenable_directmode_;
    }
    void setLSSDTSXEnableDirectmode(int32_t lssdtsxenable_directmode) {
        lssdtsxenable_directmode_ = lssdtsxenable_directmode;
        changed_properties_.emplace(kLSSDTSXEnableDirectmode);
    }
        int32_t getLSSDTSXDisableDirectmode() {
        return lssdtsxdisable_directmode_;
    }
    void setLSSDTSXDisableDirectmode(int32_t lssdtsxdisable_directmode) {
        lssdtsxdisable_directmode_ = lssdtsxdisable_directmode;
        changed_properties_.emplace(kLSSDTSXDisableDirectmode);
    }
        int32_t getLSSDTSXEnableBlindparma() {
        return lssdtsxenable_blindparma_;
    }
    void setLSSDTSXEnableBlindparma(int32_t lssdtsxenable_blindparma) {
        lssdtsxenable_blindparma_ = lssdtsxenable_blindparma;
        changed_properties_.emplace(kLSSDTSXEnableBlindparma);
    }
        int32_t getLSSDTSXDisableBlindparma() {
        return lssdtsxdisable_blindparma_;
    }
    void setLSSDTSXDisableBlindparma(int32_t lssdtsxdisable_blindparma) {
        lssdtsxdisable_blindparma_ = lssdtsxdisable_blindparma;
        changed_properties_.emplace(kLSSDTSXDisableBlindparma);
    }
        int32_t getLSSDTSXEsMatrix() {
        return lssdtsxes_matrix_;
    }
    void setLSSDTSXEsMatrix(int32_t lssdtsxes_matrix) {
        lssdtsxes_matrix_ = lssdtsxes_matrix;
        changed_properties_.emplace(kLSSDTSXEsMatrix);
    }
        int32_t getLSSDTSXEnableAnalogCompensation() {
        return lssdtsxenable_analog_compensation_;
    }
    void setLSSDTSXEnableAnalogCompensation(int32_t lssdtsxenable_analog_compensation) {
        lssdtsxenable_analog_compensation_ = lssdtsxenable_analog_compensation;
        changed_properties_.emplace(kLSSDTSXEnableAnalogCompensation);
    }
        int32_t getLSSDTSXSetAnalogcompmaxUsergain() {
        return lssdtsxset_analogcompmax_usergain_;
    }
    void setLSSDTSXSetAnalogcompmaxUsergain(int32_t lssdtsxset_analogcompmax_usergain) {
        lssdtsxset_analogcompmax_usergain_ = lssdtsxset_analogcompmax_usergain;
        changed_properties_.emplace(kLSSDTSXSetAnalogcompmaxUsergain);
    }
        std::string getLSSDDPStreamType() {
        return lssddpstream_type_;
    }
    void setLSSDDPStreamType(std::string lssddpstream_type) {
        lssddpstream_type_ = lssddpstream_type;
        changed_properties_.emplace(kLSSDDPStreamType);
    }
        int32_t getLSSDDPSurroundex() {
        return lssddpsurroundex_;
    }
    void setLSSDDPSurroundex(int32_t lssddpsurroundex) {
        lssddpsurroundex_ = lssddpsurroundex;
        changed_properties_.emplace(kLSSDDPSurroundex);
    }
        int32_t getLSSDDPDualMono() {
        return lssddpdual_mono_;
    }
    void setLSSDDPDualMono(int32_t lssddpdual_mono) {
        lssddpdual_mono_ = lssddpdual_mono;
        changed_properties_.emplace(kLSSDDPDualMono);
    }
        int32_t getLSSDDPKaraoke() {
        return lssddpkaraoke_;
    }
    void setLSSDDPKaraoke(int32_t lssddpkaraoke) {
        lssddpkaraoke_ = lssddpkaraoke;
        changed_properties_.emplace(kLSSDDPKaraoke);
    }
        int32_t getLSSDDPCenterDownmixLevel() {
        return lssddpcenter_downmix_level_;
    }
    void setLSSDDPCenterDownmixLevel(int32_t lssddpcenter_downmix_level) {
        lssddpcenter_downmix_level_ = lssddpcenter_downmix_level;
        changed_properties_.emplace(kLSSDDPCenterDownmixLevel);
    }
        int32_t getLSSDDPSurroundDownmixLevel() {
        return lssddpsurround_downmix_level_;
    }
    void setLSSDDPSurroundDownmixLevel(int32_t lssddpsurround_downmix_level) {
        lssddpsurround_downmix_level_ = lssddpsurround_downmix_level;
        changed_properties_.emplace(kLSSDDPSurroundDownmixLevel);
    }
        int32_t getLSSDDPDialoguenormalization() {
        return lssddpdialoguenormalization_;
    }
    void setLSSDDPDialoguenormalization(int32_t lssddpdialoguenormalization) {
        lssddpdialoguenormalization_ = lssddpdialoguenormalization;
        changed_properties_.emplace(kLSSDDPDialoguenormalization);
    }
        int32_t getLSSDDPAudioCodingMode() {
        return lssddpaudio_coding_mode_;
    }
    void setLSSDDPAudioCodingMode(int32_t lssddpaudio_coding_mode) {
        lssddpaudio_coding_mode_ = lssddpaudio_coding_mode;
        changed_properties_.emplace(kLSSDDPAudioCodingMode);
    }
        int32_t getLSSDDPLfe() {
        return lssddplfe_;
    }
    void setLSSDDPLfe(int32_t lssddplfe) {
        lssddplfe_ = lssddplfe;
        changed_properties_.emplace(kLSSDDPLfe);
    }
        uint32_t getLSSDDPCustomChMap() {
        return lssddpcustom_ch_map_;
    }
    void setLSSDDPCustomChMap(uint32_t lssddpcustom_ch_map) {
        lssddpcustom_ch_map_ = lssddpcustom_ch_map;
        changed_properties_.emplace(kLSSDDPCustomChMap);
    }
        int32_t getLSSDDPBitrate() {
        return lssddpbitrate_;
    }
    void setLSSDDPBitrate(int32_t lssddpbitrate) {
        lssddpbitrate_ = lssddpbitrate;
        changed_properties_.emplace(kLSSDDPBitrate);
    }
        std::vector<int32_t> getLSSDDPDrcmode() {
        return lssddpdrcmode_;
    }
    void setLSSDDPDrcmode(std::vector<int32_t> lssddpdrcmode) {
        lssddpdrcmode_ = lssddpdrcmode;
        changed_properties_.emplace(kLSSDDPDrcmode);
    }
        int32_t getLSSDDPDecChConfig() {
        return lssddpdec_ch_config_;
    }
    void setLSSDDPDecChConfig(int32_t lssddpdec_ch_config) {
        lssddpdec_ch_config_ = lssddpdec_ch_config;
        changed_properties_.emplace(kLSSDDPDecChConfig);
    }
        int32_t getLSSMATStreamtype() {
        return lssmatstreamtype_;
    }
    void setLSSMATStreamtype(int32_t lssmatstreamtype) {
        lssmatstreamtype_ = lssmatstreamtype;
        changed_properties_.emplace(kLSSMATStreamtype);
    }
        std::vector<int32_t> getLSSMATDrcmode() {
        return lssmatdrcmode_;
    }
    void setLSSMATDrcmode(std::vector<int32_t> lssmatdrcmode) {
        lssmatdrcmode_ = lssmatdrcmode;
        changed_properties_.emplace(kLSSMATDrcmode);
    }
        std::string getLSSTHDStreamtype() {
        return lssthdstreamtype_;
    }
    void setLSSTHDStreamtype(std::string lssthdstreamtype) {
        lssthdstreamtype_ = lssthdstreamtype;
        changed_properties_.emplace(kLSSTHDStreamtype);
    }
        int32_t getLSSTHDGainRequired() {
        return lssthdgain_required_;
    }
    void setLSSTHDGainRequired(int32_t lssthdgain_required) {
        lssthdgain_required_ = lssthdgain_required;
        changed_properties_.emplace(kLSSTHDGainRequired);
    }
        uint32_t getLSSTHDFbbch() {
        return lssthdfbbch_;
    }
    void setLSSTHDFbbch(uint32_t lssthdfbbch) {
        lssthdfbbch_ = lssthdfbbch;
        changed_properties_.emplace(kLSSTHDFbbch);
    }
        uint32_t getLSSTHDFbach() {
        return lssthdfbach_;
    }
    void setLSSTHDFbach(uint32_t lssthdfbach) {
        lssthdfbach_ = lssthdfbach;
        changed_properties_.emplace(kLSSTHDFbach);
    }
        int32_t getLSSTHDFbaSurroundCh() {
        return lssthdfba_surround_ch_;
    }
    void setLSSTHDFbaSurroundCh(int32_t lssthdfba_surround_ch) {
        lssthdfba_surround_ch_ = lssthdfba_surround_ch;
        changed_properties_.emplace(kLSSTHDFbaSurroundCh);
    }
        int32_t getLSSTHDFbaDialogueNormalization() {
        return lssthdfba_dialogue_normalization_;
    }
    void setLSSTHDFbaDialogueNormalization(int32_t lssthdfba_dialogue_normalization) {
        lssthdfba_dialogue_normalization_ = lssthdfba_dialogue_normalization;
        changed_properties_.emplace(kLSSTHDFbaDialogueNormalization);
    }
        uint32_t getLSSTHDTrueHdSync() {
        return lssthdtrue_hd_sync_;
    }
    void setLSSTHDTrueHdSync(uint32_t lssthdtrue_hd_sync) {
        lssthdtrue_hd_sync_ = lssthdtrue_hd_sync;
        changed_properties_.emplace(kLSSTHDTrueHdSync);
    }
        std::vector<int32_t> getLSSTHDDrcmode() {
        return lssthddrcmode_;
    }
    void setLSSTHDDrcmode(std::vector<int32_t> lssthddrcmode) {
        lssthddrcmode_ = lssthddrcmode;
        changed_properties_.emplace(kLSSTHDDrcmode);
    }
        int32_t getLSSDSDChConfig() {
        return lssdsdch_config_;
    }
    void setLSSDSDChConfig(int32_t lssdsdch_config) {
        lssdsdch_config_ = lssdsdch_config;
        changed_properties_.emplace(kLSSDSDChConfig);
    }
        uint32_t getLSSDSDSamplingrate() {
        return lssdsdsamplingrate_;
    }
    void setLSSDSDSamplingrate(uint32_t lssdsdsamplingrate) {
        lssdsdsamplingrate_ = lssdsdsamplingrate;
        changed_properties_.emplace(kLSSDSDSamplingrate);
    }
        uint32_t getLSSDSDType() {
        return lssdsdtype_;
    }
    void setLSSDSDType(uint32_t lssdsdtype) {
        lssdsdtype_ = lssdsdtype;
        changed_properties_.emplace(kLSSDSDType);
    }
        int32_t getLSSAacDualMonoChannelMode() {
        return lssaac_dual_mono_channel_mode_;
    }
    void setLSSAacDualMonoChannelMode(int32_t lssaac_dual_mono_channel_mode) {
        lssaac_dual_mono_channel_mode_ = lssaac_dual_mono_channel_mode;
        changed_properties_.emplace(kLSSAacDualMonoChannelMode);
    }
        std::string getLSSPCMChConfig() {
        return lsspcmch_config_;
    }
    void setLSSPCMChConfig(std::string lsspcmch_config) {
        lsspcmch_config_ = lsspcmch_config;
        changed_properties_.emplace(kLSSPCMChConfig);
    }
        uint32_t getLSSPCMsamplingRate() {
        return lsspcmsampling_rate_;
    }
    void setLSSPCMsamplingRate(uint32_t lsspcmsampling_rate) {
        lsspcmsampling_rate_ = lsspcmsampling_rate;
        changed_properties_.emplace(kLSSPCMsamplingRate);
    }
        bool getLSSDAPDlbsurroundmode() {
        return lssdapdlbsurroundmode_;
    }
    void setLSSDAPDlbsurroundmode(bool lssdapdlbsurroundmode) {
        lssdapdlbsurroundmode_ = lssdapdlbsurroundmode;
        changed_properties_.emplace(kLSSDAPDlbsurroundmode);
    }
        bool getLSSDlbsurroundcenterspread() {
        return lssdlbsurroundcenterspread_;
    }
    void setLSSDlbsurroundcenterspread(bool lssdlbsurroundcenterspread) {
        lssdlbsurroundcenterspread_ = lssdlbsurroundcenterspread;
        changed_properties_.emplace(kLSSDlbsurroundcenterspread);
    }
        std::string getCurrentSource() {
        return current_source_;
    }
    void setCurrentSource(std::string current_source) {
        current_source_ = current_source;
        changed_properties_.emplace(kCurrentSource);
    }
    
    void clearChangedProperties() { changed_properties_.clear(); }
    std::unordered_set<std::string> getChangedProperties() { return changed_properties_; }

 private:
        uint32_t lssdec_sampl_freq_;
        int32_t lssdec_ch_config_;
        uint32_t lssoutputsamplingfrequency_;
        std::string lssread_detectedformat_;
        int32_t lssdtsbitrate_;
        int32_t lssdtses_matrix_flag_;
        int32_t lssdtsxch_flag_;
        int32_t lssdtsx96_flag_;
        int32_t lssdtsdialogue_normalization_;
        int32_t lssdtsextension_mask_;
        uint32_t lssdtsprog_ch_mask_;
        int32_t lssdtsxllsamplerate_;
        int32_t lssdtsembeddeddownmixselect_;
        uint32_t lssdtssamplerate_;
        int32_t lssdtsstream_type_;
        int32_t lssdtsdecode_info_;
        int32_t lssdtscore_bit_rate_;
        int32_t lssdtsobject_num_;
        int32_t lssdtsdialogue_control_availability_;
        int32_t lssdtsanalog_compensation_gain_;
        int32_t lssdtsx_drcpercent_;
        int32_t lssdtsxspkrout_;
        int32_t lssdtsxset_dialogcontrol_;
        int32_t lssdtsxget_dialogcontrol_;
        int32_t lssdtsxenable_directmode_;
        int32_t lssdtsxdisable_directmode_;
        int32_t lssdtsxenable_blindparma_;
        int32_t lssdtsxdisable_blindparma_;
        int32_t lssdtsxes_matrix_;
        int32_t lssdtsxenable_analog_compensation_;
        int32_t lssdtsxset_analogcompmax_usergain_;
        std::string lssddpstream_type_;
        int32_t lssddpsurroundex_;
        int32_t lssddpdual_mono_;
        int32_t lssddpkaraoke_;
        int32_t lssddpcenter_downmix_level_;
        int32_t lssddpsurround_downmix_level_;
        int32_t lssddpdialoguenormalization_;
        int32_t lssddpaudio_coding_mode_;
        int32_t lssddplfe_;
        uint32_t lssddpcustom_ch_map_;
        int32_t lssddpbitrate_;
        std::vector<int32_t> lssddpdrcmode_;
        int32_t lssddpdec_ch_config_;
        int32_t lssmatstreamtype_;
        std::vector<int32_t> lssmatdrcmode_;
        std::string lssthdstreamtype_;
        int32_t lssthdgain_required_;
        uint32_t lssthdfbbch_;
        uint32_t lssthdfbach_;
        int32_t lssthdfba_surround_ch_;
        int32_t lssthdfba_dialogue_normalization_;
        uint32_t lssthdtrue_hd_sync_;
        std::vector<int32_t> lssthddrcmode_;
        int32_t lssdsdch_config_;
        uint32_t lssdsdsamplingrate_;
        uint32_t lssdsdtype_;
        int32_t lssaac_dual_mono_channel_mode_;
        std::string lsspcmch_config_;
        uint32_t lsspcmsampling_rate_;
        bool lssdapdlbsurroundmode_;
        bool lssdlbsurroundcenterspread_;
        std::string current_source_;
    
    std::unordered_set<std::string> changed_properties_;
};





}  // namespace adk
#endif  //  AUTOGEN_ATTRIBUTE_CLASSES_H_
