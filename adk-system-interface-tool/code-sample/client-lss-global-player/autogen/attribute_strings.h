/*
 * Copyright (c) 2019-2021, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of The Linux Foundation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef AUTOGEN_ATTRIBUTE_STRINGS_H_
#define AUTOGEN_ATTRIBUTE_STRINGS_H_

namespace adk {
const char kLSSDecSamplFreq[] = "lSSDecSamplFreq";
const char kLSSDecChConfig[] = "lSSDecChConfig";
const char kLSSOutputsamplingfrequency[] = "lSSOutputsamplingfrequency";
const char kLSSReadDetectedformat[] = "lSSReadDetectedformat";
const char kLSSDTSBitrate[] = "lSSDTSBitrate";
const char kLSSDTSEsMatrixFlag[] = "lSSDTSEsMatrixFlag";
const char kLSSDTSXchFlag[] = "lSSDTSXchFlag";
const char kLSSDTSX96Flag[] = "lSSDTSX96Flag";
const char kLSSDTSDialogueNormalization[] = "lSSDTSDialogueNormalization";
const char kLSSDTSExtensionMask[] = "lSSDTSExtensionMask";
const char kLSSDTSProgChMask[] = "lSSDTSProgChMask";
const char kLSSDTSXllsamplerate[] = "lSSDTSXllsamplerate";
const char kLSSDTSEmbeddeddownmixselect[] = "lSSDTSEmbeddeddownmixselect";
const char kLSSDTSSamplerate[] = "lSSDTSSamplerate";
const char kLSSDTSStreamType[] = "lSSDTSStreamType";
const char kLSSDTSDecodeInfo[] = "lSSDTSDecodeInfo";
const char kLSSDTSCoreBitRate[] = "lSSDTSCoreBitRate";
const char kLSSDTSObjectNum[] = "lSSDTSObjectNum";
const char kLSSDTSDialogueControlAvailability[] = "lSSDTSDialogueControlAvailability";
const char kLSSDTSAnalogCompensationGain[] = "lSSDTSAnalogCompensationGain";
const char kLSSDtsxDrcpercent[] = "lSSDtsxDrcpercent";
const char kLSSDTSXSpkrout[] = "lSSDTSXSpkrout";
const char kLSSDTSXSetDialogcontrol[] = "lSSDTSXSetDialogcontrol";
const char kLSSDTSXGetDialogcontrol[] = "lSSDTSXGetDialogcontrol";
const char kLSSDTSXEnableDirectmode[] = "lSSDTSXEnableDirectmode";
const char kLSSDTSXDisableDirectmode[] = "lSSDTSXDisableDirectmode";
const char kLSSDTSXEnableBlindparma[] = "lSSDTSXEnableBlindparma";
const char kLSSDTSXDisableBlindparma[] = "lSSDTSXDisableBlindparma";
const char kLSSDTSXEsMatrix[] = "lSSDTSXEsMatrix";
const char kLSSDTSXEnableAnalogCompensation[] = "lSSDTSXEnableAnalogCompensation";
const char kLSSDTSXSetAnalogcompmaxUsergain[] = "lSSDTSXSetAnalogcompmaxUsergain";
const char kLSSDDPStreamType[] = "lSSDDPStreamType";
const char kLSSDDPSurroundex[] = "lSSDDPSurroundex";
const char kLSSDDPDualMono[] = "lSSDDPDualMono";
const char kLSSDDPKaraoke[] = "lSSDDPKaraoke";
const char kLSSDDPCenterDownmixLevel[] = "lSSDDPCenterDownmixLevel";
const char kLSSDDPSurroundDownmixLevel[] = "lSSDDPSurroundDownmixLevel";
const char kLSSDDPDialoguenormalization[] = "lSSDDPDialoguenormalization";
const char kLSSDDPAudioCodingMode[] = "lSSDDPAudioCodingMode";
const char kLSSDDPLfe[] = "lSSDDPLfe";
const char kLSSDDPCustomChMap[] = "lSSDDPCustomChMap";
const char kLSSDDPBitrate[] = "lSSDDPBitrate";
const char kLSSDDPDrcmode[] = "lSSDDPDrcmode";
const char kLSSDDPDecChConfig[] = "lSSDDPDecChConfig";
const char kLSSMATStreamtype[] = "lSSMATStreamtype";
const char kLSSMATDrcmode[] = "lSSMATDrcmode";
const char kLSSTHDStreamtype[] = "lSSTHDStreamtype";
const char kLSSTHDGainRequired[] = "lSSTHDGainRequired";
const char kLSSTHDFbbch[] = "lSSTHDFbbch";
const char kLSSTHDFbach[] = "lSSTHDFbach";
const char kLSSTHDFbaSurroundCh[] = "lSSTHDFbaSurroundCh";
const char kLSSTHDFbaDialogueNormalization[] = "lSSTHDFbaDialogueNormalization";
const char kLSSTHDTrueHdSync[] = "lSSTHDTrueHdSync";
const char kLSSTHDDrcmode[] = "lSSTHDDrcmode";
const char kLSSDSDChConfig[] = "lSSDSDChConfig";
const char kLSSDSDSamplingrate[] = "lSSDSDSamplingrate";
const char kLSSDSDType[] = "lSSDSDType";
const char kLSSAacDualMonoChannelMode[] = "lSSAacDualMonoChannelMode";
const char kLSSPCMChConfig[] = "lSSPCMChConfig";
const char kLSSPCMsamplingRate[] = "lSSPCMsamplingRate";
const char kLSSDAPDlbsurroundmode[] = "lSSDAPDlbsurroundmode";
const char kLSSDlbsurroundcenterspread[] = "lSSDlbsurroundcenterspread";
const char kCurrentSource[] = "currentSource";
const char kLSSDecodeRestart[] = "lSSDecodeRestart";
const char kLSSDSDPath[] = "lSSDSDPath";
const char kLSSDetectedFormat[] = "lSSDetectedFormat";
const char kLSSDualMonoAAC[] = "lSSDualMonoAAC";
}  // namespace adk

#endif  // AUTOGEN_ATTRIBUTE_STRINGS_H_
