
/*
 * Copyright (c) 2019-2021, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of The Linux Foundation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "interface_handlers.h"

namespace adk {

PropertiesHandler::PropertiesHandler(Dbus *dbus) :
    dbus_(dbus) {
        dbus_->setPropertiesChangedListener(
        std::bind(&PropertiesHandler::propertiesChangedCb, this,
        std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
}

void PropertiesHandler::propertiesChangedCb(const std::string &obj_path,
    const std::string &iface_name, GVariantIter *iter) {
//    g_print("PropertiesHandler::propertiesChangedCb \n");
        if (iface_name == std::string(adk::dbus::kLivesourceserviceStreamIface)) {
        PropertiesAttr changed_properties = processLivesourceserviceStreamIfaceResponse(obj_path, iter);
        for (auto &f : properties_changed_listeners_) {
            f(changed_properties, obj_path);
        }
        return;
    }
        if (iface_name == std::string(adk::dbus::kLivesourceserviceDtsxIface)) {
        PropertiesAttr changed_properties = processLivesourceserviceDtsxIfaceResponse(obj_path, iter);
        for (auto &f : properties_changed_listeners_) {
            f(changed_properties, obj_path);
        }
        return;
    }
        if (iface_name == std::string(adk::dbus::kLivesourceserviceDdpIface)) {
        PropertiesAttr changed_properties = processLivesourceserviceDdpIfaceResponse(obj_path, iter);
        for (auto &f : properties_changed_listeners_) {
            f(changed_properties, obj_path);
        }
        return;
    }
        if (iface_name == std::string(adk::dbus::kLivesourceserviceMatIface)) {
        PropertiesAttr changed_properties = processLivesourceserviceMatIfaceResponse(obj_path, iter);
        for (auto &f : properties_changed_listeners_) {
            f(changed_properties, obj_path);
        }
        return;
    }
        if (iface_name == std::string(adk::dbus::kLivesourceserviceThdIface)) {
        PropertiesAttr changed_properties = processLivesourceserviceThdIfaceResponse(obj_path, iter);
        for (auto &f : properties_changed_listeners_) {
            f(changed_properties, obj_path);
        }
        return;
    }
        if (iface_name == std::string(adk::dbus::kLivesourceserviceDsdIface)) {
        PropertiesAttr changed_properties = processLivesourceserviceDsdIfaceResponse(obj_path, iter);
        for (auto &f : properties_changed_listeners_) {
            f(changed_properties, obj_path);
        }
        return;
    }
        if (iface_name == std::string(adk::dbus::kLivesourceserviceAacIface)) {
        PropertiesAttr changed_properties = processLivesourceserviceAacIfaceResponse(obj_path, iter);
        for (auto &f : properties_changed_listeners_) {
            f(changed_properties, obj_path);
        }
        return;
    }
        if (iface_name == std::string(adk::dbus::kLivesourceservicePcmIface)) {
        PropertiesAttr changed_properties = processLivesourceservicePcmIfaceResponse(obj_path, iter);
        for (auto &f : properties_changed_listeners_) {
            f(changed_properties, obj_path);
        }
        return;
    }
        if (iface_name == std::string(adk::dbus::kAudiomanagerDapIface)) {
        PropertiesAttr changed_properties = processAudiomanagerDapIfaceResponse(obj_path, iter);
        for (auto &f : properties_changed_listeners_) {
            f(changed_properties, obj_path);
        }
        return;
    }
        if (iface_name == std::string(adk::dbus::kAudiomanagerDeviceControllerIface)) {
        PropertiesAttr changed_properties = processAudiomanagerDeviceControllerIfaceResponse(obj_path, iter);
        for (auto &f : properties_changed_listeners_) {
            f(changed_properties, obj_path);
        }
        return;
    }
    }

PropertiesAttr PropertiesHandler::processLivesourceserviceStreamIfaceResponse(
    const std::string &obj_path, GVariantIter *iter) {
    g_print("PropertiesHandler::processKLivesourceserviceStreamIfaceResponse obj_path: %s \n", obj_path.c_str());
    char *key;
    GVariant *tmp = nullptr;
    PropertiesAttr changed_properties;
    bool obj_id_map_changed = false;

    std::lock_guard<std::mutex> lock(mutex_);
    changed_properties.clearChangedProperties();
    while (g_variant_iter_loop(iter, "{sv}", &key, &tmp)) {
                        if (std::string(key) == "DecSamplFreq") {
                                                            changed_properties.setLSSDecSamplFreq(g_variant_get_uint32(tmp));
                                                continue;
        }
                                if (std::string(key) == "DecChConfig") {
                                                            changed_properties.setLSSDecChConfig(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "OutputSamplingFrequency") {
                                                            changed_properties.setLSSOutputsamplingfrequency(g_variant_get_uint32(tmp));
                                                continue;
        }
                                if (std::string(key) == "DetectedFormat") {
                                                            gsize length;
            changed_properties.setLSSReadDetectedformat(static_cast<std::string>(g_variant_get_string(tmp, &length)));
                                                continue;
        }
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    }

    if (tmp) {
        g_variant_unref(tmp);
    }
    if (obj_id_map_changed) {
        for (auto &f : obj_id_map_changed_listeners_) {
            f(obj_id_map_);
        }
    }
    return changed_properties;
}
PropertiesAttr PropertiesHandler::processLivesourceserviceDtsxIfaceResponse(
    const std::string &obj_path, GVariantIter *iter) {
    g_print("PropertiesHandler::processKLivesourceserviceDtsxIfaceResponse obj_path: %s \n", obj_path.c_str());
    char *key;
    GVariant *tmp = nullptr;
    PropertiesAttr changed_properties;
    bool obj_id_map_changed = false;

    std::lock_guard<std::mutex> lock(mutex_);
    changed_properties.clearChangedProperties();
    while (g_variant_iter_loop(iter, "{sv}", &key, &tmp)) {
                                                                                        if (std::string(key) == "BitRate") {
                                                            changed_properties.setLSSDTSBitrate(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "EsMatrixFlag") {
                                                            changed_properties.setLSSDTSEsMatrixFlag(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "XchFlag") {
                                                            changed_properties.setLSSDTSXchFlag(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "X96Flag") {
                                                            changed_properties.setLSSDTSX96Flag(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "DialogueNormalization") {
                                                            changed_properties.setLSSDTSDialogueNormalization(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "ExtensionMask") {
                                                            changed_properties.setLSSDTSExtensionMask(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "ProgChMask") {
                                                            changed_properties.setLSSDTSProgChMask(g_variant_get_uint32(tmp));
                                                continue;
        }
                                if (std::string(key) == "XllSampleRate") {
                                                            changed_properties.setLSSDTSXllsamplerate(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "EmbeddedDownMixSelect") {
                                                            changed_properties.setLSSDTSEmbeddeddownmixselect(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "SampleRate") {
                                                            changed_properties.setLSSDTSSamplerate(g_variant_get_uint32(tmp));
                                                continue;
        }
                                if (std::string(key) == "StreamType") {
                                                            changed_properties.setLSSDTSStreamType(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "DecodeInfo") {
                                                            changed_properties.setLSSDTSDecodeInfo(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "CoreBitRate") {
                                                            changed_properties.setLSSDTSCoreBitRate(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "ObjectNum") {
                                                            changed_properties.setLSSDTSObjectNum(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "DialogueControlAvailability") {
                                                            changed_properties.setLSSDTSDialogueControlAvailability(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "AnalogCompensationGain") {
                                                            changed_properties.setLSSDTSAnalogCompensationGain(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "DrcPercent") {
                                                            changed_properties.setLSSDtsxDrcpercent(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "spkrout") {
                                                            changed_properties.setLSSDTSXSpkrout(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "set_dialogcontrol") {
                                                            changed_properties.setLSSDTSXSetDialogcontrol(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "get_dialogcontrol") {
                                                            changed_properties.setLSSDTSXGetDialogcontrol(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "enable_directmode") {
                                                            changed_properties.setLSSDTSXEnableDirectmode(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "disable_directmode") {
                                                            changed_properties.setLSSDTSXDisableDirectmode(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "enable_blindparma") {
                                                            changed_properties.setLSSDTSXEnableBlindparma(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "disable_blindparma") {
                                                            changed_properties.setLSSDTSXDisableBlindparma(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "es_matrix") {
                                                            changed_properties.setLSSDTSXEsMatrix(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "enable_analog_compensation") {
                                                            changed_properties.setLSSDTSXEnableAnalogCompensation(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "set_analogcompmax_usergain") {
                                                            changed_properties.setLSSDTSXSetAnalogcompmaxUsergain(g_variant_get_int32(tmp));
                                                continue;
        }
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    }

    if (tmp) {
        g_variant_unref(tmp);
    }
    if (obj_id_map_changed) {
        for (auto &f : obj_id_map_changed_listeners_) {
            f(obj_id_map_);
        }
    }
    return changed_properties;
}
PropertiesAttr PropertiesHandler::processLivesourceserviceDdpIfaceResponse(
    const std::string &obj_path, GVariantIter *iter) {
    g_print("PropertiesHandler::processKLivesourceserviceDdpIfaceResponse obj_path: %s \n", obj_path.c_str());
    char *key;
    GVariant *tmp = nullptr;
    PropertiesAttr changed_properties;
    bool obj_id_map_changed = false;

    std::lock_guard<std::mutex> lock(mutex_);
    changed_properties.clearChangedProperties();
    while (g_variant_iter_loop(iter, "{sv}", &key, &tmp)) {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        if (std::string(key) == "StreamType") {
                                                            gsize length;
            changed_properties.setLSSDDPStreamType(static_cast<std::string>(g_variant_get_string(tmp, &length)));
                                                continue;
        }
                                if (std::string(key) == "SurroundEx") {
                                                            changed_properties.setLSSDDPSurroundex(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "DualMono") {
                                                            changed_properties.setLSSDDPDualMono(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "Karaoke") {
                                                            changed_properties.setLSSDDPKaraoke(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "CenterDownmixLevel") {
                                                            changed_properties.setLSSDDPCenterDownmixLevel(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "SurroundDownmixLevel") {
                                                            changed_properties.setLSSDDPSurroundDownmixLevel(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "DialogueNormalization") {
                                                            changed_properties.setLSSDDPDialoguenormalization(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "AudioCodingMode") {
                                                            changed_properties.setLSSDDPAudioCodingMode(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "LfeOn") {
                                                            changed_properties.setLSSDDPLfe(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "CustomChMap") {
                                                            changed_properties.setLSSDDPCustomChMap(g_variant_get_uint32(tmp));
                                                continue;
        }
                                if (std::string(key) == "BitRate") {
                                                            changed_properties.setLSSDDPBitrate(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "DrcMode") {
                                                GVariantIter property_iter;
            g_variant_iter_init(&property_iter, tmp);
            changed_properties.setLSSDDPDrcmode(dbus_->parseArrayOfInt32(property_iter));
                                    continue;
        }
                                if (std::string(key) == "DecChConfig") {
                                                            changed_properties.setLSSDDPDecChConfig(g_variant_get_int32(tmp));
                                                continue;
        }
                                                                                                                                                                                                                                                                                                                                    }

    if (tmp) {
        g_variant_unref(tmp);
    }
    if (obj_id_map_changed) {
        for (auto &f : obj_id_map_changed_listeners_) {
            f(obj_id_map_);
        }
    }
    return changed_properties;
}
PropertiesAttr PropertiesHandler::processLivesourceserviceMatIfaceResponse(
    const std::string &obj_path, GVariantIter *iter) {
    g_print("PropertiesHandler::processKLivesourceserviceMatIfaceResponse obj_path: %s \n", obj_path.c_str());
    char *key;
    GVariant *tmp = nullptr;
    PropertiesAttr changed_properties;
    bool obj_id_map_changed = false;

    std::lock_guard<std::mutex> lock(mutex_);
    changed_properties.clearChangedProperties();
    while (g_variant_iter_loop(iter, "{sv}", &key, &tmp)) {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        if (std::string(key) == "StreamType") {
                                                            changed_properties.setLSSMATStreamtype(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "DrcMode") {
                                                GVariantIter property_iter;
            g_variant_iter_init(&property_iter, tmp);
            changed_properties.setLSSMATDrcmode(dbus_->parseArrayOfInt32(property_iter));
                                    continue;
        }
                                                                                                                                                                                                                                                                                                    }

    if (tmp) {
        g_variant_unref(tmp);
    }
    if (obj_id_map_changed) {
        for (auto &f : obj_id_map_changed_listeners_) {
            f(obj_id_map_);
        }
    }
    return changed_properties;
}
PropertiesAttr PropertiesHandler::processLivesourceserviceThdIfaceResponse(
    const std::string &obj_path, GVariantIter *iter) {
    g_print("PropertiesHandler::processKLivesourceserviceThdIfaceResponse obj_path: %s \n", obj_path.c_str());
    char *key;
    GVariant *tmp = nullptr;
    PropertiesAttr changed_properties;
    bool obj_id_map_changed = false;

    std::lock_guard<std::mutex> lock(mutex_);
    changed_properties.clearChangedProperties();
    while (g_variant_iter_loop(iter, "{sv}", &key, &tmp)) {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        if (std::string(key) == "StreamType") {
                                                            gsize length;
            changed_properties.setLSSTHDStreamtype(static_cast<std::string>(g_variant_get_string(tmp, &length)));
                                                continue;
        }
                                if (std::string(key) == "GainRequired") {
                                                            changed_properties.setLSSTHDGainRequired(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "FbbCh") {
                                                            changed_properties.setLSSTHDFbbch(g_variant_get_uint32(tmp));
                                                continue;
        }
                                if (std::string(key) == "FbaCh") {
                                                            changed_properties.setLSSTHDFbach(g_variant_get_uint32(tmp));
                                                continue;
        }
                                if (std::string(key) == "FbaSurroundCh") {
                                                            changed_properties.setLSSTHDFbaSurroundCh(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "FbaDialogueNormalization") {
                                                            changed_properties.setLSSTHDFbaDialogueNormalization(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "TrueHdSync") {
                                                            changed_properties.setLSSTHDTrueHdSync(g_variant_get_uint32(tmp));
                                                continue;
        }
                                if (std::string(key) == "DrcMode") {
                                                GVariantIter property_iter;
            g_variant_iter_init(&property_iter, tmp);
            changed_properties.setLSSTHDDrcmode(dbus_->parseArrayOfInt32(property_iter));
                                    continue;
        }
                                                                                                                                                                    }

    if (tmp) {
        g_variant_unref(tmp);
    }
    if (obj_id_map_changed) {
        for (auto &f : obj_id_map_changed_listeners_) {
            f(obj_id_map_);
        }
    }
    return changed_properties;
}
PropertiesAttr PropertiesHandler::processLivesourceserviceDsdIfaceResponse(
    const std::string &obj_path, GVariantIter *iter) {
    g_print("PropertiesHandler::processKLivesourceserviceDsdIfaceResponse obj_path: %s \n", obj_path.c_str());
    char *key;
    GVariant *tmp = nullptr;
    PropertiesAttr changed_properties;
    bool obj_id_map_changed = false;

    std::lock_guard<std::mutex> lock(mutex_);
    changed_properties.clearChangedProperties();
    while (g_variant_iter_loop(iter, "{sv}", &key, &tmp)) {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        if (std::string(key) == "ChConfig") {
                                                            changed_properties.setLSSDSDChConfig(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "SamplingRate") {
                                                            changed_properties.setLSSDSDSamplingrate(g_variant_get_uint32(tmp));
                                                continue;
        }
                                if (std::string(key) == "DSDType") {
                                                            changed_properties.setLSSDSDType(g_variant_get_uint32(tmp));
                                                continue;
        }
                                                                                                                    }

    if (tmp) {
        g_variant_unref(tmp);
    }
    if (obj_id_map_changed) {
        for (auto &f : obj_id_map_changed_listeners_) {
            f(obj_id_map_);
        }
    }
    return changed_properties;
}
PropertiesAttr PropertiesHandler::processLivesourceserviceAacIfaceResponse(
    const std::string &obj_path, GVariantIter *iter) {
    g_print("PropertiesHandler::processKLivesourceserviceAacIfaceResponse obj_path: %s \n", obj_path.c_str());
    char *key;
    GVariant *tmp = nullptr;
    PropertiesAttr changed_properties;
    bool obj_id_map_changed = false;

    std::lock_guard<std::mutex> lock(mutex_);
    changed_properties.clearChangedProperties();
    while (g_variant_iter_loop(iter, "{sv}", &key, &tmp)) {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        if (std::string(key) == "AacDualMonoChannelMode") {
                                                            changed_properties.setLSSAacDualMonoChannelMode(g_variant_get_int32(tmp));
                                                continue;
        }
                                                                                                    }

    if (tmp) {
        g_variant_unref(tmp);
    }
    if (obj_id_map_changed) {
        for (auto &f : obj_id_map_changed_listeners_) {
            f(obj_id_map_);
        }
    }
    return changed_properties;
}
PropertiesAttr PropertiesHandler::processLivesourceservicePcmIfaceResponse(
    const std::string &obj_path, GVariantIter *iter) {
    g_print("PropertiesHandler::processKLivesourceservicePcmIfaceResponse obj_path: %s \n", obj_path.c_str());
    char *key;
    GVariant *tmp = nullptr;
    PropertiesAttr changed_properties;
    bool obj_id_map_changed = false;

    std::lock_guard<std::mutex> lock(mutex_);
    changed_properties.clearChangedProperties();
    while (g_variant_iter_loop(iter, "{sv}", &key, &tmp)) {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        if (std::string(key) == "ChConfig") {
                                                            gsize length;
            changed_properties.setLSSPCMChConfig(static_cast<std::string>(g_variant_get_string(tmp, &length)));
                                                continue;
        }
                                if (std::string(key) == "SamplingRate") {
                                                            changed_properties.setLSSPCMsamplingRate(g_variant_get_uint32(tmp));
                                                continue;
        }
                                                                    }

    if (tmp) {
        g_variant_unref(tmp);
    }
    if (obj_id_map_changed) {
        for (auto &f : obj_id_map_changed_listeners_) {
            f(obj_id_map_);
        }
    }
    return changed_properties;
}
PropertiesAttr PropertiesHandler::processAudiomanagerDapIfaceResponse(
    const std::string &obj_path, GVariantIter *iter) {
    g_print("PropertiesHandler::processKAudiomanagerDapIfaceResponse obj_path: %s \n", obj_path.c_str());
    char *key;
    GVariant *tmp = nullptr;
    PropertiesAttr changed_properties;
    bool obj_id_map_changed = false;

    std::lock_guard<std::mutex> lock(mutex_);
    changed_properties.clearChangedProperties();
    while (g_variant_iter_loop(iter, "{sv}", &key, &tmp)) {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        if (std::string(key) == "UpmixEnable") {
                                                            changed_properties.setLSSDAPDlbsurroundmode(g_variant_get_boolean(tmp));
                                                continue;
        }
                                if (std::string(key) == "CenterSpread") {
                                                            changed_properties.setLSSDlbsurroundcenterspread(g_variant_get_boolean(tmp));
                                                continue;
        }
                                    }

    if (tmp) {
        g_variant_unref(tmp);
    }
    if (obj_id_map_changed) {
        for (auto &f : obj_id_map_changed_listeners_) {
            f(obj_id_map_);
        }
    }
    return changed_properties;
}
PropertiesAttr PropertiesHandler::processAudiomanagerDeviceControllerIfaceResponse(
    const std::string &obj_path, GVariantIter *iter) {
    g_print("PropertiesHandler::processKAudiomanagerDeviceControllerIfaceResponse obj_path: %s \n", obj_path.c_str());
    char *key;
    GVariant *tmp = nullptr;
    PropertiesAttr changed_properties;
    bool obj_id_map_changed = false;

    std::lock_guard<std::mutex> lock(mutex_);
    changed_properties.clearChangedProperties();
    while (g_variant_iter_loop(iter, "{sv}", &key, &tmp)) {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        if (std::string(key) == "CurrentSource") {
                                                            gsize length;
            changed_properties.setCurrentSource(static_cast<std::string>(g_variant_get_string(tmp, &length)));
                                                continue;
        }
                    }

    if (tmp) {
        g_variant_unref(tmp);
    }
    if (obj_id_map_changed) {
        for (auto &f : obj_id_map_changed_listeners_) {
            f(obj_id_map_);
        }
    }
    return changed_properties;
}

std::tuple<ErrorCode, uint32_t> PropertiesHandler::readLSSDecSamplFreq() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    uint32_t lssdec_sampl_freq;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceStreamIface,
            "DecSamplFreq"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssdec_sampl_freq = g_variant_get_uint32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssdec_sampl_freq);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readLSSDecChConfig() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t lssdec_ch_config;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceStreamIface,
            "DecChConfig"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssdec_ch_config = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssdec_ch_config);
}

ErrorCode PropertiesHandler::updateLSSDecChConfig(
    int32_t lssdec_ch_config) {
    
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ssv)",
            adk::dbus::kLivesourceserviceStreamIface,
            "DecChConfig",
                                    g_variant_new("i", lssdec_ch_config)
                        ));
    return result;
}
std::tuple<ErrorCode, uint32_t> PropertiesHandler::readLSSOutputsamplingfrequency() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    uint32_t lssoutputsamplingfrequency;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceStreamIface,
            "OutputSamplingFrequency"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssoutputsamplingfrequency = g_variant_get_uint32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssoutputsamplingfrequency);
}

std::tuple<ErrorCode, std::string> PropertiesHandler::readLSSReadDetectedformat() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::string lssread_detectedformat;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceStreamIface,
            "DetectedFormat"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                gsize length;
        lssread_detectedformat = static_cast<std::string>(g_variant_get_string(tmp, &length));
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssread_detectedformat);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readLSSDTSBitrate() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t lssdtsbitrate;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceDtsxIface,
            "BitRate"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssdtsbitrate = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssdtsbitrate);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readLSSDTSEsMatrixFlag() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t lssdtses_matrix_flag;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceDtsxIface,
            "EsMatrixFlag"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssdtses_matrix_flag = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssdtses_matrix_flag);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readLSSDTSXchFlag() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t lssdtsxch_flag;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceDtsxIface,
            "XchFlag"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssdtsxch_flag = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssdtsxch_flag);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readLSSDTSX96Flag() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t lssdtsx96_flag;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceDtsxIface,
            "X96Flag"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssdtsx96_flag = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssdtsx96_flag);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readLSSDTSDialogueNormalization() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t lssdtsdialogue_normalization;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceDtsxIface,
            "DialogueNormalization"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssdtsdialogue_normalization = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssdtsdialogue_normalization);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readLSSDTSExtensionMask() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t lssdtsextension_mask;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceDtsxIface,
            "ExtensionMask"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssdtsextension_mask = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssdtsextension_mask);
}

std::tuple<ErrorCode, uint32_t> PropertiesHandler::readLSSDTSProgChMask() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    uint32_t lssdtsprog_ch_mask;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceDtsxIface,
            "ProgChMask"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssdtsprog_ch_mask = g_variant_get_uint32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssdtsprog_ch_mask);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readLSSDTSXllsamplerate() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t lssdtsxllsamplerate;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceDtsxIface,
            "XllSampleRate"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssdtsxllsamplerate = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssdtsxllsamplerate);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readLSSDTSEmbeddeddownmixselect() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t lssdtsembeddeddownmixselect;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceDtsxIface,
            "EmbeddedDownMixSelect"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssdtsembeddeddownmixselect = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssdtsembeddeddownmixselect);
}

std::tuple<ErrorCode, uint32_t> PropertiesHandler::readLSSDTSSamplerate() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    uint32_t lssdtssamplerate;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceDtsxIface,
            "SampleRate"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssdtssamplerate = g_variant_get_uint32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssdtssamplerate);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readLSSDTSStreamType() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t lssdtsstream_type;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceDtsxIface,
            "StreamType"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssdtsstream_type = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssdtsstream_type);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readLSSDTSDecodeInfo() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t lssdtsdecode_info;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceDtsxIface,
            "DecodeInfo"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssdtsdecode_info = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssdtsdecode_info);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readLSSDTSCoreBitRate() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t lssdtscore_bit_rate;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceDtsxIface,
            "CoreBitRate"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssdtscore_bit_rate = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssdtscore_bit_rate);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readLSSDTSObjectNum() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t lssdtsobject_num;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceDtsxIface,
            "ObjectNum"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssdtsobject_num = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssdtsobject_num);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readLSSDTSDialogueControlAvailability() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t lssdtsdialogue_control_availability;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceDtsxIface,
            "DialogueControlAvailability"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssdtsdialogue_control_availability = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssdtsdialogue_control_availability);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readLSSDTSAnalogCompensationGain() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t lssdtsanalog_compensation_gain;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceDtsxIface,
            "AnalogCompensationGain"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssdtsanalog_compensation_gain = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssdtsanalog_compensation_gain);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readLSSDtsxDrcpercent() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t lssdtsx_drcpercent;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceDtsxIface,
            "DrcPercent"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssdtsx_drcpercent = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssdtsx_drcpercent);
}

ErrorCode PropertiesHandler::updateLSSDtsxDrcpercent(
    int32_t lssdtsx_drcpercent) {
    
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ssv)",
            adk::dbus::kLivesourceserviceDtsxIface,
            "DrcPercent",
                                    g_variant_new("i", lssdtsx_drcpercent)
                        ));
    return result;
}
std::tuple<ErrorCode, int32_t> PropertiesHandler::readLSSDTSXSpkrout() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t lssdtsxspkrout;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceDtsxIface,
            "spkrout"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssdtsxspkrout = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssdtsxspkrout);
}

ErrorCode PropertiesHandler::updateLSSDTSXSpkrout(
    int32_t lssdtsxspkrout) {
    
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ssv)",
            adk::dbus::kLivesourceserviceDtsxIface,
            "spkrout",
                                    g_variant_new("i", lssdtsxspkrout)
                        ));
    return result;
}
std::tuple<ErrorCode, int32_t> PropertiesHandler::readLSSDTSXSetDialogcontrol() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t lssdtsxset_dialogcontrol;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceDtsxIface,
            "set_dialogcontrol"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssdtsxset_dialogcontrol = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssdtsxset_dialogcontrol);
}

ErrorCode PropertiesHandler::updateLSSDTSXSetDialogcontrol(
    int32_t lssdtsxset_dialogcontrol) {
    
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ssv)",
            adk::dbus::kLivesourceserviceDtsxIface,
            "set_dialogcontrol",
                                    g_variant_new("i", lssdtsxset_dialogcontrol)
                        ));
    return result;
}
std::tuple<ErrorCode, int32_t> PropertiesHandler::readLSSDTSXGetDialogcontrol() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t lssdtsxget_dialogcontrol;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceDtsxIface,
            "get_dialogcontrol"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssdtsxget_dialogcontrol = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssdtsxget_dialogcontrol);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readLSSDTSXEnableDirectmode() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t lssdtsxenable_directmode;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceDtsxIface,
            "enable_directmode"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssdtsxenable_directmode = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssdtsxenable_directmode);
}

ErrorCode PropertiesHandler::updateLSSDTSXEnableDirectmode(
    int32_t lssdtsxenable_directmode) {
    
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ssv)",
            adk::dbus::kLivesourceserviceDtsxIface,
            "enable_directmode",
                                    g_variant_new("i", lssdtsxenable_directmode)
                        ));
    return result;
}
std::tuple<ErrorCode, int32_t> PropertiesHandler::readLSSDTSXDisableDirectmode() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t lssdtsxdisable_directmode;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceDtsxIface,
            "disable_directmode"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssdtsxdisable_directmode = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssdtsxdisable_directmode);
}

ErrorCode PropertiesHandler::updateLSSDTSXDisableDirectmode(
    int32_t lssdtsxdisable_directmode) {
    
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ssv)",
            adk::dbus::kLivesourceserviceDtsxIface,
            "disable_directmode",
                                    g_variant_new("i", lssdtsxdisable_directmode)
                        ));
    return result;
}
std::tuple<ErrorCode, int32_t> PropertiesHandler::readLSSDTSXEnableBlindparma() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t lssdtsxenable_blindparma;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceDtsxIface,
            "enable_blindparma"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssdtsxenable_blindparma = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssdtsxenable_blindparma);
}

ErrorCode PropertiesHandler::updateLSSDTSXEnableBlindparma(
    int32_t lssdtsxenable_blindparma) {
    
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ssv)",
            adk::dbus::kLivesourceserviceDtsxIface,
            "enable_blindparma",
                                    g_variant_new("i", lssdtsxenable_blindparma)
                        ));
    return result;
}
std::tuple<ErrorCode, int32_t> PropertiesHandler::readLSSDTSXDisableBlindparma() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t lssdtsxdisable_blindparma;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceDtsxIface,
            "disable_blindparma"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssdtsxdisable_blindparma = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssdtsxdisable_blindparma);
}

ErrorCode PropertiesHandler::updateLSSDTSXDisableBlindparma(
    int32_t lssdtsxdisable_blindparma) {
    
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ssv)",
            adk::dbus::kLivesourceserviceDtsxIface,
            "disable_blindparma",
                                    g_variant_new("i", lssdtsxdisable_blindparma)
                        ));
    return result;
}
std::tuple<ErrorCode, int32_t> PropertiesHandler::readLSSDTSXEsMatrix() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t lssdtsxes_matrix;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceDtsxIface,
            "es_matrix"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssdtsxes_matrix = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssdtsxes_matrix);
}

ErrorCode PropertiesHandler::updateLSSDTSXEsMatrix(
    int32_t lssdtsxes_matrix) {
    
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ssv)",
            adk::dbus::kLivesourceserviceDtsxIface,
            "es_matrix",
                                    g_variant_new("i", lssdtsxes_matrix)
                        ));
    return result;
}
std::tuple<ErrorCode, int32_t> PropertiesHandler::readLSSDTSXEnableAnalogCompensation() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t lssdtsxenable_analog_compensation;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceDtsxIface,
            "enable_analog_compensation"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssdtsxenable_analog_compensation = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssdtsxenable_analog_compensation);
}

ErrorCode PropertiesHandler::updateLSSDTSXEnableAnalogCompensation(
    int32_t lssdtsxenable_analog_compensation) {
    
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ssv)",
            adk::dbus::kLivesourceserviceDtsxIface,
            "enable_analog_compensation",
                                    g_variant_new("i", lssdtsxenable_analog_compensation)
                        ));
    return result;
}
std::tuple<ErrorCode, int32_t> PropertiesHandler::readLSSDTSXSetAnalogcompmaxUsergain() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t lssdtsxset_analogcompmax_usergain;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceDtsxIface,
            "set_analogcompmax_usergain"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssdtsxset_analogcompmax_usergain = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssdtsxset_analogcompmax_usergain);
}

ErrorCode PropertiesHandler::updateLSSDTSXSetAnalogcompmaxUsergain(
    int32_t lssdtsxset_analogcompmax_usergain) {
    
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ssv)",
            adk::dbus::kLivesourceserviceDtsxIface,
            "set_analogcompmax_usergain",
                                    g_variant_new("i", lssdtsxset_analogcompmax_usergain)
                        ));
    return result;
}
std::tuple<ErrorCode, std::string> PropertiesHandler::readLSSDDPStreamType() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::string lssddpstream_type;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceDdpIface,
            "StreamType"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                gsize length;
        lssddpstream_type = static_cast<std::string>(g_variant_get_string(tmp, &length));
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssddpstream_type);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readLSSDDPSurroundex() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t lssddpsurroundex;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceDdpIface,
            "SurroundEx"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssddpsurroundex = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssddpsurroundex);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readLSSDDPDualMono() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t lssddpdual_mono;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceDdpIface,
            "DualMono"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssddpdual_mono = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssddpdual_mono);
}

ErrorCode PropertiesHandler::updateLSSDDPDualMono(
    int32_t lssddpdual_mono) {
    
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ssv)",
            adk::dbus::kLivesourceserviceDdpIface,
            "DualMono",
                                    g_variant_new("i", lssddpdual_mono)
                        ));
    return result;
}
std::tuple<ErrorCode, int32_t> PropertiesHandler::readLSSDDPKaraoke() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t lssddpkaraoke;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceDdpIface,
            "Karaoke"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssddpkaraoke = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssddpkaraoke);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readLSSDDPCenterDownmixLevel() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t lssddpcenter_downmix_level;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceDdpIface,
            "CenterDownmixLevel"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssddpcenter_downmix_level = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssddpcenter_downmix_level);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readLSSDDPSurroundDownmixLevel() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t lssddpsurround_downmix_level;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceDdpIface,
            "SurroundDownmixLevel"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssddpsurround_downmix_level = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssddpsurround_downmix_level);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readLSSDDPDialoguenormalization() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t lssddpdialoguenormalization;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceDdpIface,
            "DialogueNormalization"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssddpdialoguenormalization = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssddpdialoguenormalization);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readLSSDDPAudioCodingMode() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t lssddpaudio_coding_mode;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceDdpIface,
            "AudioCodingMode"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssddpaudio_coding_mode = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssddpaudio_coding_mode);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readLSSDDPLfe() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t lssddplfe;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceDdpIface,
            "LfeOn"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssddplfe = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssddplfe);
}

std::tuple<ErrorCode, uint32_t> PropertiesHandler::readLSSDDPCustomChMap() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    uint32_t lssddpcustom_ch_map;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceDdpIface,
            "CustomChMap"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssddpcustom_ch_map = g_variant_get_uint32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssddpcustom_ch_map);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readLSSDDPBitrate() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t lssddpbitrate;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceDdpIface,
            "BitRate"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssddpbitrate = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssddpbitrate);
}

std::tuple<ErrorCode, std::vector<int32_t>> PropertiesHandler::readLSSDDPDrcmode() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::vector<int32_t> lssddpdrcmode;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceDdpIface,
            "DrcMode"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                        GVariantIter property_iter;
        g_variant_iter_init(&property_iter, tmp);
        lssddpdrcmode = dbus_->parseArrayOfInt32(property_iter);
                        g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssddpdrcmode);
}

ErrorCode PropertiesHandler::updateLSSDDPDrcmode(
    std::vector<int32_t> lssddpdrcmode) {
        GVariantBuilder lssddpdrcmode_builder;
    g_variant_builder_init(&lssddpdrcmode_builder, G_VARIANT_TYPE_ARRAY);
    for (auto &i : lssddpdrcmode) {
                g_variant_builder_add(&lssddpdrcmode_builder, "i", i);
            }
    
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ssv)",
            adk::dbus::kLivesourceserviceDdpIface,
            "DrcMode",
                        g_variant_new("ai", &lssddpdrcmode_builder)
            ));
    return result;
}
std::tuple<ErrorCode, int32_t> PropertiesHandler::readLSSDDPDecChConfig() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t lssddpdec_ch_config;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceDdpIface,
            "DecChConfig"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssddpdec_ch_config = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssddpdec_ch_config);
}

ErrorCode PropertiesHandler::updateLSSDDPDecChConfig(
    int32_t lssddpdec_ch_config) {
    
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ssv)",
            adk::dbus::kLivesourceserviceDdpIface,
            "DecChConfig",
                                    g_variant_new("i", lssddpdec_ch_config)
                        ));
    return result;
}
std::tuple<ErrorCode, int32_t> PropertiesHandler::readLSSMATStreamtype() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t lssmatstreamtype;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceMatIface,
            "StreamType"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssmatstreamtype = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssmatstreamtype);
}

std::tuple<ErrorCode, std::vector<int32_t>> PropertiesHandler::readLSSMATDrcmode() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::vector<int32_t> lssmatdrcmode;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceMatIface,
            "DrcMode"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                        GVariantIter property_iter;
        g_variant_iter_init(&property_iter, tmp);
        lssmatdrcmode = dbus_->parseArrayOfInt32(property_iter);
                        g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssmatdrcmode);
}

ErrorCode PropertiesHandler::updateLSSMATDrcmode(
    std::vector<int32_t> lssmatdrcmode) {
        GVariantBuilder lssmatdrcmode_builder;
    g_variant_builder_init(&lssmatdrcmode_builder, G_VARIANT_TYPE_ARRAY);
    for (auto &i : lssmatdrcmode) {
                g_variant_builder_add(&lssmatdrcmode_builder, "i", i);
            }
    
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ssv)",
            adk::dbus::kLivesourceserviceMatIface,
            "DrcMode",
                        g_variant_new("ai", &lssmatdrcmode_builder)
            ));
    return result;
}
std::tuple<ErrorCode, std::string> PropertiesHandler::readLSSTHDStreamtype() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::string lssthdstreamtype;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceThdIface,
            "StreamType"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                gsize length;
        lssthdstreamtype = static_cast<std::string>(g_variant_get_string(tmp, &length));
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssthdstreamtype);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readLSSTHDGainRequired() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t lssthdgain_required;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceThdIface,
            "GainRequired"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssthdgain_required = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssthdgain_required);
}

std::tuple<ErrorCode, uint32_t> PropertiesHandler::readLSSTHDFbbch() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    uint32_t lssthdfbbch;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceThdIface,
            "FbbCh"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssthdfbbch = g_variant_get_uint32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssthdfbbch);
}

std::tuple<ErrorCode, uint32_t> PropertiesHandler::readLSSTHDFbach() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    uint32_t lssthdfbach;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceThdIface,
            "FbaCh"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssthdfbach = g_variant_get_uint32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssthdfbach);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readLSSTHDFbaSurroundCh() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t lssthdfba_surround_ch;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceThdIface,
            "FbaSurroundCh"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssthdfba_surround_ch = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssthdfba_surround_ch);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readLSSTHDFbaDialogueNormalization() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t lssthdfba_dialogue_normalization;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceThdIface,
            "FbaDialogueNormalization"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssthdfba_dialogue_normalization = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssthdfba_dialogue_normalization);
}

std::tuple<ErrorCode, uint32_t> PropertiesHandler::readLSSTHDTrueHdSync() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    uint32_t lssthdtrue_hd_sync;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceThdIface,
            "TrueHdSync"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssthdtrue_hd_sync = g_variant_get_uint32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssthdtrue_hd_sync);
}

std::tuple<ErrorCode, std::vector<int32_t>> PropertiesHandler::readLSSTHDDrcmode() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::vector<int32_t> lssthddrcmode;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceThdIface,
            "DrcMode"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                        GVariantIter property_iter;
        g_variant_iter_init(&property_iter, tmp);
        lssthddrcmode = dbus_->parseArrayOfInt32(property_iter);
                        g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssthddrcmode);
}

ErrorCode PropertiesHandler::updateLSSTHDDrcmode(
    std::vector<int32_t> lssthddrcmode) {
        GVariantBuilder lssthddrcmode_builder;
    g_variant_builder_init(&lssthddrcmode_builder, G_VARIANT_TYPE_ARRAY);
    for (auto &i : lssthddrcmode) {
                g_variant_builder_add(&lssthddrcmode_builder, "i", i);
            }
    
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ssv)",
            adk::dbus::kLivesourceserviceThdIface,
            "DrcMode",
                        g_variant_new("ai", &lssthddrcmode_builder)
            ));
    return result;
}
std::tuple<ErrorCode, int32_t> PropertiesHandler::readLSSDSDChConfig() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t lssdsdch_config;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceDsdIface,
            "ChConfig"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssdsdch_config = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssdsdch_config);
}

std::tuple<ErrorCode, uint32_t> PropertiesHandler::readLSSDSDSamplingrate() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    uint32_t lssdsdsamplingrate;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceDsdIface,
            "SamplingRate"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssdsdsamplingrate = g_variant_get_uint32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssdsdsamplingrate);
}

std::tuple<ErrorCode, uint32_t> PropertiesHandler::readLSSDSDType() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    uint32_t lssdsdtype;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceDsdIface,
            "DSDType"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssdsdtype = g_variant_get_uint32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssdsdtype);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readLSSAacDualMonoChannelMode() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t lssaac_dual_mono_channel_mode;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceserviceAacIface,
            "AacDualMonoChannelMode"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssaac_dual_mono_channel_mode = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssaac_dual_mono_channel_mode);
}

ErrorCode PropertiesHandler::updateLSSAacDualMonoChannelMode(
    int32_t lssaac_dual_mono_channel_mode) {
    
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ssv)",
            adk::dbus::kLivesourceserviceAacIface,
            "AacDualMonoChannelMode",
                                    g_variant_new("i", lssaac_dual_mono_channel_mode)
                        ));
    return result;
}
std::tuple<ErrorCode, std::string> PropertiesHandler::readLSSPCMChConfig() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::string lsspcmch_config;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceservicePcmIface,
            "ChConfig"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                gsize length;
        lsspcmch_config = static_cast<std::string>(g_variant_get_string(tmp, &length));
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lsspcmch_config);
}

std::tuple<ErrorCode, uint32_t> PropertiesHandler::readLSSPCMsamplingRate() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    uint32_t lsspcmsampling_rate;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                g_variant_new("(ss)",
            adk::dbus::kLivesourceservicePcmIface,
            "SamplingRate"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lsspcmsampling_rate = g_variant_get_uint32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lsspcmsampling_rate);
}

std::tuple<ErrorCode, bool> PropertiesHandler::readLSSDAPDlbsurroundmode() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    bool lssdapdlbsurroundmode;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkAudiomanagerService,
                adk::dbus::kEffects109Ddea4D91C4969A2346Fbe2Ad0Ef3AObjPath,
                g_variant_new("(ss)",
            adk::dbus::kAudiomanagerDapIface,
            "UpmixEnable"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssdapdlbsurroundmode = g_variant_get_boolean(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssdapdlbsurroundmode);
}

ErrorCode PropertiesHandler::updateLSSDAPDlbsurroundmode(
    bool lssdapdlbsurroundmode) {
    
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kAdkAudiomanagerService,
                adk::dbus::kEffects109Ddea4D91C4969A2346Fbe2Ad0Ef3AObjPath,
                g_variant_new("(ssv)",
            adk::dbus::kAudiomanagerDapIface,
            "UpmixEnable",
                                    g_variant_new("b", static_cast<uint8_t>(lssdapdlbsurroundmode))
                        ));
    return result;
}
std::tuple<ErrorCode, bool> PropertiesHandler::readLSSDlbsurroundcenterspread() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    bool lssdlbsurroundcenterspread;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkAudiomanagerService,
                adk::dbus::kEffects109Ddea4D91C4969A2346Fbe2Ad0Ef3AObjPath,
                g_variant_new("(ss)",
            adk::dbus::kAudiomanagerDapIface,
            "CenterSpread"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                lssdlbsurroundcenterspread = g_variant_get_boolean(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, lssdlbsurroundcenterspread);
}

ErrorCode PropertiesHandler::updateLSSDlbsurroundcenterspread(
    bool lssdlbsurroundcenterspread) {
    
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kAdkAudiomanagerService,
                adk::dbus::kEffects109Ddea4D91C4969A2346Fbe2Ad0Ef3AObjPath,
                g_variant_new("(ssv)",
            adk::dbus::kAudiomanagerDapIface,
            "CenterSpread",
                                    g_variant_new("b", static_cast<uint8_t>(lssdlbsurroundcenterspread))
                        ));
    return result;
}
std::tuple<ErrorCode, std::string> PropertiesHandler::readCurrentSource() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::string current_source;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkAudiomanagerService,
                adk::dbus::kAudiomanagerDeviceControllerObjPath,
                g_variant_new("(ss)",
            adk::dbus::kAudiomanagerDeviceControllerIface,
            "CurrentSource"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                gsize length;
        current_source = static_cast<std::string>(g_variant_get_string(tmp, &length));
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, current_source);
}



MethodsHandler::MethodsHandler(Dbus *dbus) : dbus_(dbus) {
    dbus_->setPropertiesChangedListener(
        std::bind(&MethodsHandler::interfacesAddedCb, this,
        std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
}

void MethodsHandler::interfacesAddedCb(const std::string &obj_path,
    const std::string &iface_name, GVariantIter * /*iter*/) {
//    g_print("MethodsHandler::interfacesAddedCb \n");

    bool obj_id_map_changed = false;
    std::lock_guard<std::mutex> lock(mutex_);
    
    if (obj_id_map_changed) {
        for (auto &f : obj_id_map_changed_listeners_) {
            f(obj_id_map_);
        }
    }
}

ErrorCode
MethodsHandler::lSSDecodeRestart(
        std::string in_arg
        ) {
        GVariantBuilder input_builder;
        g_variant_builder_init(&input_builder, G_VARIANT_TYPE("(s)"));
                            g_variant_builder_add(&input_builder, "s", in_arg.c_str());
                
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
        std::tie(result, response) = dbus_->gdbusMethodCall(adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                adk::dbus::kLivesourceserviceStreamIface,
        "DecRestart",
                g_variant_builder_end(&input_builder)
        );
                return result;
        }
ErrorCode
MethodsHandler::lSSDSDPath(
        std::string in_arg
        ) {
        GVariantBuilder input_builder;
        g_variant_builder_init(&input_builder, G_VARIANT_TYPE("(s)"));
                            g_variant_builder_add(&input_builder, "s", in_arg.c_str());
                
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
        std::tie(result, response) = dbus_->gdbusMethodCall(adk::dbus::kAdkLivesourceserviceService,
                adk::dbus::kLivesourceserviceLssglobalplayernameObjPath,
                adk::dbus::kLivesourceserviceStreamIface,
        "DSDPath",
                g_variant_builder_end(&input_builder)
        );
                return result;
        }

SignalsHandler::SignalsHandler(Dbus *dbus) : dbus_(dbus) {
        dbus_->setLSSDetectedFormatListener(
        std::bind(&SignalsHandler::callLSSDetectedFormatListeners, this,
        std::placeholders::_1, std::placeholders::_2));
        dbus_->setLSSDualMonoAACListener(
        std::bind(&SignalsHandler::callLSSDualMonoAACListeners, this,
        std::placeholders::_1, std::placeholders::_2));
    }

void SignalsHandler::callLSSDetectedFormatListeners(const std::string &obj_path, GVariant* response) {
    g_print("SignalsHandler::callLSSDetectedFormatListeners \n");
        if (!g_variant_is_of_type(response, (const GVariantType *)"(s)")) {
        return;
    }
    
                            char * lssdetected_format_out;
        g_variant_get(response, "(s)", &lssdetected_format_out);
            for (auto &f : lssdetected_format_listeners_) {
        f(lssdetected_format_out, obj_path);
    }
    
}
void SignalsHandler::callLSSDualMonoAACListeners(const std::string &obj_path, GVariant* response) {
    g_print("SignalsHandler::callLSSDualMonoAACListeners \n");
    
        for (auto &f : lssdual_mono_aac_listeners_) {
        f(obj_path);
    }
    
}
}  // namespace adk
