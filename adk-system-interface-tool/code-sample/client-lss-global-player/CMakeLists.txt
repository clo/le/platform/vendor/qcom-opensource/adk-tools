cmake_minimum_required(VERSION 3.1)
# C needed to find Threads with cmake 3.3 (not needed with 3.5+)
get_filename_component(ADK_SI_CLIENT_FOLDER_NAME ${CMAKE_CURRENT_SOURCE_DIR} NAME)
project(adk-si-${ADK_SI_CLIENT_FOLDER_NAME} LANGUAGES CXX C)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

include(GNUInstallDirs)

# !! Thread detection must be done before adding the asan parameters.
# pthread detection doesn't work correctly with "-fsanitize=address" because
# libasan provides its own (but incomplete) pthread implementation (GCC 5.4.0
# at least).
find_package(Threads REQUIRED)
find_package(PkgConfig REQUIRED)
pkg_check_modules(GIO REQUIRED gio-2.0)
pkg_check_modules(GIOUNIX REQUIRED gio-unix-2.0)
pkg_check_modules(GLIB REQUIRED glib-2.0)

# pkg_check_modules(DBUSCPP REQUIRED dbus-cpp>=5.0)
# pkg_check_modules(ADK_MESSAGE_SERVICE REQUIRED adk-message-service)

add_compile_options(-Wall -Wextra -g)

option(ENABLE_CODE_COVERAGE "Enable code coverage" OFF)
if (ENABLE_CODE_COVERAGE)
    add_compile_options(-O0 --coverage)
    link_libraries(--coverage)
endif()

option(ENABLE_ASAN "Enable address sanitizer" OFF)
if (ENABLE_ASAN)
    add_compile_options(-fsanitize=address -fno-omit-frame-pointer)
    link_libraries(-fsanitize=address)
endif()
link_libraries("-Wl,--as-needed")

# Create extra variables for convience to distinguish between the "object
# library" and the executable
set(PROJECT_LIBNAME lib${PROJECT_NAME})

# Create the "object library" and executable (without sources yet)
add_library(${PROJECT_LIBNAME} OBJECT "")

add_executable(${PROJECT_NAME} $<TARGET_OBJECTS:${PROJECT_LIBNAME}>)
install(TARGETS ${PROJECT_NAME} DESTINATION ${CMAKE_INSTALL_BINDIR})

target_include_directories(${PROJECT_LIBNAME} PUBLIC
    ${CMAKE_CURRENT_SOURCE_DIR}
    ${CMAKE_CURRENT_SOURCE_DIR}/autogen
    )

target_include_directories(${PROJECT_LIBNAME} SYSTEM PUBLIC
    ${GIO_INCLUDE_DIRS}
    ${GIOUNIX_INCLUDE_DIRS}
    ${GLIB_INCLUDE_DIRS}
    )

target_include_directories(${PROJECT_NAME} PRIVATE
    $<TARGET_PROPERTY:${PROJECT_LIBNAME},INCLUDE_DIRECTORIES>)

set(PROJECT_LIBNAME_LDFLAGS
    ${GIO_LIBRARIES}
    ${GIOUNIX_LIBRARIES}
    ${GLIB_LIBRARIES}
    )

link_directories(${GIO_LIBRARY_DIRS}
    ${GIOUNIX_LIBRARY_DIRS}
    ${GLIB_LIBRARY_DIRS})

target_compile_options(${PROJECT_LIBNAME} PUBLIC
    ${GIO_CFLAGS_OTHER}
    ${GIOUNIX_CFLAGS_OTHER}
    ${GLIB_CFLAGS_OTHER}
    )

target_link_libraries(${PROJECT_NAME} ${PROJECT_LIBNAME_LDFLAGS} Threads::Threads)

target_sources(${PROJECT_LIBNAME} PRIVATE
    ${CMAKE_CURRENT_SOURCE_DIR}/main.cc)

add_subdirectory(autogen)
