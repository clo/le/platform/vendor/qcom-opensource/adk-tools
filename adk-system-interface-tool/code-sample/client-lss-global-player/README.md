#client-lss-global-player

This folder contains,
1. user.json file with dbus members of live source service (with global player)
2. autogen folder corresponding to above user.json
3. main.cc file with some basic usecases for unit testing
4. CMakeLists.txt file for compilation

