
/*
 * Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of The Linux Foundation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "interface_handlers.h"

namespace adk {

PropertiesHandler::PropertiesHandler(Dbus *dbus)
    : dbus_(dbus) {
    dbus_->setPropertiesChangedListener(
        std::bind(&PropertiesHandler::propertiesChangedCb, this,
            std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
}

void PropertiesHandler::propertiesChangedCb(const std::string &obj_path,
    const std::string &iface_name, GVariantIter *iter) {
    g_print("PropertiesHandler::propertiesChangedCb \n");
}

MethodsHandler::MethodsHandler(Dbus *dbus)
    : dbus_(dbus) {
    dbus_->setPropertiesChangedListener(
        std::bind(&MethodsHandler::interfacesAddedCb, this,
            std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
}

void MethodsHandler::interfacesAddedCb(const std::string &obj_path,
    const std::string &iface_name, GVariantIter * /*iter*/) {
    g_print("MethodsHandler::interfacesAddedCb \n");

    bool obj_id_map_changed = false;
    std::lock_guard<std::mutex> lock(mutex_);

    if (obj_id_map_changed) {
        for (auto &f : obj_id_map_changed_listeners_) {
            f(obj_id_map_);
        }
    }
}

SignalsHandler::SignalsHandler(Dbus *dbus)
    : dbus_(dbus) {
}

}  // namespace adk
