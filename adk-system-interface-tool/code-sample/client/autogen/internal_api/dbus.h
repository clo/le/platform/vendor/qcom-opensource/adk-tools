/*
 * Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of The Linux Foundation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef AUTOGEN_INTERNAL_API_DBUS_H_
#define AUTOGEN_INTERNAL_API_DBUS_H_

#include <gio/gio.h>
#include <glib.h>

#include <condition_variable>
#include <functional>
#include <mutex>
#include <string>
#include <thread>
#include <tuple>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "attribute_classes.h"
#include "internal_api/dbus_strings.h"

namespace adk {
class Dbus {
 public:
    Dbus() = default;
    ~Dbus();

    bool init(const std::string &application_name);

    void getManagedObjects();

    // Set up the listeners for each interface requested.
    // The first parameter of each listener is the object path the signal emerged from
    void setPropertiesChangedListener(
        std::function<void(const std::string &, const std::string &, GVariantIter *)> listener) {
        properties_listeners_.emplace_back(std::move(listener));
    }

    // convenience call for method calls on session bus
    std::tuple<ErrorCode, GVariant *> gdbusMethodCall(std::string service_name, std::string obj_path,
        std::string interface_name, std::string method_name,
        GVariant *parameters) {
        GError *error = nullptr;
        GVariant *response = nullptr;
        response = g_dbus_connection_call_sync(
            gdbus_connection_, service_name.c_str(), obj_path.c_str(),
            interface_name.c_str(), method_name.c_str(), parameters, nullptr,
            G_DBUS_CALL_FLAGS_NONE, -1, nullptr, &error);
        if (error) {
            g_print("Received Error : for method : %s \n", method_name.c_str());
            return std::make_tuple(ADK_SI_ERROR_FAILED, response);
        }
        return std::make_tuple(ADK_SI_OK, response);
    }

    // convenience call for properties GetAll on session bus
    std::tuple<ErrorCode, GVariant *> propertiesGetAll(std::string service_name, std::string obj_path,
        GVariant *parameters) {
        return gdbusMethodCall(service_name, obj_path,
            adk::dbus::kFreedesktopDBusPropertiesIface, "GetAll",
            parameters);
    }

    // convenience call for Properties Get on session bus
    std::tuple<ErrorCode, GVariant *> propertyGet(std::string service_name, std::string obj_path,
        GVariant *parameters) {
        return gdbusMethodCall(service_name, obj_path,
            adk::dbus::kFreedesktopDBusPropertiesIface, "Get",
            parameters);
    }

    // convenience call for Properties Set on session bus
    std::tuple<ErrorCode, GVariant *> propertySet(std::string service_name, std::string obj_path,
        GVariant *parameters) {
        return gdbusMethodCall(service_name, obj_path,
            adk::dbus::kFreedesktopDBusPropertiesIface, "Set",
            parameters);
    }

    // convenience call for method calls on address bus
    std::tuple<ErrorCode, GVariant *> gdbusAddressMethodCall(std::string address,
        std::string service_name, std::string obj_path,
        std::string interface_name, std::string method_name,
        GVariant *parameters) {
        GError *error = nullptr;
        GVariant *response = nullptr;
        GDBusConnection *gdbus_address_connection = nullptr;

        gdbus_address_connection = g_dbus_connection_new_for_address_sync(
            address.c_str(), G_DBUS_CONNECTION_FLAGS_AUTHENTICATION_CLIENT,
            nullptr, nullptr, &error);

        if (!gdbus_address_connection || error) {
            return std::make_tuple(ADK_SI_ERROR_FAILED, response);
        }
        response = g_dbus_connection_call_sync(
            gdbus_address_connection, service_name.c_str(), obj_path.c_str(),
            interface_name.c_str(), method_name.c_str(), parameters, nullptr,
            G_DBUS_CALL_FLAGS_NONE, -1, nullptr, &error);
        if (error) {
            if (error->message != nullptr) {
                g_print("Received Error: %s for method : %s\n", error->message, method_name.c_str());
            }
            g_object_unref(gdbus_address_connection);
            return std::make_tuple(ADK_SI_ERROR_FAILED, response);
        }
        g_object_unref(gdbus_address_connection);
        return std::make_tuple(ADK_SI_OK, response);
    }

    // convenience calls to parse through GVariants
    std::vector<std::string> parseArrayOfString(GVariantIter iter);
    std::vector<int16_t> parseArrayOfInt16(GVariantIter iter);
    std::vector<int32_t> parseArrayOfInt32(GVariantIter iter);
    std::vector<int64_t> parseArrayOfInt64(GVariantIter iter);
    std::vector<uint32_t> parseArrayOfUint32(GVariantIter iter);
    std::vector<uint64_t> parseArrayOfUint64(GVariantIter iter);
    std::vector<uint16_t> parseArrayOfUint16(GVariantIter iter);
    std::vector<uint8_t> parseArrayOfUint8(GVariantIter iter);
    std::vector<bool> parseArrayOfBool(GVariantIter iter);
    std::vector<double> parseArrayOfDouble(GVariantIter iter);

 private:
    void runMainloop();
    static gboolean gdbusInit(gpointer user_data);
    void watchServiceNames();

    // callback functions
    static void onBusAcquired(GDBusConnection *connection, const gchar *name,
        gpointer user_data);
    static void onNameAcquired(GDBusConnection *connection, const gchar *name,
        gpointer user_data);
    static void onNameLost(GDBusConnection *connection, const gchar *name,
        gpointer user_data);

    static gboolean mainloopIsRunning(gpointer user_data);
    static void propertiesChangedCb(GDBusConnection *connection,
        const gchar *sender_name,
        const gchar *object_path,
        const gchar *interface_name,
        const gchar *signal_name,
        GVariant *parameters, gpointer user_data);
    static void interfacesAddedCb(GDBusConnection *connection,
        const gchar *sender_name,
        const gchar *object_path,
        const gchar *interface_name,
        const gchar *signal_name,
        GVariant *parameters, gpointer user_data);

    void parseReceivedObjectPaths(const std::string &obj_path, GVariantIter *iface_iter);

 private:
    std::thread signal_thread_;
    GMainContext *gdbus_thread_context_ = nullptr;
    GMainLoop *gmain_loop_ = nullptr;
    GSource *gdbus_source_ = nullptr;
    guint gdbus_id_;
    GDBusConnection *gdbus_connection_ = nullptr;
    std::string gdbus_application_name_;
    bool gdbus_name_owned_ = false;

    std::mutex mutex_;
    std::condition_variable cond_;

    std::vector<std::function<void(const std::string &, const std::string &, GVariantIter *)>>
        properties_listeners_;
};
}  // namespace adk

#endif  // AUTOGEN_INTERNAL_API_DBUS_H_
