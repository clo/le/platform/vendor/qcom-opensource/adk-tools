/*
 * Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of The Linux Foundation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "internal_api/dbus.h"

#include <cstring>

namespace adk {

Dbus::~Dbus() {
    g_print("Dbus::~Dbus\n");
    if (gmain_loop_) {
        g_main_loop_quit(gmain_loop_);
    }
    if (signal_thread_.joinable()) {
        signal_thread_.join();
    }
    if (gmain_loop_) {
        g_main_loop_unref(gmain_loop_);
    }
    g_bus_unown_name(gdbus_id_);
    if (gdbus_source_) {
        g_source_destroy(gdbus_source_);
        g_source_unref(gdbus_source_);
    }
}

bool Dbus::init(const std::string &application_name) {
    g_print("Dbus::init\n");
    gdbus_application_name_ = "com.adk." + application_name;

    // create new thread context for gmain loop
    gdbus_thread_context_ = g_main_context_new();
    gmain_loop_ = g_main_loop_new(gdbus_thread_context_, FALSE);

    // wait till the gmain loop starts
    std::unique_lock<std::mutex> lock(mutex_);
    signal_thread_ = std::thread(std::bind(&Dbus::runMainloop, this));
    cond_.wait(lock);

    // wait till gdbus connection is established
    gdbus_source_ = g_idle_source_new();
    g_source_set_callback(gdbus_source_, (GSourceFunc)Dbus::gdbusInit, this,
        nullptr);
    g_source_attach(gdbus_source_, gdbus_thread_context_);
    cond_.wait(lock);

    if (!gdbus_name_owned_) {
        return false;
    }

    return true;
}

void Dbus::runMainloop() {
    g_print("Dbus::runMainloop\n");
    g_main_context_push_thread_default(gdbus_thread_context_);

    auto *idle_source = g_idle_source_new();
    g_source_set_callback(idle_source, (GSourceFunc)Dbus::mainloopIsRunning,
        this, nullptr);
    g_source_attach(idle_source, gdbus_thread_context_);

    g_main_loop_run(gmain_loop_);

    g_source_destroy(idle_source);
    g_main_context_pop_thread_default(gdbus_thread_context_);
    g_source_unref(idle_source);
}

gboolean Dbus::mainloopIsRunning(gpointer user_data) {
    g_print("Dbus::mainLoopIsRunning\n");

    Dbus *dbus_obj = reinterpret_cast<Dbus *>(user_data);

    // wake up application main thread since g_main_loop is running
    dbus_obj->cond_.notify_one();
    return false;
}

gboolean Dbus::gdbusInit(gpointer user_data) {
    g_print("Dbus::gdbusInit\n");

    Dbus *dbus_obj = reinterpret_cast<Dbus *>(user_data);
    dbus_obj->gdbus_id_ = g_bus_own_name(
        G_BUS_TYPE_SESSION, dbus_obj->gdbus_application_name_.c_str(),
        G_BUS_NAME_OWNER_FLAGS_NONE,
        Dbus::onBusAcquired,
        Dbus::onNameAcquired,
        Dbus::onNameLost,
        user_data, nullptr);
    if (dbus_obj->gdbus_id_ == 0) {
        return true;
    }
    return false;
}

void Dbus::onBusAcquired(GDBusConnection * /*connection*/, const gchar *name,
    gpointer /*user_data*/) {
    g_print("Dbus::onBusAcquired Bus Aquired with name : %s\n", name);
}

void Dbus::onNameAcquired(GDBusConnection *connection,
    const gchar *name, gpointer user_data) {
    g_print("Dbus::onNameAcquired Name Aquired : %s\n", name);
    guint r = 0;
    Dbus *dbus_obj = reinterpret_cast<Dbus *>(user_data);
    dbus_obj->gdbus_connection_ = connection;
    dbus_obj->gdbus_name_owned_ = true;

    // subscribe to required signals
    // PropertiesChanged signal from all active services
    r = g_dbus_connection_signal_subscribe(
        dbus_obj->gdbus_connection_, nullptr,
        adk::dbus::kFreedesktopDBusPropertiesIface, "PropertiesChanged",
        nullptr,
        nullptr, G_DBUS_SIGNAL_FLAGS_NONE,
        dbus_obj->propertiesChangedCb, user_data, nullptr);
    if (r == 0) {
        g_print("Dbus::onNameAcquired unable to subscribe for PropertiesChanged \n");
    }

    // InterfacesAdded signal from all services
    r = g_dbus_connection_signal_subscribe(
        dbus_obj->gdbus_connection_, nullptr,
        adk::dbus::kFreedesktopDBusObjectManagerIface, "InterfacesAdded",
        adk::dbus::kFreedesktopDBusObjectManagerObjPath,
        nullptr, G_DBUS_SIGNAL_FLAGS_NONE,
        dbus_obj->interfacesAddedCb, user_data, nullptr);
    if (r == 0) {
        g_print("Dbus::onNameAcquired unable to subscribe for InterfacesAdded \n");
    }
    dbus_obj->watchServiceNames();

    // wake up application main thread
    dbus_obj->cond_.notify_one();
}

void Dbus::watchServiceNames() {
    g_print("Dbus::watchServiceNames\n");
    int r = 0;
}

void Dbus::getManagedObjects() {
    g_print("Dbus::getManagedObjects \n");

    ErrorCode result = ADK_SI_OK;
}

void Dbus::parseReceivedObjectPaths(const std::string &obj_path,
    GVariantIter *iface_iter) {
    g_print("Dbus::parseReceivedObjectPaths \n");

    const char *interface_name;
    GVariantIter *properties_iter;
    while (g_variant_iter_loop(iface_iter, "{sa{sv}}", &interface_name, &properties_iter)) {
        for (auto &f : properties_listeners_) {
            f(obj_path, interface_name, properties_iter);
        }
    }
}

void Dbus::onNameLost(GDBusConnection * /*connection*/, const gchar *name,
    gpointer user_data) {
    g_print("Dbus::onNameLost %s name \n", name);
    Dbus *dbus_obj = reinterpret_cast<Dbus *>(user_data);
    dbus_obj->cond_.notify_one();
}

void Dbus::interfacesAddedCb(GDBusConnection * /*connection*/,
    const gchar * /*sender_name*/,
    const gchar * /*object_path*/,
    const gchar * /*interface_name*/,
    const gchar * /*signal_name*/,
    GVariant *parameters, gpointer user_data) {
    g_print("Dbus::interfacesAddedCb \n");
    char *obj_path;
    GVariantIter *iface_iter;
    g_variant_get(parameters, "(oa{sa{sv}})", &obj_path, &iface_iter);

    Dbus *dbus_obj = reinterpret_cast<Dbus *>(user_data);
    dbus_obj->parseReceivedObjectPaths(obj_path, iface_iter);
}

// sender_name of this callback would be useful when the interface names are
// same in multiple senders.
void Dbus::propertiesChangedCb(GDBusConnection * /*connection*/,
    const gchar * /*sender_name*/,
    const gchar *object_path,
    const gchar * /*interface_name*/,
    const gchar * /*signal_name*/,
    GVariant *parameters, gpointer user_data) {
    g_print("Dbus::propertiesChangedCb\n");
    Dbus *dbus_obj = reinterpret_cast<Dbus *>(user_data);
    gchar *iface_name;
    GVariantIter *iter_for_dict_sv;
    GVariantIter *iter_for_as;
    if (g_variant_is_of_type(parameters, (const GVariantType *)"(sa{sv}as)")) {
        g_variant_get(parameters, "(sa{sv}as)", &iface_name, &iter_for_dict_sv, &iter_for_as);
    } else if (g_variant_is_of_type(parameters, (const GVariantType *)"(sa{sv})")) {
        // FixMe: The specification provides the first case as the actual signature
        // Some message definitions ignore the invalidated properties field.
        // Hence the second else condition.
        g_variant_get(parameters, "(sa{sv})", &iface_name, &iter_for_dict_sv);
    } else {
        return;
    }
    for (auto &f : dbus_obj->properties_listeners_) {
        f(object_path, iface_name, iter_for_dict_sv);
    }
}

std::vector<std::string> Dbus::parseArrayOfString(GVariantIter iter) {
    char *str;
    std::vector<std::string> str_vector;

    while (g_variant_iter_next(&iter, "s", &str)) {
        str_vector.emplace_back(static_cast<std::string>(str));
    }

    return str_vector;
}

std::vector<int32_t> Dbus::parseArrayOfInt32(GVariantIter iter) {
    int32_t i;
    std::vector<int32_t> int_vector;

    while (g_variant_iter_next(&iter, "i", &i)) {
        int_vector.emplace_back(i);
    }

    return int_vector;
}

std::vector<int16_t> Dbus::parseArrayOfInt16(GVariantIter iter) {
    int16_t i;
    std::vector<int16_t> int_vector;

    while (g_variant_iter_next(&iter, "n", &i)) {
        int_vector.emplace_back(i);
    }

    return int_vector;
}

std::vector<int64_t> Dbus::parseArrayOfInt64(GVariantIter iter) {
    int64_t i;
    std::vector<int64_t> int_vector;

    while (g_variant_iter_next(&iter, "x", &i)) {
        int_vector.emplace_back(i);
    }

    return int_vector;
}

std::vector<uint8_t> Dbus::parseArrayOfUint8(GVariantIter iter) {
    uint8_t i;
    std::vector<uint8_t> int_vector;

    while (g_variant_iter_next(&iter, "y", &i)) {
        int_vector.emplace_back(i);
    }

    return int_vector;
}

std::vector<uint16_t> Dbus::parseArrayOfUint16(GVariantIter iter) {
    uint16_t i;
    std::vector<uint16_t> int_vector;

    while (g_variant_iter_next(&iter, "q", &i)) {
        int_vector.emplace_back(i);
    }

    return int_vector;
}

std::vector<uint32_t> Dbus::parseArrayOfUint32(GVariantIter iter) {
    uint32_t i;
    std::vector<uint32_t> int_vector;

    while (g_variant_iter_next(&iter, "u", &i)) {
        int_vector.emplace_back(i);
    }

    return int_vector;
}

std::vector<uint64_t> Dbus::parseArrayOfUint64(GVariantIter iter) {
    uint64_t i;
    std::vector<uint64_t> int_vector;

    while (g_variant_iter_next(&iter, "t", &i)) {
        int_vector.emplace_back(i);
    }

    return int_vector;
}

std::vector<bool> Dbus::parseArrayOfBool(GVariantIter iter) {
    uint8_t status;
    std::vector<bool> bool_vector;

    while (g_variant_iter_next(&iter, "b", &status)) {
        bool_vector.emplace_back(static_cast<bool>(status));
    }

    return bool_vector;
}

std::vector<double> Dbus::parseArrayOfDouble(GVariantIter iter) {
    double i;
    std::vector<double> double_vector;

    while (g_variant_iter_next(&iter, "d", &i)) {
        double_vector.emplace_back(i);
    }

    return double_vector;
}

}  // namespace adk
