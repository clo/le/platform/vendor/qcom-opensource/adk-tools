/*
 * Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of The Linux Foundation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <cassert>
#include <chrono>
#include <csignal>
#include <cstring>
#include <functional>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <thread>

#include "autogen/adk_system_interface.h"
#include "autogen/attribute_classes.h"
#include "autogen/attribute_strings.h"

#include <iostream>

using namespace adk;
using namespace std;

bool startAdkSystemInterface() {
    g_print("StartAdkSystemInterface\n");

    // ignore SIGPIPE
    struct sigaction action;
    action.sa_handler = SIG_IGN;
    sigemptyset(&action.sa_mask);
    action.sa_flags = 0;
    sigaction(SIGPIPE, &action, nullptr);

    // block signals we will want to "sigwait" on.
    // (needs to be set before we create any thread or the app will be killed
    // if we receive another a second signal while processing the first)
    sigset_t sig_set;
    sigemptyset(&sig_set);
    sigaddset(&sig_set, SIGHUP);
    sigaddset(&sig_set, SIGINT);
    sigaddset(&sig_set, SIGTERM);
    pthread_sigmask(SIG_BLOCK, &sig_set, nullptr);

    AdkSystemInterface &adk_si = AdkSystemInterface::getInstance();

    if (!adk_si.init("adk_system_interface_sample")) {
        return false;
    }

    g_print("-------------- this is a dummy --------------\n");
    // Note : This is a dummy main.cc, doesn't have code related to
    //        handling any members. For sample application having code related
    //        to handling some members, please refere to other folders

    /* Run mainloop */

    siginfo_t sig_info;
    int sig;
    do {
        int tries = 0;
        do {
            sig = sigwaitinfo(&sig_set, &sig_info);
            if (sig < 0) {
                if (errno == EINTR) {
                    // spurious wakeup, ignore
                    continue;
                }
                g_print("Error on sigwaitinfo errno %d %s, retrying \n", errno,
                    strerror(errno));
                tries++;
                std::this_thread::sleep_for(std::chrono::seconds(1));
            } else {
                break;
            }
        } while (tries < 5);
        if (sig < 0) {
            g_print("Error waiting signal, exiting \n");
            return false;
        }
    } while ((sig != SIGTERM) && (sig != SIGINT) && (sig != SIGHUP));
    g_print("exit signal wait \n");

    return true;
}

int main() {
    return startAdkSystemInterface() ? 0 : 1;
}
