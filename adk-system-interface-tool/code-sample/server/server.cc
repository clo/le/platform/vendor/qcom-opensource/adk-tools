/*
 * Copyright (c) 2019, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of The Linux Foundation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "server.h"

#include <functional>

static const gchar kIntrospectionXml[] =
    "<node>"
    "    <interface name='com.qti.loopback.stream.config'>"
    "        <property name='DecSamplFreq' type='u' access='read'>"
    "        </property>"
    "        <property name='DecChConfig' type='i' access='readwrite'>"
    "        </property>"
    "        <method name='DecodeRestart'>"
    "            <arg name='Format' type='s' direction='in' />"
    "        </method>"
    "        <signal name='DetectedFormat'>"
    "            <arg name='DetectedFormat' type='s' />"
    "        </signal>"
    "    </interface>"
    "</node>";

static void handleMethodCall(GDBusConnection * /*connection*/,
    const gchar * /*sender_name*/, const gchar * /*object_path*/,
    const gchar * /*interface_name*/, const gchar *method_name,
    GVariant *parameters, GDBusMethodInvocation *invocation, gpointer user_data) {
    g_print("handleMethodCall\n");
    StreamServer *obj = reinterpret_cast<StreamServer *>(user_data);
    if (g_strcmp0(method_name, "DecodeRestart") == 0) {
        // Let's assume that with every DecodeRestart the properties are set to
        // some defaults
        char *format;
        g_variant_get(parameters, "(s)", &format);
        g_print("handleMethodCall: Decoder to restart : %s\n", format);
        obj->setDecChConfig(0);
        obj->setDecSamplFreq();
        obj->setDetectedFormat();
        g_dbus_method_invocation_return_value(invocation, nullptr);
    }
}

static GVariant *handleGetProperty(GDBusConnection * /*connection*/,
    const gchar * /*sender_name*/, const gchar * /*object_path*/,
    const gchar * /*interface_name*/, const gchar *property_name,
    GError ** /*error*/, gpointer user_data) {
    g_print("handleGetProperty\n");
    StreamServer *obj = reinterpret_cast<StreamServer *>(user_data);

    if (g_strcmp0(property_name, "DecChConfig") == 0) {
        return g_variant_new_int32(obj->getDecChConfig());
    } else if (g_strcmp0(property_name, "DecSamplFreq") == 0) {
        return g_variant_new_uint32(obj->getDecSamplFreq());
    }

    return nullptr;
}

static gboolean handleSetProperty(GDBusConnection * /*connection*/,
    const gchar * /*sender_name*/, const gchar * /*object_path*/,
    const gchar * /*interface_name*/, const gchar *property_name,
    GVariant *value, GError ** /*error*/, gpointer user_data) {
    g_print("handleSetProperty\n");
    StreamServer *obj = reinterpret_cast<StreamServer *>(user_data);

    if (g_strcmp0(property_name, "DecChConfig") == 0) {
        obj->setDecChConfig(g_variant_get_int32(value));
        return true;
    }

    return false;
}

static const GDBusInterfaceVTable interface_vtable = {
    handleMethodCall,
    handleGetProperty,
    handleSetProperty};

StreamServer::~StreamServer() {
    g_print("StreamServer::~StreamServer\n");

    if (gdbus_registration_id_) {
        g_dbus_connection_unregister_object(gdbus_connection_, gdbus_registration_id_);
        gdbus_registration_id_ = 0;
    }
    if (gmain_loop_) {
        g_main_loop_quit(gmain_loop_);
    }
    if (signal_thread_.joinable()) {
        signal_thread_.join();
    }
    if (gmain_loop_) {
        g_main_loop_unref(gmain_loop_);
    }
    g_bus_unown_name(gdbus_id_);
    gdbus_id_ = 0;
    if (gdbus_source_) {
        g_source_destroy(gdbus_source_);
        g_source_unref(gdbus_source_);
    }
}

void StreamServer::init() {
    g_print("StreamServer::init\n");

    gdbus_thread_context_ = g_main_context_new();
    gmain_loop_ = g_main_loop_new(gdbus_thread_context_, FALSE);

    // wait till the gmain loop starts
    std::unique_lock<std::mutex> lock(mutex_);
    signal_thread_ = std::thread(std::bind(&StreamServer::runMainloop, this));
    cond_.wait(lock);

    // wait till gdbus connection is established
    gdbus_source_ = g_idle_source_new();
    g_source_set_callback(gdbus_source_, (GSourceFunc)StreamServer::gdbusInit, this, nullptr);
    g_source_attach(gdbus_source_, gdbus_thread_context_);
    cond_.wait(lock);
}

void StreamServer::runMainloop() {
    g_print("StreamServer::runMainloop\n");
    g_main_context_push_thread_default(gdbus_thread_context_);

    auto *idle_source = g_idle_source_new();
    g_source_set_callback(idle_source, (GSourceFunc)StreamServer::mainloopIsRunning, this,
        nullptr);
    g_source_attach(idle_source, gdbus_thread_context_);

    g_main_loop_run(gmain_loop_);

    g_source_destroy(idle_source);
    g_main_context_pop_thread_default(gdbus_thread_context_);
    g_source_unref(idle_source);
}

gboolean StreamServer::mainloopIsRunning(gpointer user_data) {
    g_print("StreamServer::mainLoopIsRunning\n");

    StreamServer *obj = reinterpret_cast<StreamServer *>(user_data);

    // wake up application main thread since g_main_loop is running
    obj->cond_.notify_one();
    return false;
}

gboolean StreamServer::gdbusInit(gpointer user_data) {
    g_print("StreamServer::gdbusInit\n");
    StreamServer *obj = reinterpret_cast<StreamServer *>(user_data);

    obj->gdbus_id_ = g_bus_own_name(
        G_BUS_TYPE_SESSION, "com.qti.loopback.stream.HDMI.config",
        G_BUS_NAME_OWNER_FLAGS_NONE, StreamServer::onBusAcquired,
        StreamServer::onNameAcquired, StreamServer::onNameLost,
        user_data, nullptr);
    if (obj->gdbus_id_ == 0) {
        return true;
    }
    return false;
}

void StreamServer::onBusAcquired(GDBusConnection * /*connection*/, const gchar *name,
    gpointer /*user_data*/) {
    g_print("StreamServer::onBusAcquired Bus Aquired with name : %s\n", name);
}

void StreamServer::onNameLost(GDBusConnection * /*connection*/, const gchar *name,
    gpointer user_data) {
    g_print("StreamServer::onNameLost Bus name lost : %s\n", name);
    StreamServer *obj = reinterpret_cast<StreamServer *>(user_data);
    obj->cond_.notify_one();
}

void StreamServer::onNameAcquired(GDBusConnection *connection, const gchar * /*name*/,
    gpointer user_data) {
    g_print("StreamServer::onNameAcquired\n");
    StreamServer *obj = reinterpret_cast<StreamServer *>(user_data);
    obj->gdbus_connection_ = connection;

    GDBusNodeInfo *introspection_data = g_dbus_node_info_new_for_xml(
        kIntrospectionXml, &obj->g_error_);

    if (obj->g_error_ != nullptr) {
        g_print("StreamServer::onNameAcquired Introspection failed\n");
        return;
    }

    obj->gdbus_registration_id_ = g_dbus_connection_register_object(connection,
        "/com/qti/loopback/stream/config",
        introspection_data->interfaces[0],
        &interface_vtable,
        user_data,
        nullptr,
        &obj->g_error_);

    obj->emitPropertyChangedSignal("DecChConfig");
    obj->emitPropertyChangedSignal("DecSamplFreq");
    obj->setDetectedFormat();
    // wake up application main thread
    obj->cond_.notify_one();
}

void StreamServer::emitPropertyChangedSignal(std::string property) {
    if (gdbus_connection_ && gdbus_id_) {
        GVariantBuilder builder;

        g_variant_builder_init(&builder, G_VARIANT_TYPE_ARRAY);
        GVariant *tmp;
        if (property == "DecChConfig") {
            tmp = g_variant_new_int32(getDecChConfig());
        } else if (property == "DecSamplFreq") {
            tmp = g_variant_new_uint32(getDecSamplFreq());
        } else {
            return;
        }
        g_variant_builder_add(&builder, "{sv}", property.c_str(), tmp);

        g_dbus_connection_emit_signal(gdbus_connection_, nullptr,
            "/com/qti/loopback/stream/config",
            "org.freedesktop.DBus.Properties",
            "PropertiesChanged",
            g_variant_new("(sa{sv}as)",
                "com.qti.loopback.stream.config",
                &builder, nullptr),
            nullptr);
    }
}

void StreamServer::setDetectedFormat() {
    std::vector<std::string> detected_formats{"DTSHD", "DDPDEC", "PCM", "DSD"};
    srand(time(0));
    std::string detected_format = detected_formats[rand() % 4];

    g_print("StreamServer::setDetectedFormat detected_format=%s\n", detected_format.c_str());
    if (gdbus_connection_ && gdbus_id_) {
        g_dbus_connection_emit_signal(gdbus_connection_, nullptr,
            "/com/qti/loopback/stream/config",
            "com.qti.loopback.stream.config",
            "DetectedFormat",
            g_variant_new("(s)", detected_format.c_str()),
            nullptr);
    }
}

void StreamServer::setDecSamplFreq() {
    std::vector<uint32_t> sample_freqs{22, 24, 48, 96, 192};
    srand(time(0));
    uint32_t sampl = sample_freqs[rand() % 5];

    if (dec_sampl_freq_ != sampl) {
        dec_sampl_freq_ = sampl;
        emitPropertyChangedSignal("DecSamplFreq");
    }
}
