/*
 * Copyright (c) 2019, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of The Linux Foundation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SERVER_H_
#define SERVER_H_

#include <gio/gio.h>
#include <glib.h>
#include <time.h>
#include <condition_variable>
#include <mutex>
#include <string>
#include <thread>
#include <vector>

class StreamServer {
 public:
    StreamServer() = default;
    ~StreamServer();

    void init();

    uint32_t getDecChConfig() { return dec_ch_config_; }
    uint32_t getDecSamplFreq() { return dec_sampl_freq_; }

    void setDecChConfig(uint32_t dec_ch_config) {
        if (dec_ch_config_ != dec_ch_config) {
            dec_ch_config_ = dec_ch_config;
            emitPropertyChangedSignal("DecChConfig");
        }
    }

    void setDecSamplFreq();
    void setDetectedFormat();

 private:
    void runMainloop();
    static gboolean gdbusInit(gpointer user_data);

    static void onBusAcquired(GDBusConnection *connection, const gchar *name,
        gpointer user_data);
    static void onNameAcquired(GDBusConnection *connection, const gchar *name,
        gpointer user_data);
    static void onNameLost(GDBusConnection *connection, const gchar *name,
        gpointer user_data);

    static gboolean mainloopIsRunning(gpointer user_data);
    void emitPropertyChangedSignal(std::string property);

 private:
    GError *g_error_ = nullptr;

    std::thread signal_thread_;
    GMainContext *gdbus_thread_context_ = nullptr;
    GMainLoop *gmain_loop_ = nullptr;
    GSource *gdbus_source_ = nullptr;
    guint gdbus_id_;
    GDBusConnection *gdbus_connection_ = nullptr;
    guint gdbus_registration_id_;

    std::mutex mutex_;
    std::condition_variable cond_;

    uint32_t dec_ch_config_ = 0;
    uint32_t dec_sampl_freq_ = 0;
};

#endif  // SERVER_H_
