/*
 * Copyright (c) 2019-2021, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of The Linux Foundation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <cassert>
#include <chrono>
#include <csignal>
#include <cstring>
#include <functional>
#include <string>
#include <thread>
#include <unordered_map>
#include <unordered_set>

#include "autogen/adk_system_interface.h"
#include "autogen/attribute_classes.h"
#include "autogen/attribute_strings.h"

#include <iostream>

using namespace adk;
using namespace std;

bool startAdkSystemInterface() {
    g_print("StartAdkSystemInterface\n");

    // ignore SIGPIPE
    struct sigaction action;
    action.sa_handler = SIG_IGN;
    sigemptyset(&action.sa_mask);
    action.sa_flags = 0;
    sigaction(SIGPIPE, &action, nullptr);

    // block signals we will want to "sigwait" on.
    // (needs to be set before we create any thread or the app will be killed
    // if we receive another a second signal while processing the first)
    sigset_t sig_set;
    sigemptyset(&sig_set);
    sigaddset(&sig_set, SIGHUP);
    sigaddset(&sig_set, SIGINT);
    sigaddset(&sig_set, SIGTERM);
    pthread_sigmask(SIG_BLOCK, &sig_set, nullptr);

    AdkSystemInterface &adk_si = AdkSystemInterface::getInstance();

    if (!adk_si.init("adk_system_interface_sample")) {
        return false;
    }

    /* Get property, method & signal handlers */
    std::shared_ptr<PropertiesHandler> properties_handler = adk_si.getPropertiesHandler();
    std::shared_ptr<MethodsHandler> methods_handler = adk_si.getMethodsHandler();
    std::shared_ptr<SignalsHandler> signals_handler = adk_si.getSignalsHandler();

    g_print("\n\n----------------- Initialize ----------------\n\n");

    g_print("registering listener for properties changed signal\n");
    properties_handler->setPropertiesChangedListener([](PropertiesAttr prop_attr,
                                                         const std::string &obj_path) -> void {
        std::unordered_set<std::string> changed_properties = prop_attr.getChangedProperties();
        // ARM based meloD effect's properties
        if (changed_properties.find(std::string(adk::kMeloDAroundProEffect))
            != changed_properties.end()) {
            uint16_t melod_aroundpro_effect = prop_attr.getMeloDAroundProEffect();
            g_print("\n--->melod_aroundpro_effect changed to %d\n", melod_aroundpro_effect);
        }
        if (changed_properties.find(std::string(adk::kMeloDAroundProEnable))
            != changed_properties.end()) {
            bool melod_aroundpro_enable = prop_attr.getMeloDAroundProEnable();
            g_print("\n---> melod_aroundpro_enable changed to %d\n", melod_aroundpro_enable);
        }
        if (changed_properties.find(std::string(adk::kMeloDAroundProMode))
            != changed_properties.end()) {
            std::string melod_around_pro_mode = prop_attr.getMeloDAroundProMode();
            g_print("\n---> melod_around_pro_mode changed to %s\n", melod_around_pro_mode.c_str());
        }
        if (changed_properties.find(std::string(adk::kMeloDBassManagementCornerFreqBassStage1))
            != changed_properties.end()) {
            int16_t melod_bass_management_corner_freq_bass_stage1 =
                prop_attr.getMeloDBassManagementCornerFreqBassStage1();
            g_print("\n---> melod_bass_management_corner_freq_bass_stage1 changed to %d\n",
                melod_bass_management_corner_freq_bass_stage1);
        }
        if (changed_properties.find(std::string(adk::kMeloDBassManagementCornerFreqBassStage2))
            != changed_properties.end()) {
            int16_t melod_bass_management_corner_freq_bass_stage2 =
                prop_attr.getMeloDBassManagementCornerFreqBassStage2();
            g_print("\n---> melod_bass_management_corner_freq_bass_stage2 changed to %d\n",
                melod_bass_management_corner_freq_bass_stage2);
        }
        if (changed_properties.find(std::string(adk::kMeloDBassManagementEnable))
            != changed_properties.end()) {
            bool melod_bass_management_enable = prop_attr.getMeloDBassManagementEnable();
            g_print("\n---> melod_bass_management_enable changed to %d\n",
                melod_bass_management_enable);
        }
        if (changed_properties.find(std::string(adk::kMeloDBassManagementSpkSurrHeightLeft))
            != changed_properties.end()) {
            bool melod_bass_management_spk_surr_height_left =
                prop_attr.getMeloDBassManagementSpkSurrHeightLeft();
            g_print("\n---> melod_bass_management_spk_surr_height_left changed to %d\n",
                melod_bass_management_spk_surr_height_left);
        }
        if (changed_properties.find(std::string(adk::kMeloDBassManagementSpkSurrHeightRight))
            != changed_properties.end()) {
            bool melod_bass_management_spk_surr_height_right =
                prop_attr.getMeloDBassManagementSpkSurrHeightRight();
            g_print("\n---> melod_bass_management_spk_surr_height_right changed to %d\n",
                melod_bass_management_spk_surr_height_right);
        }
        if (changed_properties.find(std::string(adk::kMeloDBassManagementSubWooferOut))
            != changed_properties.end()) {
            bool melod_bass_management_sub_woofer_out =
                prop_attr.getMeloDBassManagementSubWooferOut();
            g_print("\n---> melod_bass_management_sub_woofer_out changed to %d\n",
                melod_bass_management_sub_woofer_out);
        }
        if (changed_properties.find(std::string(adk::kMeloDBassManagementSurroundSpeakerLeft))
            != changed_properties.end()) {
            std::string melod_bass_management_surround_speaker_left =
                prop_attr.getMeloDBassManagementSurroundSpeakerLeft();
            g_print("\n---> melod_bass_management_surround_speaker_left changed to %s\n",
                melod_bass_management_surround_speaker_left.c_str());
        }
        if (changed_properties.find(std::string(adk::kMeloDBassManagementSurroundSpeakerRight))
            != changed_properties.end()) {
            std::string melod_bass_management_surround_speaker_right =
                prop_attr.getMeloDBassManagementSurroundSpeakerRight();
            g_print("\n---> melod_bass_management_surround_speaker_right changed to %s\n",
                melod_bass_management_surround_speaker_right.c_str());
        }
        if (changed_properties.find(std::string(adk::kMeloDDRCEnable))
            != changed_properties.end()) {
            bool melod_drcenable = prop_attr.getMeloDDRCEnable();
            g_print("\n---> melod_drcenable changed to %d\n", melod_drcenable);
        }
        if (changed_properties.find(std::string(adk::kMeloDDRCMode)) != changed_properties.end()) {
            std::string melod_drcmode = prop_attr.getMeloDDRCMode();
            g_print("\n---> melod_drcmode changed to %s\n", melod_drcmode.c_str());
        }
        if (changed_properties.find(std::string(adk::kMeloDDRCRefLevel))
            != changed_properties.end()) {
            int16_t melod_drcref_level = prop_attr.getMeloDDRCRefLevel();
            g_print("\n---> melod_drcref_level changed to %d\n", melod_drcref_level);
        }
        if (changed_properties.find(std::string(adk::kMeloDMatrixEnable))
            != changed_properties.end()) {
            bool melod_matrix_enable = prop_attr.getMeloDMatrixEnable();
            g_print("\n---> melod_matrix_enable changed to %d\n", melod_matrix_enable);
        }
        if (changed_properties.find(std::string(adk::kMeloDMatrixMode))
            != changed_properties.end()) {
            std::string melod_matrix_mode = prop_attr.getMeloDMatrixMode();
            g_print("\n---> melod_matrix_mode changed to %s\n", melod_matrix_mode.c_str());
        }
        // DSP based meloD effect's properties
        if (changed_properties.find(std::string(adk::kMeloDBassPlusBassContent))
            != changed_properties.end()) {
            uint16_t melod_bass_plus_bass_content = prop_attr.getMeloDBassPlusBassContent();
            g_print("\n---> melod_bass_plus_bass_content changed to %d\n",
                melod_bass_plus_bass_content);
        }
        if (changed_properties.find(std::string(adk::kMeloDBassPlusEnable))
            != changed_properties.end()) {
            bool melod_bass_plus_enable = prop_attr.getMeloDBassPlusEnable();
            g_print("\n---> melod_bass_plus_enable changed to %d\n", melod_bass_plus_enable);
        }
        if (changed_properties.find(std::string(adk::kMeloDBassPlusHarmonicsContent))
            != changed_properties.end()) {
            uint16_t melod_bass_plus_harmonics_content =
                prop_attr.getMeloDBassPlusHarmonicsContent();
            g_print("\n---> melod_bass_plus_harmonics_content changed to %d\n",
                melod_bass_plus_harmonics_content);
        }
        if (changed_properties.find(std::string(adk::kMeloDMultiBandLimiterEnable))
            != changed_properties.end()) {
            bool melod_multi_band_limiter_enable = prop_attr.getMeloDMultiBandLimiterEnable();
            g_print("\n---> melod_multi_band_limiter_enable changed to %d\n",
                melod_multi_band_limiter_enable);
        }
        if (changed_properties.find(std::string(adk::kMeloDVoicePlusEffectStrength))
            != changed_properties.end()) {
            uint16_t melod_voice_plus_effect_strength = prop_attr.getMeloDVoicePlusEffectStrength();
            g_print("\n---> melod_voice_plus_effect_strength changed to %d\n",
                melod_voice_plus_effect_strength);
        }
        if (changed_properties.find(std::string(adk::kMeloDVoicePlusEnable))
            != changed_properties.end()) {
            bool melod_voice_plus_enable = prop_attr.getMeloDVoicePlusEnable();
            g_print("\n---> melod_voice_plus_enable changed to %d\n", melod_voice_plus_enable);
        }
        if (changed_properties.find(std::string(adk::kMeloDXOverFilterEnable))
            != changed_properties.end()) {
            bool melod_xover_filter_enable = prop_attr.getMeloDXOverFilterEnable();
            g_print("\n---> melod_xover_filter_enable changed to %d\n", melod_xover_filter_enable);
        }
    });

    g_print("\n\n----------------- START ----------------\n\n");

    ErrorCode err_code;

    uint16_t melod_around_pro_effect;
    std::tie(err_code, melod_around_pro_effect) = properties_handler->readMeloDAroundProEffect();
    if (err_code == ADK_SI_OK) {
        g_print("\n--->read melod_around_pro_effect %d\n", melod_around_pro_effect);
    } else {
        g_print("\n--->Failed to read melod_around_pro_effect %d\n", err_code);
    }
    uint16_t update_melod_around_pro_effect = 80;
    err_code = properties_handler->updateMeloDAroundProEffect(update_melod_around_pro_effect);
    if (err_code == ADK_SI_OK) {
        g_print("\n--->updated melod_around_pro_effect %d\n", update_melod_around_pro_effect);
    } else {
        g_print("\n--->Failed to update melod_around_pro_effect %d\n", err_code);
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    std::tie(err_code, melod_around_pro_effect) = properties_handler->readMeloDAroundProEffect();
    if (err_code == ADK_SI_OK) {
        g_print("\n--->read melod_around_pro_effect %d\n", melod_around_pro_effect);
    } else {
        g_print("\n--->Failed to read melod_around_pro_effect %d\n", err_code);
    }

    bool melod_around_pro_enable;
    std::tie(err_code, melod_around_pro_enable) = properties_handler->readMeloDAroundProEnable();
    if (err_code == ADK_SI_OK) {
        g_print("\n--->read melod_around_pro_enable %d\n", melod_around_pro_enable);
    } else {
        g_print("\n--->Failed to read melod_around_pro_enable %d\n", err_code);
    }
    bool update_melod_around_pro_enable = false;
    err_code = properties_handler->updateMeloDAroundProEnable(update_melod_around_pro_enable);
    if (err_code == ADK_SI_OK) {
        g_print("\n--->updated melod_around_pro_enable %d\n", update_melod_around_pro_enable);
    } else {
        g_print("\n--->Failed to update melod_around_pro_enable %d\n", err_code);
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    std::tie(err_code, melod_around_pro_enable) = properties_handler->readMeloDAroundProEnable();
    if (err_code == ADK_SI_OK) {
        g_print("\n--->read melod_around_pro_enable %d\n", melod_around_pro_enable);
    } else {
        g_print("\n--->Failed to read melod_around_pro_enable %d\n", err_code);
    }

    std::string melod_around_pro_mode;
    std::tie(err_code, melod_around_pro_mode) = properties_handler->readMeloDAroundProMode();
    if (err_code == ADK_SI_OK) {
        g_print("\n--->read melod_around_pro_mode %s\n", melod_around_pro_mode.c_str());
    } else {
        g_print("\n--->Failed to read melod_around_pro_mode %d\n", err_code);
    }
    std::string update_melod_around_pro_mode = "music";
    err_code = properties_handler->updateMeloDAroundProMode(update_melod_around_pro_mode);
    if (err_code == ADK_SI_OK) {
        g_print("\n--->updated melod_around_pro_mode %s\n", update_melod_around_pro_mode.c_str());
    } else {
        g_print("\n--->Failed to update melod_around_pro_mode %d\n", err_code);
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    std::tie(err_code, melod_around_pro_mode) = properties_handler->readMeloDAroundProMode();
    if (err_code == ADK_SI_OK) {
        g_print("\n--->read melod_around_pro_mode %s\n", melod_around_pro_mode.c_str());
    } else {
        g_print("\n--->Failed to read melod_around_pro_mode %d\n", err_code);
    }

    MeloDInt16Range range_melod_bass_management_corner_freq_bass_stage1;
    std::tie(err_code, range_melod_bass_management_corner_freq_bass_stage1) =
        properties_handler->readMeloDBassManagementCornerFreqBassStage1Range();
    if (err_code == ADK_SI_OK) {
        g_print(
            "\n--->read range of melod_bass_management_corner_freq_bass_stage1, min : %d and max : %d\n",
            range_melod_bass_management_corner_freq_bass_stage1.getMin(),
            range_melod_bass_management_corner_freq_bass_stage1.getMax());
    } else {
        g_print("\n--->Failed to read range of melod_bass_management_corner_freq_bass_stage1 %d\n",
            err_code);
    }
    int16_t melod_bass_management_corner_freq_bass_stage1;
    std::tie(err_code, melod_bass_management_corner_freq_bass_stage1) =
        properties_handler->readMeloDBassManagementCornerFreqBassStage1();
    if (err_code == ADK_SI_OK) {
        g_print("\n--->read melod_bass_management_corner_freq_bass_stage1 %d\n",
            melod_bass_management_corner_freq_bass_stage1);
    } else {
        g_print(
            "\n--->Failed to read melod_bass_management_corner_freq_bass_stage1 %d\n", err_code);
    }
    int16_t update_melod_bass_management_corner_freq_bass_stage1 = 150;
    err_code = properties_handler->updateMeloDBassManagementCornerFreqBassStage1(
        update_melod_bass_management_corner_freq_bass_stage1);
    if (err_code == ADK_SI_OK) {
        g_print("\n--->updated melod_bass_management_corner_freq_bass_stage1 %d\n",
            update_melod_bass_management_corner_freq_bass_stage1);
    } else {
        g_print(
            "\n--->Failed to update melod_bass_management_corner_freq_bass_stage1 %d\n", err_code);
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    std::tie(err_code, melod_bass_management_corner_freq_bass_stage1) =
        properties_handler->readMeloDBassManagementCornerFreqBassStage1();
    if (err_code == ADK_SI_OK) {
        g_print("\n--->read melod_bass_management_corner_freq_bass_stage1 %d\n",
            melod_bass_management_corner_freq_bass_stage1);
    } else {
        g_print(
            "\n--->Failed to read melod_bass_management_corner_freq_bass_stage1 %d\n", err_code);
    }

    MeloDInt16Range range_melod_bass_management_corner_freq_bass_stage2;
    std::tie(err_code, range_melod_bass_management_corner_freq_bass_stage2) =
        properties_handler->readMeloDBassManagementCornerFreqBassStage2Range();
    if (err_code == ADK_SI_OK) {
        g_print(
            "\n--->read range of melod_bass_management_corner_freq_bass_stage2, min : %d and max : %d\n",
            range_melod_bass_management_corner_freq_bass_stage2.getMin(),
            range_melod_bass_management_corner_freq_bass_stage2.getMax());
    } else {
        g_print("\n--->Failed to read range of melod_bass_management_corner_freq_bass_stage2 %d\n",
            err_code);
    }
    int16_t melod_bass_management_corner_freq_bass_stage2;
    std::tie(err_code, melod_bass_management_corner_freq_bass_stage2) =
        properties_handler->readMeloDBassManagementCornerFreqBassStage2();
    if (err_code == ADK_SI_OK) {
        g_print("\n--->read melod_bass_management_corner_freq_bass_stage2 %d\n",
            melod_bass_management_corner_freq_bass_stage2);
    } else {
        g_print(
            "\n--->Failed to read melod_bass_management_corner_freq_bass_stage2 %d\n", err_code);
    }
    int16_t update_melod_bass_management_corner_freq_bass_stage2 = 200;
    err_code = properties_handler->updateMeloDBassManagementCornerFreqBassStage2(
        update_melod_bass_management_corner_freq_bass_stage2);
    if (err_code == ADK_SI_OK) {
        g_print("\n--->updated melod_bass_management_corner_freq_bass_stage2 %d\n",
            update_melod_bass_management_corner_freq_bass_stage2);
    } else {
        g_print(
            "\n--->Failed to update melod_bass_management_corner_freq_bass_stage2 %d\n", err_code);
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    std::tie(err_code, melod_bass_management_corner_freq_bass_stage2) =
        properties_handler->readMeloDBassManagementCornerFreqBassStage2();
    if (err_code == ADK_SI_OK) {
        g_print("\n--->read melod_bass_management_corner_freq_bass_stage2 %d\n",
            melod_bass_management_corner_freq_bass_stage2);
    } else {
        g_print(
            "\n--->Failed to read melod_bass_management_corner_freq_bass_stage2 %d\n", err_code);
    }

    bool melod_bass_management_enable;
    std::tie(err_code, melod_bass_management_enable) =
        properties_handler->readMeloDBassManagementEnable();
    if (err_code == ADK_SI_OK) {
        g_print("\n--->read melod_bass_management_enable %d\n", melod_bass_management_enable);
    } else {
        g_print("\n--->Failed to read melod_bass_management_enable %d\n", err_code);
    }
    bool update_melod_bass_management_enable = true;
    err_code =
        properties_handler->updateMeloDBassManagementEnable(update_melod_bass_management_enable);
    if (err_code == ADK_SI_OK) {
        g_print(
            "\n--->updated melod_bass_management_enable %d\n", update_melod_bass_management_enable);
    } else {
        g_print("\n--->Failed to update melod_bass_management_enable %d\n", err_code);
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    std::tie(err_code, melod_bass_management_enable) =
        properties_handler->readMeloDBassManagementEnable();
    if (err_code == ADK_SI_OK) {
        g_print("\n--->read melod_bass_management_enable %d\n", melod_bass_management_enable);
    } else {
        g_print("\n--->Failed to read melod_bass_management_enable %d\n", err_code);
    }

    bool melod_bass_management_spk_surr_height_left;
    std::tie(err_code, melod_bass_management_spk_surr_height_left) =
        properties_handler->readMeloDBassManagementSpkSurrHeightLeft();
    if (err_code == ADK_SI_OK) {
        g_print("\n--->read melod_bass_management_spk_surr_height_left %d\n",
            melod_bass_management_spk_surr_height_left);
    } else {
        g_print("\n--->Failed to read melod_bass_management_spk_surr_height_left %d\n", err_code);
    }
    bool update_melod_bass_management_spk_surr_height_left = true;
    err_code = properties_handler->updateMeloDBassManagementSpkSurrHeightLeft(
        update_melod_bass_management_spk_surr_height_left);
    if (err_code == ADK_SI_OK) {
        g_print("\n--->updated melod_bass_management_spk_surr_height_left %d\n",
            update_melod_bass_management_spk_surr_height_left);
    } else {
        g_print("\n--->Failed to update melod_bass_management_spk_surr_height_left %d\n", err_code);
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    std::tie(err_code, melod_bass_management_spk_surr_height_left) =
        properties_handler->readMeloDBassManagementSpkSurrHeightLeft();
    if (err_code == ADK_SI_OK) {
        g_print("\n--->read melod_bass_management_spk_surr_height_left %d\n",
            melod_bass_management_spk_surr_height_left);
    } else {
        g_print("\n--->Failed to read melod_bass_management_spk_surr_height_left %d\n", err_code);
    }

    bool melod_bass_management_spk_surr_height_right;
    std::tie(err_code, melod_bass_management_spk_surr_height_right) =
        properties_handler->readMeloDBassManagementSpkSurrHeightRight();
    if (err_code == ADK_SI_OK) {
        g_print("\n--->read melod_bass_management_spk_surr_height_right %d\n",
            melod_bass_management_spk_surr_height_right);
    } else {
        g_print("\n--->Failed to read melod_bass_management_spk_surr_height_right %d\n", err_code);
    }
    bool update_melod_bass_management_spk_surr_height_right = true;
    err_code = properties_handler->updateMeloDBassManagementSpkSurrHeightRight(
        update_melod_bass_management_spk_surr_height_right);
    if (err_code == ADK_SI_OK) {
        g_print("\n--->updated melod_bass_management_spk_surr_height_right %d\n",
            update_melod_bass_management_spk_surr_height_right);
    } else {
        g_print(
            "\n--->Failed to update melod_bass_management_spk_surr_height_right %d\n", err_code);
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    std::tie(err_code, melod_bass_management_spk_surr_height_right) =
        properties_handler->readMeloDBassManagementSpkSurrHeightRight();
    if (err_code == ADK_SI_OK) {
        g_print("\n--->read melod_bass_management_spk_surr_height_right %d\n",
            melod_bass_management_spk_surr_height_right);
    } else {
        g_print("\n--->Failed to read melod_bass_management_spk_surr_height_right %d\n", err_code);
    }

    bool melod_bass_management_sub_woofer_out;
    std::tie(err_code, melod_bass_management_sub_woofer_out) =
        properties_handler->readMeloDBassManagementSubWooferOut();
    if (err_code == ADK_SI_OK) {
        g_print("\n--->read melod_bass_management_sub_woofer_out %d\n",
            melod_bass_management_sub_woofer_out);
    } else {
        g_print("\n--->Failed to read melod_bass_management_sub_woofer_out %d\n", err_code);
    }
    bool update_melod_bass_management_sub_woofer_out = true;
    err_code = properties_handler->updateMeloDBassManagementSubWooferOut(
        update_melod_bass_management_sub_woofer_out);
    if (err_code == ADK_SI_OK) {
        g_print("\n--->updated melod_bass_management_sub_woofer_out %d\n",
            update_melod_bass_management_sub_woofer_out);
    } else {
        g_print("\n--->Failed to update melod_bass_management_sub_woofer_out %d\n", err_code);
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    std::tie(err_code, melod_bass_management_sub_woofer_out) =
        properties_handler->readMeloDBassManagementSubWooferOut();
    if (err_code == ADK_SI_OK) {
        g_print("\n--->read melod_bass_management_sub_woofer_out %d\n",
            melod_bass_management_sub_woofer_out);
    } else {
        g_print("\n--->Failed to read melod_bass_management_sub_woofer_out %d\n", err_code);
    }

    std::string melod_bass_management_surround_speaker_left;
    std::tie(err_code, melod_bass_management_surround_speaker_left) =
        properties_handler->readMeloDBassManagementSurroundSpeakerLeft();
    if (err_code == ADK_SI_OK) {
        g_print("\n--->read melod_bass_management_surround_speaker_left %s\n",
            melod_bass_management_surround_speaker_left.c_str());
    } else {
        g_print("\n--->Failed to read melod_bass_management_surround_speaker_left %d\n", err_code);
    }
    std::string update_melod_bass_management_surround_speaker_left = "large";
    err_code = properties_handler->updateMeloDBassManagementSurroundSpeakerLeft(
        update_melod_bass_management_surround_speaker_left);
    if (err_code == ADK_SI_OK) {
        g_print("\n--->updated melod_bass_management_surround_speaker_left %s\n",
            update_melod_bass_management_surround_speaker_left.c_str());
    } else {
        g_print(
            "\n--->Failed to update melod_bass_management_surround_speaker_left %d\n", err_code);
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    std::tie(err_code, melod_bass_management_surround_speaker_left) =
        properties_handler->readMeloDBassManagementSurroundSpeakerLeft();
    if (err_code == ADK_SI_OK) {
        g_print("\n--->read melod_bass_management_surround_speaker_left %s\n",
            melod_bass_management_surround_speaker_left.c_str());
    } else {
        g_print("\n--->Failed to read melod_bass_management_surround_speaker_left %d\n", err_code);
    }

    std::string melod_bass_management_surround_speaker_right;
    std::tie(err_code, melod_bass_management_surround_speaker_right) =
        properties_handler->readMeloDBassManagementSurroundSpeakerRight();
    if (err_code == ADK_SI_OK) {
        g_print("\n--->read melod_bass_management_surround_speaker_right %s\n",
            melod_bass_management_surround_speaker_right.c_str());
    } else {
        g_print("\n--->Failed to read melod_bass_management_surround_speaker_right %d\n", err_code);
    }
    std::string update_melod_bass_management_surround_speaker_right = "large";
    err_code = properties_handler->updateMeloDBassManagementSurroundSpeakerRight(
        update_melod_bass_management_surround_speaker_right);
    if (err_code == ADK_SI_OK) {
        g_print("\n--->updated melod_bass_management_surround_speaker_right %s\n",
            update_melod_bass_management_surround_speaker_right.c_str());
    } else {
        g_print(
            "\n--->Failed to update melod_bass_management_surround_speaker_right %d\n", err_code);
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    std::tie(err_code, melod_bass_management_surround_speaker_right) =
        properties_handler->readMeloDBassManagementSurroundSpeakerRight();
    if (err_code == ADK_SI_OK) {
        g_print("\n--->read melod_bass_management_surround_speaker_right %s\n",
            melod_bass_management_surround_speaker_right.c_str());
    } else {
        g_print("\n--->Failed to read melod_bass_management_surround_speaker_right %d\n", err_code);
    }

    bool melod_drcenable;
    std::tie(err_code, melod_drcenable) = properties_handler->readMeloDDRCEnable();
    if (err_code == ADK_SI_OK) {
        g_print("\n--->read melod_drcenable %d\n", melod_drcenable);
    } else {
        g_print("\n--->Failed to read melod_drcenable %d\n", err_code);
    }
    bool update_melod_drcenable = false;
    err_code = properties_handler->updateMeloDDRCEnable(update_melod_drcenable);
    if (err_code == ADK_SI_OK) {
        g_print("\n--->updated melod_drcenable %d\n", update_melod_drcenable);
    } else {
        g_print("\n--->Failed to update melod_drcenable %d\n", err_code);
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    std::tie(err_code, melod_drcenable) = properties_handler->readMeloDDRCEnable();
    if (err_code == ADK_SI_OK) {
        g_print("\n--->read melod_drcenable %d\n", melod_drcenable);
    } else {
        g_print("\n--->Failed to read melod_drcenable %d\n", err_code);
    }

    std::string melod_drcmode;
    std::tie(err_code, melod_drcmode) = properties_handler->readMeloDDRCMode();
    if (err_code == ADK_SI_OK) {
        g_print("\n--->read melod_drcmode %s\n", melod_drcmode.c_str());
    } else {
        g_print("\n--->Failed to read melod_drcmode %d\n", err_code);
    }
    std::string update_melod_drcmode = "volume_plus";
    err_code = properties_handler->updateMeloDDRCMode(update_melod_drcmode);
    if (err_code == ADK_SI_OK) {
        g_print("\n--->updated melod_drcmode %s\n", update_melod_drcmode.c_str());
    } else {
        g_print("\n--->Failed to update melod_drcmode %d\n", err_code);
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    std::tie(err_code, melod_drcmode) = properties_handler->readMeloDDRCMode();
    if (err_code == ADK_SI_OK) {
        g_print("\n--->read melod_drcmode %s\n", melod_drcmode.c_str());
    } else {
        g_print("\n--->Failed to read melod_drcmode %d\n", err_code);
    }

    MeloDInt16Range range_melod_drcref_level;
    std::tie(err_code, range_melod_drcref_level) = properties_handler->readMeloDDRCRefLevelRange();
    if (err_code == ADK_SI_OK) {
        g_print("\n--->read range of melod_drcref_level, min : %d and max : %d\n",
            range_melod_drcref_level.getMin(), range_melod_drcref_level.getMax());
    } else {
        g_print("\n--->Failed to read range of melod_drcref_level %d\n", err_code);
    }
    int16_t melod_drcref_level;
    std::tie(err_code, melod_drcref_level) = properties_handler->readMeloDDRCRefLevel();
    if (err_code == ADK_SI_OK) {
        g_print("\n--->read melod_drcref_level %d\n", melod_drcref_level);
    } else {
        g_print("\n--->Failed to read melod_drcref_level %d\n", err_code);
    }
    int16_t update_melod_drcref_level = -38;
    err_code = properties_handler->updateMeloDDRCRefLevel(update_melod_drcref_level);
    if (err_code == ADK_SI_OK) {
        g_print("\n--->updated melod_drcref_level %d\n", update_melod_drcref_level);
    } else {
        g_print("\n--->Failed to update melod_drcref_level %d\n", err_code);
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    std::tie(err_code, melod_drcref_level) = properties_handler->readMeloDDRCRefLevel();
    if (err_code == ADK_SI_OK) {
        g_print("\n--->read melod_drcref_level %d\n", melod_drcref_level);
    } else {
        g_print("\n--->Failed to read melod_drcref_level %d\n", err_code);
    }

    bool melod_matrix_enable;
    std::tie(err_code, melod_matrix_enable) = properties_handler->readMeloDMatrixEnable();
    if (err_code == ADK_SI_OK) {
        g_print("\n--->read melod_matrix_enable %d\n", melod_matrix_enable);
    } else {
        g_print("\n--->Failed to read melod_matrix_enable %d\n", err_code);
    }
    bool update_melod_matrix_enable = false;
    err_code = properties_handler->updateMeloDMatrixEnable(update_melod_matrix_enable);
    if (err_code == ADK_SI_OK) {
        g_print("\n--->updated melod_matrix_enable %d\n", update_melod_matrix_enable);
    } else {
        g_print("\n--->Failed to update melod_matrix_enable %d\n", err_code);
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    std::tie(err_code, melod_matrix_enable) = properties_handler->readMeloDMatrixEnable();
    if (err_code == ADK_SI_OK) {
        g_print("\n--->read melod_matrix_enable %d\n", melod_matrix_enable);
    } else {
        g_print("\n--->Failed to read melod_matrix_enable %d\n", err_code);
    }

    std::string melod_matrix_mode;
    std::tie(err_code, melod_matrix_mode) = properties_handler->readMeloDMatrixMode();
    if (err_code == ADK_SI_OK) {
        g_print("\n--->read melod_matrix_mode %s\n", melod_matrix_mode.c_str());
    } else {
        g_print("\n--->Failed to read melod_matrix_mode %d\n", err_code);
    }
    std::string update_melod_matrix_mode = "music";
    err_code = properties_handler->updateMeloDMatrixMode(update_melod_matrix_mode);
    if (err_code == ADK_SI_OK) {
        g_print("\n--->updated melod_matrix_mode %s\n", update_melod_matrix_mode.c_str());
    } else {
        g_print("\n--->Failed to update melod_matrix_mode %d\n", err_code);
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    std::tie(err_code, melod_matrix_mode) = properties_handler->readMeloDMatrixMode();
    if (err_code == ADK_SI_OK) {
        g_print("\n--->read melod_matrix_mode %s\n", melod_matrix_mode.c_str());
    } else {
        g_print("\n--->Failed to read melod_matrix_mode %d\n", err_code);
    }

    MeloDUint16Range range_melod_bass_plus_bass_content;
    std::tie(err_code, range_melod_bass_plus_bass_content) =
        properties_handler->readMeloDBassPlusBassContentRange();
    if (err_code == ADK_SI_OK) {
        g_print("\n--->read range of melod_bass_plus_bass_content, min : %d and max : %d\n",
            range_melod_bass_plus_bass_content.getMin(),
            range_melod_bass_plus_bass_content.getMax());
    } else {
        g_print("\n--->Failed to read range of melod_bass_plus_bass_content %d\n", err_code);
    }
    uint16_t melod_bass_plus_bass_content;
    std::tie(err_code, melod_bass_plus_bass_content) =
        properties_handler->readMeloDBassPlusBassContent();
    if (err_code == ADK_SI_OK) {
        g_print("\n--->read melod_bass_plus_bass_content %d\n", melod_bass_plus_bass_content);
    } else {
        g_print("\n--->Failed to read melod_bass_plus_bass_content %d\n", err_code);
    }
    uint16_t update_melod_bass_plus_bass_content = 5000;
    err_code =
        properties_handler->updateMeloDBassPlusBassContent(update_melod_bass_plus_bass_content);
    if (err_code == ADK_SI_OK) {
        g_print(
            "\n--->updated melod_bass_plus_bass_content %d\n", update_melod_bass_plus_bass_content);
    } else {
        g_print("\n--->Failed to update melod_bass_plus_bass_content %d\n", err_code);
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    std::tie(err_code, melod_bass_plus_bass_content) =
        properties_handler->readMeloDBassPlusBassContent();
    if (err_code == ADK_SI_OK) {
        g_print("\n--->read melod_bass_plus_bass_content %d\n", melod_bass_plus_bass_content);
    } else {
        g_print("\n--->Failed to read melod_bass_plus_bass_content %d\n", err_code);
    }

    bool melod_bass_plus_enable;
    std::tie(err_code, melod_bass_plus_enable) = properties_handler->readMeloDBassPlusEnable();
    if (err_code == ADK_SI_OK) {
        g_print("\n--->read melod_bass_plus_enable %d\n", melod_bass_plus_enable);
    } else {
        g_print("\n--->Failed to read melod_bass_plus_enable %d\n", err_code);
    }
    bool update_melod_bass_plus_enable = true;
    err_code = properties_handler->updateMeloDBassPlusEnable(update_melod_bass_plus_enable);
    if (err_code == ADK_SI_OK) {
        g_print("\n--->updated melod_bass_plus_enable %d\n", update_melod_bass_plus_enable);
    } else {
        g_print("\n--->Failed to update melod_bass_plus_enable %d\n", err_code);
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    std::tie(err_code, melod_bass_plus_enable) = properties_handler->readMeloDBassPlusEnable();
    if (err_code == ADK_SI_OK) {
        g_print("\n--->read melod_bass_plus_enable %d\n", melod_bass_plus_enable);
    } else {
        g_print("\n--->Failed to read melod_bass_plus_enable %d\n", err_code);
    }

    MeloDUint16Range range_melod_bass_plus_harmonics_content;
    std::tie(err_code, range_melod_bass_plus_harmonics_content) =
        properties_handler->readMeloDBassPlusHarmonicsContentRange();
    if (err_code == ADK_SI_OK) {
        g_print("\n--->read range of melod_bass_plus_harmonics_content, min : %d and max : %d\n",
            range_melod_bass_plus_harmonics_content.getMin(),
            range_melod_bass_plus_harmonics_content.getMax());
    } else {
        g_print("\n--->Failed to read range of melod_bass_plus_harmonics_content %d\n", err_code);
    }
    uint16_t melod_bass_plus_harmonics_content;
    std::tie(err_code, melod_bass_plus_harmonics_content) =
        properties_handler->readMeloDBassPlusHarmonicsContent();
    if (err_code == ADK_SI_OK) {
        g_print(
            "\n--->read melod_bass_plus_harmonics_content %d\n", melod_bass_plus_harmonics_content);
    } else {
        g_print("\n--->Failed to read melod_bass_plus_harmonics_content %d\n", err_code);
    }
    uint16_t update_melod_bass_plus_harmonics_content = 1000;
    err_code = properties_handler->updateMeloDBassPlusHarmonicsContent(
        update_melod_bass_plus_harmonics_content);
    if (err_code == ADK_SI_OK) {
        g_print("\n--->updated melod_bass_plus_harmonics_content %d\n",
            update_melod_bass_plus_harmonics_content);
    } else {
        g_print("\n--->Failed to update melod_bass_plus_harmonics_content %d\n", err_code);
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    std::tie(err_code, melod_bass_plus_harmonics_content) =
        properties_handler->readMeloDBassPlusHarmonicsContent();
    if (err_code == ADK_SI_OK) {
        g_print(
            "\n--->read melod_bass_plus_harmonics_content %d\n", melod_bass_plus_harmonics_content);
    } else {
        g_print("\n--->Failed to read melod_bass_plus_harmonics_content %d\n", err_code);
    }

    bool melod_multi_band_limiter_enable;
    std::tie(err_code, melod_multi_band_limiter_enable) =
        properties_handler->readMeloDMultiBandLimiterEnable();
    if (err_code == ADK_SI_OK) {
        g_print("\n--->read melod_multi_band_limiter_enable %d\n", melod_multi_band_limiter_enable);
    } else {
        g_print("\n--->Failed to read melod_multi_band_limiter_enable %d\n", err_code);
    }
    bool update_melod_multi_band_limiter_enable = true;
    err_code = properties_handler->updateMeloDMultiBandLimiterEnable(
        update_melod_multi_band_limiter_enable);
    if (err_code == ADK_SI_OK) {
        g_print("\n--->updated melod_multi_band_limiter_enable %d\n",
            update_melod_multi_band_limiter_enable);
    } else {
        g_print("\n--->Failed to update melod_multi_band_limiter_enable %d\n", err_code);
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    std::tie(err_code, melod_multi_band_limiter_enable) =
        properties_handler->readMeloDMultiBandLimiterEnable();
    if (err_code == ADK_SI_OK) {
        g_print("\n--->read melod_multi_band_limiter_enable %d\n", melod_multi_band_limiter_enable);
    } else {
        g_print("\n--->Failed to read melod_multi_band_limiter_enable %d\n", err_code);
    }

    MeloDUint16Range range_melod_voice_plus_effect_strength;
    std::tie(err_code, range_melod_voice_plus_effect_strength) =
        properties_handler->readMeloDVoicePlusEffectStrengthRange();
    if (err_code == ADK_SI_OK) {
        g_print("\n--->read range of melod_voice_plus_effect_strength, min : %d and max : %d\n",
            range_melod_voice_plus_effect_strength.getMin(),
            range_melod_voice_plus_effect_strength.getMax());
    } else {
        g_print("\n--->Failed to read range of melod_voice_plus_effect_strength %d\n", err_code);
    }
    uint16_t melod_voice_plus_effect_strength;
    std::tie(err_code, melod_voice_plus_effect_strength) =
        properties_handler->readMeloDVoicePlusEffectStrength();
    if (err_code == ADK_SI_OK) {
        g_print(
            "\n--->read melod_voice_plus_effect_strength %d\n", melod_voice_plus_effect_strength);
    } else {
        g_print("\n--->Failed to read melod_voice_plus_effect_strength %d\n", err_code);
    }
    uint16_t update_melod_voice_plus_effect_strength = 24;
    err_code = properties_handler->updateMeloDVoicePlusEffectStrength(
        update_melod_voice_plus_effect_strength);
    if (err_code == ADK_SI_OK) {
        g_print("\n--->updated melod_voice_plus_effect_strength %d\n",
            update_melod_voice_plus_effect_strength);
    } else {
        g_print("\n--->Failed to update melod_voice_plus_effect_strength %d\n", err_code);
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    std::tie(err_code, melod_voice_plus_effect_strength) =
        properties_handler->readMeloDVoicePlusEffectStrength();
    if (err_code == ADK_SI_OK) {
        g_print(
            "\n--->read melod_voice_plus_effect_strength %d\n", melod_voice_plus_effect_strength);
    } else {
        g_print("\n--->Failed to read melod_voice_plus_effect_strength %d\n", err_code);
    }

    bool melod_voice_plus_enable;
    std::tie(err_code, melod_voice_plus_enable) = properties_handler->readMeloDVoicePlusEnable();
    if (err_code == ADK_SI_OK) {
        g_print("\n--->read melod_voice_plus_enable %d\n", melod_voice_plus_enable);
    } else {
        g_print("\n--->Failed to read melod_voice_plus_enable %d\n", err_code);
    }
    bool update_melod_voice_plus_enable = true;
    err_code = properties_handler->updateMeloDVoicePlusEnable(update_melod_voice_plus_enable);
    if (err_code == ADK_SI_OK) {
        g_print("\n--->updated melod_voice_plus_enable %d\n", update_melod_voice_plus_enable);
    } else {
        g_print("\n--->Failed to update melod_voice_plus_enable %d\n", err_code);
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    std::tie(err_code, melod_voice_plus_enable) = properties_handler->readMeloDVoicePlusEnable();
    if (err_code == ADK_SI_OK) {
        g_print("\n--->read melod_voice_plus_enable %d\n", melod_voice_plus_enable);
    } else {
        g_print("\n--->Failed to read melod_voice_plus_enable %d\n", err_code);
    }

    bool melod_xover_filter_enable;
    std::tie(err_code, melod_xover_filter_enable) =
        properties_handler->readMeloDXOverFilterEnable();
    if (err_code == ADK_SI_OK) {
        g_print("\n--->read melod_xover_filter_enable %d\n", melod_xover_filter_enable);
    } else {
        g_print("\n--->Failed to read melod_xover_filter_enable %d\n", err_code);
    }
    bool update_melod_xover_filter_enable = true;
    err_code = properties_handler->updateMeloDXOverFilterEnable(update_melod_xover_filter_enable);
    if (err_code == ADK_SI_OK) {
        g_print("\n--->updated melod_xover_filter_enable %d\n", update_melod_xover_filter_enable);
    } else {
        g_print("\n--->Failed to update melod_xover_filter_enable %d\n", err_code);
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    std::tie(err_code, melod_xover_filter_enable) =
        properties_handler->readMeloDXOverFilterEnable();
    if (err_code == ADK_SI_OK) {
        g_print("\n--->read melod_xover_filter_enable %d\n", melod_xover_filter_enable);
    } else {
        g_print("\n--->Failed to read melod_xover_filter_enable %d\n", err_code);
    }

    /* Run mainloop */
    siginfo_t sig_info;
    int sig;
    do {
        int tries = 0;
        do {
            sig = sigwaitinfo(&sig_set, &sig_info);
            if (sig < 0) {
                if (errno == EINTR) {
                    // spurious wakeup, ignore
                    continue;
                }
                g_print("Error on sigwaitinfo errno %d %s, retrying \n", errno, strerror(errno));
                tries++;
                std::this_thread::sleep_for(std::chrono::seconds(1));
            } else {
                break;
            }
        } while (tries < 5);
        if (sig < 0) {
            g_print("Error waiting signal, exiting \n");
            return false;
        }
    } while ((sig != SIGTERM) && (sig != SIGINT) && (sig != SIGHUP));
    g_print("exit signal wait \n");

    return true;
}

int main() {
    return startAdkSystemInterface() ? 0 : 1;
}
