/*
 * Copyright (c) 2019-2021, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of The Linux Foundation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef AUTOGEN_ATTRIBUTE_STRINGS_H_
#define AUTOGEN_ATTRIBUTE_STRINGS_H_

namespace adk {
const char kMin[] = "min";
const char kMax[] = "max";
const char kMeloDAroundProEffect[] = "meloDAroundProEffect";
const char kMeloDAroundProEnable[] = "meloDAroundProEnable";
const char kMeloDAroundProMode[] = "meloDAroundProMode";
const char kMeloDBassManagementCornerFreqBassStage1[] = "meloDBassManagementCornerFreqBassStage1";
const char kMeloDBassManagementCornerFreqBassStage1Range[] = "meloDBassManagementCornerFreqBassStage1Range";
const char kMeloDBassManagementCornerFreqBassStage2[] = "meloDBassManagementCornerFreqBassStage2";
const char kMeloDBassManagementCornerFreqBassStage2Range[] = "meloDBassManagementCornerFreqBassStage2Range";
const char kMeloDBassManagementEnable[] = "meloDBassManagementEnable";
const char kMeloDBassManagementSpkSurrHeightLeft[] = "meloDBassManagementSpkSurrHeightLeft";
const char kMeloDBassManagementSpkSurrHeightRight[] = "meloDBassManagementSpkSurrHeightRight";
const char kMeloDBassManagementSubWooferOut[] = "meloDBassManagementSubWooferOut";
const char kMeloDBassManagementSurroundSpeakerLeft[] = "meloDBassManagementSurroundSpeakerLeft";
const char kMeloDBassManagementSurroundSpeakerRight[] = "meloDBassManagementSurroundSpeakerRight";
const char kMeloDDRCEnable[] = "meloDDRCEnable";
const char kMeloDDRCMode[] = "meloDDRCMode";
const char kMeloDDRCRefLevel[] = "meloDDRCRefLevel";
const char kMeloDDRCRefLevelRange[] = "meloDDRCRefLevelRange";
const char kMeloDMatrixEnable[] = "meloDMatrixEnable";
const char kMeloDMatrixMode[] = "meloDMatrixMode";
const char kMeloDBassPlusBassContent[] = "meloDBassPlusBassContent";
const char kMeloDBassPlusBassContentRange[] = "meloDBassPlusBassContentRange";
const char kMeloDBassPlusEnable[] = "meloDBassPlusEnable";
const char kMeloDBassPlusHarmonicsContent[] = "meloDBassPlusHarmonicsContent";
const char kMeloDBassPlusHarmonicsContentRange[] = "meloDBassPlusHarmonicsContentRange";
const char kMeloDMultiBandLimiterEnable[] = "meloDMultiBandLimiterEnable";
const char kMeloDVoicePlusEffectStrength[] = "meloDVoicePlusEffectStrength";
const char kMeloDVoicePlusEffectStrengthRange[] = "meloDVoicePlusEffectStrengthRange";
const char kMeloDVoicePlusEnable[] = "meloDVoicePlusEnable";
const char kMeloDXOverFilterEnable[] = "meloDXOverFilterEnable";
}  // namespace adk

#endif  // AUTOGEN_ATTRIBUTE_STRINGS_H_
