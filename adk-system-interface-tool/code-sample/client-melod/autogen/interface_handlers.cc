
/*
 * Copyright (c) 2019-2021, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of The Linux Foundation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "interface_handlers.h"

namespace adk {

PropertiesHandler::PropertiesHandler(Dbus *dbus)
    : dbus_(dbus) {
    dbus_->setPropertiesChangedListener(
        std::bind(&PropertiesHandler::propertiesChangedCb, this,
            std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
}

void PropertiesHandler::propertiesChangedCb(const std::string &obj_path,
    const std::string &iface_name, GVariantIter *iter) {
    g_print("PropertiesHandler::propertiesChangedCb \n");
    if (iface_name == std::string(adk::dbus::kAudiomanagerMelodIface)) {
        PropertiesAttr changed_properties = processAudiomanagerMelodIfaceResponse(obj_path, iter);
        for (auto &f : properties_changed_listeners_) {
            f(changed_properties, obj_path);
        }
        return;
    }
}

PropertiesAttr PropertiesHandler::processAudiomanagerMelodIfaceResponse(
    const std::string &obj_path, GVariantIter *iter) {
    g_print("PropertiesHandler::processKAudiomanagerMelodIfaceResponse obj_path: %s \n", obj_path.c_str());
    char *key;
    GVariant *tmp = nullptr;
    PropertiesAttr changed_properties;
    bool obj_id_map_changed = false;

    std::lock_guard<std::mutex> lock(mutex_);
    changed_properties.clearChangedProperties();
    while (g_variant_iter_loop(iter, "{sv}", &key, &tmp)) {
        if (std::string(key) == "AroundProEffect") {
            changed_properties.setMeloDAroundProEffect(g_variant_get_uint16(tmp));
            continue;
        }
        if (std::string(key) == "AroundProEnable") {
            changed_properties.setMeloDAroundProEnable(g_variant_get_boolean(tmp));
            continue;
        }
        if (std::string(key) == "AroundProMode") {
            gsize length;
            changed_properties.setMeloDAroundProMode(static_cast<std::string>(g_variant_get_string(tmp, &length)));
            continue;
        }
        if (std::string(key) == "BassManagementCornerFreqBassStage1") {
            changed_properties.setMeloDBassManagementCornerFreqBassStage1(g_variant_get_int16(tmp));
            continue;
        }
        if (std::string(key) == "BassManagementCornerFreqBassStage1Range") {
            GVariantIter property_iter;
            g_variant_iter_init(&property_iter, tmp);
            changed_properties.setMeloDBassManagementCornerFreqBassStage1Range(dbus_->parseMeloDInt16Range(property_iter));
            continue;
        }
        if (std::string(key) == "BassManagementCornerFreqBassStage2") {
            changed_properties.setMeloDBassManagementCornerFreqBassStage2(g_variant_get_int16(tmp));
            continue;
        }
        if (std::string(key) == "BassManagementCornerFreqBassStage2Range") {
            GVariantIter property_iter;
            g_variant_iter_init(&property_iter, tmp);
            changed_properties.setMeloDBassManagementCornerFreqBassStage2Range(dbus_->parseMeloDInt16Range(property_iter));
            continue;
        }
        if (std::string(key) == "BassManagementEnable") {
            changed_properties.setMeloDBassManagementEnable(g_variant_get_boolean(tmp));
            continue;
        }
        if (std::string(key) == "BassManagementSpkSurrHeightLeft") {
            changed_properties.setMeloDBassManagementSpkSurrHeightLeft(g_variant_get_boolean(tmp));
            continue;
        }
        if (std::string(key) == "BassManagementSpkSurrHeightRight") {
            changed_properties.setMeloDBassManagementSpkSurrHeightRight(g_variant_get_boolean(tmp));
            continue;
        }
        if (std::string(key) == "BassManagementSubWooferOut") {
            changed_properties.setMeloDBassManagementSubWooferOut(g_variant_get_boolean(tmp));
            continue;
        }
        if (std::string(key) == "BassManagementSurroundSpeakerLeft") {
            gsize length;
            changed_properties.setMeloDBassManagementSurroundSpeakerLeft(static_cast<std::string>(g_variant_get_string(tmp, &length)));
            continue;
        }
        if (std::string(key) == "BassManagementSurroundSpeakerRight") {
            gsize length;
            changed_properties.setMeloDBassManagementSurroundSpeakerRight(static_cast<std::string>(g_variant_get_string(tmp, &length)));
            continue;
        }
        if (std::string(key) == "DRCEnable") {
            changed_properties.setMeloDDRCEnable(g_variant_get_boolean(tmp));
            continue;
        }
        if (std::string(key) == "DRCMode") {
            gsize length;
            changed_properties.setMeloDDRCMode(static_cast<std::string>(g_variant_get_string(tmp, &length)));
            continue;
        }
        if (std::string(key) == "DRCRefLevel") {
            changed_properties.setMeloDDRCRefLevel(g_variant_get_int16(tmp));
            continue;
        }
        if (std::string(key) == "DRCRefLevelRange") {
            GVariantIter property_iter;
            g_variant_iter_init(&property_iter, tmp);
            changed_properties.setMeloDDRCRefLevelRange(dbus_->parseMeloDInt16Range(property_iter));
            continue;
        }
        if (std::string(key) == "MatrixEnable") {
            changed_properties.setMeloDMatrixEnable(g_variant_get_boolean(tmp));
            continue;
        }
        if (std::string(key) == "MatrixMode") {
            gsize length;
            changed_properties.setMeloDMatrixMode(static_cast<std::string>(g_variant_get_string(tmp, &length)));
            continue;
        }
        if (std::string(key) == "BassPlusBassContent") {
            changed_properties.setMeloDBassPlusBassContent(g_variant_get_uint16(tmp));
            continue;
        }
        if (std::string(key) == "BassPlusBassContentRange") {
            GVariantIter property_iter;
            g_variant_iter_init(&property_iter, tmp);
            changed_properties.setMeloDBassPlusBassContentRange(dbus_->parseMeloDUint16Range(property_iter));
            continue;
        }
        if (std::string(key) == "BassPlusEnable") {
            changed_properties.setMeloDBassPlusEnable(g_variant_get_boolean(tmp));
            continue;
        }
        if (std::string(key) == "BassPlusHarmonicsContent") {
            changed_properties.setMeloDBassPlusHarmonicsContent(g_variant_get_uint16(tmp));
            continue;
        }
        if (std::string(key) == "BassPlusHarmonicsContentRange") {
            GVariantIter property_iter;
            g_variant_iter_init(&property_iter, tmp);
            changed_properties.setMeloDBassPlusHarmonicsContentRange(dbus_->parseMeloDUint16Range(property_iter));
            continue;
        }
        if (std::string(key) == "MultiBandLimiterEnable") {
            changed_properties.setMeloDMultiBandLimiterEnable(g_variant_get_boolean(tmp));
            continue;
        }
        if (std::string(key) == "VoicePlusEffectStrength") {
            changed_properties.setMeloDVoicePlusEffectStrength(g_variant_get_uint16(tmp));
            continue;
        }
        if (std::string(key) == "VoicePlusEffectStrengthRange") {
            GVariantIter property_iter;
            g_variant_iter_init(&property_iter, tmp);
            changed_properties.setMeloDVoicePlusEffectStrengthRange(dbus_->parseMeloDUint16Range(property_iter));
            continue;
        }
        if (std::string(key) == "VoicePlusEnable") {
            changed_properties.setMeloDVoicePlusEnable(g_variant_get_boolean(tmp));
            continue;
        }
        if (std::string(key) == "XOverFilterEnable") {
            changed_properties.setMeloDXOverFilterEnable(g_variant_get_boolean(tmp));
            continue;
        }
    }

    if (tmp) {
        g_variant_unref(tmp);
    }
    if (obj_id_map_changed) {
        for (auto &f : obj_id_map_changed_listeners_) {
            f(obj_id_map_);
        }
    }
    return changed_properties;
}

std::tuple<ErrorCode, uint16_t> PropertiesHandler::readMeloDAroundProEffect() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    uint16_t melo_daround_pro_effect;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ss)",
            adk::dbus::kAudiomanagerMelodIface,
            "AroundProEffect"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        melo_daround_pro_effect = g_variant_get_uint16(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, melo_daround_pro_effect);
}

ErrorCode PropertiesHandler::updateMeloDAroundProEffect(
    uint16_t melo_daround_pro_effect) {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ssv)",
            adk::dbus::kAudiomanagerMelodIface,
            "AroundProEffect",
            g_variant_new("q", melo_daround_pro_effect)));
    return result;
}
std::tuple<ErrorCode, bool> PropertiesHandler::readMeloDAroundProEnable() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    bool melo_daround_pro_enable;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ss)",
            adk::dbus::kAudiomanagerMelodIface,
            "AroundProEnable"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        melo_daround_pro_enable = g_variant_get_boolean(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, melo_daround_pro_enable);
}

ErrorCode PropertiesHandler::updateMeloDAroundProEnable(
    bool melo_daround_pro_enable) {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ssv)",
            adk::dbus::kAudiomanagerMelodIface,
            "AroundProEnable",
            g_variant_new("b", static_cast<uint8_t>(melo_daround_pro_enable))));
    return result;
}
std::tuple<ErrorCode, std::string> PropertiesHandler::readMeloDAroundProMode() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::string melo_daround_pro_mode;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ss)",
            adk::dbus::kAudiomanagerMelodIface,
            "AroundProMode"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        gsize length;
        melo_daround_pro_mode = static_cast<std::string>(g_variant_get_string(tmp, &length));
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, melo_daround_pro_mode);
}

ErrorCode PropertiesHandler::updateMeloDAroundProMode(
    std::string melo_daround_pro_mode) {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ssv)",
            adk::dbus::kAudiomanagerMelodIface,
            "AroundProMode",
            g_variant_new("s", melo_daround_pro_mode.c_str())));
    return result;
}
std::tuple<ErrorCode, int16_t> PropertiesHandler::readMeloDBassManagementCornerFreqBassStage1() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int16_t melo_dbass_management_corner_freq_bass_stage1;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ss)",
            adk::dbus::kAudiomanagerMelodIface,
            "BassManagementCornerFreqBassStage1"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        melo_dbass_management_corner_freq_bass_stage1 = g_variant_get_int16(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, melo_dbass_management_corner_freq_bass_stage1);
}

ErrorCode PropertiesHandler::updateMeloDBassManagementCornerFreqBassStage1(
    int16_t melo_dbass_management_corner_freq_bass_stage1) {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ssv)",
            adk::dbus::kAudiomanagerMelodIface,
            "BassManagementCornerFreqBassStage1",
            g_variant_new("n", melo_dbass_management_corner_freq_bass_stage1)));
    return result;
}
std::tuple<ErrorCode, MeloDInt16Range> PropertiesHandler::readMeloDBassManagementCornerFreqBassStage1Range() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    MeloDInt16Range melo_dbass_management_corner_freq_bass_stage1_range;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ss)",
            adk::dbus::kAudiomanagerMelodIface,
            "BassManagementCornerFreqBassStage1Range"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        GVariantIter property_iter;
        g_variant_iter_init(&property_iter, tmp);
        melo_dbass_management_corner_freq_bass_stage1_range = dbus_->parseMeloDInt16Range(property_iter);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, melo_dbass_management_corner_freq_bass_stage1_range);
}

std::tuple<ErrorCode, int16_t> PropertiesHandler::readMeloDBassManagementCornerFreqBassStage2() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int16_t melo_dbass_management_corner_freq_bass_stage2;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ss)",
            adk::dbus::kAudiomanagerMelodIface,
            "BassManagementCornerFreqBassStage2"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        melo_dbass_management_corner_freq_bass_stage2 = g_variant_get_int16(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, melo_dbass_management_corner_freq_bass_stage2);
}

ErrorCode PropertiesHandler::updateMeloDBassManagementCornerFreqBassStage2(
    int16_t melo_dbass_management_corner_freq_bass_stage2) {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ssv)",
            adk::dbus::kAudiomanagerMelodIface,
            "BassManagementCornerFreqBassStage2",
            g_variant_new("n", melo_dbass_management_corner_freq_bass_stage2)));
    return result;
}
std::tuple<ErrorCode, MeloDInt16Range> PropertiesHandler::readMeloDBassManagementCornerFreqBassStage2Range() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    MeloDInt16Range melo_dbass_management_corner_freq_bass_stage2_range;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ss)",
            adk::dbus::kAudiomanagerMelodIface,
            "BassManagementCornerFreqBassStage2Range"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        GVariantIter property_iter;
        g_variant_iter_init(&property_iter, tmp);
        melo_dbass_management_corner_freq_bass_stage2_range = dbus_->parseMeloDInt16Range(property_iter);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, melo_dbass_management_corner_freq_bass_stage2_range);
}

std::tuple<ErrorCode, bool> PropertiesHandler::readMeloDBassManagementEnable() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    bool melo_dbass_management_enable;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ss)",
            adk::dbus::kAudiomanagerMelodIface,
            "BassManagementEnable"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        melo_dbass_management_enable = g_variant_get_boolean(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, melo_dbass_management_enable);
}

ErrorCode PropertiesHandler::updateMeloDBassManagementEnable(
    bool melo_dbass_management_enable) {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ssv)",
            adk::dbus::kAudiomanagerMelodIface,
            "BassManagementEnable",
            g_variant_new("b", static_cast<uint8_t>(melo_dbass_management_enable))));
    return result;
}
std::tuple<ErrorCode, bool> PropertiesHandler::readMeloDBassManagementSpkSurrHeightLeft() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    bool melo_dbass_management_spk_surr_height_left;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ss)",
            adk::dbus::kAudiomanagerMelodIface,
            "BassManagementSpkSurrHeightLeft"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        melo_dbass_management_spk_surr_height_left = g_variant_get_boolean(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, melo_dbass_management_spk_surr_height_left);
}

ErrorCode PropertiesHandler::updateMeloDBassManagementSpkSurrHeightLeft(
    bool melo_dbass_management_spk_surr_height_left) {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ssv)",
            adk::dbus::kAudiomanagerMelodIface,
            "BassManagementSpkSurrHeightLeft",
            g_variant_new("b", static_cast<uint8_t>(melo_dbass_management_spk_surr_height_left))));
    return result;
}
std::tuple<ErrorCode, bool> PropertiesHandler::readMeloDBassManagementSpkSurrHeightRight() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    bool melo_dbass_management_spk_surr_height_right;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ss)",
            adk::dbus::kAudiomanagerMelodIface,
            "BassManagementSpkSurrHeightRight"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        melo_dbass_management_spk_surr_height_right = g_variant_get_boolean(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, melo_dbass_management_spk_surr_height_right);
}

ErrorCode PropertiesHandler::updateMeloDBassManagementSpkSurrHeightRight(
    bool melo_dbass_management_spk_surr_height_right) {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ssv)",
            adk::dbus::kAudiomanagerMelodIface,
            "BassManagementSpkSurrHeightRight",
            g_variant_new("b", static_cast<uint8_t>(melo_dbass_management_spk_surr_height_right))));
    return result;
}
std::tuple<ErrorCode, bool> PropertiesHandler::readMeloDBassManagementSubWooferOut() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    bool melo_dbass_management_sub_woofer_out;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ss)",
            adk::dbus::kAudiomanagerMelodIface,
            "BassManagementSubWooferOut"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        melo_dbass_management_sub_woofer_out = g_variant_get_boolean(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, melo_dbass_management_sub_woofer_out);
}

ErrorCode PropertiesHandler::updateMeloDBassManagementSubWooferOut(
    bool melo_dbass_management_sub_woofer_out) {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ssv)",
            adk::dbus::kAudiomanagerMelodIface,
            "BassManagementSubWooferOut",
            g_variant_new("b", static_cast<uint8_t>(melo_dbass_management_sub_woofer_out))));
    return result;
}
std::tuple<ErrorCode, std::string> PropertiesHandler::readMeloDBassManagementSurroundSpeakerLeft() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::string melo_dbass_management_surround_speaker_left;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ss)",
            adk::dbus::kAudiomanagerMelodIface,
            "BassManagementSurroundSpeakerLeft"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        gsize length;
        melo_dbass_management_surround_speaker_left = static_cast<std::string>(g_variant_get_string(tmp, &length));
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, melo_dbass_management_surround_speaker_left);
}

ErrorCode PropertiesHandler::updateMeloDBassManagementSurroundSpeakerLeft(
    std::string melo_dbass_management_surround_speaker_left) {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ssv)",
            adk::dbus::kAudiomanagerMelodIface,
            "BassManagementSurroundSpeakerLeft",
            g_variant_new("s", melo_dbass_management_surround_speaker_left.c_str())));
    return result;
}
std::tuple<ErrorCode, std::string> PropertiesHandler::readMeloDBassManagementSurroundSpeakerRight() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::string melo_dbass_management_surround_speaker_right;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ss)",
            adk::dbus::kAudiomanagerMelodIface,
            "BassManagementSurroundSpeakerRight"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        gsize length;
        melo_dbass_management_surround_speaker_right = static_cast<std::string>(g_variant_get_string(tmp, &length));
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, melo_dbass_management_surround_speaker_right);
}

ErrorCode PropertiesHandler::updateMeloDBassManagementSurroundSpeakerRight(
    std::string melo_dbass_management_surround_speaker_right) {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ssv)",
            adk::dbus::kAudiomanagerMelodIface,
            "BassManagementSurroundSpeakerRight",
            g_variant_new("s", melo_dbass_management_surround_speaker_right.c_str())));
    return result;
}
std::tuple<ErrorCode, bool> PropertiesHandler::readMeloDDRCEnable() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    bool melo_ddrcenable;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ss)",
            adk::dbus::kAudiomanagerMelodIface,
            "DRCEnable"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        melo_ddrcenable = g_variant_get_boolean(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, melo_ddrcenable);
}

ErrorCode PropertiesHandler::updateMeloDDRCEnable(
    bool melo_ddrcenable) {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ssv)",
            adk::dbus::kAudiomanagerMelodIface,
            "DRCEnable",
            g_variant_new("b", static_cast<uint8_t>(melo_ddrcenable))));
    return result;
}
std::tuple<ErrorCode, std::string> PropertiesHandler::readMeloDDRCMode() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::string melo_ddrcmode;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ss)",
            adk::dbus::kAudiomanagerMelodIface,
            "DRCMode"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        gsize length;
        melo_ddrcmode = static_cast<std::string>(g_variant_get_string(tmp, &length));
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, melo_ddrcmode);
}

ErrorCode PropertiesHandler::updateMeloDDRCMode(
    std::string melo_ddrcmode) {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ssv)",
            adk::dbus::kAudiomanagerMelodIface,
            "DRCMode",
            g_variant_new("s", melo_ddrcmode.c_str())));
    return result;
}
std::tuple<ErrorCode, int16_t> PropertiesHandler::readMeloDDRCRefLevel() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int16_t melo_ddrcref_level;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ss)",
            adk::dbus::kAudiomanagerMelodIface,
            "DRCRefLevel"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        melo_ddrcref_level = g_variant_get_int16(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, melo_ddrcref_level);
}

ErrorCode PropertiesHandler::updateMeloDDRCRefLevel(
    int16_t melo_ddrcref_level) {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ssv)",
            adk::dbus::kAudiomanagerMelodIface,
            "DRCRefLevel",
            g_variant_new("n", melo_ddrcref_level)));
    return result;
}
std::tuple<ErrorCode, MeloDInt16Range> PropertiesHandler::readMeloDDRCRefLevelRange() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    MeloDInt16Range melo_ddrcref_level_range;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ss)",
            adk::dbus::kAudiomanagerMelodIface,
            "DRCRefLevelRange"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        GVariantIter property_iter;
        g_variant_iter_init(&property_iter, tmp);
        melo_ddrcref_level_range = dbus_->parseMeloDInt16Range(property_iter);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, melo_ddrcref_level_range);
}

std::tuple<ErrorCode, bool> PropertiesHandler::readMeloDMatrixEnable() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    bool melo_dmatrix_enable;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ss)",
            adk::dbus::kAudiomanagerMelodIface,
            "MatrixEnable"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        melo_dmatrix_enable = g_variant_get_boolean(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, melo_dmatrix_enable);
}

ErrorCode PropertiesHandler::updateMeloDMatrixEnable(
    bool melo_dmatrix_enable) {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ssv)",
            adk::dbus::kAudiomanagerMelodIface,
            "MatrixEnable",
            g_variant_new("b", static_cast<uint8_t>(melo_dmatrix_enable))));
    return result;
}
std::tuple<ErrorCode, std::string> PropertiesHandler::readMeloDMatrixMode() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::string melo_dmatrix_mode;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ss)",
            adk::dbus::kAudiomanagerMelodIface,
            "MatrixMode"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        gsize length;
        melo_dmatrix_mode = static_cast<std::string>(g_variant_get_string(tmp, &length));
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, melo_dmatrix_mode);
}

ErrorCode PropertiesHandler::updateMeloDMatrixMode(
    std::string melo_dmatrix_mode) {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ssv)",
            adk::dbus::kAudiomanagerMelodIface,
            "MatrixMode",
            g_variant_new("s", melo_dmatrix_mode.c_str())));
    return result;
}
std::tuple<ErrorCode, uint16_t> PropertiesHandler::readMeloDBassPlusBassContent() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    uint16_t melo_dbass_plus_bass_content;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ss)",
            adk::dbus::kAudiomanagerMelodIface,
            "BassPlusBassContent"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        melo_dbass_plus_bass_content = g_variant_get_uint16(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, melo_dbass_plus_bass_content);
}

ErrorCode PropertiesHandler::updateMeloDBassPlusBassContent(
    uint16_t melo_dbass_plus_bass_content) {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ssv)",
            adk::dbus::kAudiomanagerMelodIface,
            "BassPlusBassContent",
            g_variant_new("q", melo_dbass_plus_bass_content)));
    return result;
}
std::tuple<ErrorCode, MeloDUint16Range> PropertiesHandler::readMeloDBassPlusBassContentRange() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    MeloDUint16Range melo_dbass_plus_bass_content_range;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ss)",
            adk::dbus::kAudiomanagerMelodIface,
            "BassPlusBassContentRange"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        GVariantIter property_iter;
        g_variant_iter_init(&property_iter, tmp);
        melo_dbass_plus_bass_content_range = dbus_->parseMeloDUint16Range(property_iter);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, melo_dbass_plus_bass_content_range);
}

std::tuple<ErrorCode, bool> PropertiesHandler::readMeloDBassPlusEnable() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    bool melo_dbass_plus_enable;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ss)",
            adk::dbus::kAudiomanagerMelodIface,
            "BassPlusEnable"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        melo_dbass_plus_enable = g_variant_get_boolean(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, melo_dbass_plus_enable);
}

ErrorCode PropertiesHandler::updateMeloDBassPlusEnable(
    bool melo_dbass_plus_enable) {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ssv)",
            adk::dbus::kAudiomanagerMelodIface,
            "BassPlusEnable",
            g_variant_new("b", static_cast<uint8_t>(melo_dbass_plus_enable))));
    return result;
}
std::tuple<ErrorCode, uint16_t> PropertiesHandler::readMeloDBassPlusHarmonicsContent() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    uint16_t melo_dbass_plus_harmonics_content;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ss)",
            adk::dbus::kAudiomanagerMelodIface,
            "BassPlusHarmonicsContent"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        melo_dbass_plus_harmonics_content = g_variant_get_uint16(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, melo_dbass_plus_harmonics_content);
}

ErrorCode PropertiesHandler::updateMeloDBassPlusHarmonicsContent(
    uint16_t melo_dbass_plus_harmonics_content) {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ssv)",
            adk::dbus::kAudiomanagerMelodIface,
            "BassPlusHarmonicsContent",
            g_variant_new("q", melo_dbass_plus_harmonics_content)));
    return result;
}
std::tuple<ErrorCode, MeloDUint16Range> PropertiesHandler::readMeloDBassPlusHarmonicsContentRange() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    MeloDUint16Range melo_dbass_plus_harmonics_content_range;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ss)",
            adk::dbus::kAudiomanagerMelodIface,
            "BassPlusHarmonicsContentRange"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        GVariantIter property_iter;
        g_variant_iter_init(&property_iter, tmp);
        melo_dbass_plus_harmonics_content_range = dbus_->parseMeloDUint16Range(property_iter);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, melo_dbass_plus_harmonics_content_range);
}

std::tuple<ErrorCode, bool> PropertiesHandler::readMeloDMultiBandLimiterEnable() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    bool melo_dmulti_band_limiter_enable;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ss)",
            adk::dbus::kAudiomanagerMelodIface,
            "MultiBandLimiterEnable"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        melo_dmulti_band_limiter_enable = g_variant_get_boolean(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, melo_dmulti_band_limiter_enable);
}

ErrorCode PropertiesHandler::updateMeloDMultiBandLimiterEnable(
    bool melo_dmulti_band_limiter_enable) {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ssv)",
            adk::dbus::kAudiomanagerMelodIface,
            "MultiBandLimiterEnable",
            g_variant_new("b", static_cast<uint8_t>(melo_dmulti_band_limiter_enable))));
    return result;
}
std::tuple<ErrorCode, uint16_t> PropertiesHandler::readMeloDVoicePlusEffectStrength() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    uint16_t melo_dvoice_plus_effect_strength;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ss)",
            adk::dbus::kAudiomanagerMelodIface,
            "VoicePlusEffectStrength"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        melo_dvoice_plus_effect_strength = g_variant_get_uint16(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, melo_dvoice_plus_effect_strength);
}

ErrorCode PropertiesHandler::updateMeloDVoicePlusEffectStrength(
    uint16_t melo_dvoice_plus_effect_strength) {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ssv)",
            adk::dbus::kAudiomanagerMelodIface,
            "VoicePlusEffectStrength",
            g_variant_new("q", melo_dvoice_plus_effect_strength)));
    return result;
}
std::tuple<ErrorCode, MeloDUint16Range> PropertiesHandler::readMeloDVoicePlusEffectStrengthRange() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    MeloDUint16Range melo_dvoice_plus_effect_strength_range;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ss)",
            adk::dbus::kAudiomanagerMelodIface,
            "VoicePlusEffectStrengthRange"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        GVariantIter property_iter;
        g_variant_iter_init(&property_iter, tmp);
        melo_dvoice_plus_effect_strength_range = dbus_->parseMeloDUint16Range(property_iter);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, melo_dvoice_plus_effect_strength_range);
}

std::tuple<ErrorCode, bool> PropertiesHandler::readMeloDVoicePlusEnable() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    bool melo_dvoice_plus_enable;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ss)",
            adk::dbus::kAudiomanagerMelodIface,
            "VoicePlusEnable"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        melo_dvoice_plus_enable = g_variant_get_boolean(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, melo_dvoice_plus_enable);
}

ErrorCode PropertiesHandler::updateMeloDVoicePlusEnable(
    bool melo_dvoice_plus_enable) {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ssv)",
            adk::dbus::kAudiomanagerMelodIface,
            "VoicePlusEnable",
            g_variant_new("b", static_cast<uint8_t>(melo_dvoice_plus_enable))));
    return result;
}
std::tuple<ErrorCode, bool> PropertiesHandler::readMeloDXOverFilterEnable() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    bool melo_dxover_filter_enable;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ss)",
            adk::dbus::kAudiomanagerMelodIface,
            "XOverFilterEnable"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        melo_dxover_filter_enable = g_variant_get_boolean(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, melo_dxover_filter_enable);
}

ErrorCode PropertiesHandler::updateMeloDXOverFilterEnable(
    bool melo_dxover_filter_enable) {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kEffects3Bb071Bf73994F2Ebc5E4037Ee75A5FfObjPath,
        g_variant_new("(ssv)",
            adk::dbus::kAudiomanagerMelodIface,
            "XOverFilterEnable",
            g_variant_new("b", static_cast<uint8_t>(melo_dxover_filter_enable))));
    return result;
}

MethodsHandler::MethodsHandler(Dbus *dbus)
    : dbus_(dbus) {
    dbus_->setPropertiesChangedListener(
        std::bind(&MethodsHandler::interfacesAddedCb, this,
            std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
}

void MethodsHandler::interfacesAddedCb(const std::string &obj_path,
    const std::string &iface_name, GVariantIter * /*iter*/) {
    g_print("MethodsHandler::interfacesAddedCb \n");

    bool obj_id_map_changed = false;
    std::lock_guard<std::mutex> lock(mutex_);

    if (obj_id_map_changed) {
        for (auto &f : obj_id_map_changed_listeners_) {
            f(obj_id_map_);
        }
    }
}

SignalsHandler::SignalsHandler(Dbus *dbus)
    : dbus_(dbus) {
}

}  // namespace adk
