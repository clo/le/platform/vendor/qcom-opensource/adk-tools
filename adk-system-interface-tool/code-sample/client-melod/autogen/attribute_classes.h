/*
 * Copyright (c) 2019-2021, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of The Linux Foundation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef AUTOGEN_ATTRIBUTE_CLASSES_H_
#define AUTOGEN_ATTRIBUTE_CLASSES_H_

#include <string>
#include <unordered_set>
#include <vector>
#include "attribute_strings.h"

namespace adk {

enum ErrorCode {
    ADK_SI_OK,
    ADK_SI_ERROR_FAILED,
    ADK_SI_ERROR_NO_SERVICE,
    ADK_SI_ERROR_NO_OBJECT,
    ADK_SI_ERROR_TIMEOUT
};

class MeloDInt16Range {
    /*
 Support type for int16 Range
*/

 public:
    MeloDInt16Range() = default;
    ~MeloDInt16Range() = default;

    int16_t getMin() { return min_; }

    void setMin(const int16_t &min) {
        min_ = min;
        changed_properties_.emplace(kMin);
    }
    int16_t getMax() { return max_; }

    void setMax(const int16_t &max) {
        max_ = max;
        changed_properties_.emplace(kMax);
    }

 public:
    void clearChangedProperties() { changed_properties_.clear(); }
    std::unordered_set<std::string> getChangedProperties() { return changed_properties_; }

 private:
    int16_t min_;
    int16_t max_;

    std::unordered_set<std::string> changed_properties_;
};

class MeloDUint16Range {
    /*
 Support type for uint16 Range
*/

 public:
    MeloDUint16Range() = default;
    ~MeloDUint16Range() = default;

    uint16_t getMin() { return min_; }

    void setMin(const uint16_t &min) {
        min_ = min;
        changed_properties_.emplace(kMin);
    }
    uint16_t getMax() { return max_; }

    void setMax(const uint16_t &max) {
        max_ = max;
        changed_properties_.emplace(kMax);
    }

 public:
    void clearChangedProperties() { changed_properties_.clear(); }
    std::unordered_set<std::string> getChangedProperties() { return changed_properties_; }

 private:
    uint16_t min_;
    uint16_t max_;

    std::unordered_set<std::string> changed_properties_;
};

class PropertiesAttr {
 public:
    PropertiesAttr() = default;
    ~PropertiesAttr() = default;

    uint16_t getMeloDAroundProEffect() {
        return melo_daround_pro_effect_;
    }
    void setMeloDAroundProEffect(uint16_t melo_daround_pro_effect) {
        melo_daround_pro_effect_ = melo_daround_pro_effect;
        changed_properties_.emplace(kMeloDAroundProEffect);
    }
    bool getMeloDAroundProEnable() {
        return melo_daround_pro_enable_;
    }
    void setMeloDAroundProEnable(bool melo_daround_pro_enable) {
        melo_daround_pro_enable_ = melo_daround_pro_enable;
        changed_properties_.emplace(kMeloDAroundProEnable);
    }
    std::string getMeloDAroundProMode() {
        return melo_daround_pro_mode_;
    }
    void setMeloDAroundProMode(std::string melo_daround_pro_mode) {
        melo_daround_pro_mode_ = melo_daround_pro_mode;
        changed_properties_.emplace(kMeloDAroundProMode);
    }
    int16_t getMeloDBassManagementCornerFreqBassStage1() {
        return melo_dbass_management_corner_freq_bass_stage1_;
    }
    void setMeloDBassManagementCornerFreqBassStage1(int16_t melo_dbass_management_corner_freq_bass_stage1) {
        melo_dbass_management_corner_freq_bass_stage1_ = melo_dbass_management_corner_freq_bass_stage1;
        changed_properties_.emplace(kMeloDBassManagementCornerFreqBassStage1);
    }
    MeloDInt16Range getMeloDBassManagementCornerFreqBassStage1Range() {
        return melo_dbass_management_corner_freq_bass_stage1_range_;
    }
    void setMeloDBassManagementCornerFreqBassStage1Range(MeloDInt16Range melo_dbass_management_corner_freq_bass_stage1_range) {
        melo_dbass_management_corner_freq_bass_stage1_range_ = melo_dbass_management_corner_freq_bass_stage1_range;
        changed_properties_.emplace(kMeloDBassManagementCornerFreqBassStage1Range);
    }
    int16_t getMeloDBassManagementCornerFreqBassStage2() {
        return melo_dbass_management_corner_freq_bass_stage2_;
    }
    void setMeloDBassManagementCornerFreqBassStage2(int16_t melo_dbass_management_corner_freq_bass_stage2) {
        melo_dbass_management_corner_freq_bass_stage2_ = melo_dbass_management_corner_freq_bass_stage2;
        changed_properties_.emplace(kMeloDBassManagementCornerFreqBassStage2);
    }
    MeloDInt16Range getMeloDBassManagementCornerFreqBassStage2Range() {
        return melo_dbass_management_corner_freq_bass_stage2_range_;
    }
    void setMeloDBassManagementCornerFreqBassStage2Range(MeloDInt16Range melo_dbass_management_corner_freq_bass_stage2_range) {
        melo_dbass_management_corner_freq_bass_stage2_range_ = melo_dbass_management_corner_freq_bass_stage2_range;
        changed_properties_.emplace(kMeloDBassManagementCornerFreqBassStage2Range);
    }
    bool getMeloDBassManagementEnable() {
        return melo_dbass_management_enable_;
    }
    void setMeloDBassManagementEnable(bool melo_dbass_management_enable) {
        melo_dbass_management_enable_ = melo_dbass_management_enable;
        changed_properties_.emplace(kMeloDBassManagementEnable);
    }
    bool getMeloDBassManagementSpkSurrHeightLeft() {
        return melo_dbass_management_spk_surr_height_left_;
    }
    void setMeloDBassManagementSpkSurrHeightLeft(bool melo_dbass_management_spk_surr_height_left) {
        melo_dbass_management_spk_surr_height_left_ = melo_dbass_management_spk_surr_height_left;
        changed_properties_.emplace(kMeloDBassManagementSpkSurrHeightLeft);
    }
    bool getMeloDBassManagementSpkSurrHeightRight() {
        return melo_dbass_management_spk_surr_height_right_;
    }
    void setMeloDBassManagementSpkSurrHeightRight(bool melo_dbass_management_spk_surr_height_right) {
        melo_dbass_management_spk_surr_height_right_ = melo_dbass_management_spk_surr_height_right;
        changed_properties_.emplace(kMeloDBassManagementSpkSurrHeightRight);
    }
    bool getMeloDBassManagementSubWooferOut() {
        return melo_dbass_management_sub_woofer_out_;
    }
    void setMeloDBassManagementSubWooferOut(bool melo_dbass_management_sub_woofer_out) {
        melo_dbass_management_sub_woofer_out_ = melo_dbass_management_sub_woofer_out;
        changed_properties_.emplace(kMeloDBassManagementSubWooferOut);
    }
    std::string getMeloDBassManagementSurroundSpeakerLeft() {
        return melo_dbass_management_surround_speaker_left_;
    }
    void setMeloDBassManagementSurroundSpeakerLeft(std::string melo_dbass_management_surround_speaker_left) {
        melo_dbass_management_surround_speaker_left_ = melo_dbass_management_surround_speaker_left;
        changed_properties_.emplace(kMeloDBassManagementSurroundSpeakerLeft);
    }
    std::string getMeloDBassManagementSurroundSpeakerRight() {
        return melo_dbass_management_surround_speaker_right_;
    }
    void setMeloDBassManagementSurroundSpeakerRight(std::string melo_dbass_management_surround_speaker_right) {
        melo_dbass_management_surround_speaker_right_ = melo_dbass_management_surround_speaker_right;
        changed_properties_.emplace(kMeloDBassManagementSurroundSpeakerRight);
    }
    bool getMeloDDRCEnable() {
        return melo_ddrcenable_;
    }
    void setMeloDDRCEnable(bool melo_ddrcenable) {
        melo_ddrcenable_ = melo_ddrcenable;
        changed_properties_.emplace(kMeloDDRCEnable);
    }
    std::string getMeloDDRCMode() {
        return melo_ddrcmode_;
    }
    void setMeloDDRCMode(std::string melo_ddrcmode) {
        melo_ddrcmode_ = melo_ddrcmode;
        changed_properties_.emplace(kMeloDDRCMode);
    }
    int16_t getMeloDDRCRefLevel() {
        return melo_ddrcref_level_;
    }
    void setMeloDDRCRefLevel(int16_t melo_ddrcref_level) {
        melo_ddrcref_level_ = melo_ddrcref_level;
        changed_properties_.emplace(kMeloDDRCRefLevel);
    }
    MeloDInt16Range getMeloDDRCRefLevelRange() {
        return melo_ddrcref_level_range_;
    }
    void setMeloDDRCRefLevelRange(MeloDInt16Range melo_ddrcref_level_range) {
        melo_ddrcref_level_range_ = melo_ddrcref_level_range;
        changed_properties_.emplace(kMeloDDRCRefLevelRange);
    }
    bool getMeloDMatrixEnable() {
        return melo_dmatrix_enable_;
    }
    void setMeloDMatrixEnable(bool melo_dmatrix_enable) {
        melo_dmatrix_enable_ = melo_dmatrix_enable;
        changed_properties_.emplace(kMeloDMatrixEnable);
    }
    std::string getMeloDMatrixMode() {
        return melo_dmatrix_mode_;
    }
    void setMeloDMatrixMode(std::string melo_dmatrix_mode) {
        melo_dmatrix_mode_ = melo_dmatrix_mode;
        changed_properties_.emplace(kMeloDMatrixMode);
    }
    uint16_t getMeloDBassPlusBassContent() {
        return melo_dbass_plus_bass_content_;
    }
    void setMeloDBassPlusBassContent(uint16_t melo_dbass_plus_bass_content) {
        melo_dbass_plus_bass_content_ = melo_dbass_plus_bass_content;
        changed_properties_.emplace(kMeloDBassPlusBassContent);
    }
    MeloDUint16Range getMeloDBassPlusBassContentRange() {
        return melo_dbass_plus_bass_content_range_;
    }
    void setMeloDBassPlusBassContentRange(MeloDUint16Range melo_dbass_plus_bass_content_range) {
        melo_dbass_plus_bass_content_range_ = melo_dbass_plus_bass_content_range;
        changed_properties_.emplace(kMeloDBassPlusBassContentRange);
    }
    bool getMeloDBassPlusEnable() {
        return melo_dbass_plus_enable_;
    }
    void setMeloDBassPlusEnable(bool melo_dbass_plus_enable) {
        melo_dbass_plus_enable_ = melo_dbass_plus_enable;
        changed_properties_.emplace(kMeloDBassPlusEnable);
    }
    uint16_t getMeloDBassPlusHarmonicsContent() {
        return melo_dbass_plus_harmonics_content_;
    }
    void setMeloDBassPlusHarmonicsContent(uint16_t melo_dbass_plus_harmonics_content) {
        melo_dbass_plus_harmonics_content_ = melo_dbass_plus_harmonics_content;
        changed_properties_.emplace(kMeloDBassPlusHarmonicsContent);
    }
    MeloDUint16Range getMeloDBassPlusHarmonicsContentRange() {
        return melo_dbass_plus_harmonics_content_range_;
    }
    void setMeloDBassPlusHarmonicsContentRange(MeloDUint16Range melo_dbass_plus_harmonics_content_range) {
        melo_dbass_plus_harmonics_content_range_ = melo_dbass_plus_harmonics_content_range;
        changed_properties_.emplace(kMeloDBassPlusHarmonicsContentRange);
    }
    bool getMeloDMultiBandLimiterEnable() {
        return melo_dmulti_band_limiter_enable_;
    }
    void setMeloDMultiBandLimiterEnable(bool melo_dmulti_band_limiter_enable) {
        melo_dmulti_band_limiter_enable_ = melo_dmulti_band_limiter_enable;
        changed_properties_.emplace(kMeloDMultiBandLimiterEnable);
    }
    uint16_t getMeloDVoicePlusEffectStrength() {
        return melo_dvoice_plus_effect_strength_;
    }
    void setMeloDVoicePlusEffectStrength(uint16_t melo_dvoice_plus_effect_strength) {
        melo_dvoice_plus_effect_strength_ = melo_dvoice_plus_effect_strength;
        changed_properties_.emplace(kMeloDVoicePlusEffectStrength);
    }
    MeloDUint16Range getMeloDVoicePlusEffectStrengthRange() {
        return melo_dvoice_plus_effect_strength_range_;
    }
    void setMeloDVoicePlusEffectStrengthRange(MeloDUint16Range melo_dvoice_plus_effect_strength_range) {
        melo_dvoice_plus_effect_strength_range_ = melo_dvoice_plus_effect_strength_range;
        changed_properties_.emplace(kMeloDVoicePlusEffectStrengthRange);
    }
    bool getMeloDVoicePlusEnable() {
        return melo_dvoice_plus_enable_;
    }
    void setMeloDVoicePlusEnable(bool melo_dvoice_plus_enable) {
        melo_dvoice_plus_enable_ = melo_dvoice_plus_enable;
        changed_properties_.emplace(kMeloDVoicePlusEnable);
    }
    bool getMeloDXOverFilterEnable() {
        return melo_dxover_filter_enable_;
    }
    void setMeloDXOverFilterEnable(bool melo_dxover_filter_enable) {
        melo_dxover_filter_enable_ = melo_dxover_filter_enable;
        changed_properties_.emplace(kMeloDXOverFilterEnable);
    }

    void clearChangedProperties() { changed_properties_.clear(); }
    std::unordered_set<std::string> getChangedProperties() { return changed_properties_; }

 private:
    uint16_t melo_daround_pro_effect_;
    bool melo_daround_pro_enable_;
    std::string melo_daround_pro_mode_;
    int16_t melo_dbass_management_corner_freq_bass_stage1_;
    MeloDInt16Range melo_dbass_management_corner_freq_bass_stage1_range_;
    int16_t melo_dbass_management_corner_freq_bass_stage2_;
    MeloDInt16Range melo_dbass_management_corner_freq_bass_stage2_range_;
    bool melo_dbass_management_enable_;
    bool melo_dbass_management_spk_surr_height_left_;
    bool melo_dbass_management_spk_surr_height_right_;
    bool melo_dbass_management_sub_woofer_out_;
    std::string melo_dbass_management_surround_speaker_left_;
    std::string melo_dbass_management_surround_speaker_right_;
    bool melo_ddrcenable_;
    std::string melo_ddrcmode_;
    int16_t melo_ddrcref_level_;
    MeloDInt16Range melo_ddrcref_level_range_;
    bool melo_dmatrix_enable_;
    std::string melo_dmatrix_mode_;
    uint16_t melo_dbass_plus_bass_content_;
    MeloDUint16Range melo_dbass_plus_bass_content_range_;
    bool melo_dbass_plus_enable_;
    uint16_t melo_dbass_plus_harmonics_content_;
    MeloDUint16Range melo_dbass_plus_harmonics_content_range_;
    bool melo_dmulti_band_limiter_enable_;
    uint16_t melo_dvoice_plus_effect_strength_;
    MeloDUint16Range melo_dvoice_plus_effect_strength_range_;
    bool melo_dvoice_plus_enable_;
    bool melo_dxover_filter_enable_;

    std::unordered_set<std::string> changed_properties_;
};

}  // namespace adk
#endif  //  AUTOGEN_ATTRIBUTE_CLASSES_H_
