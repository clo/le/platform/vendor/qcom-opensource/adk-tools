
/*
 * Copyright (c) 2019-2021, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of The Linux Foundation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef AUTOGEN_INTERNAL_API_INTERFACE_HANDLERS_H_
#define AUTOGEN_INTERNAL_API_INTERFACE_HANDLERS_H_

#include <gio/gio.h>
#include <glib.h>
#include <functional>
#include <memory>
#include <mutex>
#include <string>
#include <tuple>
#include <unordered_map>
#include <vector>

// #include "adk_ipc.h"
#include "attribute_classes.h"
#include "internal_api/dbus.h"

namespace adk {
class PropertiesHandler {
 public:
    explicit PropertiesHandler(Dbus *dbus);
    ~PropertiesHandler() = default;

    std::tuple<ErrorCode, uint16_t> readMeloDAroundProEffect();

    ErrorCode updateMeloDAroundProEffect(
        uint16_t melo_daround_pro_effect);

    std::tuple<ErrorCode, bool> readMeloDAroundProEnable();

    ErrorCode updateMeloDAroundProEnable(
        bool melo_daround_pro_enable);

    std::tuple<ErrorCode, std::string> readMeloDAroundProMode();

    ErrorCode updateMeloDAroundProMode(
        std::string melo_daround_pro_mode);

    std::tuple<ErrorCode, int16_t> readMeloDBassManagementCornerFreqBassStage1();

    ErrorCode updateMeloDBassManagementCornerFreqBassStage1(
        int16_t melo_dbass_management_corner_freq_bass_stage1);

    std::tuple<ErrorCode, MeloDInt16Range> readMeloDBassManagementCornerFreqBassStage1Range();

    std::tuple<ErrorCode, int16_t> readMeloDBassManagementCornerFreqBassStage2();

    ErrorCode updateMeloDBassManagementCornerFreqBassStage2(
        int16_t melo_dbass_management_corner_freq_bass_stage2);

    std::tuple<ErrorCode, MeloDInt16Range> readMeloDBassManagementCornerFreqBassStage2Range();

    std::tuple<ErrorCode, bool> readMeloDBassManagementEnable();

    ErrorCode updateMeloDBassManagementEnable(
        bool melo_dbass_management_enable);

    std::tuple<ErrorCode, bool> readMeloDBassManagementSpkSurrHeightLeft();

    ErrorCode updateMeloDBassManagementSpkSurrHeightLeft(
        bool melo_dbass_management_spk_surr_height_left);

    std::tuple<ErrorCode, bool> readMeloDBassManagementSpkSurrHeightRight();

    ErrorCode updateMeloDBassManagementSpkSurrHeightRight(
        bool melo_dbass_management_spk_surr_height_right);

    std::tuple<ErrorCode, bool> readMeloDBassManagementSubWooferOut();

    ErrorCode updateMeloDBassManagementSubWooferOut(
        bool melo_dbass_management_sub_woofer_out);

    std::tuple<ErrorCode, std::string> readMeloDBassManagementSurroundSpeakerLeft();

    ErrorCode updateMeloDBassManagementSurroundSpeakerLeft(
        std::string melo_dbass_management_surround_speaker_left);

    std::tuple<ErrorCode, std::string> readMeloDBassManagementSurroundSpeakerRight();

    ErrorCode updateMeloDBassManagementSurroundSpeakerRight(
        std::string melo_dbass_management_surround_speaker_right);

    std::tuple<ErrorCode, bool> readMeloDDRCEnable();

    ErrorCode updateMeloDDRCEnable(
        bool melo_ddrcenable);

    std::tuple<ErrorCode, std::string> readMeloDDRCMode();

    ErrorCode updateMeloDDRCMode(
        std::string melo_ddrcmode);

    std::tuple<ErrorCode, int16_t> readMeloDDRCRefLevel();

    ErrorCode updateMeloDDRCRefLevel(
        int16_t melo_ddrcref_level);

    std::tuple<ErrorCode, MeloDInt16Range> readMeloDDRCRefLevelRange();

    std::tuple<ErrorCode, bool> readMeloDMatrixEnable();

    ErrorCode updateMeloDMatrixEnable(
        bool melo_dmatrix_enable);

    std::tuple<ErrorCode, std::string> readMeloDMatrixMode();

    ErrorCode updateMeloDMatrixMode(
        std::string melo_dmatrix_mode);

    std::tuple<ErrorCode, uint16_t> readMeloDBassPlusBassContent();

    ErrorCode updateMeloDBassPlusBassContent(
        uint16_t melo_dbass_plus_bass_content);

    std::tuple<ErrorCode, MeloDUint16Range> readMeloDBassPlusBassContentRange();

    std::tuple<ErrorCode, bool> readMeloDBassPlusEnable();

    ErrorCode updateMeloDBassPlusEnable(
        bool melo_dbass_plus_enable);

    std::tuple<ErrorCode, uint16_t> readMeloDBassPlusHarmonicsContent();

    ErrorCode updateMeloDBassPlusHarmonicsContent(
        uint16_t melo_dbass_plus_harmonics_content);

    std::tuple<ErrorCode, MeloDUint16Range> readMeloDBassPlusHarmonicsContentRange();

    std::tuple<ErrorCode, bool> readMeloDMultiBandLimiterEnable();

    ErrorCode updateMeloDMultiBandLimiterEnable(
        bool melo_dmulti_band_limiter_enable);

    std::tuple<ErrorCode, uint16_t> readMeloDVoicePlusEffectStrength();

    ErrorCode updateMeloDVoicePlusEffectStrength(
        uint16_t melo_dvoice_plus_effect_strength);

    std::tuple<ErrorCode, MeloDUint16Range> readMeloDVoicePlusEffectStrengthRange();

    std::tuple<ErrorCode, bool> readMeloDVoicePlusEnable();

    ErrorCode updateMeloDVoicePlusEnable(
        bool melo_dvoice_plus_enable);

    std::tuple<ErrorCode, bool> readMeloDXOverFilterEnable();

    ErrorCode updateMeloDXOverFilterEnable(
        bool melo_dxover_filter_enable);

    // obj_path and set of strings that suggest the changed properties.
    void setPropertiesChangedListener(
        const std::function<void(PropertiesAttr, const std::string &)> &listener) {
        properties_changed_listeners_.emplace_back(listener);
    }

    void setObjIdMapChangedListener(
        const std::function<void(const std::unordered_map<std::string, std::unordered_set<std::string>>)> &listener) {
        obj_id_map_changed_listeners_.emplace_back(listener);
    }

 private:
    void propertiesChangedCb(const std::string &obj_path,
        const std::string &iface_name, GVariantIter *iter);

    PropertiesAttr processAudiomanagerMelodIfaceResponse(
        const std::string &obj_path, GVariantIter *iter);

 private:
    Dbus *dbus_;
    std::mutex mutex_;

    std::vector<std::function<void(PropertiesAttr, const std::string &)>> properties_changed_listeners_;
    std::vector<std::function<void(const std::unordered_map<std::string, std::unordered_set<std::string>>)>>
        obj_id_map_changed_listeners_;

    std::unordered_map<std::string, std::unordered_set<std::string>> obj_id_map_;
};

class MethodsHandler {
 public:
    explicit MethodsHandler(Dbus *dbus);
    ~MethodsHandler() = default;

    void setObjIdMapChangedListener(
        const std::function<void(const std::unordered_map<std::string, std::unordered_set<std::string>>)> &listener) {
        obj_id_map_changed_listeners_.emplace_back(listener);
    }

 private:
    void interfacesAddedCb(const std::string &obj_path,
        const std::string &iface_name, GVariantIter *iter);

 private:
    Dbus *dbus_;
    std::mutex mutex_;

    std::vector<std::function<void(const std::unordered_map<std::string, std::unordered_set<std::string>>)>>
        obj_id_map_changed_listeners_;

    std::unordered_map<std::string, std::unordered_set<std::string>> obj_id_map_;
};

class SignalsHandler {
 public:
    explicit SignalsHandler(Dbus *dbus);
    ~SignalsHandler() = default;

 private:
 private:
    Dbus *dbus_;
    std::mutex mutex_;
};
}  // namespace adk

#endif  // AUTOGEN_INTERNAL_API_INTERFACE_HANDLERS_H_
