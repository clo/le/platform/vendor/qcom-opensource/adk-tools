/*
 * Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of The Linux Foundation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef AUTOGEN_ATTRIBUTE_CLASSES_H_
#define AUTOGEN_ATTRIBUTE_CLASSES_H_

#include <string>
#include <unordered_set>
#include <vector>

#include "attribute_strings.h"

namespace adk {

enum ErrorCode {
    ADK_SI_OK,
    ADK_SI_ERROR_FAILED,
    ADK_SI_ERROR_NO_SERVICE,
    ADK_SI_ERROR_NO_OBJECT,
    ADK_SI_ERROR_TIMEOUT
};

class PropertiesAttr {
 public:
    PropertiesAttr() = default;
    ~PropertiesAttr() = default;

    bool getHdmiDAPDlbsurroundmode() {
        return hdmi_dapdlbsurroundmode_;
    }
    void setHdmiDAPDlbsurroundmode(bool hdmi_dapdlbsurroundmode) {
        hdmi_dapdlbsurroundmode_ = hdmi_dapdlbsurroundmode;
        changed_properties_.emplace(kHdmiDAPDlbsurroundmode);
    }
    bool getSpdifDAPDlbsurroundmode() {
        return spdif_dapdlbsurroundmode_;
    }
    void setSpdifDAPDlbsurroundmode(bool spdif_dapdlbsurroundmode) {
        spdif_dapdlbsurroundmode_ = spdif_dapdlbsurroundmode;
        changed_properties_.emplace(kSpdifDAPDlbsurroundmode);
    }
    int32_t getSpdifDDPSurroundDownmixLevel() {
        return spdif_ddpsurround_downmix_level_;
    }
    void setSpdifDDPSurroundDownmixLevel(int32_t spdif_ddpsurround_downmix_level) {
        spdif_ddpsurround_downmix_level_ = spdif_ddpsurround_downmix_level;
        changed_properties_.emplace(kSpdifDDPSurroundDownmixLevel);
    }
    std::string getSpdifDDPStreamType() {
        return spdif_ddpstream_type_;
    }
    void setSpdifDDPStreamType(std::string spdif_ddpstream_type) {
        spdif_ddpstream_type_ = spdif_ddpstream_type;
        changed_properties_.emplace(kSpdifDDPStreamType);
    }
    int32_t getHdmiDTSX96Flag() {
        return hdmi_dtsx96_flag_;
    }
    void setHdmiDTSX96Flag(int32_t hdmi_dtsx96_flag) {
        hdmi_dtsx96_flag_ = hdmi_dtsx96_flag;
        changed_properties_.emplace(kHdmiDTSX96Flag);
    }
    int32_t getHdmiDDPLfe() {
        return hdmi_ddplfe_;
    }
    void setHdmiDDPLfe(int32_t hdmi_ddplfe) {
        hdmi_ddplfe_ = hdmi_ddplfe;
        changed_properties_.emplace(kHdmiDDPLfe);
    }
    uint32_t getHdmiDSDSamplingrate() {
        return hdmi_dsdsamplingrate_;
    }
    void setHdmiDSDSamplingrate(uint32_t hdmi_dsdsamplingrate) {
        hdmi_dsdsamplingrate_ = hdmi_dsdsamplingrate;
        changed_properties_.emplace(kHdmiDSDSamplingrate);
    }
    int32_t getHdmiDDPDualMono() {
        return hdmi_ddpdual_mono_;
    }
    void setHdmiDDPDualMono(int32_t hdmi_ddpdual_mono) {
        hdmi_ddpdual_mono_ = hdmi_ddpdual_mono;
        changed_properties_.emplace(kHdmiDDPDualMono);
    }
    int32_t getSpdifDTSDialogueNormalization() {
        return spdif_dtsdialogue_normalization_;
    }
    void setSpdifDTSDialogueNormalization(int32_t spdif_dtsdialogue_normalization) {
        spdif_dtsdialogue_normalization_ = spdif_dtsdialogue_normalization;
        changed_properties_.emplace(kSpdifDTSDialogueNormalization);
    }
    int32_t getSpdifDDPDualMono() {
        return spdif_ddpdual_mono_;
    }
    void setSpdifDDPDualMono(int32_t spdif_ddpdual_mono) {
        spdif_ddpdual_mono_ = spdif_ddpdual_mono;
        changed_properties_.emplace(kSpdifDDPDualMono);
    }
    int32_t getSpdifDDPDialoguenormalization() {
        return spdif_ddpdialoguenormalization_;
    }
    void setSpdifDDPDialoguenormalization(int32_t spdif_ddpdialoguenormalization) {
        spdif_ddpdialoguenormalization_ = spdif_ddpdialoguenormalization;
        changed_properties_.emplace(kSpdifDDPDialoguenormalization);
    }
    int32_t getSpdifDTSDecodeInfo() {
        return spdif_dtsdecode_info_;
    }
    void setSpdifDTSDecodeInfo(int32_t spdif_dtsdecode_info) {
        spdif_dtsdecode_info_ = spdif_dtsdecode_info;
        changed_properties_.emplace(kSpdifDTSDecodeInfo);
    }
    uint32_t getHdmiDDPCustomChMap() {
        return hdmi_ddpcustom_ch_map_;
    }
    void setHdmiDDPCustomChMap(uint32_t hdmi_ddpcustom_ch_map) {
        hdmi_ddpcustom_ch_map_ = hdmi_ddpcustom_ch_map;
        changed_properties_.emplace(kHdmiDDPCustomChMap);
    }
    int32_t getSpdifDTSBitrate() {
        return spdif_dtsbitrate_;
    }
    void setSpdifDTSBitrate(int32_t spdif_dtsbitrate) {
        spdif_dtsbitrate_ = spdif_dtsbitrate;
        changed_properties_.emplace(kSpdifDTSBitrate);
    }
    int32_t getSpdifMATGainRequired() {
        return spdif_matgain_required_;
    }
    void setSpdifMATGainRequired(int32_t spdif_matgain_required) {
        spdif_matgain_required_ = spdif_matgain_required;
        changed_properties_.emplace(kSpdifMATGainRequired);
    }
    int32_t getHdmiDDPAudioCodingMode() {
        return hdmi_ddpaudio_coding_mode_;
    }
    void setHdmiDDPAudioCodingMode(int32_t hdmi_ddpaudio_coding_mode) {
        hdmi_ddpaudio_coding_mode_ = hdmi_ddpaudio_coding_mode;
        changed_properties_.emplace(kHdmiDDPAudioCodingMode);
    }
    int32_t getHdmiDDPSurroundex() {
        return hdmi_ddpsurroundex_;
    }
    void setHdmiDDPSurroundex(int32_t hdmi_ddpsurroundex) {
        hdmi_ddpsurroundex_ = hdmi_ddpsurroundex;
        changed_properties_.emplace(kHdmiDDPSurroundex);
    }
    int32_t getHdmiDTSDecodeInfo() {
        return hdmi_dtsdecode_info_;
    }
    void setHdmiDTSDecodeInfo(int32_t hdmi_dtsdecode_info) {
        hdmi_dtsdecode_info_ = hdmi_dtsdecode_info;
        changed_properties_.emplace(kHdmiDTSDecodeInfo);
    }
    int32_t getSpdifDDPSurroundex() {
        return spdif_ddpsurroundex_;
    }
    void setSpdifDDPSurroundex(int32_t spdif_ddpsurroundex) {
        spdif_ddpsurroundex_ = spdif_ddpsurroundex;
        changed_properties_.emplace(kSpdifDDPSurroundex);
    }
    int32_t getHdmiDTSBitrate() {
        return hdmi_dtsbitrate_;
    }
    void setHdmiDTSBitrate(int32_t hdmi_dtsbitrate) {
        hdmi_dtsbitrate_ = hdmi_dtsbitrate;
        changed_properties_.emplace(kHdmiDTSBitrate);
    }
    int32_t getSpdifDDPBitrate() {
        return spdif_ddpbitrate_;
    }
    void setSpdifDDPBitrate(int32_t spdif_ddpbitrate) {
        spdif_ddpbitrate_ = spdif_ddpbitrate;
        changed_properties_.emplace(kSpdifDDPBitrate);
    }
    int32_t getSpdifDDPAudioCodingMode() {
        return spdif_ddpaudio_coding_mode_;
    }
    void setSpdifDDPAudioCodingMode(int32_t spdif_ddpaudio_coding_mode) {
        spdif_ddpaudio_coding_mode_ = spdif_ddpaudio_coding_mode;
        changed_properties_.emplace(kSpdifDDPAudioCodingMode);
    }
    int32_t getSpdifMATFbaSurroundCh() {
        return spdif_matfba_surround_ch_;
    }
    void setSpdifMATFbaSurroundCh(int32_t spdif_matfba_surround_ch) {
        spdif_matfba_surround_ch_ = spdif_matfba_surround_ch;
        changed_properties_.emplace(kSpdifMATFbaSurroundCh);
    }
    int32_t getSpdifDTSXllsamplerate() {
        return spdif_dtsxllsamplerate_;
    }
    void setSpdifDTSXllsamplerate(int32_t spdif_dtsxllsamplerate) {
        spdif_dtsxllsamplerate_ = spdif_dtsxllsamplerate;
        changed_properties_.emplace(kSpdifDTSXllsamplerate);
    }
    bool getSpdifDlbsurroundcenterspread() {
        return spdif_dlbsurroundcenterspread_;
    }
    void setSpdifDlbsurroundcenterspread(bool spdif_dlbsurroundcenterspread) {
        spdif_dlbsurroundcenterspread_ = spdif_dlbsurroundcenterspread;
        changed_properties_.emplace(kSpdifDlbsurroundcenterspread);
    }
    uint32_t getHdmiMATFbach() {
        return hdmi_matfbach_;
    }
    void setHdmiMATFbach(uint32_t hdmi_matfbach) {
        hdmi_matfbach_ = hdmi_matfbach;
        changed_properties_.emplace(kHdmiMATFbach);
    }
    int32_t getSpdifDecChConfig() {
        return spdif_dec_ch_config_;
    }
    void setSpdifDecChConfig(int32_t spdif_dec_ch_config) {
        spdif_dec_ch_config_ = spdif_dec_ch_config;
        changed_properties_.emplace(kSpdifDecChConfig);
    }
        int32_t getSpdifDDPDecChConfig() {
        return spdif_ddpdec_ch_config_;
    }
    void setSpdifDDPDecChConfig(int32_t spdif_ddpdec_ch_config) {
        spdif_ddpdec_ch_config_ = spdif_ddpdec_ch_config;
        changed_properties_.emplace(kSpdifDDPDecChConfig);
    }
    int32_t getHdmiDTSXchFlag() {
        return hdmi_dtsxch_flag_;
    }
    void setHdmiDTSXchFlag(int32_t hdmi_dtsxch_flag) {
        hdmi_dtsxch_flag_ = hdmi_dtsxch_flag;
        changed_properties_.emplace(kHdmiDTSXchFlag);
    }
    int32_t getHdmiAacDualMonoChannelMode() {
        return hdmi_aac_dual_mono_channel_mode_;
    }
    void setHdmiAacDualMonoChannelMode(int32_t hdmi_aac_dual_mono_channel_mode) {
        hdmi_aac_dual_mono_channel_mode_ = hdmi_aac_dual_mono_channel_mode;
        changed_properties_.emplace(kHdmiAacDualMonoChannelMode);
    }
    int32_t getHdmiDDPCenterDownmixLevel() {
        return hdmi_ddpcenter_downmix_level_;
    }
    void setHdmiDDPCenterDownmixLevel(int32_t hdmi_ddpcenter_downmix_level) {
        hdmi_ddpcenter_downmix_level_ = hdmi_ddpcenter_downmix_level;
        changed_properties_.emplace(kHdmiDDPCenterDownmixLevel);
    }
    uint32_t getSpdifDSDSamplingrate() {
        return spdif_dsdsamplingrate_;
    }
    void setSpdifDSDSamplingrate(uint32_t spdif_dsdsamplingrate) {
        spdif_dsdsamplingrate_ = spdif_dsdsamplingrate;
        changed_properties_.emplace(kSpdifDSDSamplingrate);
    }
    uint32_t getSpdifMATTrueHdSync() {
        return spdif_mattrue_hd_sync_;
    }
    void setSpdifMATTrueHdSync(uint32_t spdif_mattrue_hd_sync) {
        spdif_mattrue_hd_sync_ = spdif_mattrue_hd_sync;
        changed_properties_.emplace(kSpdifMATTrueHdSync);
    }
    int32_t getSpdifDTSExtensionMask() {
        return spdif_dtsextension_mask_;
    }
    void setSpdifDTSExtensionMask(int32_t spdif_dtsextension_mask) {
        spdif_dtsextension_mask_ = spdif_dtsextension_mask;
        changed_properties_.emplace(kSpdifDTSExtensionMask);
    }
    uint32_t getSpdifMATFbach() {
        return spdif_matfbach_;
    }
    void setSpdifMATFbach(uint32_t spdif_matfbach) {
        spdif_matfbach_ = spdif_matfbach;
        changed_properties_.emplace(kSpdifMATFbach);
    }
    int32_t getHdmiDTSStreamType() {
        return hdmi_dtsstream_type_;
    }
    void setHdmiDTSStreamType(int32_t hdmi_dtsstream_type) {
        hdmi_dtsstream_type_ = hdmi_dtsstream_type;
        changed_properties_.emplace(kHdmiDTSStreamType);
    }
    uint32_t getHdmiDecSamplFreq() {
        return hdmi_dec_sampl_freq_;
    }
    void setHdmiDecSamplFreq(uint32_t hdmi_dec_sampl_freq) {
        hdmi_dec_sampl_freq_ = hdmi_dec_sampl_freq;
        changed_properties_.emplace(kHdmiDecSamplFreq);
    }
    std::string getSpdifMATStreamtype() {
        return spdif_matstreamtype_;
    }
    void setSpdifMATStreamtype(std::string spdif_matstreamtype) {
        spdif_matstreamtype_ = spdif_matstreamtype;
        changed_properties_.emplace(kSpdifMATStreamtype);
    }
    uint32_t getSpdifDecSamplFreq() {
        return spdif_dec_sampl_freq_;
    }
    void setSpdifDecSamplFreq(uint32_t spdif_dec_sampl_freq) {
        spdif_dec_sampl_freq_ = spdif_dec_sampl_freq;
        changed_properties_.emplace(kSpdifDecSamplFreq);
    }
    int32_t getSpdifDTSAnalogCompensationGain() {
        return spdif_dtsanalog_compensation_gain_;
    }
    void setSpdifDTSAnalogCompensationGain(int32_t spdif_dtsanalog_compensation_gain) {
        spdif_dtsanalog_compensation_gain_ = spdif_dtsanalog_compensation_gain;
        changed_properties_.emplace(kSpdifDTSAnalogCompensationGain);
    }
    int32_t getSpdifDTSStreamType() {
        return spdif_dtsstream_type_;
    }
    void setSpdifDTSStreamType(int32_t spdif_dtsstream_type) {
        spdif_dtsstream_type_ = spdif_dtsstream_type;
        changed_properties_.emplace(kSpdifDTSStreamType);
    }
    int32_t getSpdifMATFbaDialogueNormalization() {
        return spdif_matfba_dialogue_normalization_;
    }
    void setSpdifMATFbaDialogueNormalization(int32_t spdif_matfba_dialogue_normalization) {
        spdif_matfba_dialogue_normalization_ = spdif_matfba_dialogue_normalization;
        changed_properties_.emplace(kSpdifMATFbaDialogueNormalization);
    }
    int32_t getHdmiDTSAnalogCompensationGain() {
        return hdmi_dtsanalog_compensation_gain_;
    }
    void setHdmiDTSAnalogCompensationGain(int32_t hdmi_dtsanalog_compensation_gain) {
        hdmi_dtsanalog_compensation_gain_ = hdmi_dtsanalog_compensation_gain;
        changed_properties_.emplace(kHdmiDTSAnalogCompensationGain);
    }
    int32_t getHdmiMATFbaSurroundCh() {
        return hdmi_matfba_surround_ch_;
    }
    void setHdmiMATFbaSurroundCh(int32_t hdmi_matfba_surround_ch) {
        hdmi_matfba_surround_ch_ = hdmi_matfba_surround_ch;
        changed_properties_.emplace(kHdmiMATFbaSurroundCh);
    }
    int32_t getSpdifDtsxDrcpercent() {
        return spdif_dtsx_drcpercent_;
    }
    void setSpdifDtsxDrcpercent(int32_t spdif_dtsx_drcpercent) {
        spdif_dtsx_drcpercent_ = spdif_dtsx_drcpercent;
        changed_properties_.emplace(kSpdifDtsxDrcpercent);
    }
    uint32_t getHdmiDTSSamplerate() {
        return hdmi_dtssamplerate_;
    }
    void setHdmiDTSSamplerate(uint32_t hdmi_dtssamplerate) {
        hdmi_dtssamplerate_ = hdmi_dtssamplerate;
        changed_properties_.emplace(kHdmiDTSSamplerate);
    }
    uint32_t getHdmiMATTrueHdSync() {
        return hdmi_mattrue_hd_sync_;
    }
    void setHdmiMATTrueHdSync(uint32_t hdmi_mattrue_hd_sync) {
        hdmi_mattrue_hd_sync_ = hdmi_mattrue_hd_sync;
        changed_properties_.emplace(kHdmiMATTrueHdSync);
    }
    uint32_t getSpdifDTSProgChMask() {
        return spdif_dtsprog_ch_mask_;
    }
    void setSpdifDTSProgChMask(uint32_t spdif_dtsprog_ch_mask) {
        spdif_dtsprog_ch_mask_ = spdif_dtsprog_ch_mask;
        changed_properties_.emplace(kSpdifDTSProgChMask);
    }
    int32_t getHdmiDtsxDrcpercent() {
        return hdmi_dtsx_drcpercent_;
    }
    void setHdmiDtsxDrcpercent(int32_t hdmi_dtsx_drcpercent) {
        hdmi_dtsx_drcpercent_ = hdmi_dtsx_drcpercent;
        changed_properties_.emplace(kHdmiDtsxDrcpercent);
    }
    std::string getHdmiPCMChConfig() {
        return hdmi_pcmch_config_;
    }
    void setHdmiPCMChConfig(std::string hdmi_pcmch_config) {
        hdmi_pcmch_config_ = hdmi_pcmch_config;
        changed_properties_.emplace(kHdmiPCMChConfig);
    }
    int32_t getSpdifDSDChConfig() {
        return spdif_dsdch_config_;
    }
    void setSpdifDSDChConfig(int32_t spdif_dsdch_config) {
        spdif_dsdch_config_ = spdif_dsdch_config;
        changed_properties_.emplace(kSpdifDSDChConfig);
    }
    int32_t getHdmiDTSDialogueControlAvailability() {
        return hdmi_dtsdialogue_control_availability_;
    }
    void setHdmiDTSDialogueControlAvailability(int32_t hdmi_dtsdialogue_control_availability) {
        hdmi_dtsdialogue_control_availability_ = hdmi_dtsdialogue_control_availability;
        changed_properties_.emplace(kHdmiDTSDialogueControlAvailability);
    }
    std::string getHdmiMATStreamtype() {
        return hdmi_matstreamtype_;
    }
    void setHdmiMATStreamtype(std::string hdmi_matstreamtype) {
        hdmi_matstreamtype_ = hdmi_matstreamtype;
        changed_properties_.emplace(kHdmiMATStreamtype);
    }
    int32_t getHdmiDSDChConfig() {
        return hdmi_dsdch_config_;
    }
    void setHdmiDSDChConfig(int32_t hdmi_dsdch_config) {
        hdmi_dsdch_config_ = hdmi_dsdch_config;
        changed_properties_.emplace(kHdmiDSDChConfig);
    }
    int32_t getHdmiDecChConfig() {
        return hdmi_dec_ch_config_;
    }
    void setHdmiDecChConfig(int32_t hdmi_dec_ch_config) {
        hdmi_dec_ch_config_ = hdmi_dec_ch_config;
        changed_properties_.emplace(kHdmiDecChConfig);
    }
        int32_t getHdmiDDPDecChConfig() {
        return hdmi_ddpdec_ch_config_;
    }
    void setHdmiDDPDecChConfig(int32_t hdmi_ddpdec_ch_config) {
        hdmi_ddpdec_ch_config_ = hdmi_ddpdec_ch_config;
        changed_properties_.emplace(kHdmiDDPDecChConfig);
    }
    int32_t getHdmiDTSDialogueNormalization() {
        return hdmi_dtsdialogue_normalization_;
    }
    void setHdmiDTSDialogueNormalization(int32_t hdmi_dtsdialogue_normalization) {
        hdmi_dtsdialogue_normalization_ = hdmi_dtsdialogue_normalization;
        changed_properties_.emplace(kHdmiDTSDialogueNormalization);
    }
    int32_t getSpdifDDPKaraoke() {
        return spdif_ddpkaraoke_;
    }
    void setSpdifDDPKaraoke(int32_t spdif_ddpkaraoke) {
        spdif_ddpkaraoke_ = spdif_ddpkaraoke;
        changed_properties_.emplace(kSpdifDDPKaraoke);
    }
    int32_t getSpdifDTSCoreBitRate() {
        return spdif_dtscore_bit_rate_;
    }
    void setSpdifDTSCoreBitRate(int32_t spdif_dtscore_bit_rate) {
        spdif_dtscore_bit_rate_ = spdif_dtscore_bit_rate;
        changed_properties_.emplace(kSpdifDTSCoreBitRate);
    }
    int32_t getHdmiDTSObjectNum() {
        return hdmi_dtsobject_num_;
    }
    void setHdmiDTSObjectNum(int32_t hdmi_dtsobject_num) {
        hdmi_dtsobject_num_ = hdmi_dtsobject_num;
        changed_properties_.emplace(kHdmiDTSObjectNum);
    }
    bool getHdmiDlbsurroundcenterspread() {
        return hdmi_dlbsurroundcenterspread_;
    }
    void setHdmiDlbsurroundcenterspread(bool hdmi_dlbsurroundcenterspread) {
        hdmi_dlbsurroundcenterspread_ = hdmi_dlbsurroundcenterspread;
        changed_properties_.emplace(kHdmiDlbsurroundcenterspread);
    }
    int32_t getSpdifDTSX96Flag() {
        return spdif_dtsx96_flag_;
    }
    void setSpdifDTSX96Flag(int32_t spdif_dtsx96_flag) {
        spdif_dtsx96_flag_ = spdif_dtsx96_flag;
        changed_properties_.emplace(kSpdifDTSX96Flag);
    }
    int32_t getHdmiDTSEsMatrixFlag() {
        return hdmi_dtses_matrix_flag_;
    }
    void setHdmiDTSEsMatrixFlag(int32_t hdmi_dtses_matrix_flag) {
        hdmi_dtses_matrix_flag_ = hdmi_dtses_matrix_flag;
        changed_properties_.emplace(kHdmiDTSEsMatrixFlag);
    }
    int32_t getSpdifDTSEmbeddeddownmixselect() {
        return spdif_dtsembeddeddownmixselect_;
    }
    void setSpdifDTSEmbeddeddownmixselect(int32_t spdif_dtsembeddeddownmixselect) {
        spdif_dtsembeddeddownmixselect_ = spdif_dtsembeddeddownmixselect;
        changed_properties_.emplace(kSpdifDTSEmbeddeddownmixselect);
    }
    uint32_t getHdmiMATFbbch() {
        return hdmi_matfbbch_;
    }
    void setHdmiMATFbbch(uint32_t hdmi_matfbbch) {
        hdmi_matfbbch_ = hdmi_matfbbch;
        changed_properties_.emplace(kHdmiMATFbbch);
    }
    int32_t getSpdifDTSXchFlag() {
        return spdif_dtsxch_flag_;
    }
    void setSpdifDTSXchFlag(int32_t spdif_dtsxch_flag) {
        spdif_dtsxch_flag_ = spdif_dtsxch_flag;
        changed_properties_.emplace(kSpdifDTSXchFlag);
    }
    int32_t getSpdifDDPLfe() {
        return spdif_ddplfe_;
    }
    void setSpdifDDPLfe(int32_t spdif_ddplfe) {
        spdif_ddplfe_ = spdif_ddplfe;
        changed_properties_.emplace(kSpdifDDPLfe);
    }
    int32_t getHdmiDDPBitrate() {
        return hdmi_ddpbitrate_;
    }
    void setHdmiDDPBitrate(int32_t hdmi_ddpbitrate) {
        hdmi_ddpbitrate_ = hdmi_ddpbitrate;
        changed_properties_.emplace(kHdmiDDPBitrate);
    }
    int32_t getHdmiDDPDialoguenormalization() {
        return hdmi_ddpdialoguenormalization_;
    }
    void setHdmiDDPDialoguenormalization(int32_t hdmi_ddpdialoguenormalization) {
        hdmi_ddpdialoguenormalization_ = hdmi_ddpdialoguenormalization;
        changed_properties_.emplace(kHdmiDDPDialoguenormalization);
    }
    std::vector<int32_t> getHdmiDDPDrcmode() {
        return hdmi_ddpdrcmode_;
    }
    void setHdmiDDPDrcmode(std::vector<int32_t> hdmi_ddpdrcmode) {
        hdmi_ddpdrcmode_ = hdmi_ddpdrcmode;
        changed_properties_.emplace(kHdmiDDPDrcmode);
    }
    uint32_t getSpdifDDPCustomChMap() {
        return spdif_ddpcustom_ch_map_;
    }
    void setSpdifDDPCustomChMap(uint32_t spdif_ddpcustom_ch_map) {
        spdif_ddpcustom_ch_map_ = spdif_ddpcustom_ch_map;
        changed_properties_.emplace(kSpdifDDPCustomChMap);
    }
    int32_t getHdmiMATFbaDialogueNormalization() {
        return hdmi_matfba_dialogue_normalization_;
    }
    void setHdmiMATFbaDialogueNormalization(int32_t hdmi_matfba_dialogue_normalization) {
        hdmi_matfba_dialogue_normalization_ = hdmi_matfba_dialogue_normalization;
        changed_properties_.emplace(kHdmiMATFbaDialogueNormalization);
    }
    int32_t getSpdifDDPCenterDownmixLevel() {
        return spdif_ddpcenter_downmix_level_;
    }
    void setSpdifDDPCenterDownmixLevel(int32_t spdif_ddpcenter_downmix_level) {
        spdif_ddpcenter_downmix_level_ = spdif_ddpcenter_downmix_level;
        changed_properties_.emplace(kSpdifDDPCenterDownmixLevel);
    }
    uint32_t getHdmiDTSProgChMask() {
        return hdmi_dtsprog_ch_mask_;
    }
    void setHdmiDTSProgChMask(uint32_t hdmi_dtsprog_ch_mask) {
        hdmi_dtsprog_ch_mask_ = hdmi_dtsprog_ch_mask;
        changed_properties_.emplace(kHdmiDTSProgChMask);
    }
    std::vector<int32_t> getSpdifDDPDrcmode() {
        return spdif_ddpdrcmode_;
    }
    void setSpdifDDPDrcmode(std::vector<int32_t> spdif_ddpdrcmode) {
        spdif_ddpdrcmode_ = spdif_ddpdrcmode;
        changed_properties_.emplace(kSpdifDDPDrcmode);
    }
    int32_t getHdmiDTSEmbeddeddownmixselect() {
        return hdmi_dtsembeddeddownmixselect_;
    }
    void setHdmiDTSEmbeddeddownmixselect(int32_t hdmi_dtsembeddeddownmixselect) {
        hdmi_dtsembeddeddownmixselect_ = hdmi_dtsembeddeddownmixselect;
        changed_properties_.emplace(kHdmiDTSEmbeddeddownmixselect);
    }
    uint32_t getHdmiPCMsamplingRate() {
        return hdmi_pcmsampling_rate_;
    }
    void setHdmiPCMsamplingRate(uint32_t hdmi_pcmsampling_rate) {
        hdmi_pcmsampling_rate_ = hdmi_pcmsampling_rate;
        changed_properties_.emplace(kHdmiPCMsamplingRate);
    }
    int32_t getHdmiDTSXllsamplerate() {
        return hdmi_dtsxllsamplerate_;
    }
    void setHdmiDTSXllsamplerate(int32_t hdmi_dtsxllsamplerate) {
        hdmi_dtsxllsamplerate_ = hdmi_dtsxllsamplerate;
        changed_properties_.emplace(kHdmiDTSXllsamplerate);
    }
    uint32_t getSpdifDTSSamplerate() {
        return spdif_dtssamplerate_;
    }
    void setSpdifDTSSamplerate(uint32_t spdif_dtssamplerate) {
        spdif_dtssamplerate_ = spdif_dtssamplerate;
        changed_properties_.emplace(kSpdifDTSSamplerate);
    }
    std::string getHdmiDDPStreamType() {
        return hdmi_ddpstream_type_;
    }
    void setHdmiDDPStreamType(std::string hdmi_ddpstream_type) {
        hdmi_ddpstream_type_ = hdmi_ddpstream_type;
        changed_properties_.emplace(kHdmiDDPStreamType);
    }
    uint32_t getSpdifPCMsamplingRate() {
        return spdif_pcmsampling_rate_;
    }
    void setSpdifPCMsamplingRate(uint32_t spdif_pcmsampling_rate) {
        spdif_pcmsampling_rate_ = spdif_pcmsampling_rate;
        changed_properties_.emplace(kSpdifPCMsamplingRate);
    }
    uint32_t getSpdifOutputsamplingfrequency() {
        return spdif_outputsamplingfrequency_;
    }
    void setSpdifOutputsamplingfrequency(uint32_t spdif_outputsamplingfrequency) {
        spdif_outputsamplingfrequency_ = spdif_outputsamplingfrequency;
        changed_properties_.emplace(kSpdifOutputsamplingfrequency);
    }
    int32_t getSpdifDTSEsMatrixFlag() {
        return spdif_dtses_matrix_flag_;
    }
    void setSpdifDTSEsMatrixFlag(int32_t spdif_dtses_matrix_flag) {
        spdif_dtses_matrix_flag_ = spdif_dtses_matrix_flag;
        changed_properties_.emplace(kSpdifDTSEsMatrixFlag);
    }
    uint32_t getSpdifMATFbbch() {
        return spdif_matfbbch_;
    }
    void setSpdifMATFbbch(uint32_t spdif_matfbbch) {
        spdif_matfbbch_ = spdif_matfbbch;
        changed_properties_.emplace(kSpdifMATFbbch);
    }
    int32_t getHdmiMATGainRequired() {
        return hdmi_matgain_required_;
    }
    void setHdmiMATGainRequired(int32_t hdmi_matgain_required) {
        hdmi_matgain_required_ = hdmi_matgain_required;
        changed_properties_.emplace(kHdmiMATGainRequired);
    }
    int32_t getSpdifDTSObjectNum() {
        return spdif_dtsobject_num_;
    }
    void setSpdifDTSObjectNum(int32_t spdif_dtsobject_num) {
        spdif_dtsobject_num_ = spdif_dtsobject_num;
        changed_properties_.emplace(kSpdifDTSObjectNum);
    }
    std::string getSpdifPCMChConfig() {
        return spdif_pcmch_config_;
    }
    void setSpdifPCMChConfig(std::string spdif_pcmch_config) {
        spdif_pcmch_config_ = spdif_pcmch_config;
        changed_properties_.emplace(kSpdifPCMChConfig);
    }
    uint32_t getHdmiOutputsamplingfrequency() {
        return hdmi_outputsamplingfrequency_;
    }
    void setHdmiOutputsamplingfrequency(uint32_t hdmi_outputsamplingfrequency) {
        hdmi_outputsamplingfrequency_ = hdmi_outputsamplingfrequency;
        changed_properties_.emplace(kHdmiOutputsamplingfrequency);
    }
    int32_t getHdmiDTSExtensionMask() {
        return hdmi_dtsextension_mask_;
    }
    void setHdmiDTSExtensionMask(int32_t hdmi_dtsextension_mask) {
        hdmi_dtsextension_mask_ = hdmi_dtsextension_mask;
        changed_properties_.emplace(kHdmiDTSExtensionMask);
    }
    int32_t getHdmiDDPSurroundDownmixLevel() {
        return hdmi_ddpsurround_downmix_level_;
    }
    void setHdmiDDPSurroundDownmixLevel(int32_t hdmi_ddpsurround_downmix_level) {
        hdmi_ddpsurround_downmix_level_ = hdmi_ddpsurround_downmix_level;
        changed_properties_.emplace(kHdmiDDPSurroundDownmixLevel);
    }
    int32_t getSpdifDTSDialogueControlAvailability() {
        return spdif_dtsdialogue_control_availability_;
    }
    void setSpdifDTSDialogueControlAvailability(int32_t spdif_dtsdialogue_control_availability) {
        spdif_dtsdialogue_control_availability_ = spdif_dtsdialogue_control_availability;
        changed_properties_.emplace(kSpdifDTSDialogueControlAvailability);
    }
    int32_t getHdmiDTSCoreBitRate() {
        return hdmi_dtscore_bit_rate_;
    }
    void setHdmiDTSCoreBitRate(int32_t hdmi_dtscore_bit_rate) {
        hdmi_dtscore_bit_rate_ = hdmi_dtscore_bit_rate;
        changed_properties_.emplace(kHdmiDTSCoreBitRate);
    }
    int32_t getHdmiDDPKaraoke() {
        return hdmi_ddpkaraoke_;
    }
    void setHdmiDDPKaraoke(int32_t hdmi_ddpkaraoke) {
        hdmi_ddpkaraoke_ = hdmi_ddpkaraoke;
        changed_properties_.emplace(kHdmiDDPKaraoke);
    }
        int32_t getHdmiDTSXSpkrout() {
        return hdmi_dtsxspkrout_;
    }
    void setHdmiDTSXSpkrout(int32_t hdmi_dtsxspkrout) {
        hdmi_dtsxspkrout_ = hdmi_dtsxspkrout;
        changed_properties_.emplace(kHdmiDTSXSpkrout);
    }
        int32_t getSpdifDTSXSpkrout() {
        return spdif_dtsxspkrout_;
    }
    void setSpdifDTSXSpkrout(int32_t spdif_dtsxspkrout) {
        spdif_dtsxspkrout_ = spdif_dtsxspkrout;
        changed_properties_.emplace(kSpdifDTSXSpkrout);
    }
        int32_t getHdmiDTSXSetDialogcontrol() {
        return hdmi_dtsxset_dialogcontrol_;
    }
    void setHdmiDTSXSetDialogcontrol(int32_t hdmi_dtsxset_dialogcontrol) {
        hdmi_dtsxset_dialogcontrol_ = hdmi_dtsxset_dialogcontrol;
        changed_properties_.emplace(kHdmiDTSXSetDialogcontrol);
    }
        int32_t getSpdifDTSXSetDialogcontrol() {
        return spdif_dtsxset_dialogcontrol_;
    }
    void setSpdifDTSXSetDialogcontrol(int32_t spdif_dtsxset_dialogcontrol) {
        spdif_dtsxset_dialogcontrol_ = spdif_dtsxset_dialogcontrol;
        changed_properties_.emplace(kSpdifDTSXSetDialogcontrol);
    }
        int32_t getHdmiDTSXGetDialogcontrol() {
        return hdmi_dtsxget_dialogcontrol_;
    }
    void setHdmiDTSXGetDialogcontrol(int32_t hdmi_dtsxget_dialogcontrol) {
        hdmi_dtsxget_dialogcontrol_ = hdmi_dtsxget_dialogcontrol;
        changed_properties_.emplace(kHdmiDTSXGetDialogcontrol);
    }
        int32_t getSpdifDTSXGetDialogcontrol() {
        return spdif_dtsxget_dialogcontrol_;
    }
    void setSpdifDTSXGetDialogcontrol(int32_t spdif_dtsxget_dialogcontrol) {
        spdif_dtsxget_dialogcontrol_ = spdif_dtsxget_dialogcontrol;
        changed_properties_.emplace(kSpdifDTSXGetDialogcontrol);
    }
        int32_t getHdmiDTSXEnableDirectmode() {
        return hdmi_dtsxenable_directmode_;
    }
    void setHdmiDTSXEnableDirectmode(int32_t hdmi_dtsxenable_directmode) {
        hdmi_dtsxenable_directmode_ = hdmi_dtsxenable_directmode;
        changed_properties_.emplace(kHdmiDTSXEnableDirectmode);
    }
        int32_t getSpdifDTSXEnableDirectmode() {
        return spdif_dtsxenable_directmode_;
    }
    void setSpdifDTSXEnableDirectmode(int32_t spdif_dtsxenable_directmode) {
        spdif_dtsxenable_directmode_ = spdif_dtsxenable_directmode;
        changed_properties_.emplace(kSpdifDTSXEnableDirectmode);
    }
        int32_t getHdmiDTSXDisableDirectmode() {
        return hdmi_dtsxdisable_directmode_;
    }
    void setHdmiDTSXDisableDirectmode(int32_t hdmi_dtsxdisable_directmode) {
        hdmi_dtsxdisable_directmode_ = hdmi_dtsxdisable_directmode;
        changed_properties_.emplace(kHdmiDTSXDisableDirectmode);
    }
        int32_t getSpdifDTSXDisableDirectmode() {
        return spdif_dtsxdisable_directmode_;
    }
    void setSpdifDTSXDisableDirectmode(int32_t spdif_dtsxdisable_directmode) {
        spdif_dtsxdisable_directmode_ = spdif_dtsxdisable_directmode;
        changed_properties_.emplace(kSpdifDTSXDisableDirectmode);
    }
        int32_t getHdmiDTSXEnableBlindparma() {
        return hdmi_dtsxenable_blindparma_;
    }
    void setHdmiDTSXEnableBlindparma(int32_t hdmi_dtsxenable_blindparma) {
        hdmi_dtsxenable_blindparma_ = hdmi_dtsxenable_blindparma;
        changed_properties_.emplace(kHdmiDTSXEnableBlindparma);
    }
        int32_t getSpdifDTSXEnableBlindparma() {
        return spdif_dtsxenable_blindparma_;
    }
    void setSpdifDTSXEnableBlindparma(int32_t spdif_dtsxenable_blindparma) {
        spdif_dtsxenable_blindparma_ = spdif_dtsxenable_blindparma;
        changed_properties_.emplace(kSpdifDTSXEnableBlindparma);
    }
        int32_t getHdmiDTSXDisableBlindparma() {
        return hdmi_dtsxdisable_blindparma_;
    }
    void setHdmiDTSXDisableBlindparma(int32_t hdmi_dtsxdisable_blindparma) {
        hdmi_dtsxdisable_blindparma_ = hdmi_dtsxdisable_blindparma;
        changed_properties_.emplace(kHdmiDTSXDisableBlindparma);
    }
        int32_t getSpdifDTSXDisableBlindparma() {
        return spdif_dtsxdisable_blindparma_;
    }
    void setSpdifDTSXDisableBlindparma(int32_t spdif_dtsxdisable_blindparma) {
        spdif_dtsxdisable_blindparma_ = spdif_dtsxdisable_blindparma;
        changed_properties_.emplace(kSpdifDTSXDisableBlindparma);
    }
        int32_t getHdmiDTSXEsMatrix() {
        return hdmi_dtsxes_matrix_;
    }
    void setHdmiDTSXEsMatrix(int32_t hdmi_dtsxes_matrix) {
        hdmi_dtsxes_matrix_ = hdmi_dtsxes_matrix;
        changed_properties_.emplace(kHdmiDTSXEsMatrix);
    }
        int32_t getSpdifDTSXEsMatrix() {
        return spdif_dtsxes_matrix_;
    }
    void setSpdifDTSXEsMatrix(int32_t spdif_dtsxes_matrix) {
        spdif_dtsxes_matrix_ = spdif_dtsxes_matrix;
        changed_properties_.emplace(kSpdifDTSXEsMatrix);
    }
        int32_t getHdmiDTSXEnableAnalogCompensation() {
        return hdmi_dtsxenable_analog_compensation_;
    }
    void setHdmiDTSXEnableAnalogCompensation(int32_t hdmi_dtsxenable_analog_compensation) {
        hdmi_dtsxenable_analog_compensation_ = hdmi_dtsxenable_analog_compensation;
        changed_properties_.emplace(kHdmiDTSXEnableAnalogCompensation);
    }
        int32_t getSpdifDTSXEnableAnalogCompensation() {
        return spdif_dtsxenable_analog_compensation_;
    }
    void setSpdifDTSXEnableAnalogCompensation(int32_t spdif_dtsxenable_analog_compensation) {
        spdif_dtsxenable_analog_compensation_ = spdif_dtsxenable_analog_compensation;
        changed_properties_.emplace(kSpdifDTSXEnableAnalogCompensation);
    }
        int32_t getHdmiDTSXSetAnalogcompmaxUsergain() {
        return hdmi_dtsxset_analogcompmax_usergain_;
    }
    void setHdmiDTSXSetAnalogcompmaxUsergain(int32_t hdmi_dtsxset_analogcompmax_usergain) {
        hdmi_dtsxset_analogcompmax_usergain_ = hdmi_dtsxset_analogcompmax_usergain;
        changed_properties_.emplace(kHdmiDTSXSetAnalogcompmaxUsergain);
    }
        int32_t getSpdifDTSXSetAnalogcompmaxUsergain() {
        return spdif_dtsxset_analogcompmax_usergain_;
    }
    void setSpdifDTSXSetAnalogcompmaxUsergain(int32_t spdif_dtsxset_analogcompmax_usergain) {
        spdif_dtsxset_analogcompmax_usergain_ = spdif_dtsxset_analogcompmax_usergain;
        changed_properties_.emplace(kSpdifDTSXSetAnalogcompmaxUsergain);
    }
        std::vector<int32_t> getHdmiDTSXDrcmode() {
        return hdmi_dtsxdrcmode_;
    }
    void setHdmiDTSXDrcmode(std::vector<int32_t> hdmi_dtsxdrcmode) {
        hdmi_dtsxdrcmode_ = hdmi_dtsxdrcmode;
        changed_properties_.emplace(kHdmiDTSXDrcmode);
    }
        std::vector<int32_t> getSpdifDTSXDrcmode() {
        return spdif_dtsxdrcmode_;
    }
    void setSpdifDTSXDrcmode(std::vector<int32_t> spdif_dtsxdrcmode) {
        spdif_dtsxdrcmode_ = spdif_dtsxdrcmode;
        changed_properties_.emplace(kSpdifDTSXDrcmode);
    }
        std::vector<int32_t> getHdmiMATDrcmode() {
        return hdmi_matdrcmode_;
    }
    void setHdmiMATDrcmode(std::vector<int32_t> hdmi_matdrcmode) {
        hdmi_matdrcmode_ = hdmi_matdrcmode;
        changed_properties_.emplace(kHdmiMATDrcmode);
    }
        std::vector<int32_t> getSpdifMATDrcmode() {
        return spdif_matdrcmode_;
    }
    void setSpdifMATDrcmode(std::vector<int32_t> spdif_matdrcmode) {
        spdif_matdrcmode_ = spdif_matdrcmode;
        changed_properties_.emplace(kSpdifMATDrcmode);
    }
        std::string getHdmiReadDetectedformat() {
        return hdmi_read_detectedformat_;
    }
    void setHdmiReadDetectedformat(std::string hdmi_read_detectedformat) {
        hdmi_read_detectedformat_ = hdmi_read_detectedformat;
        changed_properties_.emplace(kHdmiReadDetectedformat);
    }
        std::string getSpdifReadDetectedformat() {
        return spdif_read_detectedformat_;
    }
    void setSpdifReadDetectedformat(std::string spdif_read_detectedformat) {
        spdif_read_detectedformat_ = spdif_read_detectedformat;
        changed_properties_.emplace(kSpdifReadDetectedformat);
    }
        std::string getCurrentClock() {
        return current_clock_;
    }
    void setCurrentClock(std::string current_clock) {
        current_clock_ = current_clock;
        changed_properties_.emplace(kCurrentClock);
    }
        std::string getCurrentSource() {
        return current_source_;
    }
    void setCurrentSource(std::string current_source) {
        current_source_ = current_source;
        changed_properties_.emplace(kCurrentSource);
    }
    void clearChangedProperties() { changed_properties_.clear(); }
    std::unordered_set<std::string> getChangedProperties() { return changed_properties_; }

 private:
    bool hdmi_dapdlbsurroundmode_;
    bool spdif_dapdlbsurroundmode_;
    int32_t spdif_ddpsurround_downmix_level_;
    std::string spdif_ddpstream_type_;
    int32_t hdmi_dtsx96_flag_;
    int32_t hdmi_ddplfe_;
    uint32_t hdmi_dsdsamplingrate_;
    int32_t hdmi_ddpdual_mono_;
    int32_t spdif_dtsdialogue_normalization_;
    int32_t spdif_ddpdual_mono_;
    int32_t spdif_ddpdialoguenormalization_;
    int32_t spdif_dtsdecode_info_;
    uint32_t hdmi_ddpcustom_ch_map_;
    int32_t spdif_dtsbitrate_;
    int32_t spdif_matgain_required_;
    int32_t hdmi_ddpaudio_coding_mode_;
    int32_t hdmi_ddpsurroundex_;
    int32_t hdmi_dtsdecode_info_;
    int32_t spdif_ddpsurroundex_;
    int32_t hdmi_dtsbitrate_;
    int32_t spdif_ddpbitrate_;
    int32_t spdif_ddpaudio_coding_mode_;
    int32_t spdif_matfba_surround_ch_;
    int32_t spdif_dtsxllsamplerate_;
    bool spdif_dlbsurroundcenterspread_;
    uint32_t hdmi_matfbach_;
    int32_t spdif_dec_ch_config_;
        int32_t spdif_ddpdec_ch_config_;
    int32_t hdmi_dtsxch_flag_;
    int32_t hdmi_aac_dual_mono_channel_mode_;
    int32_t hdmi_ddpcenter_downmix_level_;
    uint32_t spdif_dsdsamplingrate_;
    uint32_t spdif_mattrue_hd_sync_;
    int32_t spdif_dtsextension_mask_;
    uint32_t spdif_matfbach_;
    int32_t hdmi_dtsstream_type_;
    uint32_t hdmi_dec_sampl_freq_;
    std::string spdif_matstreamtype_;
    uint32_t spdif_dec_sampl_freq_;
    int32_t spdif_dtsanalog_compensation_gain_;
    int32_t spdif_dtsstream_type_;
    int32_t spdif_matfba_dialogue_normalization_;
    int32_t hdmi_dtsanalog_compensation_gain_;
    int32_t hdmi_matfba_surround_ch_;
    int32_t spdif_dtsx_drcpercent_;
    uint32_t hdmi_dtssamplerate_;
    uint32_t hdmi_mattrue_hd_sync_;
    uint32_t spdif_dtsprog_ch_mask_;
    int32_t hdmi_dtsx_drcpercent_;
    std::string hdmi_pcmch_config_;
    int32_t spdif_dsdch_config_;
    int32_t hdmi_dtsdialogue_control_availability_;
    std::string hdmi_matstreamtype_;
    int32_t hdmi_dsdch_config_;
    int32_t hdmi_dec_ch_config_;
        int32_t hdmi_ddpdec_ch_config_;
    int32_t hdmi_dtsdialogue_normalization_;
    int32_t spdif_ddpkaraoke_;
    int32_t spdif_dtscore_bit_rate_;
    int32_t hdmi_dtsobject_num_;
    bool hdmi_dlbsurroundcenterspread_;
    int32_t spdif_dtsx96_flag_;
    int32_t hdmi_dtses_matrix_flag_;
    int32_t spdif_dtsembeddeddownmixselect_;
    uint32_t hdmi_matfbbch_;
    int32_t spdif_dtsxch_flag_;
    int32_t spdif_ddplfe_;
    int32_t hdmi_ddpbitrate_;
    int32_t hdmi_ddpdialoguenormalization_;
    std::vector<int32_t> hdmi_ddpdrcmode_;
    uint32_t spdif_ddpcustom_ch_map_;
    int32_t hdmi_matfba_dialogue_normalization_;
    int32_t spdif_ddpcenter_downmix_level_;
    uint32_t hdmi_dtsprog_ch_mask_;
    std::vector<int32_t> spdif_ddpdrcmode_;
    int32_t hdmi_dtsembeddeddownmixselect_;
    uint32_t hdmi_pcmsampling_rate_;
    int32_t hdmi_dtsxllsamplerate_;
    uint32_t spdif_dtssamplerate_;
    std::string hdmi_ddpstream_type_;
    uint32_t spdif_pcmsampling_rate_;
    uint32_t spdif_outputsamplingfrequency_;
    int32_t spdif_dtses_matrix_flag_;
    uint32_t spdif_matfbbch_;
    int32_t hdmi_matgain_required_;
    int32_t spdif_dtsobject_num_;
    std::string spdif_pcmch_config_;
    uint32_t hdmi_outputsamplingfrequency_;
    int32_t hdmi_dtsextension_mask_;
    int32_t hdmi_ddpsurround_downmix_level_;
    int32_t spdif_dtsdialogue_control_availability_;
    int32_t hdmi_dtscore_bit_rate_;
    int32_t hdmi_ddpkaraoke_;
        int32_t hdmi_dtsxspkrout_;
        int32_t spdif_dtsxspkrout_;
        int32_t hdmi_dtsxset_dialogcontrol_;
        int32_t spdif_dtsxset_dialogcontrol_;
        int32_t hdmi_dtsxget_dialogcontrol_;
        int32_t spdif_dtsxget_dialogcontrol_;
        int32_t hdmi_dtsxenable_directmode_;
        int32_t spdif_dtsxenable_directmode_;
        int32_t hdmi_dtsxdisable_directmode_;
        int32_t spdif_dtsxdisable_directmode_;
        int32_t hdmi_dtsxenable_blindparma_;
        int32_t spdif_dtsxenable_blindparma_;
        int32_t hdmi_dtsxdisable_blindparma_;
        int32_t spdif_dtsxdisable_blindparma_;
        int32_t hdmi_dtsxes_matrix_;
        int32_t spdif_dtsxes_matrix_;
        int32_t hdmi_dtsxenable_analog_compensation_;
        int32_t spdif_dtsxenable_analog_compensation_;
        int32_t hdmi_dtsxset_analogcompmax_usergain_;
        int32_t spdif_dtsxset_analogcompmax_usergain_;
        std::vector<int32_t> hdmi_dtsxdrcmode_;
        std::vector<int32_t> spdif_dtsxdrcmode_;
        std::vector<int32_t> hdmi_matdrcmode_;
        std::vector<int32_t> spdif_matdrcmode_;
        std::string hdmi_read_detectedformat_;
        std::string spdif_read_detectedformat_;
        std::string current_clock_;
        std::string current_source_;

    std::unordered_set<std::string> changed_properties_;
};

class CreateSourceIn {
 public:
    CreateSourceIn() = default;
    ~CreateSourceIn() = default;

    std::string getEncoding() { return encoding_; }
    void setEncoding(const std::string &encoding) {
        encoding_ = encoding;
    }

    uint32_t getRate() { return rate_; }
    void setRate(const uint32_t &rate) {
        rate_ = rate;
    }

    std::string getFormat() { return format_; }
    void setFormat(const std::string &format) {
        format_ = format;
    }

    std::string getChannelMap() { return channel_map_; }
    void setChannelMap(const std::string &channel_map) {
        channel_map_ = channel_map;
    }

    std::unordered_map<std::string, uint32_t> getExtraConfigs() { return extra_configs_; }
    void setExtraConfigs(const std::unordered_map<std::string, uint32_t> &extra_configs) {
        extra_configs_ = extra_configs;
    }

 private:
    std::string encoding_;
    uint32_t rate_;
    std::string format_;
    std::string channel_map_;
    std::unordered_map<std::string, uint32_t> extra_configs_;
};

class SetSourceExtClkIn {
 public:
    SetSourceExtClkIn() = default;
    ~SetSourceExtClkIn() = default;
        std::string getSourceName() { return source_name_; }
    void setSourceName(const std::string &source_name) {
       source_name_ = source_name;
    }
        std::string getClockType() { return clock_type_; }
    void setClockType(const std::string &clock_type) {
       clock_type_ = clock_type;
    }
        uint64_t getClockFrequency() { return clock_frequency_; }
    void setClockFrequency(const uint64_t &clock_frequency) {
       clock_frequency_ = clock_frequency;
    }
 private:
        std::string source_name_;
        std::string clock_type_;
        uint64_t clock_frequency_;
    };
}  // namespace adk
#endif  //  AUTOGEN_ATTRIBUTE_CLASSES_H_
