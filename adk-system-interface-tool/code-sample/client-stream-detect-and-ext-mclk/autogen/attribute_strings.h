/*
 * Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of The Linux Foundation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef AUTOGEN_ATTRIBUTE_STRINGS_H_
#define AUTOGEN_ATTRIBUTE_STRINGS_H_

namespace adk {
const char kHdmiDAPDlbsurroundmode[] = "hdmiDAPDlbsurroundmode";
const char kSpdifDAPDlbsurroundmode[] = "spdifDAPDlbsurroundmode";
const char kSpdifDDPSurroundDownmixLevel[] = "spdifDDPSurroundDownmixLevel";
const char kSpdifDDPStreamType[] = "spdifDDPStreamType";
const char kHdmiDTSX96Flag[] = "hdmiDTSX96Flag";
const char kHdmiDDPLfe[] = "hdmiDDPLfe";
const char kHdmiDSDSamplingrate[] = "hdmiDSDSamplingrate";
const char kHdmiDDPDualMono[] = "hdmiDDPDualMono";
const char kSpdifDTSDialogueNormalization[] = "spdifDTSDialogueNormalization";
const char kSpdifDDPDualMono[] = "spdifDDPDualMono";
const char kSpdifDDPDialoguenormalization[] = "spdifDDPDialoguenormalization";
const char kSpdifDTSDecodeInfo[] = "spdifDTSDecodeInfo";
const char kHdmiDDPCustomChMap[] = "hdmiDDPCustomChMap";
const char kSpdifDTSBitrate[] = "spdifDTSBitrate";
const char kSpdifMATGainRequired[] = "spdifMATGainRequired";
const char kHdmiDDPAudioCodingMode[] = "hdmiDDPAudioCodingMode";
const char kHdmiDDPSurroundex[] = "hdmiDDPSurroundex";
const char kHdmiDTSDecodeInfo[] = "hdmiDTSDecodeInfo";
const char kSpdifDDPSurroundex[] = "spdifDDPSurroundex";
const char kHdmiDTSBitrate[] = "hdmiDTSBitrate";
const char kSpdifDDPBitrate[] = "spdifDDPBitrate";
const char kSpdifDDPAudioCodingMode[] = "spdifDDPAudioCodingMode";
const char kSpdifMATFbaSurroundCh[] = "spdifMATFbaSurroundCh";
const char kSpdifDTSXllsamplerate[] = "spdifDTSXllsamplerate";
const char kSpdifDlbsurroundcenterspread[] = "spdifDlbsurroundcenterspread";
const char kHdmiMATFbach[] = "hdmiMATFbach";
const char kSpdifDecChConfig[] = "spdifDecChConfig";
const char kSpdifDDPDecChConfig[] = "spdifDDPDecChConfig";
const char kHdmiDTSXchFlag[] = "hdmiDTSXchFlag";
const char kHdmiAacDualMonoChannelMode[] = "hdmiAacDualMonoChannelMode";
const char kHdmiDDPCenterDownmixLevel[] = "hdmiDDPCenterDownmixLevel";
const char kSpdifDSDSamplingrate[] = "spdifDSDSamplingrate";
const char kSpdifMATTrueHdSync[] = "spdifMATTrueHdSync";
const char kSpdifDTSExtensionMask[] = "spdifDTSExtensionMask";
const char kSpdifMATFbach[] = "spdifMATFbach";
const char kHdmiDTSStreamType[] = "hdmiDTSStreamType";
const char kHdmiDecSamplFreq[] = "hdmiDecSamplFreq";
const char kSpdifMATStreamtype[] = "spdifMATStreamtype";
const char kSpdifDecSamplFreq[] = "spdifDecSamplFreq";
const char kSpdifDTSAnalogCompensationGain[] = "spdifDTSAnalogCompensationGain";
const char kSpdifDTSStreamType[] = "spdifDTSStreamType";
const char kSpdifMATFbaDialogueNormalization[] = "spdifMATFbaDialogueNormalization";
const char kHdmiDTSAnalogCompensationGain[] = "hdmiDTSAnalogCompensationGain";
const char kHdmiMATFbaSurroundCh[] = "hdmiMATFbaSurroundCh";
const char kSpdifDtsxDrcpercent[] = "spdifDtsxDrcpercent";
const char kHdmiDTSSamplerate[] = "hdmiDTSSamplerate";
const char kHdmiMATTrueHdSync[] = "hdmiMATTrueHdSync";
const char kSpdifDTSProgChMask[] = "spdifDTSProgChMask";
const char kHdmiDtsxDrcpercent[] = "hdmiDtsxDrcpercent";
const char kHdmiPCMChConfig[] = "hdmiPCMChConfig";
const char kSpdifDSDChConfig[] = "spdifDSDChConfig";
const char kHdmiDTSDialogueControlAvailability[] = "hdmiDTSDialogueControlAvailability";
const char kHdmiMATStreamtype[] = "hdmiMATStreamtype";
const char kHdmiDSDChConfig[] = "hdmiDSDChConfig";
const char kHdmiDecChConfig[] = "hdmiDecChConfig";
const char kHdmiDDPDecChConfig[] = "hdmiDDPDecChConfig";
const char kHdmiDTSDialogueNormalization[] = "hdmiDTSDialogueNormalization";
const char kSpdifDDPKaraoke[] = "spdifDDPKaraoke";
const char kSpdifDTSCoreBitRate[] = "spdifDTSCoreBitRate";
const char kHdmiDTSObjectNum[] = "hdmiDTSObjectNum";
const char kHdmiDlbsurroundcenterspread[] = "hdmiDlbsurroundcenterspread";
const char kSpdifDTSX96Flag[] = "spdifDTSX96Flag";
const char kHdmiDTSEsMatrixFlag[] = "hdmiDTSEsMatrixFlag";
const char kSpdifDTSEmbeddeddownmixselect[] = "spdifDTSEmbeddeddownmixselect";
const char kHdmiMATFbbch[] = "hdmiMATFbbch";
const char kSpdifDTSXchFlag[] = "spdifDTSXchFlag";
const char kSpdifDDPLfe[] = "spdifDDPLfe";
const char kHdmiDDPBitrate[] = "hdmiDDPBitrate";
const char kHdmiDDPDialoguenormalization[] = "hdmiDDPDialoguenormalization";
const char kHdmiDDPDrcmode[] = "hdmiDDPDrcmode";
const char kSpdifDDPCustomChMap[] = "spdifDDPCustomChMap";
const char kHdmiMATFbaDialogueNormalization[] = "hdmiMATFbaDialogueNormalization";
const char kSpdifDDPCenterDownmixLevel[] = "spdifDDPCenterDownmixLevel";
const char kHdmiDTSProgChMask[] = "hdmiDTSProgChMask";
const char kSpdifDDPDrcmode[] = "spdifDDPDrcmode";
const char kHdmiDTSEmbeddeddownmixselect[] = "hdmiDTSEmbeddeddownmixselect";
const char kHdmiPCMsamplingRate[] = "hdmiPCMsamplingRate";
const char kHdmiDTSXllsamplerate[] = "hdmiDTSXllsamplerate";
const char kSpdifDTSSamplerate[] = "spdifDTSSamplerate";
const char kHdmiDDPStreamType[] = "hdmiDDPStreamType";
const char kSpdifPCMsamplingRate[] = "spdifPCMsamplingRate";
const char kSpdifOutputsamplingfrequency[] = "spdifOutputsamplingfrequency";
const char kSpdifDTSEsMatrixFlag[] = "spdifDTSEsMatrixFlag";
const char kSpdifMATFbbch[] = "spdifMATFbbch";
const char kHdmiMATGainRequired[] = "hdmiMATGainRequired";
const char kSpdifDTSObjectNum[] = "spdifDTSObjectNum";
const char kSpdifPCMChConfig[] = "spdifPCMChConfig";
const char kHdmiOutputsamplingfrequency[] = "hdmiOutputsamplingfrequency";
const char kHdmiDTSExtensionMask[] = "hdmiDTSExtensionMask";
const char kHdmiDDPSurroundDownmixLevel[] = "hdmiDDPSurroundDownmixLevel";
const char kSpdifDTSDialogueControlAvailability[] = "spdifDTSDialogueControlAvailability";
const char kHdmiDTSCoreBitRate[] = "hdmiDTSCoreBitRate";
const char kHdmiDDPKaraoke[] = "hdmiDDPKaraoke";
const char kHdmiDTSXSpkrout[] = "hdmiDTSXSpkrout";
const char kSpdifDTSXSpkrout[] = "spdifDTSXSpkrout";
const char kHdmiDTSXSetDialogcontrol[] = "hdmiDTSXSetDialogcontrol";
const char kSpdifDTSXSetDialogcontrol[] = "spdifDTSXSetDialogcontrol";
const char kHdmiDTSXGetDialogcontrol[] = "hdmiDTSXGetDialogcontrol";
const char kSpdifDTSXGetDialogcontrol[] = "spdifDTSXGetDialogcontrol";
const char kHdmiDTSXEnableDirectmode[] = "hdmiDTSXEnableDirectmode";
const char kSpdifDTSXEnableDirectmode[] = "spdifDTSXEnableDirectmode";
const char kHdmiDTSXDisableDirectmode[] = "hdmiDTSXDisableDirectmode";
const char kSpdifDTSXDisableDirectmode[] = "spdifDTSXDisableDirectmode";
const char kHdmiDTSXEnableBlindparma[] = "hdmiDTSXEnableBlindparma";
const char kSpdifDTSXEnableBlindparma[] = "spdifDTSXEnableBlindparma";
const char kHdmiDTSXDisableBlindparma[] = "hdmiDTSXDisableBlindparma";
const char kSpdifDTSXDisableBlindparma[] = "spdifDTSXDisableBlindparma";
const char kHdmiDTSXEsMatrix[] = "hdmiDTSXEsMatrix";
const char kSpdifDTSXEsMatrix[] = "spdifDTSXEsMatrix";
const char kHdmiDTSXEnableAnalogCompensation[] = "hdmiDTSXEnableAnalogCompensation";
const char kSpdifDTSXEnableAnalogCompensation[] = "spdifDTSXEnableAnalogCompensation";
const char kHdmiDTSXSetAnalogcompmaxUsergain[] = "hdmiDTSXSetAnalogcompmaxUsergain";
const char kSpdifDTSXSetAnalogcompmaxUsergain[] = "spdifDTSXSetAnalogcompmaxUsergain";
const char kHdmiDTSXDrcmode[] = "hdmiDTSXDrcmode";
const char kSpdifDTSXDrcmode[] = "spdifDTSXDrcmode";
const char kHdmiMATDrcmode[] = "hdmiMATDrcmode";
const char kSpdifMATDrcmode[] = "spdifMATDrcmode";
const char kHdmiReadDetectedformat[] = "hdmiReadDetectedformat";
const char kSpdifReadDetectedformat[] = "spdifReadDetectedformat";
const char kCurrentClock[] = "currentClock";
const char kCurrentSource[] = "currentSource";
const char kSpdifDecodeRestart[] = "spdifDecodeRestart";
const char kDeleteSource[] = "deleteSource";
const char kCreateSource[] = "createSource";
const char kHdmiDecodeRestart[] = "hdmiDecodeRestart";
const char kSetSourceExtClk[] = "setSourceExtClk";
const char kHdmiDetectedFormat[] = "hdmiDetectedFormat";
const char kHdmiDualMonoAAC[] = "hdmiDualMonoAAC";
const char kSpdifDetectedFormat[] = "spdifDetectedFormat";
const char kSetSourceEvent[] = "setSourceEvent";
}  // namespace adk

#endif  // AUTOGEN_ATTRIBUTE_STRINGS_H_
