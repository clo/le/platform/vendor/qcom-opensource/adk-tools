target_sources(${PROJECT_LIBNAME} PRIVATE
    ${CMAKE_CURRENT_SOURCE_DIR}/attribute_strings.h
    ${CMAKE_CURRENT_SOURCE_DIR}/attribute_classes.h
    ${CMAKE_CURRENT_SOURCE_DIR}/adk_system_interface.h
    ${CMAKE_CURRENT_SOURCE_DIR}/adk_system_interface.cc
    ${CMAKE_CURRENT_SOURCE_DIR}/interface_handlers.h
    ${CMAKE_CURRENT_SOURCE_DIR}/interface_handlers.cc
    )

add_subdirectory(internal_api)
