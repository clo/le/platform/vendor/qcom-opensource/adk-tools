
/*
 * Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of The Linux Foundation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "interface_handlers.h"

namespace adk {

PropertiesHandler::PropertiesHandler(Dbus *dbus) :
    dbus_(dbus) {
    dbus_->setPropertiesChangedListener(
        std::bind(&PropertiesHandler::propertiesChangedCb, this,
            std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
}

void PropertiesHandler::propertiesChangedCb(const std::string &obj_path,
    const std::string &iface_name, GVariantIter *iter) {
    g_print("PropertiesHandler::propertiesChangedCb \n");
    if (iface_name == std::string(adk::dbus::kDapConfigIface)) {
        PropertiesAttr changed_properties = processDapConfigIfaceResponse(obj_path, iter);
        for (auto &f : properties_changed_listeners_) {
            f(changed_properties, obj_path);
        }
        return;
    }
    if (iface_name == std::string(adk::dbus::kDdpConfigIface)) {
        PropertiesAttr changed_properties = processDdpConfigIfaceResponse(obj_path, iter);
        for (auto &f : properties_changed_listeners_) {
            f(changed_properties, obj_path);
        }
        return;
    }
    if (iface_name == std::string(adk::dbus::kDtsxConfigIface)) {
        PropertiesAttr changed_properties = processDtsxConfigIfaceResponse(obj_path, iter);
        for (auto &f : properties_changed_listeners_) {
            f(changed_properties, obj_path);
        }
        return;
    }
    if (iface_name == std::string(adk::dbus::kDsdConfigIface)) {
        PropertiesAttr changed_properties = processDsdConfigIfaceResponse(obj_path, iter);
        for (auto &f : properties_changed_listeners_) {
            f(changed_properties, obj_path);
        }
        return;
    }
    if (iface_name == std::string(adk::dbus::kMatConfigIface)) {
        PropertiesAttr changed_properties = processMatConfigIfaceResponse(obj_path, iter);
        for (auto &f : properties_changed_listeners_) {
            f(changed_properties, obj_path);
        }
        return;
    }
    if (iface_name == std::string(adk::dbus::kStreamConfigIface)) {
        PropertiesAttr changed_properties = processStreamConfigIfaceResponse(obj_path, iter);
        for (auto &f : properties_changed_listeners_) {
            f(changed_properties, obj_path);
        }
        return;
    }
    if (iface_name == std::string(adk::dbus::kAacConfigIface)) {
        PropertiesAttr changed_properties = processAacConfigIfaceResponse(obj_path, iter);
        for (auto &f : properties_changed_listeners_) {
            f(changed_properties, obj_path);
        }
        return;
    }
    if (iface_name == std::string(adk::dbus::kPcmConfigIface)) {
        PropertiesAttr changed_properties = processPcmConfigIfaceResponse(obj_path, iter);
        for (auto &f : properties_changed_listeners_) {
            f(changed_properties, obj_path);
        }
        return;
    }
        if (iface_name == std::string(adk::dbus::kAudiomanagerDeviceControllerIface)) {
        PropertiesAttr changed_properties = processAudiomanagerDeviceControllerIfaceResponse(obj_path, iter);
        for (auto &f : properties_changed_listeners_) {
            f(changed_properties, obj_path);
        }
        return;
    }
}

PropertiesAttr PropertiesHandler::processDapConfigIfaceResponse(
    const std::string &obj_path, GVariantIter *iter) {
    g_print("PropertiesHandler::processKDapConfigIfaceResponse obj_path: %s \n", obj_path.c_str());
    char *key;
    GVariant *tmp = nullptr;
    PropertiesAttr changed_properties;
    bool obj_id_map_changed = false;

    std::lock_guard<std::mutex> lock(mutex_);
    changed_properties.clearChangedProperties();
    while (g_variant_iter_loop(iter, "{sv}", &key, &tmp)) {
        if (std::string(key) == "DlbSurroundMode") {
            changed_properties.setHdmiDAPDlbsurroundmode(g_variant_get_boolean(tmp));
            continue;
        }
        if (std::string(key) == "DlbSurroundMode") {
            changed_properties.setSpdifDAPDlbsurroundmode(g_variant_get_boolean(tmp));
            continue;
        }
        if (std::string(key) == "DlbSurroundCenterSpread") {
            changed_properties.setSpdifDlbsurroundcenterspread(g_variant_get_boolean(tmp));
            continue;
        }
        if (std::string(key) == "DlbSurroundCenterSpread") {
            changed_properties.setHdmiDlbsurroundcenterspread(g_variant_get_boolean(tmp));
            continue;
        }
    }

    if (tmp) {
        g_variant_unref(tmp);
    }
    if (obj_id_map_changed) {
        for (auto &f : obj_id_map_changed_listeners_) {
            f(obj_id_map_);
        }
    }
    return changed_properties;
}
PropertiesAttr PropertiesHandler::processDdpConfigIfaceResponse(
    const std::string &obj_path, GVariantIter *iter) {
    g_print("PropertiesHandler::processKDdpConfigIfaceResponse obj_path: %s \n", obj_path.c_str());
    char *key;
    GVariant *tmp = nullptr;
    PropertiesAttr changed_properties;
    bool obj_id_map_changed = false;

    std::lock_guard<std::mutex> lock(mutex_);
    changed_properties.clearChangedProperties();
    while (g_variant_iter_loop(iter, "{sv}", &key, &tmp)) {
        if (std::string(key) == "SurroundDownmixLevel") {
            changed_properties.setSpdifDDPSurroundDownmixLevel(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "StreamType") {
            gsize length;
            changed_properties.setSpdifDDPStreamType(static_cast<std::string>(g_variant_get_string(tmp, &length)));
            continue;
        }
        if (std::string(key) == "LfeOn") {
            changed_properties.setHdmiDDPLfe(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "DualMono") {
            changed_properties.setHdmiDDPDualMono(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "DualMono") {
            changed_properties.setSpdifDDPDualMono(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "DialogueNormalization") {
            changed_properties.setSpdifDDPDialoguenormalization(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "CustomChMap") {
            changed_properties.setHdmiDDPCustomChMap(g_variant_get_uint32(tmp));
            continue;
        }
        if (std::string(key) == "AudioCodingMode") {
            changed_properties.setHdmiDDPAudioCodingMode(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "SurroundEx") {
            changed_properties.setHdmiDDPSurroundex(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "SurroundEx") {
            changed_properties.setSpdifDDPSurroundex(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "BitRate") {
            changed_properties.setSpdifDDPBitrate(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "AudioCodingMode") {
            changed_properties.setSpdifDDPAudioCodingMode(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "DecChConfig") {
            changed_properties.setSpdifDDPDecChConfig(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "CenterDownmixLevel") {
            changed_properties.setHdmiDDPCenterDownmixLevel(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "DecChConfig") {
            changed_properties.setHdmiDDPDecChConfig(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "Karaoke") {
            changed_properties.setSpdifDDPKaraoke(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "LfeOn") {
            changed_properties.setSpdifDDPLfe(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "BitRate") {
            changed_properties.setHdmiDDPBitrate(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "DialogueNormalization") {
            changed_properties.setHdmiDDPDialoguenormalization(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "DrcMode") {
            GVariantIter property_iter;
            g_variant_iter_init(&property_iter, tmp);
            changed_properties.setHdmiDDPDrcmode(dbus_->parseArrayOfInt32(property_iter));
            continue;
        }
        if (std::string(key) == "CustomChMap") {
            changed_properties.setSpdifDDPCustomChMap(g_variant_get_uint32(tmp));
            continue;
        }
        if (std::string(key) == "CenterDownmixLevel") {
            changed_properties.setSpdifDDPCenterDownmixLevel(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "DrcMode") {
            GVariantIter property_iter;
            g_variant_iter_init(&property_iter, tmp);
            changed_properties.setSpdifDDPDrcmode(dbus_->parseArrayOfInt32(property_iter));
            continue;
        }
        if (std::string(key) == "StreamType") {
            gsize length;
            changed_properties.setHdmiDDPStreamType(static_cast<std::string>(g_variant_get_string(tmp, &length)));
            continue;
        }
        if (std::string(key) == "SurroundDownmixLevel") {
            changed_properties.setHdmiDDPSurroundDownmixLevel(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "Karaoke") {
            changed_properties.setHdmiDDPKaraoke(g_variant_get_int32(tmp));
            continue;
        }
    }

    if (tmp) {
        g_variant_unref(tmp);
    }
    if (obj_id_map_changed) {
        for (auto &f : obj_id_map_changed_listeners_) {
            f(obj_id_map_);
        }
    }
    return changed_properties;
}
PropertiesAttr PropertiesHandler::processDtsxConfigIfaceResponse(
    const std::string &obj_path, GVariantIter *iter) {
    g_print("PropertiesHandler::processKDtsxConfigIfaceResponse obj_path: %s \n", obj_path.c_str());
    char *key;
    GVariant *tmp = nullptr;
    PropertiesAttr changed_properties;
    bool obj_id_map_changed = false;

    std::lock_guard<std::mutex> lock(mutex_);
    changed_properties.clearChangedProperties();
    while (g_variant_iter_loop(iter, "{sv}", &key, &tmp)) {
        if (std::string(key) == "X96Flag") {
            changed_properties.setHdmiDTSX96Flag(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "DialogueNormalization") {
            changed_properties.setSpdifDTSDialogueNormalization(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "DecodeInfo") {
            changed_properties.setSpdifDTSDecodeInfo(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "BitRate") {
            changed_properties.setSpdifDTSBitrate(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "DecodeInfo") {
            changed_properties.setHdmiDTSDecodeInfo(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "BitRate") {
            changed_properties.setHdmiDTSBitrate(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "XllSampleRate") {
            changed_properties.setSpdifDTSXllsamplerate(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "XchFlag") {
            changed_properties.setHdmiDTSXchFlag(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "ExtensionMask") {
            changed_properties.setSpdifDTSExtensionMask(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "StreamType") {
            changed_properties.setHdmiDTSStreamType(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "AnalogCompensationGain") {
            changed_properties.setSpdifDTSAnalogCompensationGain(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "StreamType") {
            changed_properties.setSpdifDTSStreamType(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "AnalogCompensationGain") {
            changed_properties.setHdmiDTSAnalogCompensationGain(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "DrcPercent") {
            changed_properties.setSpdifDtsxDrcpercent(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "SampleRate") {
            changed_properties.setHdmiDTSSamplerate(g_variant_get_uint32(tmp));
            continue;
        }
        if (std::string(key) == "ProgChMask") {
            changed_properties.setSpdifDTSProgChMask(g_variant_get_uint32(tmp));
            continue;
        }
        if (std::string(key) == "DrcPercent") {
            changed_properties.setHdmiDtsxDrcpercent(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "DialogueControlAvailability") {
            changed_properties.setHdmiDTSDialogueControlAvailability(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "DialogueNormalization") {
            changed_properties.setHdmiDTSDialogueNormalization(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "CoreBitRate") {
            changed_properties.setSpdifDTSCoreBitRate(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "ObjectNum") {
            changed_properties.setHdmiDTSObjectNum(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "X96Flag") {
            changed_properties.setSpdifDTSX96Flag(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "EsMatrixFlag") {
            changed_properties.setHdmiDTSEsMatrixFlag(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "EmbeddedDownMixSelect") {
            changed_properties.setSpdifDTSEmbeddeddownmixselect(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "XchFlag") {
            changed_properties.setSpdifDTSXchFlag(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "ProgChMask") {
            changed_properties.setHdmiDTSProgChMask(g_variant_get_uint32(tmp));
            continue;
        }
        if (std::string(key) == "EmbeddedDownMixSelect") {
            changed_properties.setHdmiDTSEmbeddeddownmixselect(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "XllSampleRate") {
            changed_properties.setHdmiDTSXllsamplerate(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "SampleRate") {
            changed_properties.setSpdifDTSSamplerate(g_variant_get_uint32(tmp));
            continue;
        }
        if (std::string(key) == "EsMatrixFlag") {
            changed_properties.setSpdifDTSEsMatrixFlag(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "ObjectNum") {
            changed_properties.setSpdifDTSObjectNum(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "ExtensionMask") {
            changed_properties.setHdmiDTSExtensionMask(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "DialogueControlAvailability") {
            changed_properties.setSpdifDTSDialogueControlAvailability(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "CoreBitRate") {
            changed_properties.setHdmiDTSCoreBitRate(g_variant_get_int32(tmp));
            continue;
        }
                                                if (std::string(key) == "spkrout") {
                                                            changed_properties.setHdmiDTSXSpkrout(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "spkrout") {
                                                            changed_properties.setSpdifDTSXSpkrout(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "set_dialogcontrol") {
                                                            changed_properties.setHdmiDTSXSetDialogcontrol(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "set_dialogcontrol") {
                                                            changed_properties.setSpdifDTSXSetDialogcontrol(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "get_dialogcontrol") {
                                                            changed_properties.setHdmiDTSXGetDialogcontrol(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "get_dialogcontrol") {
                                                            changed_properties.setSpdifDTSXGetDialogcontrol(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "enable_directmode") {
                                                            changed_properties.setHdmiDTSXEnableDirectmode(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "enable_directmode") {
                                                            changed_properties.setSpdifDTSXEnableDirectmode(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "disable_directmode") {
                                                            changed_properties.setHdmiDTSXDisableDirectmode(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "disable_directmode") {
                                                            changed_properties.setSpdifDTSXDisableDirectmode(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "enable_blindparma") {
                                                            changed_properties.setHdmiDTSXEnableBlindparma(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "enable_blindparma") {
                                                            changed_properties.setSpdifDTSXEnableBlindparma(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "disable_blindparma") {
                                                            changed_properties.setHdmiDTSXDisableBlindparma(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "disable_blindparma") {
                                                            changed_properties.setSpdifDTSXDisableBlindparma(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "es_matrix") {
                                                            changed_properties.setHdmiDTSXEsMatrix(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "es_matrix") {
                                                            changed_properties.setSpdifDTSXEsMatrix(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "enable_analog_compensation") {
                                                            changed_properties.setHdmiDTSXEnableAnalogCompensation(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "enable_analog_compensation") {
                                                            changed_properties.setSpdifDTSXEnableAnalogCompensation(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "set_analogcompmax_usergain") {
                                                            changed_properties.setHdmiDTSXSetAnalogcompmaxUsergain(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "set_analogcompmax_usergain") {
                                                            changed_properties.setSpdifDTSXSetAnalogcompmaxUsergain(g_variant_get_int32(tmp));
                                                continue;
        }
                                if (std::string(key) == "DrcMode") {
                                                GVariantIter property_iter;
            g_variant_iter_init(&property_iter, tmp);
            changed_properties.setHdmiDTSXDrcmode(dbus_->parseArrayOfInt32(property_iter));
                                    continue;
        }
                                if (std::string(key) == "DrcMode") {
                                                GVariantIter property_iter;
            g_variant_iter_init(&property_iter, tmp);
            changed_properties.setSpdifDTSXDrcmode(dbus_->parseArrayOfInt32(property_iter));
                                    continue;
        }
    }

    if (tmp) {
        g_variant_unref(tmp);
    }
    if (obj_id_map_changed) {
        for (auto &f : obj_id_map_changed_listeners_) {
            f(obj_id_map_);
        }
    }
    return changed_properties;
}
PropertiesAttr PropertiesHandler::processDsdConfigIfaceResponse(
    const std::string &obj_path, GVariantIter *iter) {
    g_print("PropertiesHandler::processKDsdConfigIfaceResponse obj_path: %s \n", obj_path.c_str());
    char *key;
    GVariant *tmp = nullptr;
    PropertiesAttr changed_properties;
    bool obj_id_map_changed = false;

    std::lock_guard<std::mutex> lock(mutex_);
    changed_properties.clearChangedProperties();
    while (g_variant_iter_loop(iter, "{sv}", &key, &tmp)) {
        if (std::string(key) == "SamplingRate") {
            changed_properties.setHdmiDSDSamplingrate(g_variant_get_uint32(tmp));
            continue;
        }
        if (std::string(key) == "SamplingRate") {
            changed_properties.setSpdifDSDSamplingrate(g_variant_get_uint32(tmp));
            continue;
        }
        if (std::string(key) == "ChConfig") {
            changed_properties.setSpdifDSDChConfig(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "ChConfig") {
            changed_properties.setHdmiDSDChConfig(g_variant_get_int32(tmp));
            continue;
        }
    }

    if (tmp) {
        g_variant_unref(tmp);
    }
    if (obj_id_map_changed) {
        for (auto &f : obj_id_map_changed_listeners_) {
            f(obj_id_map_);
        }
    }
    return changed_properties;
}
PropertiesAttr PropertiesHandler::processMatConfigIfaceResponse(
    const std::string &obj_path, GVariantIter *iter) {
    g_print("PropertiesHandler::processKMatConfigIfaceResponse obj_path: %s \n", obj_path.c_str());
    char *key;
    GVariant *tmp = nullptr;
    PropertiesAttr changed_properties;
    bool obj_id_map_changed = false;

    std::lock_guard<std::mutex> lock(mutex_);
    changed_properties.clearChangedProperties();
    while (g_variant_iter_loop(iter, "{sv}", &key, &tmp)) {
        if (std::string(key) == "GainRequired") {
            changed_properties.setSpdifMATGainRequired(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "FbaSurroundCh") {
            changed_properties.setSpdifMATFbaSurroundCh(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "FbaCh") {
            changed_properties.setHdmiMATFbach(g_variant_get_uint32(tmp));
            continue;
        }
        if (std::string(key) == "TrueHdSync") {
            changed_properties.setSpdifMATTrueHdSync(g_variant_get_uint32(tmp));
            continue;
        }
        if (std::string(key) == "FbaCh") {
            changed_properties.setSpdifMATFbach(g_variant_get_uint32(tmp));
            continue;
        }
        if (std::string(key) == "StreamType") {
            gsize length;
            changed_properties.setSpdifMATStreamtype(static_cast<std::string>(g_variant_get_string(tmp, &length)));
            continue;
        }
        if (std::string(key) == "FbaDialogueNormalization") {
            changed_properties.setSpdifMATFbaDialogueNormalization(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "FbaSurroundCh") {
            changed_properties.setHdmiMATFbaSurroundCh(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "TrueHdSync") {
            changed_properties.setHdmiMATTrueHdSync(g_variant_get_uint32(tmp));
            continue;
        }
        if (std::string(key) == "StreamType") {
            gsize length;
            changed_properties.setHdmiMATStreamtype(static_cast<std::string>(g_variant_get_string(tmp, &length)));
            continue;
        }
        if (std::string(key) == "FbbCh") {
            changed_properties.setHdmiMATFbbch(g_variant_get_uint32(tmp));
            continue;
        }
        if (std::string(key) == "FbaDialogueNormalization") {
            changed_properties.setHdmiMATFbaDialogueNormalization(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "FbbCh") {
            changed_properties.setSpdifMATFbbch(g_variant_get_uint32(tmp));
            continue;
        }
        if (std::string(key) == "GainRequired") {
            changed_properties.setHdmiMATGainRequired(g_variant_get_int32(tmp));
            continue;
        }
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                if (std::string(key) == "DrcMode") {
                                                GVariantIter property_iter;
            g_variant_iter_init(&property_iter, tmp);
            changed_properties.setHdmiMATDrcmode(dbus_->parseArrayOfInt32(property_iter));
                                    continue;
        }
                                if (std::string(key) == "DrcMode") {
                                                GVariantIter property_iter;
            g_variant_iter_init(&property_iter, tmp);
            changed_properties.setSpdifMATDrcmode(dbus_->parseArrayOfInt32(property_iter));
                                    continue;
        }
    }

    if (tmp) {
        g_variant_unref(tmp);
    }
    if (obj_id_map_changed) {
        for (auto &f : obj_id_map_changed_listeners_) {
            f(obj_id_map_);
        }
    }
    return changed_properties;
}
PropertiesAttr PropertiesHandler::processStreamConfigIfaceResponse(
    const std::string &obj_path, GVariantIter *iter) {
    g_print("PropertiesHandler::processKStreamConfigIfaceResponse obj_path: %s \n", obj_path.c_str());
    char *key;
    GVariant *tmp = nullptr;
    PropertiesAttr changed_properties;
    bool obj_id_map_changed = false;

    std::lock_guard<std::mutex> lock(mutex_);
    changed_properties.clearChangedProperties();
    while (g_variant_iter_loop(iter, "{sv}", &key, &tmp)) {
        if (std::string(key) == "DecChConfig") {
            changed_properties.setSpdifDecChConfig(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "DecSamplFreq") {
            changed_properties.setHdmiDecSamplFreq(g_variant_get_uint32(tmp));
            continue;
        }
        if (std::string(key) == "DecSamplFreq") {
            changed_properties.setSpdifDecSamplFreq(g_variant_get_uint32(tmp));
            continue;
        }
        if (std::string(key) == "DecChConfig") {
            changed_properties.setHdmiDecChConfig(g_variant_get_int32(tmp));
            continue;
        }
        if (std::string(key) == "OutputSamplingFrequency") {
            changed_properties.setSpdifOutputsamplingfrequency(g_variant_get_uint32(tmp));
            continue;
        }
        if (std::string(key) == "OutputSamplingFrequency") {
            changed_properties.setHdmiOutputsamplingfrequency(g_variant_get_uint32(tmp));
                                                continue;
        }
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                if (std::string(key) == "DetectedFormat") {
                                                            gsize length;
            changed_properties.setHdmiReadDetectedformat(static_cast<std::string>(g_variant_get_string(tmp, &length)));
                                                continue;
        }
                                if (std::string(key) == "DetectedFormat") {
                                                            gsize length;
            changed_properties.setSpdifReadDetectedformat(static_cast<std::string>(g_variant_get_string(tmp, &length)));
            continue;
        }
    }

    if (tmp) {
        g_variant_unref(tmp);
    }
    if (obj_id_map_changed) {
        for (auto &f : obj_id_map_changed_listeners_) {
            f(obj_id_map_);
        }
    }
    return changed_properties;
}
PropertiesAttr PropertiesHandler::processAacConfigIfaceResponse(
    const std::string &obj_path, GVariantIter *iter) {
    g_print("PropertiesHandler::processKAacConfigIfaceResponse obj_path: %s \n", obj_path.c_str());
    char *key;
    GVariant *tmp = nullptr;
    PropertiesAttr changed_properties;
    bool obj_id_map_changed = false;

    std::lock_guard<std::mutex> lock(mutex_);
    changed_properties.clearChangedProperties();
    while (g_variant_iter_loop(iter, "{sv}", &key, &tmp)) {
        if (std::string(key) == "AacDualMonoChannelMode") {
            changed_properties.setHdmiAacDualMonoChannelMode(g_variant_get_int32(tmp));
            continue;
        }
    }

    if (tmp) {
        g_variant_unref(tmp);
    }
    if (obj_id_map_changed) {
        for (auto &f : obj_id_map_changed_listeners_) {
            f(obj_id_map_);
        }
    }
    return changed_properties;
}
PropertiesAttr PropertiesHandler::processPcmConfigIfaceResponse(
    const std::string &obj_path, GVariantIter *iter) {
    g_print("PropertiesHandler::processKPcmConfigIfaceResponse obj_path: %s \n", obj_path.c_str());
    char *key;
    GVariant *tmp = nullptr;
    PropertiesAttr changed_properties;
    bool obj_id_map_changed = false;

    std::lock_guard<std::mutex> lock(mutex_);
    changed_properties.clearChangedProperties();
    while (g_variant_iter_loop(iter, "{sv}", &key, &tmp)) {
        if (std::string(key) == "ChConfig") {
            gsize length;
            changed_properties.setHdmiPCMChConfig(static_cast<std::string>(g_variant_get_string(tmp, &length)));
            continue;
        }
        if (std::string(key) == "SamplingRate") {
            changed_properties.setHdmiPCMsamplingRate(g_variant_get_uint32(tmp));
            continue;
        }
        if (std::string(key) == "SamplingRate") {
            changed_properties.setSpdifPCMsamplingRate(g_variant_get_uint32(tmp));
            continue;
        }
        if (std::string(key) == "ChConfig") {
            gsize length;
            changed_properties.setSpdifPCMChConfig(static_cast<std::string>(g_variant_get_string(tmp, &length)));
            continue;
        }
    }

    if (tmp) {
        g_variant_unref(tmp);
    }
    if (obj_id_map_changed) {
        for (auto &f : obj_id_map_changed_listeners_) {
            f(obj_id_map_);
        }
    }
    return changed_properties;
}
PropertiesAttr PropertiesHandler::processAudiomanagerDeviceControllerIfaceResponse(
    const std::string &obj_path, GVariantIter *iter) {
    g_print("PropertiesHandler::processKAudiomanagerDeviceControllerIfaceResponse obj_path: %s \n", obj_path.c_str());
    char *key;
    GVariant *tmp = nullptr;
    PropertiesAttr changed_properties;
    bool obj_id_map_changed = false;
    std::lock_guard<std::mutex> lock(mutex_);
    changed_properties.clearChangedProperties();
    while (g_variant_iter_loop(iter, "{sv}", &key, &tmp)) {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        if (std::string(key) == "Clock") {
                                                            gsize length;
            changed_properties.setCurrentClock(static_cast<std::string>(g_variant_get_string(tmp, &length)));
                                                continue;
        }
                                if (std::string(key) == "CurrentSource") {
                                                            gsize length;
            changed_properties.setCurrentSource(static_cast<std::string>(g_variant_get_string(tmp, &length)));
                                                continue;
        }
                    }
    if (tmp) {
        g_variant_unref(tmp);
    }
    if (obj_id_map_changed) {
        for (auto &f : obj_id_map_changed_listeners_) {
            f(obj_id_map_);
        }
    }
    return changed_properties;
}

std::tuple<ErrorCode, bool> PropertiesHandler::readHdmiDAPDlbsurroundmode() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    bool hdmi_dapdlbsurroundmode;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDapConfigIface,
            "DlbSurroundMode"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        hdmi_dapdlbsurroundmode = g_variant_get_boolean(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_dapdlbsurroundmode);
}

ErrorCode PropertiesHandler::updateHdmiDAPDlbsurroundmode(
    bool hdmi_dapdlbsurroundmode) {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ssv)",
            adk::dbus::kDapConfigIface,
            "DlbSurroundMode",
                                    g_variant_new("b", static_cast<uint8_t>(hdmi_dapdlbsurroundmode))
                        ));
    return result;
}
std::tuple<ErrorCode, bool> PropertiesHandler::readSpdifDAPDlbsurroundmode() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    bool spdif_dapdlbsurroundmode;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDapConfigIface,
            "DlbSurroundMode"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        spdif_dapdlbsurroundmode = g_variant_get_boolean(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_dapdlbsurroundmode);
}

ErrorCode PropertiesHandler::updateSpdifDAPDlbsurroundmode(
    bool spdif_dapdlbsurroundmode) {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ssv)",
            adk::dbus::kDapConfigIface,
            "DlbSurroundMode",
                                    g_variant_new("b", static_cast<uint8_t>(spdif_dapdlbsurroundmode))
                        ));
    return result;
}
std::tuple<ErrorCode, int32_t> PropertiesHandler::readSpdifDDPSurroundDownmixLevel() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t spdif_ddpsurround_downmix_level;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDdpConfigIface,
            "SurroundDownmixLevel"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        spdif_ddpsurround_downmix_level = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_ddpsurround_downmix_level);
}

std::tuple<ErrorCode, std::string> PropertiesHandler::readSpdifDDPStreamType() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::string spdif_ddpstream_type;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDdpConfigIface,
            "StreamType"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        gsize length;
        spdif_ddpstream_type = static_cast<std::string>(g_variant_get_string(tmp, &length));
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_ddpstream_type);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readHdmiDTSX96Flag() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t hdmi_dtsx96_flag;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "X96Flag"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        hdmi_dtsx96_flag = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_dtsx96_flag);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readHdmiDDPLfe() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t hdmi_ddplfe;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDdpConfigIface,
            "LfeOn"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        hdmi_ddplfe = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_ddplfe);
}

std::tuple<ErrorCode, uint32_t> PropertiesHandler::readHdmiDSDSamplingrate() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    uint32_t hdmi_dsdsamplingrate;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDsdConfigIface,
            "SamplingRate"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        hdmi_dsdsamplingrate = g_variant_get_uint32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_dsdsamplingrate);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readHdmiDDPDualMono() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t hdmi_ddpdual_mono;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDdpConfigIface,
            "DualMono"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        hdmi_ddpdual_mono = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_ddpdual_mono);
}

ErrorCode PropertiesHandler::updateHdmiDDPDualMono(
    int32_t hdmi_ddpdual_mono) {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ssv)",
            adk::dbus::kDdpConfigIface,
            "DualMono",
                                    g_variant_new("i", hdmi_ddpdual_mono)
                        ));
    return result;
}
std::tuple<ErrorCode, int32_t> PropertiesHandler::readSpdifDTSDialogueNormalization() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t spdif_dtsdialogue_normalization;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "DialogueNormalization"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        spdif_dtsdialogue_normalization = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_dtsdialogue_normalization);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readSpdifDDPDualMono() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t spdif_ddpdual_mono;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDdpConfigIface,
            "DualMono"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        spdif_ddpdual_mono = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_ddpdual_mono);
}

ErrorCode PropertiesHandler::updateSpdifDDPDualMono(
    int32_t spdif_ddpdual_mono) {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ssv)",
            adk::dbus::kDdpConfigIface,
            "DualMono",
                                    g_variant_new("i", spdif_ddpdual_mono)
                        ));
    return result;
}
std::tuple<ErrorCode, int32_t> PropertiesHandler::readSpdifDDPDialoguenormalization() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t spdif_ddpdialoguenormalization;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDdpConfigIface,
            "DialogueNormalization"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        spdif_ddpdialoguenormalization = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_ddpdialoguenormalization);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readSpdifDTSDecodeInfo() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t spdif_dtsdecode_info;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "DecodeInfo"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        spdif_dtsdecode_info = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_dtsdecode_info);
}

std::tuple<ErrorCode, uint32_t> PropertiesHandler::readHdmiDDPCustomChMap() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    uint32_t hdmi_ddpcustom_ch_map;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDdpConfigIface,
            "CustomChMap"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        hdmi_ddpcustom_ch_map = g_variant_get_uint32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_ddpcustom_ch_map);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readSpdifDTSBitrate() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t spdif_dtsbitrate;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "BitRate"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        spdif_dtsbitrate = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_dtsbitrate);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readSpdifMATGainRequired() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t spdif_matgain_required;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kMatConfigIface,
            "GainRequired"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        spdif_matgain_required = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_matgain_required);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readHdmiDDPAudioCodingMode() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t hdmi_ddpaudio_coding_mode;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDdpConfigIface,
            "AudioCodingMode"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        hdmi_ddpaudio_coding_mode = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_ddpaudio_coding_mode);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readHdmiDDPSurroundex() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t hdmi_ddpsurroundex;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDdpConfigIface,
            "SurroundEx"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        hdmi_ddpsurroundex = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_ddpsurroundex);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readHdmiDTSDecodeInfo() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t hdmi_dtsdecode_info;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "DecodeInfo"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        hdmi_dtsdecode_info = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_dtsdecode_info);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readSpdifDDPSurroundex() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t spdif_ddpsurroundex;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDdpConfigIface,
            "SurroundEx"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        spdif_ddpsurroundex = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_ddpsurroundex);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readHdmiDTSBitrate() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t hdmi_dtsbitrate;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "BitRate"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        hdmi_dtsbitrate = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_dtsbitrate);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readSpdifDDPBitrate() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t spdif_ddpbitrate;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDdpConfigIface,
            "BitRate"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        spdif_ddpbitrate = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_ddpbitrate);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readSpdifDDPAudioCodingMode() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t spdif_ddpaudio_coding_mode;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDdpConfigIface,
            "AudioCodingMode"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        spdif_ddpaudio_coding_mode = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_ddpaudio_coding_mode);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readSpdifMATFbaSurroundCh() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t spdif_matfba_surround_ch;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kMatConfigIface,
            "FbaSurroundCh"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        spdif_matfba_surround_ch = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_matfba_surround_ch);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readSpdifDTSXllsamplerate() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t spdif_dtsxllsamplerate;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "XllSampleRate"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        spdif_dtsxllsamplerate = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_dtsxllsamplerate);
}

std::tuple<ErrorCode, bool> PropertiesHandler::readSpdifDlbsurroundcenterspread() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    bool spdif_dlbsurroundcenterspread;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDapConfigIface,
            "DlbSurroundCenterSpread"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        spdif_dlbsurroundcenterspread = g_variant_get_boolean(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_dlbsurroundcenterspread);
}

ErrorCode PropertiesHandler::updateSpdifDlbsurroundcenterspread(
    bool spdif_dlbsurroundcenterspread) {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ssv)",
            adk::dbus::kDapConfigIface,
            "DlbSurroundCenterSpread",
                                    g_variant_new("b", static_cast<uint8_t>(spdif_dlbsurroundcenterspread))
                        ));
    return result;
}
std::tuple<ErrorCode, uint32_t> PropertiesHandler::readHdmiMATFbach() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    uint32_t hdmi_matfbach;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kMatConfigIface,
            "FbaCh"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        hdmi_matfbach = g_variant_get_uint32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_matfbach);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readSpdifDecChConfig() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t spdif_dec_ch_config;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kStreamConfigIface,
            "DecChConfig"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        spdif_dec_ch_config = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_dec_ch_config);
}

ErrorCode PropertiesHandler::updateSpdifDecChConfig(
    int32_t spdif_dec_ch_config) {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ssv)",
            adk::dbus::kStreamConfigIface,
            "DecChConfig",
                                    g_variant_new("i", spdif_dec_ch_config)
                        ));
    return result;
}
std::tuple<ErrorCode, int32_t> PropertiesHandler::readSpdifDDPDecChConfig() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t spdif_ddpdec_ch_config;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ss)",
            adk::dbus::kDdpConfigIface,
            "DecChConfig"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                spdif_ddpdec_ch_config = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_ddpdec_ch_config);
}

ErrorCode PropertiesHandler::updateSpdifDDPDecChConfig(
    int32_t spdif_ddpdec_ch_config) {
    
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kSpdifConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ssv)",
            adk::dbus::kDdpConfigIface,
            "DecChConfig",
                                    g_variant_new("i", spdif_ddpdec_ch_config)
                        ));
    return result;
}
std::tuple<ErrorCode, int32_t> PropertiesHandler::readHdmiDTSXchFlag() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t hdmi_dtsxch_flag;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "XchFlag"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        hdmi_dtsxch_flag = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_dtsxch_flag);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readHdmiAacDualMonoChannelMode() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t hdmi_aac_dual_mono_channel_mode;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kAacConfigIface,
            "AacDualMonoChannelMode"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        hdmi_aac_dual_mono_channel_mode = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_aac_dual_mono_channel_mode);
}

ErrorCode PropertiesHandler::updateHdmiAacDualMonoChannelMode(
    int32_t hdmi_aac_dual_mono_channel_mode) {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ssv)",
            adk::dbus::kAacConfigIface,
            "AacDualMonoChannelMode",
                                    g_variant_new("i", hdmi_aac_dual_mono_channel_mode)
                        ));
    return result;
}
std::tuple<ErrorCode, int32_t> PropertiesHandler::readHdmiDDPCenterDownmixLevel() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t hdmi_ddpcenter_downmix_level;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDdpConfigIface,
            "CenterDownmixLevel"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        hdmi_ddpcenter_downmix_level = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_ddpcenter_downmix_level);
}

std::tuple<ErrorCode, uint32_t> PropertiesHandler::readSpdifDSDSamplingrate() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    uint32_t spdif_dsdsamplingrate;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDsdConfigIface,
            "SamplingRate"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        spdif_dsdsamplingrate = g_variant_get_uint32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_dsdsamplingrate);
}

std::tuple<ErrorCode, uint32_t> PropertiesHandler::readSpdifMATTrueHdSync() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    uint32_t spdif_mattrue_hd_sync;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kMatConfigIface,
            "TrueHdSync"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        spdif_mattrue_hd_sync = g_variant_get_uint32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_mattrue_hd_sync);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readSpdifDTSExtensionMask() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t spdif_dtsextension_mask;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "ExtensionMask"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        spdif_dtsextension_mask = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_dtsextension_mask);
}

std::tuple<ErrorCode, uint32_t> PropertiesHandler::readSpdifMATFbach() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    uint32_t spdif_matfbach;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kMatConfigIface,
            "FbaCh"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        spdif_matfbach = g_variant_get_uint32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_matfbach);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readHdmiDTSStreamType() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t hdmi_dtsstream_type;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "StreamType"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        hdmi_dtsstream_type = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_dtsstream_type);
}

std::tuple<ErrorCode, uint32_t> PropertiesHandler::readHdmiDecSamplFreq() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    uint32_t hdmi_dec_sampl_freq;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kStreamConfigIface,
            "DecSamplFreq"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        hdmi_dec_sampl_freq = g_variant_get_uint32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_dec_sampl_freq);
}

std::tuple<ErrorCode, std::string> PropertiesHandler::readSpdifMATStreamtype() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::string spdif_matstreamtype;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kMatConfigIface,
            "StreamType"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        gsize length;
        spdif_matstreamtype = static_cast<std::string>(g_variant_get_string(tmp, &length));
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_matstreamtype);
}

std::tuple<ErrorCode, uint32_t> PropertiesHandler::readSpdifDecSamplFreq() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    uint32_t spdif_dec_sampl_freq;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kStreamConfigIface,
            "DecSamplFreq"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        spdif_dec_sampl_freq = g_variant_get_uint32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_dec_sampl_freq);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readSpdifDTSAnalogCompensationGain() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t spdif_dtsanalog_compensation_gain;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "AnalogCompensationGain"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        spdif_dtsanalog_compensation_gain = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_dtsanalog_compensation_gain);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readSpdifDTSStreamType() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t spdif_dtsstream_type;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "StreamType"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        spdif_dtsstream_type = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_dtsstream_type);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readSpdifMATFbaDialogueNormalization() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t spdif_matfba_dialogue_normalization;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kMatConfigIface,
            "FbaDialogueNormalization"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        spdif_matfba_dialogue_normalization = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_matfba_dialogue_normalization);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readHdmiDTSAnalogCompensationGain() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t hdmi_dtsanalog_compensation_gain;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "AnalogCompensationGain"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        hdmi_dtsanalog_compensation_gain = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_dtsanalog_compensation_gain);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readHdmiMATFbaSurroundCh() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t hdmi_matfba_surround_ch;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kMatConfigIface,
            "FbaSurroundCh"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        hdmi_matfba_surround_ch = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_matfba_surround_ch);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readSpdifDtsxDrcpercent() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t spdif_dtsx_drcpercent;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "DrcPercent"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        spdif_dtsx_drcpercent = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_dtsx_drcpercent);
}

ErrorCode PropertiesHandler::updateSpdifDtsxDrcpercent(
    int32_t spdif_dtsx_drcpercent) {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ssv)",
            adk::dbus::kDtsxConfigIface,
            "DrcPercent",
                                    g_variant_new("i", spdif_dtsx_drcpercent)
                        ));
    return result;
}
std::tuple<ErrorCode, uint32_t> PropertiesHandler::readHdmiDTSSamplerate() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    uint32_t hdmi_dtssamplerate;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "SampleRate"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        hdmi_dtssamplerate = g_variant_get_uint32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_dtssamplerate);
}

std::tuple<ErrorCode, uint32_t> PropertiesHandler::readHdmiMATTrueHdSync() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    uint32_t hdmi_mattrue_hd_sync;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kMatConfigIface,
            "TrueHdSync"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        hdmi_mattrue_hd_sync = g_variant_get_uint32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_mattrue_hd_sync);
}

std::tuple<ErrorCode, uint32_t> PropertiesHandler::readSpdifDTSProgChMask() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    uint32_t spdif_dtsprog_ch_mask;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "ProgChMask"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        spdif_dtsprog_ch_mask = g_variant_get_uint32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_dtsprog_ch_mask);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readHdmiDtsxDrcpercent() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t hdmi_dtsx_drcpercent;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "DrcPercent"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        hdmi_dtsx_drcpercent = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_dtsx_drcpercent);
}

ErrorCode PropertiesHandler::updateHdmiDtsxDrcpercent(
    int32_t hdmi_dtsx_drcpercent) {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ssv)",
            adk::dbus::kDtsxConfigIface,
            "DrcPercent",
                                    g_variant_new("i", hdmi_dtsx_drcpercent)
                        ));
    return result;
}
std::tuple<ErrorCode, std::string> PropertiesHandler::readHdmiPCMChConfig() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::string hdmi_pcmch_config;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kPcmConfigIface,
            "ChConfig"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        gsize length;
        hdmi_pcmch_config = static_cast<std::string>(g_variant_get_string(tmp, &length));
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_pcmch_config);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readSpdifDSDChConfig() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t spdif_dsdch_config;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDsdConfigIface,
            "ChConfig"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        spdif_dsdch_config = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_dsdch_config);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readHdmiDTSDialogueControlAvailability() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t hdmi_dtsdialogue_control_availability;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "DialogueControlAvailability"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        hdmi_dtsdialogue_control_availability = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_dtsdialogue_control_availability);
}

std::tuple<ErrorCode, std::string> PropertiesHandler::readHdmiMATStreamtype() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::string hdmi_matstreamtype;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kMatConfigIface,
            "StreamType"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        gsize length;
        hdmi_matstreamtype = static_cast<std::string>(g_variant_get_string(tmp, &length));
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_matstreamtype);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readHdmiDSDChConfig() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t hdmi_dsdch_config;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDsdConfigIface,
            "ChConfig"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        hdmi_dsdch_config = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_dsdch_config);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readHdmiDecChConfig() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t hdmi_dec_ch_config;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kStreamConfigIface,
            "DecChConfig"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        hdmi_dec_ch_config = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_dec_ch_config);
}

ErrorCode PropertiesHandler::updateHdmiDecChConfig(
    int32_t hdmi_dec_ch_config) {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ssv)",
            adk::dbus::kStreamConfigIface,
            "DecChConfig",
                                    g_variant_new("i", hdmi_dec_ch_config)
                        ));
    return result;
}
std::tuple<ErrorCode, int32_t> PropertiesHandler::readHdmiDDPDecChConfig() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t hdmi_ddpdec_ch_config;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ss)",
            adk::dbus::kDdpConfigIface,
            "DecChConfig"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                hdmi_ddpdec_ch_config = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_ddpdec_ch_config);
}

ErrorCode PropertiesHandler::updateHdmiDDPDecChConfig(
    int32_t hdmi_ddpdec_ch_config) {
    
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kHdmiConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ssv)",
            adk::dbus::kDdpConfigIface,
            "DecChConfig",
                                    g_variant_new("i", hdmi_ddpdec_ch_config)
                        ));
    return result;
}
std::tuple<ErrorCode, int32_t> PropertiesHandler::readHdmiDTSDialogueNormalization() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t hdmi_dtsdialogue_normalization;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "DialogueNormalization"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        hdmi_dtsdialogue_normalization = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_dtsdialogue_normalization);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readSpdifDDPKaraoke() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t spdif_ddpkaraoke;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDdpConfigIface,
            "Karaoke"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        spdif_ddpkaraoke = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_ddpkaraoke);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readSpdifDTSCoreBitRate() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t spdif_dtscore_bit_rate;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "CoreBitRate"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        spdif_dtscore_bit_rate = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_dtscore_bit_rate);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readHdmiDTSObjectNum() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t hdmi_dtsobject_num;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "ObjectNum"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        hdmi_dtsobject_num = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_dtsobject_num);
}

std::tuple<ErrorCode, bool> PropertiesHandler::readHdmiDlbsurroundcenterspread() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    bool hdmi_dlbsurroundcenterspread;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDapConfigIface,
            "DlbSurroundCenterSpread"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        hdmi_dlbsurroundcenterspread = g_variant_get_boolean(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_dlbsurroundcenterspread);
}

ErrorCode PropertiesHandler::updateHdmiDlbsurroundcenterspread(
    bool hdmi_dlbsurroundcenterspread) {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ssv)",
            adk::dbus::kDapConfigIface,
            "DlbSurroundCenterSpread",
                                    g_variant_new("b", static_cast<uint8_t>(hdmi_dlbsurroundcenterspread))
                        ));
    return result;
}
std::tuple<ErrorCode, int32_t> PropertiesHandler::readSpdifDTSX96Flag() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t spdif_dtsx96_flag;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "X96Flag"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        spdif_dtsx96_flag = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_dtsx96_flag);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readHdmiDTSEsMatrixFlag() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t hdmi_dtses_matrix_flag;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "EsMatrixFlag"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        hdmi_dtses_matrix_flag = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_dtses_matrix_flag);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readSpdifDTSEmbeddeddownmixselect() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t spdif_dtsembeddeddownmixselect;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "EmbeddedDownMixSelect"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        spdif_dtsembeddeddownmixselect = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_dtsembeddeddownmixselect);
}

std::tuple<ErrorCode, uint32_t> PropertiesHandler::readHdmiMATFbbch() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    uint32_t hdmi_matfbbch;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kMatConfigIface,
            "FbbCh"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        hdmi_matfbbch = g_variant_get_uint32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_matfbbch);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readSpdifDTSXchFlag() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t spdif_dtsxch_flag;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "XchFlag"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        spdif_dtsxch_flag = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_dtsxch_flag);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readSpdifDDPLfe() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t spdif_ddplfe;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDdpConfigIface,
            "LfeOn"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        spdif_ddplfe = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_ddplfe);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readHdmiDDPBitrate() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t hdmi_ddpbitrate;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDdpConfigIface,
            "BitRate"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        hdmi_ddpbitrate = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_ddpbitrate);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readHdmiDDPDialoguenormalization() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t hdmi_ddpdialoguenormalization;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDdpConfigIface,
            "DialogueNormalization"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        hdmi_ddpdialoguenormalization = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_ddpdialoguenormalization);
}

std::tuple<ErrorCode, std::vector<int32_t>> PropertiesHandler::readHdmiDDPDrcmode() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::vector<int32_t> hdmi_ddpdrcmode;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDdpConfigIface,
            "DrcMode"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        GVariantIter property_iter;
        g_variant_iter_init(&property_iter, tmp);
        hdmi_ddpdrcmode = dbus_->parseArrayOfInt32(property_iter);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_ddpdrcmode);
}

ErrorCode PropertiesHandler::updateHdmiDDPDrcmode(
    std::vector<int32_t> hdmi_ddpdrcmode) {
    GVariantBuilder hdmi_ddpdrcmode_builder;
    g_variant_builder_init(&hdmi_ddpdrcmode_builder, G_VARIANT_TYPE_ARRAY);
    for (auto &i : hdmi_ddpdrcmode) {
        g_variant_builder_add(&hdmi_ddpdrcmode_builder, "i", i);
    }

    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ssv)",
            adk::dbus::kDdpConfigIface,
            "DrcMode",
                        g_variant_new("ai", &hdmi_ddpdrcmode_builder)
            ));
    return result;
}
std::tuple<ErrorCode, uint32_t> PropertiesHandler::readSpdifDDPCustomChMap() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    uint32_t spdif_ddpcustom_ch_map;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDdpConfigIface,
            "CustomChMap"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        spdif_ddpcustom_ch_map = g_variant_get_uint32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_ddpcustom_ch_map);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readHdmiMATFbaDialogueNormalization() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t hdmi_matfba_dialogue_normalization;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kMatConfigIface,
            "FbaDialogueNormalization"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        hdmi_matfba_dialogue_normalization = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_matfba_dialogue_normalization);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readSpdifDDPCenterDownmixLevel() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t spdif_ddpcenter_downmix_level;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDdpConfigIface,
            "CenterDownmixLevel"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        spdif_ddpcenter_downmix_level = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_ddpcenter_downmix_level);
}

std::tuple<ErrorCode, uint32_t> PropertiesHandler::readHdmiDTSProgChMask() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    uint32_t hdmi_dtsprog_ch_mask;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "ProgChMask"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        hdmi_dtsprog_ch_mask = g_variant_get_uint32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_dtsprog_ch_mask);
}

std::tuple<ErrorCode, std::vector<int32_t>> PropertiesHandler::readSpdifDDPDrcmode() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::vector<int32_t> spdif_ddpdrcmode;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDdpConfigIface,
            "DrcMode"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        GVariantIter property_iter;
        g_variant_iter_init(&property_iter, tmp);
        spdif_ddpdrcmode = dbus_->parseArrayOfInt32(property_iter);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_ddpdrcmode);
}

ErrorCode PropertiesHandler::updateSpdifDDPDrcmode(
    std::vector<int32_t> spdif_ddpdrcmode) {
    GVariantBuilder spdif_ddpdrcmode_builder;
    g_variant_builder_init(&spdif_ddpdrcmode_builder, G_VARIANT_TYPE_ARRAY);
    for (auto &i : spdif_ddpdrcmode) {
        g_variant_builder_add(&spdif_ddpdrcmode_builder, "i", i);
    }

    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ssv)",
            adk::dbus::kDdpConfigIface,
            "DrcMode",
                        g_variant_new("ai", &spdif_ddpdrcmode_builder)
            ));
    return result;
}
std::tuple<ErrorCode, int32_t> PropertiesHandler::readHdmiDTSEmbeddeddownmixselect() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t hdmi_dtsembeddeddownmixselect;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "EmbeddedDownMixSelect"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        hdmi_dtsembeddeddownmixselect = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_dtsembeddeddownmixselect);
}

std::tuple<ErrorCode, uint32_t> PropertiesHandler::readHdmiPCMsamplingRate() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    uint32_t hdmi_pcmsampling_rate;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kPcmConfigIface,
            "SamplingRate"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        hdmi_pcmsampling_rate = g_variant_get_uint32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_pcmsampling_rate);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readHdmiDTSXllsamplerate() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t hdmi_dtsxllsamplerate;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "XllSampleRate"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        hdmi_dtsxllsamplerate = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_dtsxllsamplerate);
}

std::tuple<ErrorCode, uint32_t> PropertiesHandler::readSpdifDTSSamplerate() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    uint32_t spdif_dtssamplerate;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "SampleRate"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        spdif_dtssamplerate = g_variant_get_uint32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_dtssamplerate);
}

std::tuple<ErrorCode, std::string> PropertiesHandler::readHdmiDDPStreamType() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::string hdmi_ddpstream_type;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDdpConfigIface,
            "StreamType"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        gsize length;
        hdmi_ddpstream_type = static_cast<std::string>(g_variant_get_string(tmp, &length));
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_ddpstream_type);
}

std::tuple<ErrorCode, uint32_t> PropertiesHandler::readSpdifPCMsamplingRate() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    uint32_t spdif_pcmsampling_rate;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kPcmConfigIface,
            "SamplingRate"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        spdif_pcmsampling_rate = g_variant_get_uint32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_pcmsampling_rate);
}

std::tuple<ErrorCode, uint32_t> PropertiesHandler::readSpdifOutputsamplingfrequency() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    uint32_t spdif_outputsamplingfrequency;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kStreamConfigIface,
            "OutputSamplingFrequency"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        spdif_outputsamplingfrequency = g_variant_get_uint32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_outputsamplingfrequency);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readSpdifDTSEsMatrixFlag() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t spdif_dtses_matrix_flag;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "EsMatrixFlag"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        spdif_dtses_matrix_flag = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_dtses_matrix_flag);
}

std::tuple<ErrorCode, uint32_t> PropertiesHandler::readSpdifMATFbbch() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    uint32_t spdif_matfbbch;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kMatConfigIface,
            "FbbCh"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        spdif_matfbbch = g_variant_get_uint32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_matfbbch);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readHdmiMATGainRequired() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t hdmi_matgain_required;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kMatConfigIface,
            "GainRequired"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        hdmi_matgain_required = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_matgain_required);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readSpdifDTSObjectNum() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t spdif_dtsobject_num;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "ObjectNum"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        spdif_dtsobject_num = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_dtsobject_num);
}

std::tuple<ErrorCode, std::string> PropertiesHandler::readSpdifPCMChConfig() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::string spdif_pcmch_config;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kPcmConfigIface,
            "ChConfig"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        gsize length;
        spdif_pcmch_config = static_cast<std::string>(g_variant_get_string(tmp, &length));
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_pcmch_config);
}

std::tuple<ErrorCode, uint32_t> PropertiesHandler::readHdmiOutputsamplingfrequency() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    uint32_t hdmi_outputsamplingfrequency;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kStreamConfigIface,
            "OutputSamplingFrequency"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        hdmi_outputsamplingfrequency = g_variant_get_uint32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_outputsamplingfrequency);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readHdmiDTSExtensionMask() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t hdmi_dtsextension_mask;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "ExtensionMask"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        hdmi_dtsextension_mask = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_dtsextension_mask);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readHdmiDDPSurroundDownmixLevel() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t hdmi_ddpsurround_downmix_level;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDdpConfigIface,
            "SurroundDownmixLevel"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        hdmi_ddpsurround_downmix_level = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_ddpsurround_downmix_level);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readSpdifDTSDialogueControlAvailability() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t spdif_dtsdialogue_control_availability;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "DialogueControlAvailability"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        spdif_dtsdialogue_control_availability = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_dtsdialogue_control_availability);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readHdmiDTSCoreBitRate() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t hdmi_dtscore_bit_rate;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "CoreBitRate"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        hdmi_dtscore_bit_rate = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_dtscore_bit_rate);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readHdmiDDPKaraoke() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    int32_t hdmi_ddpkaraoke;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        g_variant_new("(ss)",
            adk::dbus::kDdpConfigIface,
            "Karaoke"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        hdmi_ddpkaraoke = g_variant_get_int32(tmp);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_ddpkaraoke);
}

std::tuple<ErrorCode, int32_t> PropertiesHandler::readHdmiDTSXSpkrout() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t hdmi_dtsxspkrout;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "spkrout"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                hdmi_dtsxspkrout = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_dtsxspkrout);
}
ErrorCode PropertiesHandler::updateHdmiDTSXSpkrout(
    int32_t hdmi_dtsxspkrout) {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kHdmiConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ssv)",
            adk::dbus::kDtsxConfigIface,
            "spkrout",
                                    g_variant_new("i", hdmi_dtsxspkrout)
                        ));
    return result;
}
std::tuple<ErrorCode, int32_t> PropertiesHandler::readSpdifDTSXSpkrout() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t spdif_dtsxspkrout;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "spkrout"));
    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                spdif_dtsxspkrout = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_dtsxspkrout);
}
ErrorCode PropertiesHandler::updateSpdifDTSXSpkrout(
    int32_t spdif_dtsxspkrout) {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kSpdifConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ssv)",
            adk::dbus::kDtsxConfigIface,
            "spkrout",
                                    g_variant_new("i", spdif_dtsxspkrout)
                        ));
    return result;
}
std::tuple<ErrorCode, int32_t> PropertiesHandler::readHdmiDTSXSetDialogcontrol() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t hdmi_dtsxset_dialogcontrol;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "set_dialogcontrol"));
    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                hdmi_dtsxset_dialogcontrol = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_dtsxset_dialogcontrol);
}
ErrorCode PropertiesHandler::updateHdmiDTSXSetDialogcontrol(
    int32_t hdmi_dtsxset_dialogcontrol) {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kHdmiConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ssv)",
            adk::dbus::kDtsxConfigIface,
            "set_dialogcontrol",
                                    g_variant_new("i", hdmi_dtsxset_dialogcontrol)
                        ));
    return result;
}
std::tuple<ErrorCode, int32_t> PropertiesHandler::readSpdifDTSXSetDialogcontrol() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t spdif_dtsxset_dialogcontrol;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "set_dialogcontrol"));
    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                spdif_dtsxset_dialogcontrol = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_dtsxset_dialogcontrol);
}
ErrorCode PropertiesHandler::updateSpdifDTSXSetDialogcontrol(
    int32_t spdif_dtsxset_dialogcontrol) {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kSpdifConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ssv)",
            adk::dbus::kDtsxConfigIface,
            "set_dialogcontrol",
                                    g_variant_new("i", spdif_dtsxset_dialogcontrol)
                        ));
    return result;
}
std::tuple<ErrorCode, int32_t> PropertiesHandler::readHdmiDTSXGetDialogcontrol() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t hdmi_dtsxget_dialogcontrol;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "get_dialogcontrol"));
    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                hdmi_dtsxget_dialogcontrol = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_dtsxget_dialogcontrol);
}
std::tuple<ErrorCode, int32_t> PropertiesHandler::readSpdifDTSXGetDialogcontrol() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t spdif_dtsxget_dialogcontrol;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "get_dialogcontrol"));
    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                spdif_dtsxget_dialogcontrol = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_dtsxget_dialogcontrol);
}
std::tuple<ErrorCode, int32_t> PropertiesHandler::readHdmiDTSXEnableDirectmode() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t hdmi_dtsxenable_directmode;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "enable_directmode"));
    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                hdmi_dtsxenable_directmode = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_dtsxenable_directmode);
}
ErrorCode PropertiesHandler::updateHdmiDTSXEnableDirectmode(
    int32_t hdmi_dtsxenable_directmode) {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kHdmiConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ssv)",
            adk::dbus::kDtsxConfigIface,
            "enable_directmode",
                                    g_variant_new("i", hdmi_dtsxenable_directmode)
                        ));
    return result;
}
std::tuple<ErrorCode, int32_t> PropertiesHandler::readSpdifDTSXEnableDirectmode() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t spdif_dtsxenable_directmode;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "enable_directmode"));
    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                spdif_dtsxenable_directmode = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_dtsxenable_directmode);
}
ErrorCode PropertiesHandler::updateSpdifDTSXEnableDirectmode(
    int32_t spdif_dtsxenable_directmode) {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kSpdifConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ssv)",
            adk::dbus::kDtsxConfigIface,
            "enable_directmode",
                                    g_variant_new("i", spdif_dtsxenable_directmode)
                        ));
    return result;
}
std::tuple<ErrorCode, int32_t> PropertiesHandler::readHdmiDTSXDisableDirectmode() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t hdmi_dtsxdisable_directmode;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "disable_directmode"));
    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                hdmi_dtsxdisable_directmode = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_dtsxdisable_directmode);
}
ErrorCode PropertiesHandler::updateHdmiDTSXDisableDirectmode(
    int32_t hdmi_dtsxdisable_directmode) {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kHdmiConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ssv)",
            adk::dbus::kDtsxConfigIface,
            "disable_directmode",
                                    g_variant_new("i", hdmi_dtsxdisable_directmode)
                        ));
    return result;
}
std::tuple<ErrorCode, int32_t> PropertiesHandler::readSpdifDTSXDisableDirectmode() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t spdif_dtsxdisable_directmode;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "disable_directmode"));
    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                spdif_dtsxdisable_directmode = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_dtsxdisable_directmode);
}
ErrorCode PropertiesHandler::updateSpdifDTSXDisableDirectmode(
    int32_t spdif_dtsxdisable_directmode) {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kSpdifConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ssv)",
            adk::dbus::kDtsxConfigIface,
            "disable_directmode",
                                    g_variant_new("i", spdif_dtsxdisable_directmode)
                        ));
    return result;
}
std::tuple<ErrorCode, int32_t> PropertiesHandler::readHdmiDTSXEnableBlindparma() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t hdmi_dtsxenable_blindparma;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "enable_blindparma"));
    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                hdmi_dtsxenable_blindparma = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_dtsxenable_blindparma);
}
ErrorCode PropertiesHandler::updateHdmiDTSXEnableBlindparma(
    int32_t hdmi_dtsxenable_blindparma) {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kHdmiConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ssv)",
            adk::dbus::kDtsxConfigIface,
            "enable_blindparma",
                                    g_variant_new("i", hdmi_dtsxenable_blindparma)
                        ));
    return result;
}
std::tuple<ErrorCode, int32_t> PropertiesHandler::readSpdifDTSXEnableBlindparma() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t spdif_dtsxenable_blindparma;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "enable_blindparma"));
    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                spdif_dtsxenable_blindparma = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_dtsxenable_blindparma);
}
ErrorCode PropertiesHandler::updateSpdifDTSXEnableBlindparma(
    int32_t spdif_dtsxenable_blindparma) {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kSpdifConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ssv)",
            adk::dbus::kDtsxConfigIface,
            "enable_blindparma",
                                    g_variant_new("i", spdif_dtsxenable_blindparma)
                        ));
    return result;
}
std::tuple<ErrorCode, int32_t> PropertiesHandler::readHdmiDTSXDisableBlindparma() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t hdmi_dtsxdisable_blindparma;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "disable_blindparma"));
    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                hdmi_dtsxdisable_blindparma = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_dtsxdisable_blindparma);
}
ErrorCode PropertiesHandler::updateHdmiDTSXDisableBlindparma(
    int32_t hdmi_dtsxdisable_blindparma) {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kHdmiConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ssv)",
            adk::dbus::kDtsxConfigIface,
            "disable_blindparma",
                                    g_variant_new("i", hdmi_dtsxdisable_blindparma)
                        ));
    return result;
}
std::tuple<ErrorCode, int32_t> PropertiesHandler::readSpdifDTSXDisableBlindparma() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t spdif_dtsxdisable_blindparma;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "disable_blindparma"));
    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                spdif_dtsxdisable_blindparma = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_dtsxdisable_blindparma);
}
ErrorCode PropertiesHandler::updateSpdifDTSXDisableBlindparma(
    int32_t spdif_dtsxdisable_blindparma) {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kSpdifConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ssv)",
            adk::dbus::kDtsxConfigIface,
            "disable_blindparma",
                                    g_variant_new("i", spdif_dtsxdisable_blindparma)
                        ));
    return result;
}
std::tuple<ErrorCode, int32_t> PropertiesHandler::readHdmiDTSXEsMatrix() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t hdmi_dtsxes_matrix;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "es_matrix"));
    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                hdmi_dtsxes_matrix = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_dtsxes_matrix);
}
ErrorCode PropertiesHandler::updateHdmiDTSXEsMatrix(
    int32_t hdmi_dtsxes_matrix) {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kHdmiConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ssv)",
            adk::dbus::kDtsxConfigIface,
            "es_matrix",
                                    g_variant_new("i", hdmi_dtsxes_matrix)
                        ));
    return result;
}
std::tuple<ErrorCode, int32_t> PropertiesHandler::readSpdifDTSXEsMatrix() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t spdif_dtsxes_matrix;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "es_matrix"));
    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                spdif_dtsxes_matrix = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_dtsxes_matrix);
}
ErrorCode PropertiesHandler::updateSpdifDTSXEsMatrix(
    int32_t spdif_dtsxes_matrix) {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kSpdifConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ssv)",
            adk::dbus::kDtsxConfigIface,
            "es_matrix",
                                    g_variant_new("i", spdif_dtsxes_matrix)
                        ));
    return result;
}
std::tuple<ErrorCode, int32_t> PropertiesHandler::readHdmiDTSXEnableAnalogCompensation() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t hdmi_dtsxenable_analog_compensation;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "enable_analog_compensation"));
    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                hdmi_dtsxenable_analog_compensation = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_dtsxenable_analog_compensation);
}
ErrorCode PropertiesHandler::updateHdmiDTSXEnableAnalogCompensation(
    int32_t hdmi_dtsxenable_analog_compensation) {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kHdmiConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ssv)",
            adk::dbus::kDtsxConfigIface,
            "enable_analog_compensation",
                                    g_variant_new("i", hdmi_dtsxenable_analog_compensation)
                        ));
    return result;
}
std::tuple<ErrorCode, int32_t> PropertiesHandler::readSpdifDTSXEnableAnalogCompensation() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t spdif_dtsxenable_analog_compensation;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "enable_analog_compensation"));
    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                spdif_dtsxenable_analog_compensation = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_dtsxenable_analog_compensation);
}
ErrorCode PropertiesHandler::updateSpdifDTSXEnableAnalogCompensation(
    int32_t spdif_dtsxenable_analog_compensation) {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kSpdifConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ssv)",
            adk::dbus::kDtsxConfigIface,
            "enable_analog_compensation",
                                    g_variant_new("i", spdif_dtsxenable_analog_compensation)
                        ));
    return result;
}
std::tuple<ErrorCode, int32_t> PropertiesHandler::readHdmiDTSXSetAnalogcompmaxUsergain() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t hdmi_dtsxset_analogcompmax_usergain;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "set_analogcompmax_usergain"));
    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                hdmi_dtsxset_analogcompmax_usergain = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_dtsxset_analogcompmax_usergain);
}
ErrorCode PropertiesHandler::updateHdmiDTSXSetAnalogcompmaxUsergain(
    int32_t hdmi_dtsxset_analogcompmax_usergain) {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kHdmiConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ssv)",
            adk::dbus::kDtsxConfigIface,
            "set_analogcompmax_usergain",
                                    g_variant_new("i", hdmi_dtsxset_analogcompmax_usergain)
                        ));
    return result;
}
std::tuple<ErrorCode, int32_t> PropertiesHandler::readSpdifDTSXSetAnalogcompmaxUsergain() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    int32_t spdif_dtsxset_analogcompmax_usergain;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "set_analogcompmax_usergain"));
    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                spdif_dtsxset_analogcompmax_usergain = g_variant_get_int32(tmp);
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_dtsxset_analogcompmax_usergain);
}
ErrorCode PropertiesHandler::updateSpdifDTSXSetAnalogcompmaxUsergain(
    int32_t spdif_dtsxset_analogcompmax_usergain) {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kSpdifConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ssv)",
            adk::dbus::kDtsxConfigIface,
            "set_analogcompmax_usergain",
                                    g_variant_new("i", spdif_dtsxset_analogcompmax_usergain)
                        ));
    return result;
}
std::tuple<ErrorCode, std::vector<int32_t>> PropertiesHandler::readHdmiDTSXDrcmode() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::vector<int32_t> hdmi_dtsxdrcmode;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "DrcMode"));
    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                        GVariantIter property_iter;
        g_variant_iter_init(&property_iter, tmp);
        hdmi_dtsxdrcmode = dbus_->parseArrayOfInt32(property_iter);
                        g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_dtsxdrcmode);
}
ErrorCode PropertiesHandler::updateHdmiDTSXDrcmode(
    std::vector<int32_t> hdmi_dtsxdrcmode) {
        GVariantBuilder hdmi_dtsxdrcmode_builder;
    g_variant_builder_init(&hdmi_dtsxdrcmode_builder, G_VARIANT_TYPE_ARRAY);
    for (auto &i : hdmi_dtsxdrcmode) {
                g_variant_builder_add(&hdmi_dtsxdrcmode_builder, "i", i);
            }
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kHdmiConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ssv)",
            adk::dbus::kDtsxConfigIface,
            "DrcMode",
                        g_variant_new("ai", &hdmi_dtsxdrcmode_builder)
            ));
    return result;
}
std::tuple<ErrorCode, std::vector<int32_t>> PropertiesHandler::readSpdifDTSXDrcmode() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::vector<int32_t> spdif_dtsxdrcmode;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ss)",
            adk::dbus::kDtsxConfigIface,
            "DrcMode"));
    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                        GVariantIter property_iter;
        g_variant_iter_init(&property_iter, tmp);
        spdif_dtsxdrcmode = dbus_->parseArrayOfInt32(property_iter);
                        g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_dtsxdrcmode);
}
ErrorCode PropertiesHandler::updateSpdifDTSXDrcmode(
    std::vector<int32_t> spdif_dtsxdrcmode) {
        GVariantBuilder spdif_dtsxdrcmode_builder;
    g_variant_builder_init(&spdif_dtsxdrcmode_builder, G_VARIANT_TYPE_ARRAY);
    for (auto &i : spdif_dtsxdrcmode) {
                g_variant_builder_add(&spdif_dtsxdrcmode_builder, "i", i);
            }
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kSpdifConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ssv)",
            adk::dbus::kDtsxConfigIface,
            "DrcMode",
                        g_variant_new("ai", &spdif_dtsxdrcmode_builder)
            ));
    return result;
}
std::tuple<ErrorCode, std::vector<int32_t>> PropertiesHandler::readHdmiMATDrcmode() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::vector<int32_t> hdmi_matdrcmode;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ss)",
            adk::dbus::kMatConfigIface,
            "DrcMode"));
    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                        GVariantIter property_iter;
        g_variant_iter_init(&property_iter, tmp);
        hdmi_matdrcmode = dbus_->parseArrayOfInt32(property_iter);
                        g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_matdrcmode);
}
ErrorCode PropertiesHandler::updateHdmiMATDrcmode(
    std::vector<int32_t> hdmi_matdrcmode) {
        GVariantBuilder hdmi_matdrcmode_builder;
    g_variant_builder_init(&hdmi_matdrcmode_builder, G_VARIANT_TYPE_ARRAY);
    for (auto &i : hdmi_matdrcmode) {
                g_variant_builder_add(&hdmi_matdrcmode_builder, "i", i);
            }
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kHdmiConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ssv)",
            adk::dbus::kMatConfigIface,
            "DrcMode",
                        g_variant_new("ai", &hdmi_matdrcmode_builder)
            ));
    return result;
}
std::tuple<ErrorCode, std::vector<int32_t>> PropertiesHandler::readSpdifMATDrcmode() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::vector<int32_t> spdif_matdrcmode;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ss)",
            adk::dbus::kMatConfigIface,
            "DrcMode"));
    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                        GVariantIter property_iter;
        g_variant_iter_init(&property_iter, tmp);
        spdif_matdrcmode = dbus_->parseArrayOfInt32(property_iter);
                        g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_matdrcmode);
}
ErrorCode PropertiesHandler::updateSpdifMATDrcmode(
    std::vector<int32_t> spdif_matdrcmode) {
        GVariantBuilder spdif_matdrcmode_builder;
    g_variant_builder_init(&spdif_matdrcmode_builder, G_VARIANT_TYPE_ARRAY);
    for (auto &i : spdif_matdrcmode) {
                g_variant_builder_add(&spdif_matdrcmode_builder, "i", i);
            }
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kSpdifConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ssv)",
            adk::dbus::kMatConfigIface,
            "DrcMode",
                        g_variant_new("ai", &spdif_matdrcmode_builder)
            ));
    return result;
}
std::tuple<ErrorCode, std::string> PropertiesHandler::readHdmiReadDetectedformat() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::string hdmi_read_detectedformat;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kHdmiConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ss)",
            adk::dbus::kStreamConfigIface,
            "DetectedFormat"));
    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                gsize length;
        hdmi_read_detectedformat = static_cast<std::string>(g_variant_get_string(tmp, &length));
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, hdmi_read_detectedformat);
}
std::tuple<ErrorCode, std::string> PropertiesHandler::readSpdifReadDetectedformat() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::string spdif_read_detectedformat;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kSpdifConfigService,
                adk::dbus::kStreamConfigObjPath,
                g_variant_new("(ss)",
            adk::dbus::kStreamConfigIface,
            "DetectedFormat"));
    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                gsize length;
        spdif_read_detectedformat = static_cast<std::string>(g_variant_get_string(tmp, &length));
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, spdif_read_detectedformat);
}
std::tuple<ErrorCode, std::string> PropertiesHandler::readCurrentClock() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::string current_clock;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkAudiomanagerService,
                adk::dbus::kAudiomanagerDeviceControllerObjPath,
                g_variant_new("(ss)",
            adk::dbus::kAudiomanagerDeviceControllerIface,
            "Clock"));
    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                gsize length;
        current_clock = static_cast<std::string>(g_variant_get_string(tmp, &length));
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, current_clock);
}
std::tuple<ErrorCode, std::string> PropertiesHandler::readCurrentSource() {
    ErrorCode result = ADK_SI_OK;
    GVariant * response = nullptr;
    std::string current_source;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkAudiomanagerService,
                adk::dbus::kAudiomanagerDeviceControllerObjPath,
                g_variant_new("(ss)",
            adk::dbus::kAudiomanagerDeviceControllerIface,
            "CurrentSource"));
    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
                                gsize length;
        current_source = static_cast<std::string>(g_variant_get_string(tmp, &length));
                                g_variant_unref(tmp);
    }
    return std::make_tuple(result, current_source);
}
MethodsHandler::MethodsHandler(Dbus *dbus) : dbus_(dbus) {
    dbus_->setPropertiesChangedListener(
        std::bind(&MethodsHandler::interfacesAddedCb, this,
            std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
}

void MethodsHandler::interfacesAddedCb(const std::string &obj_path,
    const std::string &iface_name, GVariantIter * /*iter*/) {
    g_print("MethodsHandler::interfacesAddedCb \n");

    bool obj_id_map_changed = false;
    std::lock_guard<std::mutex> lock(mutex_);
    if (iface_name == std::string(adk::dbus::kQahwModuleIface)) {
        std::unordered_set<std::string> deleteSource_obj_paths = obj_id_map_[std::string(adk::kDeleteSource)];
        if (deleteSource_obj_paths.find(obj_path) == deleteSource_obj_paths.end()) {
            deleteSource_obj_paths.emplace(obj_path);
            obj_id_map_[std::string(adk::kDeleteSource)] = deleteSource_obj_paths;
            obj_id_map_changed = true;
        }
        std::unordered_set<std::string> createSource_obj_paths = obj_id_map_[std::string(adk::kCreateSource)];
        if (createSource_obj_paths.find(obj_path) == createSource_obj_paths.end()) {
            createSource_obj_paths.emplace(obj_path);
            obj_id_map_[std::string(adk::kCreateSource)] = createSource_obj_paths;
            obj_id_map_changed = true;
        }
    }

    if (obj_id_map_changed) {
        for (auto &f : obj_id_map_changed_listeners_) {
            f(obj_id_map_);
        }
    }
}

ErrorCode
MethodsHandler::spdifDecodeRestart(
        std::string in_arg
        ) {
    GVariantBuilder input_builder;
    g_variant_builder_init(&input_builder, G_VARIANT_TYPE("(s)"));
    g_variant_builder_add(&input_builder, "s", in_arg.c_str());

    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::tie(result, response) = dbus_->gdbusMethodCall(adk::dbus::kSpdifConfigService,
        adk::dbus::kStreamConfigObjPath,
        adk::dbus::kStreamConfigIface,
        "DecRestart",
                g_variant_builder_end(&input_builder)
        );
    return result;
}
ErrorCode
MethodsHandler::deleteSource(
            const std::string &obj_id
    ) {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::tie(result, response) = dbus_->gdbusAddressMethodCall(
        "unix:path=/var/run/pulse/dbus-socket",
        adk::dbus::kQahwModuleService,
        obj_id,
        adk::dbus::kQahwModuleIface,
        "StopStream",
                nullptr
        );
    return result;
}
ErrorCode
MethodsHandler::createSource(
        CreateSourceIn in_arg
        ,    const std::string &obj_id
    ) {
    GVariantBuilder input_builder;
    g_variant_builder_init(&input_builder, G_VARIANT_TYPE("((sussa{su}))"));
    g_variant_builder_open(&input_builder, G_VARIANT_TYPE("(sussa{su})"));
    std::string encoding_str = in_arg.getEncoding();
    g_variant_builder_add(&input_builder, "s", encoding_str.c_str());
    g_variant_builder_add(&input_builder, "u", in_arg.getRate());
    std::string format_str = in_arg.getFormat();
    g_variant_builder_add(&input_builder, "s", format_str.c_str());
    std::string channel_map_str = in_arg.getChannelMap();
    g_variant_builder_add(&input_builder, "s", channel_map_str.c_str());
    g_variant_builder_open(&input_builder, G_VARIANT_TYPE("a{su}"));
    for (auto &i : in_arg.getExtraConfigs()) {
        std::string key = i.first;
        uint32_t value = i.second;
        g_variant_builder_open(&input_builder, G_VARIANT_TYPE("{su}"));
        g_variant_builder_add(&input_builder, "s", key.c_str());
        g_variant_builder_add(&input_builder, "u", value);
        g_variant_builder_close(&input_builder);
    }
    g_variant_builder_close(&input_builder);
    g_variant_builder_close(&input_builder);

    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::tie(result, response) = dbus_->gdbusAddressMethodCall(
        "unix:path=/var/run/pulse/dbus-socket",
        adk::dbus::kQahwModuleService,
        obj_id,
        adk::dbus::kQahwModuleIface,
        "StartCompressStream",
                g_variant_builder_end(&input_builder)
        );
    return result;
}
ErrorCode
MethodsHandler::hdmiDecodeRestart(
        std::string in_arg
        ) {
    GVariantBuilder input_builder;
    g_variant_builder_init(&input_builder, G_VARIANT_TYPE("(s)"));
    g_variant_builder_add(&input_builder, "s", in_arg.c_str());

    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::tie(result, response) = dbus_->gdbusMethodCall(adk::dbus::kHdmiConfigService,
        adk::dbus::kStreamConfigObjPath,
        adk::dbus::kStreamConfigIface,
        "DecRestart",
                g_variant_builder_end(&input_builder)
        );
    return result;
}
ErrorCode
MethodsHandler::setSourceExtClk(
        SetSourceExtClkIn in_arg
        ) {
        GVariantBuilder input_builder;
        g_variant_builder_init(&input_builder, G_VARIANT_TYPE("(sst)"));
                            std::string source_name_str = in_arg.getSourceName();
    g_variant_builder_add(&input_builder, "s", source_name_str.c_str());
                            std::string clock_type_str = in_arg.getClockType();
    g_variant_builder_add(&input_builder, "s", clock_type_str.c_str());
                            g_variant_builder_add(&input_builder, "t", in_arg.getClockFrequency());
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
        std::tie(result, response) = dbus_->gdbusMethodCall(adk::dbus::kAdkAudiomanagerService,
                adk::dbus::kAudiomanagerDeviceControllerObjPath,
                adk::dbus::kAudiomanagerDeviceControllerIface,
        "SetSource",
                g_variant_builder_end(&input_builder)
        );
                return result;
        }
SignalsHandler::SignalsHandler(Dbus *dbus) : dbus_(dbus) {
    dbus_->setHdmiDetectedFormatListener(
        std::bind(&SignalsHandler::callHdmiDetectedFormatListeners, this,
            std::placeholders::_1, std::placeholders::_2));
    dbus_->setHdmiDualMonoAACListener(
        std::bind(&SignalsHandler::callHdmiDualMonoAACListeners, this,
            std::placeholders::_1, std::placeholders::_2));
    dbus_->setSpdifDetectedFormatListener(
        std::bind(&SignalsHandler::callSpdifDetectedFormatListeners, this,
            std::placeholders::_1, std::placeholders::_2));
        dbus_->setSetSourceEventListener(
        std::bind(&SignalsHandler::callSetSourceEventListeners, this,
        std::placeholders::_1, std::placeholders::_2));
}

void SignalsHandler::callHdmiDetectedFormatListeners(const std::string &obj_path, GVariant *response) {
    g_print("SignalsHandler::callHdmiDetectedFormatListeners \n");
    if (!g_variant_is_of_type(response, (const GVariantType *)"(s)")) {
        return;
    }

    char *hdmi_detected_format_out;
    g_variant_get(response, "(s)", &hdmi_detected_format_out);
    for (auto &f : hdmi_detected_format_listeners_) {
        f(hdmi_detected_format_out, obj_path);
    }
}
void SignalsHandler::callHdmiDualMonoAACListeners(const std::string &obj_path, GVariant *response) {
    g_print("SignalsHandler::callHdmiDualMonoAACListeners \n");

    for (auto &f : hdmi_dual_mono_aac_listeners_) {
        f(obj_path);
    }
}
void SignalsHandler::callSpdifDetectedFormatListeners(const std::string &obj_path, GVariant *response) {
    g_print("SignalsHandler::callSpdifDetectedFormatListeners \n");
    if (!g_variant_is_of_type(response, (const GVariantType *)"(s)")) {
        return;
    }

    char *spdif_detected_format_out;
    g_variant_get(response, "(s)", &spdif_detected_format_out);
    for (auto &f : spdif_detected_format_listeners_) {
        f(spdif_detected_format_out, obj_path);
    }
}
void SignalsHandler::callSetSourceEventListeners(const std::string &obj_path, GVariant* response) {
    g_print("SignalsHandler::callSetSourceEventListeners \n");
        if (!g_variant_is_of_type(response, (const GVariantType *)"(s)")) {
        return;
    }
                            char * set_source_event_out;
        g_variant_get(response, "(s)", &set_source_event_out);
            for (auto &f : set_source_event_listeners_) {
        f(set_source_event_out, obj_path);
    }
}
}  // namespace adk
