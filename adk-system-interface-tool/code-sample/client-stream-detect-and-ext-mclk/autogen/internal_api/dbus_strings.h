/*
 * Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of The Linux Foundation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef AUTOGEN_INTERNAL_API_DBUS_STRINGS_H_
#define AUTOGEN_INTERNAL_API_DBUS_STRINGS_H_

namespace adk {
namespace dbus {
// dbus service destinations
const char kFreedesktopDBusService[] = "org.freedesktop.DBus";
const char kHdmiConfigService[] = "com.qti.loopback.stream.HDMI.config";
const char kSpdifConfigService[] = "com.qti.loopback.stream.SPDIF.config";
const char kAdkAudiomanagerService[] = "com.qualcomm.qti.adk.audiomanager";
const char kQahwModuleService[] = "org.PulseAudio.Ext.Qahw.Module";

// dbus mandatory object paths
const char kFreedesktopDBusObjPath[] = "/org/freedesktop/DBus";
const char kFreedesktopDBusObjectManagerObjPath[] = "/";
const char kStreamConfigObjPath[] = "/com/qti/loopback/stream/config";
const char kAudiomanagerDeviceControllerObjPath[] = "/com/qualcomm/qti/adk/audiomanager/device_controller";

// dbus interfaces
const char kFreedesktopDBusIface[] = "org.freedesktop.DBus";
const char kFreedesktopDBusPropertiesIface[] = "org.freedesktop.DBus.Properties";
const char kFreedesktopDBusObjectManagerIface[] = "org.freedesktop.DBus.ObjectManager";
const char kDapConfigIface[] = "com.qti.loopback.stream.DAP.config";
const char kDdpConfigIface[] = "com.qti.loopback.stream.DDP.config";
const char kDtsxConfigIface[] = "com.qti.loopback.stream.DTSX.config";
const char kDsdConfigIface[] = "com.qti.loopback.stream.DSD.config";
const char kMatConfigIface[] = "com.qti.loopback.stream.MAT.config";
const char kStreamConfigIface[] = "com.qti.loopback.stream.config";
const char kAacConfigIface[] = "com.qti.loopback.stream.AAC.config";
const char kPcmConfigIface[] = "com.qti.loopback.stream.PCM.config";
const char kAudiomanagerDeviceControllerIface[] = "com.qualcomm.qti.adk.audiomanager.device_controller";
const char kQahwModuleIface[] = "org.PulseAudio.Ext.Qahw.Module";

}  // namespace dbus
}  // namespace adk
#endif  // AUTOGEN_INTERNAL_API_DBUS_STRINGS_H_

