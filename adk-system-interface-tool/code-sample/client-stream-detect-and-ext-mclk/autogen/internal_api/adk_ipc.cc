/*
 * Copyright (c) 2019, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of The Linux Foundation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "adk_ipc.h"

namespace adk {
static const char kAdkSystemInterfaceIPCName[] = "adkSI";

bool AdkIpc::init() {
    message_service_(std::string(kAdkSystemInterfaceIPCName));
    if (!message_service_.Initialise()) {
        return false;
    }

    subscribeMessages();
    return true;
}

void AdkIpc::subscribeMessages() {
    using std::placeholders::_1;
    message_service_.Subscribe(adk::msg::AdkMessage::kVoiceuiDatabaseUpdated,
        std::bind(&AdkIpc::voiceUIDbUpdated, this, _1));
    message_service_.Subscribe(adk::msg::AdkMessage::kVoiceuiAuthenticateAvs,
        std::bind(&AdkIpc::authenticateAvs, this, _1));
}

void AdkIpc::voiceUIDbUpdated(adk::msg::AdkMessage /*msg*/) {
    // notify corresponding interface handler listeners
}

void AdkIpc::authenticateAvs(adk::msg::AdkMessage /*msg*/) {
    // notify corresponding interface handler listeners
}
}  // namespace adk
