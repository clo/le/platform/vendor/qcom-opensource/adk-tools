
/*
 * Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of The Linux Foundation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef AUTOGEN_INTERNAL_API_INTERFACE_HANDLERS_H_
#define AUTOGEN_INTERNAL_API_INTERFACE_HANDLERS_H_

#include <gio/gio.h>
#include <glib.h>

#include <functional>
#include <memory>
#include <mutex>
#include <string>
#include <tuple>
#include <unordered_map>
#include <vector>

// #include "adk_ipc.h"
#include "attribute_classes.h"
#include "internal_api/dbus.h"

namespace adk {
class PropertiesHandler {
 public:
    explicit PropertiesHandler(Dbus *dbus);
    ~PropertiesHandler() = default;

            std::tuple<ErrorCode, bool> readHdmiDAPDlbsurroundmode(
            );

    ErrorCode updateHdmiDAPDlbsurroundmode(
        bool hdmi_dapdlbsurroundmode);

                    std::tuple<ErrorCode, bool> readSpdifDAPDlbsurroundmode(
            );

    ErrorCode updateSpdifDAPDlbsurroundmode(
        bool spdif_dapdlbsurroundmode);

                    std::tuple<ErrorCode, int32_t> readSpdifDDPSurroundDownmixLevel(
            );

                    std::tuple<ErrorCode, std::string> readSpdifDDPStreamType(
            );

                    std::tuple<ErrorCode, int32_t> readHdmiDTSX96Flag(
            );

                    std::tuple<ErrorCode, int32_t> readHdmiDDPLfe(
            );

                    std::tuple<ErrorCode, uint32_t> readHdmiDSDSamplingrate(
            );

                    std::tuple<ErrorCode, int32_t> readHdmiDDPDualMono(
            );

    ErrorCode updateHdmiDDPDualMono(
        int32_t hdmi_ddpdual_mono);

                    std::tuple<ErrorCode, int32_t> readSpdifDTSDialogueNormalization(
            );

                    std::tuple<ErrorCode, int32_t> readSpdifDDPDualMono(
            );

    ErrorCode updateSpdifDDPDualMono(
        int32_t spdif_ddpdual_mono);

                    std::tuple<ErrorCode, int32_t> readSpdifDDPDialoguenormalization(
            );

                    std::tuple<ErrorCode, int32_t> readSpdifDTSDecodeInfo(
            );

                    std::tuple<ErrorCode, uint32_t> readHdmiDDPCustomChMap(
            );

                    std::tuple<ErrorCode, int32_t> readSpdifDTSBitrate(
            );

                    std::tuple<ErrorCode, int32_t> readSpdifMATGainRequired(
            );

                    std::tuple<ErrorCode, int32_t> readHdmiDDPAudioCodingMode(
            );

                    std::tuple<ErrorCode, int32_t> readHdmiDDPSurroundex(
            );

                    std::tuple<ErrorCode, int32_t> readHdmiDTSDecodeInfo(
            );

                    std::tuple<ErrorCode, int32_t> readSpdifDDPSurroundex(
            );

                    std::tuple<ErrorCode, int32_t> readHdmiDTSBitrate(
            );

                    std::tuple<ErrorCode, int32_t> readSpdifDDPBitrate(
            );

                    std::tuple<ErrorCode, int32_t> readSpdifDDPAudioCodingMode(
            );

                    std::tuple<ErrorCode, int32_t> readSpdifMATFbaSurroundCh(
            );

                    std::tuple<ErrorCode, int32_t> readSpdifDTSXllsamplerate(
            );

                    std::tuple<ErrorCode, bool> readSpdifDlbsurroundcenterspread(
            );

    ErrorCode updateSpdifDlbsurroundcenterspread(
        bool spdif_dlbsurroundcenterspread);

                    std::tuple<ErrorCode, uint32_t> readHdmiMATFbach(
            );

                    std::tuple<ErrorCode, int32_t> readSpdifDecChConfig(
            );

    ErrorCode updateSpdifDecChConfig(
        int32_t spdif_dec_ch_config);

                    std::tuple<ErrorCode, int32_t> readSpdifDDPDecChConfig(
            );

        ErrorCode updateSpdifDDPDecChConfig(
        int32_t spdif_ddpdec_ch_config);
                    std::tuple<ErrorCode, int32_t> readHdmiDTSXchFlag(
            );

                    std::tuple<ErrorCode, int32_t> readHdmiAacDualMonoChannelMode(
            );

    ErrorCode updateHdmiAacDualMonoChannelMode(
        int32_t hdmi_aac_dual_mono_channel_mode);

                    std::tuple<ErrorCode, int32_t> readHdmiDDPCenterDownmixLevel(
            );

                    std::tuple<ErrorCode, uint32_t> readSpdifDSDSamplingrate(
            );

                    std::tuple<ErrorCode, uint32_t> readSpdifMATTrueHdSync(
            );

                    std::tuple<ErrorCode, int32_t> readSpdifDTSExtensionMask(
            );

                    std::tuple<ErrorCode, uint32_t> readSpdifMATFbach(
            );

                    std::tuple<ErrorCode, int32_t> readHdmiDTSStreamType(
            );

                    std::tuple<ErrorCode, uint32_t> readHdmiDecSamplFreq(
            );

                    std::tuple<ErrorCode, std::string> readSpdifMATStreamtype(
            );

                    std::tuple<ErrorCode, uint32_t> readSpdifDecSamplFreq(
            );

                    std::tuple<ErrorCode, int32_t> readSpdifDTSAnalogCompensationGain(
            );

                    std::tuple<ErrorCode, int32_t> readSpdifDTSStreamType(
            );

                    std::tuple<ErrorCode, int32_t> readSpdifMATFbaDialogueNormalization(
            );

                    std::tuple<ErrorCode, int32_t> readHdmiDTSAnalogCompensationGain(
            );

                    std::tuple<ErrorCode, int32_t> readHdmiMATFbaSurroundCh(
            );

                    std::tuple<ErrorCode, int32_t> readSpdifDtsxDrcpercent(
            );

    ErrorCode updateSpdifDtsxDrcpercent(
        int32_t spdif_dtsx_drcpercent);

                    std::tuple<ErrorCode, uint32_t> readHdmiDTSSamplerate(
            );

                    std::tuple<ErrorCode, uint32_t> readHdmiMATTrueHdSync(
            );

                    std::tuple<ErrorCode, uint32_t> readSpdifDTSProgChMask(
            );

                    std::tuple<ErrorCode, int32_t> readHdmiDtsxDrcpercent(
            );

    ErrorCode updateHdmiDtsxDrcpercent(
        int32_t hdmi_dtsx_drcpercent);

                    std::tuple<ErrorCode, std::string> readHdmiPCMChConfig(
            );

                    std::tuple<ErrorCode, int32_t> readSpdifDSDChConfig(
            );

                    std::tuple<ErrorCode, int32_t> readHdmiDTSDialogueControlAvailability(
            );

                    std::tuple<ErrorCode, std::string> readHdmiMATStreamtype(
            );

                    std::tuple<ErrorCode, int32_t> readHdmiDSDChConfig(
            );

                    std::tuple<ErrorCode, int32_t> readHdmiDecChConfig(
            );

        ErrorCode updateHdmiDecChConfig(
        int32_t hdmi_dec_ch_config);

                    std::tuple<ErrorCode, int32_t> readHdmiDDPDecChConfig(
            );

        ErrorCode updateHdmiDDPDecChConfig(
        int32_t hdmi_ddpdec_ch_config);
                    std::tuple<ErrorCode, int32_t> readHdmiDTSDialogueNormalization(
            );

                    std::tuple<ErrorCode, int32_t> readSpdifDDPKaraoke(
            );

                    std::tuple<ErrorCode, int32_t> readSpdifDTSCoreBitRate(
            );

                    std::tuple<ErrorCode, int32_t> readHdmiDTSObjectNum(
            );

                    std::tuple<ErrorCode, bool> readHdmiDlbsurroundcenterspread(
            );

    ErrorCode updateHdmiDlbsurroundcenterspread(
        bool hdmi_dlbsurroundcenterspread);

                    std::tuple<ErrorCode, int32_t> readSpdifDTSX96Flag(
            );

                    std::tuple<ErrorCode, int32_t> readHdmiDTSEsMatrixFlag(
            );

                    std::tuple<ErrorCode, int32_t> readSpdifDTSEmbeddeddownmixselect(
            );

                    std::tuple<ErrorCode, uint32_t> readHdmiMATFbbch(
            );

                    std::tuple<ErrorCode, int32_t> readSpdifDTSXchFlag(
            );

                    std::tuple<ErrorCode, int32_t> readSpdifDDPLfe(
            );

                    std::tuple<ErrorCode, int32_t> readHdmiDDPBitrate(
            );

                    std::tuple<ErrorCode, int32_t> readHdmiDDPDialoguenormalization(
            );

                    std::tuple<ErrorCode, std::vector<int32_t>> readHdmiDDPDrcmode(
            );

    ErrorCode updateHdmiDDPDrcmode(
        std::vector<int32_t> hdmi_ddpdrcmode);

                    std::tuple<ErrorCode, uint32_t> readSpdifDDPCustomChMap(
            );

                    std::tuple<ErrorCode, int32_t> readHdmiMATFbaDialogueNormalization(
            );

                    std::tuple<ErrorCode, int32_t> readSpdifDDPCenterDownmixLevel(
            );

                    std::tuple<ErrorCode, uint32_t> readHdmiDTSProgChMask(
            );

                    std::tuple<ErrorCode, std::vector<int32_t>> readSpdifDDPDrcmode(
            );

    ErrorCode updateSpdifDDPDrcmode(
        std::vector<int32_t> spdif_ddpdrcmode);

                    std::tuple<ErrorCode, int32_t> readHdmiDTSEmbeddeddownmixselect(
            );

                    std::tuple<ErrorCode, uint32_t> readHdmiPCMsamplingRate(
            );

                    std::tuple<ErrorCode, int32_t> readHdmiDTSXllsamplerate(
            );

                    std::tuple<ErrorCode, uint32_t> readSpdifDTSSamplerate(
            );

                    std::tuple<ErrorCode, std::string> readHdmiDDPStreamType(
            );

                    std::tuple<ErrorCode, uint32_t> readSpdifPCMsamplingRate(
            );

                    std::tuple<ErrorCode, uint32_t> readSpdifOutputsamplingfrequency(
            );

                    std::tuple<ErrorCode, int32_t> readSpdifDTSEsMatrixFlag(
            );

                    std::tuple<ErrorCode, uint32_t> readSpdifMATFbbch(
            );

                    std::tuple<ErrorCode, int32_t> readHdmiMATGainRequired(
            );

                    std::tuple<ErrorCode, int32_t> readSpdifDTSObjectNum(
            );

                    std::tuple<ErrorCode, std::string> readSpdifPCMChConfig(
            );

                    std::tuple<ErrorCode, uint32_t> readHdmiOutputsamplingfrequency(
            );

                    std::tuple<ErrorCode, int32_t> readHdmiDTSExtensionMask(
            );

                    std::tuple<ErrorCode, int32_t> readHdmiDDPSurroundDownmixLevel(
            );

                    std::tuple<ErrorCode, int32_t> readSpdifDTSDialogueControlAvailability(
            );

                    std::tuple<ErrorCode, int32_t> readHdmiDTSCoreBitRate(
            );

                    std::tuple<ErrorCode, int32_t> readHdmiDDPKaraoke(
            );
                    std::tuple<ErrorCode, int32_t> readHdmiDTSXSpkrout(
            );
        ErrorCode updateHdmiDTSXSpkrout(
        int32_t hdmi_dtsxspkrout);
                    std::tuple<ErrorCode, int32_t> readSpdifDTSXSpkrout(
            );
        ErrorCode updateSpdifDTSXSpkrout(
        int32_t spdif_dtsxspkrout);
                    std::tuple<ErrorCode, int32_t> readHdmiDTSXSetDialogcontrol(
            );
        ErrorCode updateHdmiDTSXSetDialogcontrol(
        int32_t hdmi_dtsxset_dialogcontrol);
                    std::tuple<ErrorCode, int32_t> readSpdifDTSXSetDialogcontrol(
            );
        ErrorCode updateSpdifDTSXSetDialogcontrol(
        int32_t spdif_dtsxset_dialogcontrol);
                    std::tuple<ErrorCode, int32_t> readHdmiDTSXGetDialogcontrol(
            );
                    std::tuple<ErrorCode, int32_t> readSpdifDTSXGetDialogcontrol(
            );
                    std::tuple<ErrorCode, int32_t> readHdmiDTSXEnableDirectmode(
            );
        ErrorCode updateHdmiDTSXEnableDirectmode(
        int32_t hdmi_dtsxenable_directmode);
                    std::tuple<ErrorCode, int32_t> readSpdifDTSXEnableDirectmode(
            );
        ErrorCode updateSpdifDTSXEnableDirectmode(
        int32_t spdif_dtsxenable_directmode);
                    std::tuple<ErrorCode, int32_t> readHdmiDTSXDisableDirectmode(
            );
        ErrorCode updateHdmiDTSXDisableDirectmode(
        int32_t hdmi_dtsxdisable_directmode);
                    std::tuple<ErrorCode, int32_t> readSpdifDTSXDisableDirectmode(
            );
        ErrorCode updateSpdifDTSXDisableDirectmode(
        int32_t spdif_dtsxdisable_directmode);
                    std::tuple<ErrorCode, int32_t> readHdmiDTSXEnableBlindparma(
            );
        ErrorCode updateHdmiDTSXEnableBlindparma(
        int32_t hdmi_dtsxenable_blindparma);
                    std::tuple<ErrorCode, int32_t> readSpdifDTSXEnableBlindparma(
            );
        ErrorCode updateSpdifDTSXEnableBlindparma(
        int32_t spdif_dtsxenable_blindparma);
                    std::tuple<ErrorCode, int32_t> readHdmiDTSXDisableBlindparma(
            );
        ErrorCode updateHdmiDTSXDisableBlindparma(
        int32_t hdmi_dtsxdisable_blindparma);
                    std::tuple<ErrorCode, int32_t> readSpdifDTSXDisableBlindparma(
            );
        ErrorCode updateSpdifDTSXDisableBlindparma(
        int32_t spdif_dtsxdisable_blindparma);
                    std::tuple<ErrorCode, int32_t> readHdmiDTSXEsMatrix(
            );
        ErrorCode updateHdmiDTSXEsMatrix(
        int32_t hdmi_dtsxes_matrix);
                    std::tuple<ErrorCode, int32_t> readSpdifDTSXEsMatrix(
            );
        ErrorCode updateSpdifDTSXEsMatrix(
        int32_t spdif_dtsxes_matrix);
                    std::tuple<ErrorCode, int32_t> readHdmiDTSXEnableAnalogCompensation(
            );
        ErrorCode updateHdmiDTSXEnableAnalogCompensation(
        int32_t hdmi_dtsxenable_analog_compensation);
                    std::tuple<ErrorCode, int32_t> readSpdifDTSXEnableAnalogCompensation(
            );
        ErrorCode updateSpdifDTSXEnableAnalogCompensation(
        int32_t spdif_dtsxenable_analog_compensation);
                    std::tuple<ErrorCode, int32_t> readHdmiDTSXSetAnalogcompmaxUsergain(
            );
        ErrorCode updateHdmiDTSXSetAnalogcompmaxUsergain(
        int32_t hdmi_dtsxset_analogcompmax_usergain);
                    std::tuple<ErrorCode, int32_t> readSpdifDTSXSetAnalogcompmaxUsergain(
            );
        ErrorCode updateSpdifDTSXSetAnalogcompmaxUsergain(
        int32_t spdif_dtsxset_analogcompmax_usergain);
                    std::tuple<ErrorCode, std::vector<int32_t>> readHdmiDTSXDrcmode(
            );
        ErrorCode updateHdmiDTSXDrcmode(
        std::vector<int32_t> hdmi_dtsxdrcmode);
                    std::tuple<ErrorCode, std::vector<int32_t>> readSpdifDTSXDrcmode(
            );
        ErrorCode updateSpdifDTSXDrcmode(
        std::vector<int32_t> spdif_dtsxdrcmode);
                    std::tuple<ErrorCode, std::vector<int32_t>> readHdmiMATDrcmode(
            );
        ErrorCode updateHdmiMATDrcmode(
        std::vector<int32_t> hdmi_matdrcmode);
                    std::tuple<ErrorCode, std::vector<int32_t>> readSpdifMATDrcmode(
            );
        ErrorCode updateSpdifMATDrcmode(
        std::vector<int32_t> spdif_matdrcmode);
                    std::tuple<ErrorCode, std::string> readHdmiReadDetectedformat(
            );
                    std::tuple<ErrorCode, std::string> readSpdifReadDetectedformat(
            );
                    std::tuple<ErrorCode, std::string> readCurrentClock(
            );
                    std::tuple<ErrorCode, std::string> readCurrentSource(
            );

    // obj_path and set of strings that suggest the changed properties.
    void setPropertiesChangedListener(
        const std::function<void(PropertiesAttr, const std::string &)> &listener) {
        properties_changed_listeners_.emplace_back(listener);
    }

    void setObjIdMapChangedListener(
        const std::function<void(const std::unordered_map<std::string, std::unordered_set<std::string>>)> &listener) {
        obj_id_map_changed_listeners_.emplace_back(listener);
    }

 private:
    void propertiesChangedCb(const std::string &obj_path,
        const std::string &iface_name, GVariantIter *iter);

    PropertiesAttr processDapConfigIfaceResponse(
        const std::string &obj_path, GVariantIter *iter);
    PropertiesAttr processDdpConfigIfaceResponse(
        const std::string &obj_path, GVariantIter *iter);
    PropertiesAttr processDtsxConfigIfaceResponse(
        const std::string &obj_path, GVariantIter *iter);
    PropertiesAttr processDsdConfigIfaceResponse(
        const std::string &obj_path, GVariantIter *iter);
    PropertiesAttr processMatConfigIfaceResponse(
        const std::string &obj_path, GVariantIter *iter);
    PropertiesAttr processStreamConfigIfaceResponse(
        const std::string &obj_path, GVariantIter *iter);
    PropertiesAttr processAacConfigIfaceResponse(
        const std::string &obj_path, GVariantIter *iter);
    PropertiesAttr processPcmConfigIfaceResponse(
        const std::string &obj_path, GVariantIter *iter);
        PropertiesAttr processAudiomanagerDeviceControllerIfaceResponse(
        const std::string& obj_path, GVariantIter *iter);

 private:
    Dbus *dbus_;
    std::mutex mutex_;

    std::vector<std::function<void(PropertiesAttr, const std::string &)>> properties_changed_listeners_;
    std::vector<std::function<void(const std::unordered_map<std::string, std::unordered_set<std::string>>)>>
        obj_id_map_changed_listeners_;

    std::unordered_map<std::string, std::unordered_set<std::string>> obj_id_map_;
};

class MethodsHandler {
 public:
    explicit MethodsHandler(Dbus *dbus);
    ~MethodsHandler() = default;

    void setObjIdMapChangedListener(
        const std::function<void(const std::unordered_map<std::string, std::unordered_set<std::string>>)> &listener) {
        obj_id_map_changed_listeners_.emplace_back(listener);
    }
    ErrorCode
    spdifDecodeRestart(
                std::string in_arg
                );
    ErrorCode
    deleteSource(
                        const std::string &obj_id
        );
    ErrorCode
    createSource(
                CreateSourceIn in_arg
                ,        const std::string &obj_id
        );
    ErrorCode
    hdmiDecodeRestart(
                std::string in_arg
                );
            ErrorCode
        setSourceExtClk(
                SetSourceExtClkIn in_arg
                );
 private:
    void interfacesAddedCb(const std::string &obj_path,
        const std::string &iface_name, GVariantIter *iter);

 private:
    Dbus *dbus_;
    std::mutex mutex_;

    std::vector<std::function<void(const std::unordered_map<std::string, std::unordered_set<std::string>>)>>
        obj_id_map_changed_listeners_;

    std::unordered_map<std::string, std::unordered_set<std::string>> obj_id_map_;
};

class SignalsHandler {
 public:
    explicit SignalsHandler(Dbus *dbus);
    ~SignalsHandler() = default;

    void setHdmiDetectedFormatListener(
        std::function<void(
            const std::string &,
                        const std::string &
        )> listener) {
        hdmi_detected_format_listeners_.emplace_back(listener);
    }
    void setHdmiDualMonoAACListener(
        std::function<void(
                        const std::string &
        )> listener) {
        hdmi_dual_mono_aac_listeners_.emplace_back(listener);
    }
    void setSpdifDetectedFormatListener(
        std::function<void(
            const std::string &,
                        const std::string &
        )> listener) {
        spdif_detected_format_listeners_.emplace_back(listener);
    }
        void setSetSourceEventListener(
        std::function<void(
                        const std::string &,
                        const std::string &
        )> listener) {
        set_source_event_listeners_.emplace_back(listener);
    }
 private:
    void callHdmiDetectedFormatListeners(const std::string &obj_path, GVariant *response);
    void callHdmiDualMonoAACListeners(const std::string &obj_path, GVariant *response);
    void callSpdifDetectedFormatListeners(const std::string &obj_path, GVariant *response);
        void callSetSourceEventListeners(const std::string& obj_path, GVariant *response);

 private:
    Dbus *dbus_;
    std::mutex mutex_;

    std::vector<std::function<void(
        const std::string &,
                const std::string &)>> hdmi_detected_format_listeners_;
        std::vector<std::function<void(
                const std::string &)>> hdmi_dual_mono_aac_listeners_;
    std::vector<std::function<void(
                const std::string &,
                const std::string &)>> spdif_detected_format_listeners_;
    std::vector<std::function<void(
        const std::string &,
                const std::string &)>> set_source_event_listeners_;
};
}  // namespace adk

#endif  // AUTOGEN_INTERNAL_API_INTERFACE_HANDLERS_H_
