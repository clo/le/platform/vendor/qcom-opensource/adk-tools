/*
 * Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of The Linux Foundation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <cassert>
#include <chrono>
#include <csignal>
#include <cstring>
#include <functional>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <thread>

#include "autogen/adk_system_interface.h"
#include "autogen/attribute_classes.h"
#include "autogen/attribute_strings.h"

#include <iostream>

using namespace adk;
using namespace std;

bool startAdkSystemInterface() {
    g_print("StartAdkSystemInterface\n");

    // ignore SIGPIPE
    struct sigaction action;
    action.sa_handler = SIG_IGN;
    sigemptyset(&action.sa_mask);
    action.sa_flags = 0;
    sigaction(SIGPIPE, &action, nullptr);

    // block signals we will want to "sigwait" on.
    // (needs to be set before we create any thread or the app will be killed
    // if we receive another a second signal while processing the first)
    sigset_t sig_set;
    sigemptyset(&sig_set);
    sigaddset(&sig_set, SIGHUP);
    sigaddset(&sig_set, SIGINT);
    sigaddset(&sig_set, SIGTERM);
    pthread_sigmask(SIG_BLOCK, &sig_set, nullptr);

    AdkSystemInterface &adk_si = AdkSystemInterface::getInstance();

    if (!adk_si.init("adk_system_interface_sample")) {
        return false;
    }

    std::unordered_map<std::string, std::unordered_set<std::string>>
        properties_obj_id_map;
    std::shared_ptr<PropertiesHandler> properties_handler =
        adk_si.getPropertiesHandler();

    properties_handler->setObjIdMapChangedListener(
        [&properties_obj_id_map](const std::unordered_map<std::string, std::unordered_set<std::string>> &obj_id_map) -> void {
            properties_obj_id_map = obj_id_map;
            for (auto &prop_it : obj_id_map) {
                std::string prop_name = prop_it.first;
                for (auto &obj_id : prop_it.second) {
                    g_print(
                        "ObjIdMapChangedListenerCb: prop_name - %s, "
                        "obj_path - %s \n",
                        prop_name.c_str(), obj_id.c_str());
                }
            }
        });

    properties_handler->setPropertiesChangedListener(
        [](PropertiesAttr prop_attr, const std::string &obj_path) -> void {
            g_print("Main::PropertiesChangedCb \n");
            g_print("Main::propertiesChangedCb obj_path : %s\n", obj_path.c_str());

            /* Get and Set Properties for Hdmi */

            std::unordered_set<std::string> changed_properties = prop_attr.getChangedProperties();
            if (changed_properties.find(std::string(adk::kHdmiDDPDrcmode)) != changed_properties.end()) {
                std::vector<int32_t> ddp_drc_mode = prop_attr.getHdmiDDPDrcmode();
                for (auto &it : ddp_drc_mode) {
                    g_print("Main::PropertiesChangedCb Hdmi DDP Dynamic Range Control %d \n", it);
                }
            }
            if (changed_properties.find(std::string(adk::kHdmiDDPDualMono)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi DDP Dual Mono %d \n",
                    prop_attr.getHdmiDDPDualMono());
            }
            if (changed_properties.find(std::string(adk::kHdmiDDPDecChConfig)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi DDP Decode Channel Configuration %d \n",
                    prop_attr.getHdmiDDPDecChConfig());
            }
            if (changed_properties.find(std::string(adk::kHdmiDecChConfig)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi Decode Channel Configuration %d \n",
                    prop_attr.getHdmiDecChConfig());
            }
            if (changed_properties.find(std::string(adk::kHdmiDDPCustomChMap)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi DDP Custom Channel Map %u \n",
                    prop_attr.getHdmiDDPCustomChMap());
            }
            if (changed_properties.find(std::string(adk::kHdmiDDPLfe)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi DDP LFE Status %d \n",
                    prop_attr.getHdmiDDPLfe());
            }
            if (changed_properties.find(std::string(adk::kHdmiDDPAudioCodingMode)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi DDP AudioCodingMode Status %d \n",
                    prop_attr.getHdmiDDPAudioCodingMode());
            }
            if (changed_properties.find(std::string(adk::kHdmiDDPStreamType)) != changed_properties.end()) {
                std::string stream_type = prop_attr.getHdmiDDPStreamType();
                g_print("Main::PropertiesChangedCb Hdmi DDP StreamType Status %s \n",
                    stream_type.c_str());
            }
            if (changed_properties.find(std::string(adk::kHdmiDecSamplFreq)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi DecSamplFreq Status %u \n",
                    prop_attr.getHdmiDecSamplFreq());
            }
            if (changed_properties.find(std::string(adk::kHdmiPCMsamplingRate)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi PCM Sampling Rate Status %u \n",
                    prop_attr.getHdmiPCMsamplingRate());
            }
            if (changed_properties.find(std::string(adk::kHdmiPCMChConfig)) != changed_properties.end()) {
                std::string pcm_ch_config = prop_attr.getHdmiPCMChConfig();
                g_print("Main::PropertiesChangedCb Hdmi PCM ChConfig Status %s \n",
                    pcm_ch_config.c_str());
            }
            if (changed_properties.find(std::string(adk::kHdmiMATFbbch)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi MAT Fbbch Status %u \n",
                    prop_attr.getHdmiMATFbbch());
            }
            if (changed_properties.find(std::string(adk::kHdmiMATTrueHdSync)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi MAT TrueHdSync Status %u \n",
                    prop_attr.getHdmiMATTrueHdSync());
            }
            if (changed_properties.find(std::string(adk::kHdmiMATStreamtype)) != changed_properties.end()) {
                std::string mat_stream_type = prop_attr.getHdmiMATStreamtype();
                g_print("Main::PropertiesChangedCb Hdmi MAT Stream Type Status %s \n",
                    mat_stream_type.c_str());
            }
            if (changed_properties.find(std::string(adk::kHdmiMATFbach)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi MAT Fbach Status %u \n",
                    prop_attr.getHdmiMATFbach());
            }
            if (changed_properties.find(std::string(adk::kHdmiOutputsamplingfrequency)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi Output Sampling Frequency Status %u \n",
                    prop_attr.getHdmiOutputsamplingfrequency());
            }
            if (changed_properties.find(std::string(adk::kHdmiDTSBitrate)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi DTS BitRate Status %d \n",
                    prop_attr.getHdmiDTSBitrate());
            }
            if (changed_properties.find(std::string(adk::kHdmiDTSEsMatrixFlag)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi DTS EsMatrix Flag Status %d \n",
                    prop_attr.getHdmiDTSEsMatrixFlag());
            }
            if (changed_properties.find(std::string(adk::kHdmiDTSXchFlag)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi DTS XchFlag Status %d \n",
                    prop_attr.getHdmiDTSXchFlag());
            }
            if (changed_properties.find(std::string(adk::kHdmiDTSX96Flag)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi DTS X96Flag Status %d \n",
                    prop_attr.getHdmiDTSX96Flag());
            }
            if (changed_properties.find(std::string(adk::kHdmiDTSDialogueNormalization)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi DTS Dialogue Normalization Status %d \n",
                    prop_attr.getHdmiDTSDialogueNormalization());
            }
            if (changed_properties.find(std::string(adk::kHdmiDTSExtensionMask)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi DTS Extension Mask %d \n",
                    static_cast<int>(prop_attr.getHdmiDTSExtensionMask()));
            }
            if (changed_properties.find(std::string(adk::kHdmiDTSProgChMask)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi DTS ProgChMask %u \n",
                    prop_attr.getHdmiDTSProgChMask());
            }
            if (changed_properties.find(std::string(adk::kHdmiDTSXllsamplerate)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi DTS Xll samplerate %d \n",
                    prop_attr.getHdmiDTSXllsamplerate());
            }
            if (changed_properties.find(std::string(adk::kHdmiDTSEmbeddeddownmixselect)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi DTS Embedded downmixselect %d \n",
                    prop_attr.getHdmiDTSEmbeddeddownmixselect());
            }
            if (changed_properties.find(std::string(adk::kHdmiDTSSamplerate)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi DTS Sample rate %u \n",
                    prop_attr.getHdmiDTSSamplerate());
            }
            if (changed_properties.find(std::string(adk::kHdmiDTSStreamType)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi DTS StreamType %d \n",
                    prop_attr.getHdmiDTSStreamType());
            }
            if (changed_properties.find(std::string(adk::kHdmiDTSDecodeInfo)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi DTS Decode Info %d \n",
                    prop_attr.getHdmiDTSDecodeInfo());
            }
            if (changed_properties.find(std::string(adk::kHdmiDTSProgChMask)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi DTS ProgChMask %u \n",
                    prop_attr.getHdmiDTSProgChMask());
            }
            if (changed_properties.find(std::string(adk::kHdmiDTSCoreBitRate)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi DTS Core BitRate %d \n",
                    prop_attr.getHdmiDTSCoreBitRate());
            }
            if (changed_properties.find(std::string(adk::kHdmiDTSObjectNum)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi DTS ObjectNum %d \n",
                    prop_attr.getHdmiDTSObjectNum());
            }
            if (changed_properties.find(std::string(adk::kHdmiDTSDialogueControlAvailability)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi DTS Dialogue Control Availability %d \n",
                    prop_attr.getHdmiDTSDialogueControlAvailability());
            }
            if (changed_properties.find(std::string(adk::kHdmiDTSAnalogCompensationGain)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi DTS Analog Compensation Gain %d \n",
                    prop_attr.getHdmiDTSAnalogCompensationGain());
            }
            if (changed_properties.find(std::string(adk::kHdmiDDPSurroundex)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi DDP Surroundex %d \n",
                    prop_attr.getHdmiDDPSurroundex());
            }
            if (changed_properties.find(std::string(adk::kHdmiDDPKaraoke)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi DDP Karaoke %d \n",
                    prop_attr.getHdmiDDPKaraoke());
            }
            if (changed_properties.find(std::string(adk::kHdmiDDPCenterDownmixLevel)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi DDP Center Downmix Level %d \n",
                    prop_attr.getHdmiDDPCenterDownmixLevel());
            }
            if (changed_properties.find(std::string(adk::kHdmiDDPSurroundDownmixLevel)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi DDP Surround Downmix Level %d \n",
                    prop_attr.getHdmiDDPSurroundDownmixLevel());
            }
            if (changed_properties.find(std::string(adk::kHdmiDDPDialoguenormalization)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi DDP Dialogue normalization %d \n",
                    prop_attr.getHdmiDDPDialoguenormalization());
            }
            if (changed_properties.find(std::string(adk::kHdmiDDPBitrate)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi DDP Bitrate %d \n",
                    prop_attr.getHdmiDDPBitrate());
            }
            if (changed_properties.find(std::string(adk::kHdmiMATGainRequired)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi MAT GainRequired %d \n",
                    prop_attr.getHdmiMATGainRequired());
            }
            if (changed_properties.find(std::string(adk::kHdmiMATFbaSurroundCh)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi MAT Fba SurroundEX %d \n",
                    prop_attr.getHdmiMATFbaSurroundCh());
            }
            if (changed_properties.find(std::string(adk::kHdmiMATFbaDialogueNormalization)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi MAT Fba Dialogue Normalization %d \n",
                    prop_attr.getHdmiMATFbaDialogueNormalization());
            }
            if (changed_properties.find(std::string(adk::kHdmiDSDSamplingrate)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi DSD Sampling rate %u \n",
                    prop_attr.getHdmiDSDSamplingrate());
            }
            if (changed_properties.find(std::string(adk::kHdmiDSDChConfig)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi DSD ChDConfig %d \n",
                    prop_attr.getHdmiDSDChConfig());
            }
            if (changed_properties.find(std::string(adk::kHdmiDtsxDrcpercent)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi Dtsx Drcpercent %d \n",
                    prop_attr.getHdmiDtsxDrcpercent());
            }
            if (changed_properties.find(std::string(adk::kHdmiDAPDlbsurroundmode)) != changed_properties.end()) {
                bool dlb_surround_mode = prop_attr.getHdmiDAPDlbsurroundmode();
                g_print("Main::PropertiesChangedCb Hdmi DAP Dlbsurroundmode %s \n",
                    dlb_surround_mode ? "Enabled" : "Disabled");
            }
            if (changed_properties.find(std::string(adk::kHdmiDlbsurroundcenterspread)) != changed_properties.end()) {
                bool dlb_surround_center_spread = prop_attr.getHdmiDlbsurroundcenterspread();
                g_print("Main::PropertiesChangedCb Hdmi Dlbsurround Centerspread %s \n",
                    dlb_surround_center_spread ? "Enabled" : "Disabled");
            }
            if (changed_properties.find(std::string(adk::kHdmiAacDualMonoChannelMode)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi AAC DualMono Channel Mode %d \n",
                    (prop_attr.getHdmiAacDualMonoChannelMode()));
            }

            /*Added New GET/SET Properties for FR62629 */
            if (changed_properties.find(std::string(adk::kHdmiDTSXSpkrout)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi Speaker out of HDMI Source %d \n",
                    (prop_attr.getHdmiDTSXSpkrout()));
            }
            if (changed_properties.find(std::string(adk::kHdmiDTSXSetDialogcontrol)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi Set Dialogue Control %d \n",
                    (prop_attr.getHdmiDTSXSetDialogcontrol()));
            }
            if (changed_properties.find(std::string(adk::kHdmiDTSXGetDialogcontrol)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi Get Dialogue Control %d \n",
                    (prop_attr.getHdmiDTSXGetDialogcontrol()));
            }
            if (changed_properties.find(std::string(adk::kHdmiDTSXEnableDirectmode)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi Enable Direct Mode %d \n",
                    (prop_attr.getHdmiDTSXEnableDirectmode()));
            }
            if (changed_properties.find(std::string(adk::kHdmiDTSXDisableDirectmode)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi Disable Direct Mode %d \n",
                    (prop_attr.getHdmiDTSXDisableDirectmode()));
            }
            if (changed_properties.find(std::string(adk::kHdmiDTSXEnableBlindparma)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi Enable Blindparma %d \n",
                    (prop_attr.getHdmiDTSXEnableBlindparma()));
            }
            if (changed_properties.find(std::string(adk::kHdmiDTSXDisableBlindparma)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi Disable Blindparma %d \n",
                    (prop_attr.getHdmiDTSXDisableBlindparma()));
            }
            if (changed_properties.find(std::string(adk::kHdmiDTSXEsMatrix)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi DTSx EsMatrix %d \n",
                    (prop_attr.getHdmiDTSXEsMatrix()));
            }
            if (changed_properties.find(std::string(adk::kHdmiDTSXEnableAnalogCompensation)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi DTSX Enable Analog Compensation %d \n",
                    (prop_attr.getHdmiDTSXEnableAnalogCompensation()));
            }
            if (changed_properties.find(std::string(adk::kHdmiDTSXSetAnalogcompmaxUsergain)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Hdmi DTSX Set AnalogcompmaxUsergain %d \n",
                    (prop_attr.getHdmiDTSXSetAnalogcompmaxUsergain()));
            }
            if (changed_properties.find(std::string(adk::kHdmiDTSXDrcmode)) != changed_properties.end()) {
                std::vector<int32_t> hdmi_dtsxdrcmode = prop_attr.getHdmiDTSXDrcmode();
                for (auto &it : hdmi_dtsxdrcmode) {
                    g_print("Main::PropertiesChangedCb Hdmi DTSx Dynamic Range Control %d \n", it);
                }
            }
            if (changed_properties.find(std::string(adk::kHdmiMATDrcmode)) != changed_properties.end()) {
                std::vector<int32_t> hdmi_matdrcmode = prop_attr.getHdmiMATDrcmode();
                for (auto &it : hdmi_matdrcmode) {
                    g_print("Main::PropertiesChangedCb Hdmi DTSx Dynamic Range Control %d \n", it);
                }
            }
            if (changed_properties.find(std::string(adk::kHdmiReadDetectedformat)) != changed_properties.end()) {
                std::string read_detected_format = prop_attr.getHdmiReadDetectedformat();
                g_print("Main::PropertiesChangedCb Hdmi Read Detected_Format %s \n",
                    read_detected_format.c_str());
            }
            /* Get and Set Properties for Spdif */

            if (changed_properties.find(std::string(adk::kSpdifDDPDrcmode)) != changed_properties.end()) {
                std::vector<int32_t> ddp_drc_mode = prop_attr.getSpdifDDPDrcmode();
                for (auto &it : ddp_drc_mode) {
                    g_print("Main::PropertiesChangedCb Spdif Dynamic Range Control %d \n", it);
                }
            }
            if (changed_properties.find(std::string(adk::kSpdifDDPDualMono)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif DDP dual mono %d \n",
                    prop_attr.getSpdifDDPDualMono());
            }
            if (changed_properties.find(std::string(adk::kSpdifDecChConfig)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif Decode Channel Configuration %d \n",
                    prop_attr.getSpdifDecChConfig());
            }
            if (changed_properties.find(std::string(adk::kSpdifDDPDecChConfig)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif DDP Decode Channel Configuration %d \n",
                    prop_attr.getSpdifDDPDecChConfig());
            }
            if (changed_properties.find(std::string(adk::kSpdifDDPCustomChMap)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif DDP custom channel map %u \n",
                    prop_attr.getSpdifDDPCustomChMap());
            }
            if (changed_properties.find(std::string(adk::kSpdifDDPLfe)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif DDP lfe Status %d \n",
                    prop_attr.getSpdifDDPLfe());
            }
            if (changed_properties.find(std::string(adk::kSpdifDDPAudioCodingMode)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif DDP AudioCodingMode Status %d \n",
                    prop_attr.getSpdifDDPAudioCodingMode());
            }
            if (changed_properties.find(std::string(adk::kSpdifDDPStreamType)) != changed_properties.end()) {
                std::string stream_type = prop_attr.getSpdifDDPStreamType();
                g_print("Main::PropertiesChangedCb Spdif DDP StreamType Status %s \n",
                    stream_type.c_str());
            }
            if (changed_properties.find(std::string(adk::kSpdifDecSamplFreq)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif DecSamplFreq Status %u \n",
                    prop_attr.getSpdifDecSamplFreq());
            }
            if (changed_properties.find(std::string(adk::kSpdifPCMsamplingRate)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif PCM Sampling Rate Status %u \n",
                    prop_attr.getSpdifPCMsamplingRate());
            }
            if (changed_properties.find(std::string(adk::kSpdifPCMChConfig)) != changed_properties.end()) {
                std::string pcm_ch_config = prop_attr.getSpdifPCMChConfig();
                g_print("Main::PropertiesChangedCb Spdif PCM ChConfig Status %s \n",
                    pcm_ch_config.c_str());
            }
            if (changed_properties.find(std::string(adk::kSpdifMATFbbch)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif MAT Fbbch Status %u \n",
                    prop_attr.getSpdifMATFbbch());
            }
            if (changed_properties.find(std::string(adk::kSpdifMATTrueHdSync)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif MAT TrueHdSync Status %u \n",
                    prop_attr.getSpdifMATTrueHdSync());
            }
            if (changed_properties.find(std::string(adk::kSpdifMATStreamtype)) != changed_properties.end()) {
                std::string stream_type = prop_attr.getSpdifMATStreamtype();
                g_print("Main::PropertiesChangedCb Spdif MAT Stream Type Status %s \n",
                    stream_type.c_str());
            }
            if (changed_properties.find(std::string(adk::kSpdifMATFbach)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif MAT Fbach Status %u \n",
                    prop_attr.getSpdifMATFbach());
            }
            if (changed_properties.find(std::string(adk::kSpdifOutputsamplingfrequency)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif Output sampling frequency Status %u \n",
                    prop_attr.getSpdifOutputsamplingfrequency());
            }
            if (changed_properties.find(std::string(adk::kSpdifDTSBitrate)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif DTS BitRate Status %d \n",
                    prop_attr.getSpdifDTSBitrate());
            }
            if (changed_properties.find(std::string(adk::kSpdifDTSEsMatrixFlag)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif DTS EsMatrix Flag Status %d \n",
                    prop_attr.getSpdifDTSEsMatrixFlag());
            }
            if (changed_properties.find(std::string(adk::kSpdifDTSXchFlag)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif DTS XchFlag Status %d \n",
                    prop_attr.getSpdifDTSXchFlag());
            }
            if (changed_properties.find(std::string(adk::kSpdifDTSX96Flag)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif DTS X96Flag Status %d \n",
                    prop_attr.getSpdifDTSX96Flag());
            }
            if (changed_properties.find(std::string(adk::kSpdifDTSDialogueNormalization)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif DTS Dialogue Normalization Status %d \n",
                    prop_attr.getSpdifDTSDialogueNormalization());
            }
            if (changed_properties.find(std::string(adk::kSpdifDTSExtensionMask)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif DTS Extension Mask %d \n",
                    prop_attr.getSpdifDTSExtensionMask());
            }
            if (changed_properties.find(std::string(adk::kSpdifDTSProgChMask)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif DTS ProgChMask %u \n",
                    prop_attr.getSpdifDTSProgChMask());
            }
            if (changed_properties.find(std::string(adk::kSpdifDTSXllsamplerate)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif DTS Xll samplerate %d \n",
                    prop_attr.getSpdifDTSXllsamplerate());
            }
            if (changed_properties.find(std::string(adk::kSpdifDTSEmbeddeddownmixselect)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif DTS Embedded DownmixSelect %d \n",
                    prop_attr.getSpdifDTSEmbeddeddownmixselect());
            }
            if (changed_properties.find(std::string(adk::kSpdifDTSSamplerate)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif DTS Sample Rate %u \n",
                    prop_attr.getSpdifDTSSamplerate());
            }
            if (changed_properties.find(std::string(adk::kSpdifDTSStreamType)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif DTS StreamType %d \n",
                    prop_attr.getSpdifDTSStreamType());
            }
            if (changed_properties.find(std::string(adk::kSpdifDTSDecodeInfo)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif DTS Decode Info %d \n",
                    prop_attr.getSpdifDTSDecodeInfo());
            }
            if (changed_properties.find(std::string(adk::kSpdifDTSProgChMask)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif DTS ProgChMask %u \n",
                    prop_attr.getSpdifDTSProgChMask());
            }
            if (changed_properties.find(std::string(adk::kSpdifDTSCoreBitRate)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif DTS Core BitRate %d \n",
                    prop_attr.getSpdifDTSCoreBitRate());
            }
            if (changed_properties.find(std::string(adk::kSpdifDTSObjectNum)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif DTS ObjectNum %d \n",
                    prop_attr.getSpdifDTSObjectNum());
            }
            if (changed_properties.find(std::string(adk::kSpdifDTSDialogueControlAvailability)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif DTS Dialogue Control Availability %d \n",
                    prop_attr.getSpdifDTSDialogueControlAvailability());
            }
            if (changed_properties.find(std::string(adk::kSpdifDTSAnalogCompensationGain)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif DTS Analog Compensation Gain %d \n",
                    prop_attr.getSpdifDTSAnalogCompensationGain());
            }
            if (changed_properties.find(std::string(adk::kSpdifDDPSurroundex)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif DDP Surroundex %d \n",
                    prop_attr.getSpdifDDPSurroundex());
            }
            if (changed_properties.find(std::string(adk::kSpdifDDPKaraoke)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif DDP Karaoke %d \n",
                    prop_attr.getSpdifDDPKaraoke());
            }
            if (changed_properties.find(std::string(adk::kSpdifDDPCenterDownmixLevel)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif DDP Center Downmix Level %d \n",
                    prop_attr.getSpdifDDPCenterDownmixLevel());
            }
            if (changed_properties.find(std::string(adk::kSpdifDDPSurroundDownmixLevel)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif DDP Surround Downmix Level %d \n",
                    prop_attr.getSpdifDDPSurroundDownmixLevel());
            }
            if (changed_properties.find(std::string(adk::kSpdifDDPDialoguenormalization)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif DDP Dialogue normalization %d \n",
                    prop_attr.getSpdifDDPDialoguenormalization());
            }
            if (changed_properties.find(std::string(adk::kSpdifDDPBitrate)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif DDP Bitrate %d \n",
                    prop_attr.getSpdifDDPBitrate());
            }
            if (changed_properties.find(std::string(adk::kSpdifMATGainRequired)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif MAT GainRequired %d \n",
                    prop_attr.getSpdifMATGainRequired());
            }
            if (changed_properties.find(std::string(adk::kSpdifMATFbaSurroundCh)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif MAT Fba SurroundEX %d \n",
                    prop_attr.getSpdifMATFbaSurroundCh());
            }
            if (changed_properties.find(std::string(adk::kSpdifMATFbaDialogueNormalization)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif MAT Fba Dialogue Normalization %d \n",
                    prop_attr.getSpdifMATFbaDialogueNormalization());
            }
            if (changed_properties.find(std::string(adk::kSpdifDSDSamplingrate)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif DSD Sampling rate %u \n",
                    prop_attr.getSpdifDSDSamplingrate());
            }
            if (changed_properties.find(std::string(adk::kSpdifDSDChConfig)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif DSChD Config %d \n",
                    prop_attr.getSpdifDSDChConfig());
            }
            if (changed_properties.find(std::string(adk::kSpdifDtsxDrcpercent)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif Dtsx Drcpercent %d \n",
                    prop_attr.getSpdifDtsxDrcpercent());
            }
            if (changed_properties.find(std::string(adk::kSpdifDAPDlbsurroundmode)) != changed_properties.end()) {
                bool dlb_surround_mode = prop_attr.getSpdifDAPDlbsurroundmode();
                g_print("Main::PropertiesChangedCb Spdif Dtsx Drcpercent %s \n",
                    dlb_surround_mode ? "Enabled" : "Disabled");
            }
            if (changed_properties.find(std::string(adk::kSpdifDlbsurroundcenterspread)) != changed_properties.end()) {
                bool dlb_surround_center_spread = prop_attr.getSpdifDlbsurroundcenterspread();
                g_print("Main::PropertiesChangedCb Spdif Dlb Surround Centerspread %s \n",
                    dlb_surround_center_spread ? "Enabled" : "Disabled");
            }
            /*Added New GET/SET Properties for FR62629 */
            if (changed_properties.find(std::string(adk::kSpdifDTSXSpkrout)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif Speaker out of HDMI source %d \n",
                    (prop_attr.getSpdifDTSXSpkrout()));
            }
            if (changed_properties.find(std::string(adk::kSpdifDTSXSetDialogcontrol)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif Set Dialogue Control %d \n",
                    (prop_attr.getSpdifDTSXSetDialogcontrol()));
            }
            if (changed_properties.find(std::string(adk::kSpdifDTSXGetDialogcontrol)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif Get Dialogue Control %d \n",
                    (prop_attr.getSpdifDTSXGetDialogcontrol()));
            }
            if (changed_properties.find(std::string(adk::kSpdifDTSXEnableDirectmode)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif Enable Direct Mode %d \n",
                    (prop_attr.getSpdifDTSXEnableDirectmode()));
            }
            if (changed_properties.find(std::string(adk::kSpdifDTSXDisableDirectmode)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif Disable Direct Mode %d \n",
                    (prop_attr.getSpdifDTSXDisableDirectmode()));
            }
            if (changed_properties.find(std::string(adk::kSpdifDTSXEnableBlindparma)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif Enable Blindparma %d \n",
                    (prop_attr.getSpdifDTSXEnableBlindparma()));
            }
            if (changed_properties.find(std::string(adk::kSpdifDTSXDisableBlindparma)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif Disable Blindparma %d \n",
                    (prop_attr.getSpdifDTSXDisableBlindparma()));
            }
            if (changed_properties.find(std::string(adk::kSpdifDTSXEsMatrix)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif DTSx EsMatrix %d \n",
                    (prop_attr.getSpdifDTSXEsMatrix()));
            }
            if (changed_properties.find(std::string(adk::kSpdifDTSXEnableAnalogCompensation)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif DTSX Enable Analog Compensation %d \n",
                    (prop_attr.getSpdifDTSXEnableAnalogCompensation()));
            }
            if (changed_properties.find(std::string(adk::kSpdifDTSXSetAnalogcompmaxUsergain)) != changed_properties.end()) {
                g_print("Main::PropertiesChangedCb Spdif DTSX Set AnalogcompmaxUsergain %d \n",
                    (prop_attr.getSpdifDTSXSetAnalogcompmaxUsergain()));
            }
            if (changed_properties.find(std::string(adk::kSpdifDTSXDrcmode)) != changed_properties.end()) {
                std::vector<int32_t> spdif_dtsxdrcmode = prop_attr.getSpdifDTSXDrcmode();
                for (auto &it : spdif_dtsxdrcmode) {
                    g_print("Main::PropertiesChangedCb Spdif DTSx Dynamic Range Control %d \n", it);
                }
            }
            if (changed_properties.find(std::string(adk::kSpdifMATDrcmode)) != changed_properties.end()) {
                std::vector<int32_t> spdif_matdrcmode = prop_attr.getHdmiMATDrcmode();
                for (auto &it : spdif_matdrcmode) {
                    g_print("Main::PropertiesChangedCb Spdif DTSx Dynamic Range Control %d \n", it);
                }
            }
            if (changed_properties.find(std::string(adk::kSpdifReadDetectedformat)) != changed_properties.end()) {
                std::string read_detected_format = prop_attr.getSpdifReadDetectedformat();
                g_print("Main::PropertiesChangedCb spdif Read Detected_Format %s \n",
                    read_detected_format.c_str());
            }
        });

    std::unordered_map<std::string, std::unordered_set<std::string>>
        methods_obj_id_map;
    std::shared_ptr<MethodsHandler> methods_handler = adk_si.getMethodsHandler();

    methods_handler->setObjIdMapChangedListener(
        [&methods_obj_id_map](const std::unordered_map<std::string, std::unordered_set<std::string>> &obj_id_map) -> void {
            methods_obj_id_map = obj_id_map;
            for (auto &prop_it : obj_id_map) {
                std::string prop_name = prop_it.first;
                for (auto &obj_id : prop_it.second) {
                    g_print(
                        "ObjIdMapChangedListenerCb: prop_name - %s, "
                        "obj_path - %s \n",
                        prop_name.c_str(), obj_id.c_str());
                }
            }
        });

    std::shared_ptr<SignalsHandler> signals_handler = adk_si.getSignalsHandler();

    /* Hdmi Handlers for Get and Set parameters */

    signals_handler->setHdmiDetectedFormatListener(
        [&properties_handler, &methods_handler](const std::string &detected_format, const std::string &obj_path) -> void {
            g_print("Hdmi format detected on %s obj_path \n", obj_path.c_str());
            g_print("Hdmi Detected Format : %s \n", detected_format.c_str());

            if ((detected_format == "DDP") || (detected_format == "DDP ATMOS") || (detected_format == "DDP CH")){
                ErrorCode result = ADK_SI_OK;
                uint32_t prop_value;
                std::tie(result, prop_value) = properties_handler->readHdmiDecSamplFreq(); /* Integrated Decoder System Get Property */
                if (result == ADK_SI_OK) {
                    g_print("Hdmi Integrated Decode Sample Frequency Signal -> %u \n", prop_value);
                }
                std::tie(result, prop_value) = properties_handler->readHdmiOutputsamplingfrequency(); /* Integrated Decoder System Get Property */
                if (result == ADK_SI_OK) {
                    g_print("Hdmi Integrated Output Sampling Frequency -> %u \n", prop_value);
                }
                std::tie(result, prop_value) = properties_handler->readHdmiDDPCustomChMap();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi DDP Custom Channel Map Signal -> %u \n", prop_value);
                }
                int32_t value;
                std::tie(result, value) = properties_handler->readHdmiDDPSurroundex();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi DDP SurroundEX Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readHdmiDDPKaraoke();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi DDP karaoke Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readHdmiDDPCenterDownmixLevel();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi DDP Center Downmix level Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readHdmiDDPSurroundDownmixLevel();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi DDP Surround Downmix level Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readHdmiDDPDialoguenormalization();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi DDP Dialogue Normalization Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readHdmiDDPBitrate();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi DDP BitRate Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readHdmiDDPLfe();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi DDP Lfe Status Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readHdmiDecChConfig(); /* Integrated Decoder System Get Property */
                if (result == ADK_SI_OK) {
                    g_print("Hdmi Integrated Decode Channel Configuration Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readHdmiDDPDualMono();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi DDP Dual Mono Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readHdmiDDPAudioCodingMode();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi DDP Audio Coding Config Signal -> %d \n", value);
                }
                std::string stream_type;
                std::tie(result, stream_type) = properties_handler->readHdmiDDPStreamType();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi DDP Stream Type Signal -> %s \n", stream_type.c_str());
                }

                g_print("HDMI DDP Dual Mono \n");
                int32_t set_DualMono = 2;
                properties_handler->updateHdmiDDPDualMono(set_DualMono);
                g_print("HDMI DDP Dlb Surround Mode On \n");
                bool on = true;
                bool off = false;
                properties_handler->updateHdmiDAPDlbsurroundmode(on);
                g_print("HDMI DDP Dlb Surround Mode off \n");
                std::this_thread::sleep_for(std::chrono::milliseconds(1000));
                properties_handler->updateHdmiDAPDlbsurroundmode(off);
                g_print("HDMI DDP Dlb Surround CenterSpread On \n");
                bool enable = true;
                bool disable = false;
                properties_handler->updateHdmiDlbsurroundcenterspread(enable);
                g_print("HDMI DDP Dlb Surround CenterSpread Off \n");
                std::this_thread::sleep_for(std::chrono::milliseconds(1000));
                properties_handler->updateHdmiDlbsurroundcenterspread(disable);
				
		        g_print("HDMI DDP DRCMode \n");
		        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
                std::vector<int32_t> valued{1,2,3};
                result = properties_handler->updateHdmiDDPDrcmode(valued);

                if (result != ADK_SI_OK) {
                    g_print("HDMI DDP Decoder Restart \n");
                    std::string format = "DDP";
                    methods_handler->hdmiDecodeRestart(format);
                }

            } else if ((detected_format == "DD") || (detected_format == "DD CH") || (detected_format == "DD ATMOS")){
                ErrorCode result = ADK_SI_OK;
                uint32_t prop_value;
                std::tie(result, prop_value) = properties_handler->readHdmiDecSamplFreq(); /* Integrated Decoder System Get Property */
                if (result == ADK_SI_OK) {
                    g_print("Hdmi Integrated Decode Sample Frequency Signal -> %u \n", prop_value);
                }
                std::tie(result, prop_value) = properties_handler->readHdmiOutputsamplingfrequency(); /* Integrated Decoder System Get Property */
                if (result == ADK_SI_OK) {
                    g_print("Hdmi Integrated Output Sampling Frequency -> %u \n", prop_value);
                }
                std::tie(result, prop_value) = properties_handler->readHdmiDDPCustomChMap();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi DD Custom Channel Map Signal -> %u \n", prop_value);
                }
                int32_t value;
                std::tie(result, value) = properties_handler->readHdmiDDPSurroundex();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi DD SurroundEX Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readHdmiDDPKaraoke();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi DD karaoke Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readHdmiDDPCenterDownmixLevel();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi DD Center Downmix level Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readHdmiDDPSurroundDownmixLevel();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi DD Surround Downmix level Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readHdmiDDPDialoguenormalization();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi DD Dialogue Normalization Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readHdmiDDPBitrate();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi DD BitRate Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readHdmiDDPLfe();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi DD Lfe Status Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readHdmiDecChConfig(); /* Integrated Decoder System Get Property */
                if (result == ADK_SI_OK) {
                    g_print("Hdmi Integrated Decode Channel Configuration Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readHdmiDDPDualMono();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi DD Dual Mono Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readHdmiDDPAudioCodingMode();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi DD Audio Coding Config Signal -> %d \n", value);
                }
                std::string stream_type;
                std::tie(result, stream_type) = properties_handler->readHdmiDDPStreamType();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi DD Stream Type Signal -> %s \n", stream_type.c_str());
                }
                /* HDMI DD Set parameters */

                g_print("HDMI DD DualMono \n");
                int32_t set_DualMono = 2;
                properties_handler->updateHdmiDDPDualMono(set_DualMono);
                g_print("HDMI DD SET Dlb Surround Mode On \n");
                bool on = true;
                bool off = false;
                properties_handler->updateHdmiDAPDlbsurroundmode(on);
                g_print("HDMI DD SET Dlb Surround Mode Off \n");
                std::this_thread::sleep_for(std::chrono::milliseconds(1000));
                properties_handler->updateHdmiDAPDlbsurroundmode(off);
                g_print("HDMI DD SET Dlb Surround Center Spread Enable \n");
                bool enable = true;
                bool disable = false;
                properties_handler->updateHdmiDlbsurroundcenterspread(enable);
                g_print("HDMI DD SET Dlb Surround Center Spread Disable \n");
                std::this_thread::sleep_for(std::chrono::milliseconds(1000));
                properties_handler->updateHdmiDlbsurroundcenterspread(disable);
                
                g_print("HDMI DD DRCMode \n");
                std::vector<int32_t> valued{1,2,3};
                std::this_thread::sleep_for(std::chrono::milliseconds(1000));
                result = properties_handler->updateHdmiDDPDrcmode(valued);

                if (result != ADK_SI_OK) {
                    g_print("HDMI DD Decoder Restart \n");
                    std::string format = "DD";
                    methods_handler->hdmiDecodeRestart(format);
                }


            } else if (detected_format == "PCM") {
                ErrorCode result = ADK_SI_OK;
                uint32_t prop_value;
                std::tie(result, prop_value) = properties_handler->readHdmiPCMsamplingRate();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi PCM Sampling Rate-> %u \n", prop_value);
                }
                std::string props_value;
                std::tie(result, props_value) = properties_handler->readHdmiPCMChConfig();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi PCM ChConfig Signal -> %s \n", props_value.c_str());
                }
            } else if (detected_format == "AAC") {
                ErrorCode result = ADK_SI_OK;
                uint32_t prop_value;
                std::tie(result, prop_value) = properties_handler->readHdmiDecSamplFreq(); /* Integrated Decoder System Get Property */
                if (result == ADK_SI_OK) {
                    g_print("Hdmi Integrated Decode Sample Frequency Signal -> %u \n", prop_value);
                }
                std::tie(result, prop_value) = properties_handler->readHdmiOutputsamplingfrequency(); /* Integrated Decoder System Get Property */
                if (result == ADK_SI_OK) {
                    g_print("Hdmi Integrated Output Sampling Frequency -> %u \n", prop_value);
                }
                int32_t value;
                std::tie(result, value) = properties_handler->readHdmiDecChConfig(); /* Integrated Decoder System Get Property */
                if (result == ADK_SI_OK) {
                    g_print("Hdmi Integrated Decode Channel Configuration Signal -> %d \n", value);
                }

                g_print("Hdmi AAC DecChConfig \n");
                int32_t set_channel = 2;
                properties_handler->updateHdmiDecChConfig(set_channel);
                std::this_thread::sleep_for(std::chrono::milliseconds(1000));
                g_print("Hdmi AAC Dual Mono \n");
                int32_t set_DualMono = 2;
                properties_handler->updateHdmiDDPDualMono(set_DualMono);

            } else if (detected_format == "DSD") {
                ErrorCode result = ADK_SI_OK;
                uint32_t prop_value;
                std::tie(result, prop_value) = properties_handler->readHdmiDSDSamplingrate();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi DSD Decode Sampling Frequency Signal -> %u \n", prop_value);
                }
                std::string props_value;
                std::tie(result, props_value) = properties_handler->readHdmiDSDChConfig();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi DSD Channel Config Signal -> %s \n", props_value.c_str());
                }

                g_print("HDMI DSD DecChConfig \n");
                int32_t set_channel = 2;
                properties_handler->updateHdmiDecChConfig(set_channel);
                std::this_thread::sleep_for(std::chrono::milliseconds(1000));
                g_print("HDMI DSD Dual Mono \n");
                int32_t set_DualMono = 2;
                std::this_thread::sleep_for(std::chrono::milliseconds(1000));
                properties_handler->updateHdmiDDPDualMono(set_DualMono);

            } else if ((detected_format == "DTS") || (detected_format == "DTSX") || (detected_format == "DTS CD")) {
                ErrorCode result = ADK_SI_OK;
                uint32_t prop_value;
                std::tie(result, prop_value) = properties_handler->readHdmiDecSamplFreq(); /* Integrated Decoder System Get Property */
                if (result == ADK_SI_OK) {
                    g_print("Hdmi Integrated Decode Sample Frequency Signal -> %u \n", prop_value);
                }
                std::tie(result, prop_value) = properties_handler->readHdmiOutputsamplingfrequency(); /* Integrated Decoder System Get Property */
                if (result == ADK_SI_OK) {
                    g_print("Hdmi Integrated Output Sampling Frequency -> %u \n", prop_value);
                }
                std::tie(result, prop_value) = properties_handler->readHdmiDTSSamplerate();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi DTS Sample Rate Signal -> %u \n", prop_value);
                }
                std::tie(result, prop_value) = properties_handler->readHdmiDTSProgChMask();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi DTS Program Channel Mask Signal -> %u \n", prop_value);
                }
                std::tie(result, prop_value) = properties_handler->readHdmiDTSXllsamplerate();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi DTS Xllsample rateS ignal -> %u \n", prop_value);
                }
                int32_t value;
                std::tie(result, value) = properties_handler->readHdmiDecChConfig(); /* Integrated Decoder System Get Property */
                if (result == ADK_SI_OK) {
                    g_print("Hdmi Integrated Decode Channel Configuration Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readHdmiDTSBitrate();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi DTS BitRate Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readHdmiDTSEsMatrixFlag();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi DTS ES Matrix Flag Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readHdmiDTSXchFlag();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi DTS Xch Flag Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readHdmiDTSX96Flag();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi DTS X96 Flag Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readHdmiDTSDialogueNormalization();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi DTS Dialogue Normalization Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readHdmiDTSExtensionMask();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi DTS Extension Mask Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readHdmiDTSProgChMask();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi DTS Prog ChMask Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readHdmiDTSEmbeddeddownmixselect();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi DTS Embedded downmix select Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readHdmiDTSStreamType();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi DTS Stream Type Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readHdmiDTSDecodeInfo();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi DTS DecodeInfo Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readHdmiDTSCoreBitRate();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi DTS CoreBitRate Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readHdmiDTSObjectNum();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi DTS ObjectNum Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readHdmiDTSDialogueControlAvailability();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi DTS Dialogue Control Availability Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readHdmiDTSAnalogCompensationGain();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi DTS Analog Compensatio nGain Signal -> %d \n", value);
                }

                g_print("Hdmi DTS DecChConfig \n");
                int32_t set_channel = 2;
                properties_handler->updateHdmiDecChConfig(set_channel);
                std::this_thread::sleep_for(std::chrono::milliseconds(1000));
                g_print("Hdmi DTS Drcpercent \n");
                int32_t set_DyRange = 2;
                properties_handler->updateHdmiDtsxDrcpercent(set_DyRange);
                std::this_thread::sleep_for(std::chrono::milliseconds(1000));
                g_print("Hdmi DTS DualMono \n");
                int32_t set_DualMono = 2;
                properties_handler->updateHdmiDDPDualMono(set_DualMono);
                std::this_thread::sleep_for(std::chrono::milliseconds(1000));

                if (result != ADK_SI_OK) {
                    g_print("HDMI DTS Decoder Restart \n");
                    std::string format = "DTS";
                    methods_handler->hdmiDecodeRestart(format);
                }
            } else if ((detected_format == "MAT") || (detected_format == "THD CH") || (detected_format == "THD ATMOS")) {
                ErrorCode result = ADK_SI_OK;
                uint32_t prop_value;
                std::tie(result, prop_value) = properties_handler->readHdmiMATFbach();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi MAT Fbach Signal -> %u \n", prop_value);
                }
                std::tie(result, prop_value) = properties_handler->readHdmiMATTrueHdSync();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi MAT TrueHDSync Signal -> %u \n", prop_value);
                }
                std::tie(result, prop_value) = properties_handler->readHdmiDecSamplFreq(); /* Integrated Decoder System Get Property */
                if (result == ADK_SI_OK) {
                    g_print("Hdmi Decode Sample Frequency Signal -> %u \n", prop_value);
                }
                std::tie(result, prop_value) = properties_handler->readHdmiOutputsamplingfrequency(); /* Integrated Decoder System Get Property */
                if (result == ADK_SI_OK) {
                    g_print("Hdmi Output Sampling Frequency -> %u \n", prop_value);
                }
                int32_t value;
                std::tie(result, value) = properties_handler->readHdmiDecChConfig(); /* Integrated Decoder System Get Property */
                if (result == ADK_SI_OK) {
                    g_print("Hdmi Decode Channel Configuration Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readHdmiMATFbbch();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi MAT Fbbch Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readHdmiMATGainRequired();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi MAT Gain Required Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readHdmiMATFbaSurroundCh();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi MAT Fba SurroundCh Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readHdmiMATFbaDialogueNormalization();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi MAT Fba Dialogue Normalization Signal -> %d \n", value);
                }
                std::string props_value;
                std::tie(result, props_value) = properties_handler->readHdmiMATStreamtype();
                if (result == ADK_SI_OK) {
                    g_print("Hdmi MAT Stream type Signal -> %s \n", props_value.c_str());
                }

                g_print("Hdmi MAT Dec ChConfig \n");
                std::this_thread::sleep_for(std::chrono::milliseconds(1000));
                int32_t set_channel = 2;
                properties_handler->updateHdmiDecChConfig(set_channel);

                g_print("Hdmi MAT DualMono \n");
                int32_t set_DualMono = 2;
                properties_handler->updateHdmiDDPDualMono(set_DualMono);
                std::this_thread::sleep_for(std::chrono::milliseconds(1000));

                if (result != ADK_SI_OK) {
                    g_print("HDMI MAT Decoder Restart \n");
                    std::string format = "MAT";
                    methods_handler->hdmiDecodeRestart(format);
                }
            } else {
                g_print("*********PCM Zero Packet************\n");
                g_print("Hdmi Format Detected on %s obj_path \n", obj_path.c_str());
                g_print("Hdmi Detected Format : %s \n", detected_format.c_str());
            }
        });
    signals_handler->setHdmiDualMonoAACListener(
        [&signals_handler, &properties_handler](const std::string &obj_path) -> void {
            g_print("Hdmi format detected for AAC DualMono Listener %s obj_path \n", obj_path.c_str());

            ErrorCode result = ADK_SI_OK;
            uint32_t value;
            std::tie(result, value) = properties_handler->readHdmiAacDualMonoChannelMode(); /* AAC DualMono Get Property */

            if (result == ADK_SI_OK) {
                g_print("Get Hdmi AAC DualMono Channel Mode  -> %d \n", static_cast<int>(value));
            }
            std::int32_t set_channel = 0;
            properties_handler->updateHdmiAacDualMonoChannelMode(set_channel);
            g_print("Set HDMI AAC DualMono DualMono Value \n");  /* Update the Dual Mono AAC Value */
            std::this_thread::sleep_for(std::chrono::milliseconds(3000));
            std::tie(result, value) = properties_handler->readHdmiAacDualMonoChannelMode(); /* Get AAC DualMono Get Property */
            if (result == ADK_SI_OK) {
                g_print("After SettingUp Hdmi AAC DualMono Channel mode  -> %d \n", static_cast<int>(value));
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(3000));
            std::int32_t set_channel_One = 1;
            properties_handler->updateHdmiAacDualMonoChannelMode(set_channel_One);
            g_print("Set HDMI AAC DualMono DualMono Value \n");  /* Update the Dual Mono AAC Value */
            std::this_thread::sleep_for(std::chrono::milliseconds(3000));
            std::tie(result, value) = properties_handler->readHdmiAacDualMonoChannelMode(); /* Get AAC DualMono Get Property */
            if (result == ADK_SI_OK) {
                g_print("After SettingUp Hdmi AAC DualMono Channel mode  -> %d \n", static_cast<int>(value));
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(3000));
            std::int32_t set_channel_update = 2;
            properties_handler->updateHdmiAacDualMonoChannelMode(set_channel_update);
            g_print("Set HDMI AAC DualMono Value \n");
            std::this_thread::sleep_for(std::chrono::milliseconds(3000));
            std::tie(result, value) = properties_handler->readHdmiAacDualMonoChannelMode(); /* AAC DualMono Get Property */
            if (result == ADK_SI_OK) {
                g_print("After Changing Hdmi AAC DualMono Channel mode  -> %d \n", static_cast<int>(value));
            }
        });

    /* Spdif Handlers for Get and Set parameters */

    signals_handler->setSpdifDetectedFormatListener(
        [&properties_handler, &methods_handler](const std::string &detected_format, const std::string &obj_path) -> void {
            g_print("Spdif format detected on %s obj_path \n", obj_path.c_str());
            g_print("Spdif detected format : %s \n", detected_format.c_str());

            if ((detected_format == "DDP") || (detected_format == "DDP ATMOS") || (detected_format == "DDP CH")){
                ErrorCode result = ADK_SI_OK;
                uint32_t prop_value;
                std::tie(result, prop_value) = properties_handler->readSpdifDecSamplFreq(); /* Integrated Decoder System Get Property */
                if (result == ADK_SI_OK) {
                    g_print("Spdif Integrated Decode Sample Frequency Signal -> %u \n", prop_value);
                }
                std::tie(result, prop_value) = properties_handler->readSpdifOutputsamplingfrequency(); /* Integrated Decoder System Get Property */
                if (result == ADK_SI_OK) {
                    g_print("Spdif Integrated Output Sampling Frequency -> %u \n", prop_value);
                }
                std::tie(result, prop_value) = properties_handler->readSpdifDDPCustomChMap();
                if (result == ADK_SI_OK) {
                    g_print("Spdif DDP Custom Channel Map Signal -> %u \n", prop_value);
                }
                int32_t value;
                std::tie(result, value) = properties_handler->readSpdifDDPSurroundex();
                if (result == ADK_SI_OK) {
                    g_print("Spdif DDP SurroundEX Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readSpdifDDPKaraoke();
                if (result == ADK_SI_OK) {
                    g_print("Spdif DDP karaoke Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readSpdifDDPCenterDownmixLevel();
                if (result == ADK_SI_OK) {
                    g_print("Spdif DDP Center Downmix level Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readSpdifDDPSurroundDownmixLevel();
                if (result == ADK_SI_OK) {
                    g_print("Spdif DDP Surround Downmix level Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readSpdifDDPDialoguenormalization();
                if (result == ADK_SI_OK) {
                    g_print("Spdif DDP Dialogue Normalization Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readSpdifDDPBitrate();
                if (result == ADK_SI_OK) {
                    g_print("Spdif DDP BitRate Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readSpdifDDPLfe();
                if (result == ADK_SI_OK) {
                    g_print("Spdif DDP Lfe Status Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readSpdifDecChConfig(); /* Integrated Decoder System Get Property */
                if (result == ADK_SI_OK) {
                    g_print("Spdif Integrated Decode Channel Configuration Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readSpdifDDPDualMono();
                if (result == ADK_SI_OK) {
                    g_print("Spdif DDP Dual Mono Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readSpdifDDPAudioCodingMode();
                if (result == ADK_SI_OK) {
                    g_print("Spdif DDP Audio Coding Config Signal -> %d \n", value);
                }
                std::string stream_type;
                std::tie(result, stream_type) = properties_handler->readSpdifDDPStreamType();
                if (result == ADK_SI_OK) {
                    g_print("Spdif DDP Stream Type Signal -> %s \n", stream_type.c_str());
                }



                g_print("Spdif DDP Dual Mono \n");
                int32_t set_DualMono = 2;
                properties_handler->updateSpdifDDPDualMono(set_DualMono);

                g_print("Spdif DDP Dlb Surround Mode On \n");
                bool on = true;
                bool off = false;
                properties_handler->updateSpdifDAPDlbsurroundmode(on);
                g_print("Spdif DDP Dlb Surround Mode off \n");
                std::this_thread::sleep_for(std::chrono::milliseconds(1000));
                properties_handler->updateSpdifDAPDlbsurroundmode(off);

                g_print("Spdif DDP Dlb Surround CenterSpread On \n");
                bool enable = true;
                bool disable = false;
                properties_handler->updateSpdifDlbsurroundcenterspread(enable);
                g_print("Spdif DDP Dlb Surround CenterSpread Off \n");
                std::this_thread::sleep_for(std::chrono::milliseconds(1000));
                properties_handler->updateSpdifDlbsurroundcenterspread(disable);
                g_print("Spdif DDP DRCMode \n");
                std::this_thread::sleep_for(std::chrono::milliseconds(1000));
                std::vector<int32_t> valued{1,2,3};
                result = properties_handler->updateSpdifDDPDrcmode(valued);

                if (result != ADK_SI_OK) {
                    g_print("Spdif DDP Decoder Restart \n");
                    std::string format = "DDP";
                    methods_handler->spdifDecodeRestart(format);
                }

            } else if ((detected_format == "DD") || (detected_format == "DD CH") || (detected_format == "DD ATMOS")){
                ErrorCode result = ADK_SI_OK;
                uint32_t prop_value;
                std::tie(result, prop_value) = properties_handler->readSpdifDecSamplFreq(); /* Integrated Decoder System Get Property */
                if (result == ADK_SI_OK) {
                    g_print("Spdif Integrated Decode Sample Frequency Signal -> %u \n", prop_value);
                }
                std::tie(result, prop_value) = properties_handler->readSpdifOutputsamplingfrequency(); /* Integrated Decoder System Get Property */
                if (result == ADK_SI_OK) {
                    g_print("Spdif Integrated Output Sampling Frequency -> %u \n", prop_value);
                }
                std::tie(result, prop_value) = properties_handler->readSpdifDDPCustomChMap();
                if (result == ADK_SI_OK) {
                    g_print("Spdif DD Custom Channel Map Signal -> %u \n", prop_value);
                }
                int32_t value;
                std::tie(result, value) = properties_handler->readSpdifDDPSurroundex();
                if (result == ADK_SI_OK) {
                    g_print("Spdif DD SurroundEX Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readSpdifDDPKaraoke();
                if (result == ADK_SI_OK) {
                    g_print("Spdif DD karaoke Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readSpdifDDPCenterDownmixLevel();
                if (result == ADK_SI_OK) {
                    g_print("Spdif DD Center Downmix level Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readSpdifDDPSurroundDownmixLevel();
                if (result == ADK_SI_OK) {
                    g_print("Spdif DD Surround Downmix level Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readSpdifDDPDialoguenormalization();
                if (result == ADK_SI_OK) {
                    g_print("Spdif DD Dialogue Normalization Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readSpdifDDPBitrate();
                if (result == ADK_SI_OK) {
                    g_print("Spdif DD BitRate Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readSpdifDDPLfe();
                if (result == ADK_SI_OK) {
                    g_print("Spdif DD Lfe Status Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readSpdifDecChConfig(); /* Integrated Decoder System Get Property */
                if (result == ADK_SI_OK) {
                    g_print("Spdif Integrated Decode Channel Configuration Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readSpdifDDPDualMono();
                if (result == ADK_SI_OK) {
                    g_print("Spdif DD Dual Mono Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readSpdifDDPAudioCodingMode();
                if (result == ADK_SI_OK) {
                    g_print("Spdif DD Audio Coding Config Signal -> %d \n", value);
                }
                std::string stream_type;
                std::tie(result, stream_type) = properties_handler->readSpdifDDPStreamType();
                if (result == ADK_SI_OK) {
                    g_print("Spdif DD Stream Type Signal -> %s \n", stream_type.c_str());
                }


                g_print("Spdif DD Dual Mono \n");
                int32_t set_DualMono = 2;
                properties_handler->updateSpdifDDPDualMono(set_DualMono);

                g_print("Spdif DD Dlb Surround Mode On \n");
                bool on = true;
                bool off = false;
                properties_handler->updateSpdifDAPDlbsurroundmode(on);
                g_print("Spdif DD Dlb Surround Mode off \n");
                std::this_thread::sleep_for(std::chrono::milliseconds(1000));
                properties_handler->updateSpdifDAPDlbsurroundmode(off);

                g_print("Spdif DD Dlb Surround CenterSpread On \n");
                bool enable = true;
                bool disable = false;
                properties_handler->updateSpdifDlbsurroundcenterspread(enable);
                g_print("Spdif DD Dlb Surround CenterSpread Off \n");
                std::this_thread::sleep_for(std::chrono::milliseconds(1000));
                properties_handler->updateSpdifDlbsurroundcenterspread(disable);
                g_print("Spdif DD DRCMode \n");
                std::this_thread::sleep_for(std::chrono::milliseconds(1000));
                std::vector<int32_t> valued{1,2,3};
                result = properties_handler->updateSpdifDDPDrcmode(valued);

                if (result != ADK_SI_OK) {
                    g_print("Spdif DDP Decoder Restart \n");
                    std::string format = "DDP";
                    methods_handler->spdifDecodeRestart(format);
                }
                

            } else if (detected_format == "PCM") {
                ErrorCode result = ADK_SI_OK;
                uint32_t prop_value;
                std::tie(result, prop_value) = properties_handler->readSpdifPCMsamplingRate();
                if (result == ADK_SI_OK) {
                    g_print("Spdif PCM Sampling Rate-> %u \n", prop_value);
                }
                std::string props_value;
                std::tie(result, props_value) = properties_handler->readSpdifPCMChConfig();
                if (result == ADK_SI_OK) {
                    g_print("Spdif PCM ChConfig Signal -> %s \n", props_value.c_str());
                }
            } else if (detected_format == "AAC") {
                ErrorCode result = ADK_SI_OK;
                uint32_t prop_value;
                std::tie(result, prop_value) = properties_handler->readSpdifDecSamplFreq(); /* Integrated Decoder System Get Property */
                if (result == ADK_SI_OK) {
                    g_print("Spdif Integrated Decode Sample Frequency Signal -> %u \n", prop_value);
                }
                std::tie(result, prop_value) = properties_handler->readSpdifOutputsamplingfrequency(); /* Integrated Decoder System Get Property */
                if (result == ADK_SI_OK) {
                    g_print("Spdif Integrated Output Sampling Frequency -> %u \n", prop_value);
                }
                int32_t value;
                std::tie(result, value) = properties_handler->readSpdifDecChConfig(); /* Integrated Decoder System Get Property */
                if (result == ADK_SI_OK) {
                    g_print("Spdif Integrated Decode Channel Configuration Signal -> %d \n", value);
                }

                g_print("Spdif AAC DecChConfig \n");
                int32_t set_channel = 2;
                properties_handler->updateSpdifDecChConfig(set_channel);
                std::this_thread::sleep_for(std::chrono::milliseconds(1000));

                g_print("Spdif AAC Dual Mono \n");
                int32_t set_DualMono = 2;
                properties_handler->updateSpdifDDPDualMono(set_DualMono);

            } else if (detected_format == "DSD") {
                ErrorCode result = ADK_SI_OK;
                uint32_t prop_value;
                std::tie(result, prop_value) = properties_handler->readSpdifDSDSamplingrate();
                if (result == ADK_SI_OK) {
                    g_print("Spdif DSD Decode Sampling Frequency Signal -> %u \n", prop_value);
                }
                std::string props_value;
                std::tie(result, props_value) = properties_handler->readSpdifDSDChConfig();
                if (result == ADK_SI_OK) {
                    g_print("Spdif DSD Channel Config Signal -> %s \n", props_value.c_str());
                }

                g_print("Spdif DSD DecChConfig \n");
                int32_t set_channel = 2;
                properties_handler->updateSpdifDecChConfig(set_channel);
                std::this_thread::sleep_for(std::chrono::milliseconds(1000));

                g_print("Spdif DSD Dual Mono \n");
                int32_t set_DualMono = 2;
                std::this_thread::sleep_for(std::chrono::milliseconds(1000));
                properties_handler->updateSpdifDDPDualMono(set_DualMono);

            } else if ((detected_format == "DTS") || (detected_format == "DTSX") || (detected_format == "DTS CD")) {
                ErrorCode result = ADK_SI_OK;
                uint32_t prop_value;
                std::tie(result, prop_value) = properties_handler->readSpdifDecSamplFreq(); /* Integrated Decoder System Get Property */
                if (result == ADK_SI_OK) {
                    g_print("Spdif Integrated Decode Sample Frequency Signal -> %u \n", prop_value);
                }
                std::tie(result, prop_value) = properties_handler->readSpdifOutputsamplingfrequency(); /* Integrated Decoder System Get Property */
                if (result == ADK_SI_OK) {
                    g_print("Spdif Integrated Output Sampling Frequency -> %u \n", prop_value);
                }
                std::tie(result, prop_value) = properties_handler->readSpdifDTSSamplerate();
                if (result == ADK_SI_OK) {
                    g_print("Spdif DTS Sample Rate Signal -> %u \n", prop_value);
                }
                std::tie(result, prop_value) = properties_handler->readSpdifDTSProgChMask();
                if (result == ADK_SI_OK) {
                    g_print("Spdif DTS Program Channel Mask Signal -> %u \n", prop_value);
                }
                std::tie(result, prop_value) = properties_handler->readSpdifDTSXllsamplerate();
                if (result == ADK_SI_OK) {
                    g_print("Spdif DTS Xllsample rateS ignal -> %u \n", prop_value);
                }
                int32_t value;
                std::tie(result, value) = properties_handler->readSpdifDecChConfig(); /* Integrated Decoder System Get Property */
                if (result == ADK_SI_OK) {
                    g_print("Spdif Integrated Decode Channel Configuration Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readSpdifDTSBitrate();
                if (result == ADK_SI_OK) {
                    g_print("Spdif DTS BitRate Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readSpdifDTSEsMatrixFlag();
                if (result == ADK_SI_OK) {
                    g_print("Spdif DTS ES Matrix Flag Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readSpdifDTSXchFlag();
                if (result == ADK_SI_OK) {
                    g_print("Spdif DTS Xch Flag Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readSpdifDTSX96Flag();
                if (result == ADK_SI_OK) {
                    g_print("Spdif DTS X96 Flag Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readSpdifDTSDialogueNormalization();
                if (result == ADK_SI_OK) {
                    g_print("Spdif DTS Dialogue Normalization Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readSpdifDTSExtensionMask();
                if (result == ADK_SI_OK) {
                    g_print("Spdif DTS Extension Mask Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readSpdifDTSProgChMask();
                if (result == ADK_SI_OK) {
                    g_print("Spdif DTS Prog ChMask Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readSpdifDTSEmbeddeddownmixselect();
                if (result == ADK_SI_OK) {
                    g_print("Spdif DTS Embedded downmix select Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readSpdifDTSStreamType();
                if (result == ADK_SI_OK) {
                    g_print("Spdif DTS Stream Type Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readSpdifDTSDecodeInfo();
                if (result == ADK_SI_OK) {
                    g_print("Spdif DTS DecodeInfo Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readSpdifDTSCoreBitRate();
                if (result == ADK_SI_OK) {
                    g_print("Spdif DTS CoreBitRate Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readSpdifDTSObjectNum();
                if (result == ADK_SI_OK) {
                    g_print("Spdif DTS ObjectNum Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readSpdifDTSDialogueControlAvailability();
                if (result == ADK_SI_OK) {
                    g_print("Spdif DTS Dialogue Control Availability Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readSpdifDTSAnalogCompensationGain();
                if (result == ADK_SI_OK) {
                    g_print("Spdif DTS Analog Compensation Gain Signal -> %d \n", value);
                }


                g_print("Spdif DTS DecChConfig \n");
                int32_t set_channel = 2;
                properties_handler->updateSpdifDecChConfig(set_channel);
                std::this_thread::sleep_for(std::chrono::milliseconds(1000));

                g_print("Spdif DTS Drcpercent \n");
                int32_t set_DyRange = 2;
                properties_handler->updateSpdifDtsxDrcpercent(set_DyRange);
                std::this_thread::sleep_for(std::chrono::milliseconds(1000));
                g_print("Spdif DTS Dual Mono \n");
                int32_t set_DualMono = 2;
                properties_handler->updateSpdifDDPDualMono(set_DualMono);
                std::this_thread::sleep_for(std::chrono::milliseconds(1000));

                if (result != ADK_SI_OK) {
                    g_print("Spdif DTS Decoder Restart \n");
                    std::string format = "DTS";
                    methods_handler->spdifDecodeRestart(format);
                }
            } else if ((detected_format == "MAT") || (detected_format == "THD CH") || (detected_format == "THD ATMOS")) {
                ErrorCode result = ADK_SI_OK;
                uint32_t prop_value;
                std::tie(result, prop_value) = properties_handler->readSpdifMATFbach();
                if (result == ADK_SI_OK) {
                    g_print("Spdif MAT Fbach Signal -> %u \n", prop_value);
                }
                std::tie(result, prop_value) = properties_handler->readSpdifMATTrueHdSync();
                if (result == ADK_SI_OK) {
                    g_print("Spdif MAT TrueHDSync Signal -> %u \n", prop_value);
                }
                std::tie(result, prop_value) = properties_handler->readSpdifDecSamplFreq(); /* Integrated Decoder System Get Property */
                if (result == ADK_SI_OK) {
                    g_print("Spdif Decode Sample Frequency Signal -> %u \n", prop_value);
                }
                std::tie(result, prop_value) = properties_handler->readSpdifOutputsamplingfrequency(); /* Integrated Decoder System Get Property */
                if (result == ADK_SI_OK) {
                    g_print("Spdif Output Sampling Frequency -> %u \n", prop_value);
                }
                int32_t value;
                std::tie(result, value) = properties_handler->readSpdifDecChConfig(); /* Integrated Decoder System Get Property */
                if (result == ADK_SI_OK) {
                    g_print("Spdif Decode Channel Configuration Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readSpdifMATFbbch();
                if (result == ADK_SI_OK) {
                    g_print("Spdif MAT Fbbch Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readSpdifMATGainRequired();
                if (result == ADK_SI_OK) {
                    g_print("Spdif MAT Gain Required Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readSpdifMATFbaSurroundCh();
                if (result == ADK_SI_OK) {
                    g_print("Spdif MAT Fba SurroundCh Signal -> %d \n", value);
                }
                std::tie(result, value) = properties_handler->readSpdifMATFbaDialogueNormalization();
                if (result == ADK_SI_OK) {
                    g_print("Spdif MAT Fba Dialogue Normalization Signal -> %d \n", value);
                }
                std::string props_value;
                std::tie(result, props_value) = properties_handler->readSpdifMATStreamtype();
                if (result == ADK_SI_OK) {
                    g_print("Spdif MAT Stream type Signal -> %s \n", props_value.c_str());
                }

                g_print("Spdif MAT Dec ChConfig \n");
                std::this_thread::sleep_for(std::chrono::milliseconds(1000));
                int32_t set_channel = 2;
                properties_handler->updateSpdifDecChConfig(set_channel);

                g_print("Spdif MAT DualMono \n");
                int32_t set_DualMono = 2;
                properties_handler->updateSpdifDDPDualMono(set_DualMono);
                std::this_thread::sleep_for(std::chrono::milliseconds(1000));

                if (result != ADK_SI_OK) {
                    g_print("Spdif MAT Decoder Restart \n");
                    std::string format = "MAT";
                    methods_handler->spdifDecodeRestart(format);
                }
            } else {
                g_print("*********PCM Zero Packet************\n");
                g_print("Spdif format detected on %s obj_path \n", obj_path.c_str());
                g_print("Spdif Detected Format : %s \n", detected_format.c_str());
            }
        });

    ErrorCode result = ADK_SI_OK;
    uint32_t prop_value;
    std::tie(result, prop_value) = properties_handler->readHdmiDDPCustomChMap();
    if (result == ADK_SI_OK) {
        g_print("HDMI DDP Custom ChMap %u \n", prop_value);
    }
    int32_t value;
    std::tie(result, value) = properties_handler->readHdmiDDPLfe();
    if (result == ADK_SI_OK) {
        g_print("HDMI DDP Lfe %d \n", value);
    }
    std::tie(result, value) = properties_handler->readHdmiDDPDualMono();
    if (result == ADK_SI_OK) {
        g_print("HDMI DualMono %d \n", value);
    }
    g_print("HDMI DD Dec ChConfig \n");
    int32_t set_channel = 3;
    properties_handler->updateHdmiDDPDecChConfig(set_channel);

    /* External Jack Detection*/

    CreateSourceIn in_arg;
    in_arg.setEncoding("dsd");
    in_arg.setRate(44100);
    in_arg.setFormat("s24le");
    in_arg.setChannelMap("front-left,front-right");
    std::unordered_map<std::string, uint32_t> extra_configs = {{"dsd-rate", 64}};
    in_arg.setExtraConfigs(extra_configs);
    result = methods_handler->createSource(in_arg, "/org/pulseaudio/ext/qahw/port/hdmi_in");
    g_print("CreateSource result %d \n", result);

    /* Run mainloop */

    siginfo_t sig_info;
    int sig;
    do {
        int tries = 0;
        do {
            sig = sigwaitinfo(&sig_set, &sig_info);
            if (sig < 0) {
                if (errno == EINTR) {
                    // spurious wakeup, ignore
                    continue;
                }
                g_print("Error on sigwaitinfo errno %d %s, retrying \n", errno,
                    strerror(errno));
                tries++;
                std::this_thread::sleep_for(std::chrono::seconds(1));
            } else {
                break;
            }
        } while (tries < 5);
        if (sig < 0) {
            g_print("Error waiting signal, exiting \n");
            return false;
        }
    } while ((sig != SIGTERM) && (sig != SIGINT) && (sig != SIGHUP));
    g_print("exit signal wait \n");

    return true;
}

int main() {
    return startAdkSystemInterface() ? 0 : 1;
}
