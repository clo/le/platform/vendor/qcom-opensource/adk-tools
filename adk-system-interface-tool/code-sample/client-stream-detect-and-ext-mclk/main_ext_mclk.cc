/*
 * Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of The Linux Foundation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <cassert>
#include <chrono>
#include <csignal>
#include <cstring>
#include <functional>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <memory>
#include <fstream>

#include "autogen/adk_system_interface.h"
#include "autogen/attribute_classes.h"
#include "autogen/attribute_strings.h"

#include <iostream>
#include <unistd.h>

using namespace adk;
using namespace std;

#include "gtest/gtest.h"
// The fixture for testing class AdkSystemInterface.
class AdkSystemInterfaceTest : public ::testing::Test {
 protected:
  AdkSystemInterfaceTest() {
    // You can do set-up work for each test here.
  }
  virtual ~AdkSystemInterfaceTest() {
    // You can do clean-up work that doesn't throw exceptions here.
  }

  AdkSystemInterface& adk_si = AdkSystemInterface::getInstance();
  std::unordered_map<std::string, std::unordered_set<std::string>>  properties_obj_id_map;
  std::shared_ptr<PropertiesHandler> properties_handler;
  std::unordered_map<std::string, std::unordered_set<std::string>>
            methods_obj_id_map;
  std::shared_ptr<MethodsHandler> methods_handler;
  std::shared_ptr<SignalsHandler> signals_handler;

  //Test case inputs
    CreateSourceIn in_arg;
    ErrorCode result;
    SetSourceExtClkIn SetSourceInput;
    CreateSourceIn createSourceinput;

  //TestEvents
  bool _tc_SetSourceEvent;
  std::string _tc_SetSourceEvent_string;
  bool _tc_ClockSourcePropChanged;
  std::string _tc_ClockSourcePropChanged_string;
  bool _tc_ClockTypePropChanged;
  std::string _tc_ClockTypePropChanged_string;

  void ResetTestEvents() {
        _tc_SetSourceEvent = false;
        _tc_SetSourceEvent_string.clear();
        _tc_ClockSourcePropChanged = false;
        _tc_ClockSourcePropChanged_string.clear();
        _tc_ClockTypePropChanged = false;
        _tc_ClockTypePropChanged_string.clear();
  }

  virtual void SetUp() {

    if (!adk_si.init("adk_system_interface_sample")) {
        assert(0);
    }

    //Initialise the test events

    ResetTestEvents();

    properties_handler = adk_si.getPropertiesHandler();
    properties_handler->setObjIdMapChangedListener(
        [this](const std::unordered_map<std::string, std::unordered_set<std::string>> &obj_id_map) -> void {
            properties_obj_id_map = obj_id_map;
            for (auto &prop_it : obj_id_map) {
                std::string prop_name = prop_it.first;
                for (auto &obj_id : prop_it.second) {
                    g_print(
                        "ObjIdMapChangedListenerCb: prop_name - %s, "
                        "obj_path - %s \n",
                        prop_name.c_str(), obj_id.c_str());
                }
            }
        });

        properties_handler->setPropertiesChangedListener(
        [this](PropertiesAttr prop_attr, const std::string &obj_path) -> void {

            g_print("main::propertiesChangedCb obj_path : %s\n", obj_path.c_str());

            /* Get and Set Properties for Hdmi */

            std::unordered_set<std::string> changed_properties = prop_attr.getChangedProperties();
            if (changed_properties.find(std::string(adk::kCurrentClock)) != changed_properties.end()) {
                std::string clock_type = prop_attr.getCurrentClock();
                g_print("main:: Current clock type is  %s \n",
                    clock_type.c_str());
                _tc_ClockTypePropChanged = true;
                _tc_ClockTypePropChanged_string=clock_type;
            }
            if (changed_properties.find(std::string(adk::kCurrentSource)) != changed_properties.end()) {
                std::string clock_source = prop_attr.getCurrentSource();
                g_print("main:: Current clock type is  %s \n",
                    clock_source.c_str());
                _tc_ClockSourcePropChanged = true;
                _tc_ClockSourcePropChanged_string = clock_source;
            }
        });

       methods_handler = adk_si.getMethodsHandler();

        methods_handler->setObjIdMapChangedListener(
            [this](const std::unordered_map<std::string, std::unordered_set<std::string>> &obj_id_map) -> void {
                methods_obj_id_map = obj_id_map;
                for (auto &prop_it : obj_id_map) {
                    std::string prop_name = prop_it.first;
                    for (auto &obj_id : prop_it.second) {
                        g_print(
                            "ObjIdMapChangedListenerCb: prop_name - %s, "
                            "obj_path - %s \n",
                            prop_name.c_str(), obj_id.c_str());
                    }
                }
            });

        signals_handler = adk_si.getSignalsHandler();

        /* Spdif Handlers for Get and Set parameters */

        signals_handler->setSetSourceEventListener(
            [this](const std::string &Event, const std::string &obj_path) -> void {
                g_print("Set Source Event received on %s obj_path \n", obj_path.c_str());
                g_print("Event : %s \n", Event.c_str());
                _tc_SetSourceEvent = true;
                _tc_SetSourceEvent_string = Event;
            });
  }
 ErrorCode sendSetSource( std::string source_name_, std::string clock_type_, uint64_t clock_frequency_) {
      SetSourceInput.setSourceName(source_name_);
      SetSourceInput.setClockType(clock_type_);
      SetSourceInput.setClockFrequency(clock_frequency_);
      return methods_handler->setSourceExtClk(SetSourceInput);
 }

 ErrorCode sendDeleteSource() {
    return methods_handler->deleteSource("/org/pulseaudio/ext/qahw/port/hdmi_in");
 }

 ErrorCode sendCreateSource(std::string encoding_,uint32_t rate_,std::string format_,std::string channel_map_ ) {
    createSourceinput.setEncoding(encoding_);
    createSourceinput.setRate(rate_);
    createSourceinput.setFormat(format_);
    createSourceinput.setChannelMap(channel_map_);
    return methods_handler->createSource(createSourceinput,"/org/pulseaudio/ext/qahw/port/hdmi_in");
 }

void powerHDMI(bool status)
{
  if(true == status){
    g_print("HDMI STATUS Before Power Up\n");
    system("cat /sys/devices/platform/soc/78b7000.i2c/i2c-3/3-0064/power_on");
    system("echo 1 > /sys/devices/platform/soc/78b7000.i2c/i2c-3/3-0064/power_on");
    g_print("HDMI STATUS After Power Up\n");
    system("cat /sys/devices/platform/soc/78b7000.i2c/i2c-3/3-0064/power_on");
  }
  else{
    g_print("HDMI STATUS Before Power down\n");
    system("cat /sys/devices/platform/soc/78b7000.i2c/i2c-3/3-0064/power_on");
    system("echo 0 > /sys/devices/platform/soc/78b7000.i2c/i2c-3/3-0064/power_on");
    g_print("HDMI STATUS After Power down\n");
    system("cat /sys/devices/platform/soc/78b7000.i2c/i2c-3/3-0064/power_on");
  }
}

// Funtion to check the gtest assertions
void checkAssertion(std::string source_name, std::string clock_type, std::string event_name)
{
  //check for change in source
  ASSERT_EQ(true,_tc_ClockSourcePropChanged);
  ASSERT_STREQ(source_name.c_str(),_tc_ClockSourcePropChanged_string.c_str());

  //check for change in clock-type
  ASSERT_EQ(true,_tc_ClockTypePropChanged);
  ASSERT_STREQ(clock_type.c_str(),_tc_ClockTypePropChanged_string.c_str());

  //check for the signal(event) indicating the result of change request
  ASSERT_EQ(true,_tc_SetSourceEvent);
  ASSERT_STREQ(event_name.c_str(),_tc_SetSourceEvent_string.c_str());

}

  virtual void TearDown() {
  }
};

//Function to search substring in a file
bool searchStringInFile(std::string fileName, std::string searchString) {
    ifstream inFile;
    string line;

    inFile.open(fileName);

    if(!inFile){
        g_print("Unable to open file\n");
        return false;
    }

    size_t pos;
    while(inFile.good())
      {
          getline(inFile,line); // get line from file
          pos=line.find(searchString); // search
          if(pos!=string::npos) // string::npos is returned if string is not found
            {
                g_print("Found!");
                return true;
            }
      }
     return false;
}

// Test to check for external detection in QAHW config file
TEST_F(AdkSystemInterfaceTest, MethodTestQahwConfigForExtDetection) {
  g_print("***** MethodTestQahwConfigForExtDetection - START ***** \n");
  bool pattern1,pattern2,pattern3,pattern4;
  pattern1 = searchStringInFile("/usr/share/pulseaudio/qahw/configs/qcs405-csra8-snd-card.conf","detection = external");
  pattern2 = searchStringInFile("/usr/share/pulseaudio/qahw/configs/qcs405-csra8-snd-card.conf","detection=external");
  pattern3 = searchStringInFile("/usr/share/pulseaudio/qahw/configs/qcs405-csra8-snd-card.conf","detection =external");
  pattern4 = searchStringInFile("/usr/share/pulseaudio/qahw/configs/qcs405-csra8-snd-card.conf","detection= external");

  ASSERT_EQ(true,pattern1 || pattern2 || pattern3 || pattern4);
  g_print("***** MethodTestQahwConfigForExtDetection - END ***** \n");
}

// Test to check for setting wrong,unsupported source
TEST_F(AdkSystemInterfaceTest, MethodTestWrongSource) {
  g_print("***** MethodTestWrongSource - START *****\n");
  ASSERT_NE(nullptr,methods_handler);
  ASSERT_EQ(ADK_SI_OK, sendSetSource("hdmi-in23","external",24576000));
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  //We should not be receiving any of the signals
  ASSERT_EQ(false,_tc_ClockSourcePropChanged);
  ASSERT_EQ(true,_tc_ClockSourcePropChanged_string.empty());
  ASSERT_EQ(false,_tc_ClockTypePropChanged);
  ASSERT_EQ(true,_tc_ClockTypePropChanged_string.empty());
  ASSERT_EQ(true,_tc_SetSourceEvent);
  ASSERT_STREQ("Not Supported",_tc_SetSourceEvent_string.c_str());
  g_print("***** MethodTestWrongSource - END *****\n");
}

// Test to check for setting same source repeatedly
TEST_F(AdkSystemInterfaceTest, MethodTestDuplicateSource) {
  g_print("***** MethodTestDuplicateSource - START ***** \n");
  ASSERT_NE(nullptr,methods_handler);
  ASSERT_EQ(ADK_SI_OK, sendSetSource("hdmi-in","external",24576000));
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));

  //Reset Variables before the next set
  ResetTestEvents();
  ASSERT_EQ(ADK_SI_OK, sendSetSource("hdmi-in","external",24576000));
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));

  //None of the properties are changed after the second set source
  ASSERT_EQ(false,_tc_ClockSourcePropChanged);
  ASSERT_EQ(true,_tc_ClockSourcePropChanged_string.empty());
  ASSERT_EQ(false,_tc_ClockTypePropChanged);
  ASSERT_EQ(true,_tc_ClockTypePropChanged_string.empty());
  ASSERT_EQ(false,_tc_SetSourceEvent);
  ASSERT_EQ(true,_tc_SetSourceEvent_string.empty());

  //Reset to Internal Source
  ASSERT_EQ(ADK_SI_OK, sendSetSource("none","internal",0));

  g_print("***** MethodTestDuplicateSource - END *****\n");
}

// Test to set external mclock with frequency 24576000 for playing at 48000Hz
TEST_F(AdkSystemInterfaceTest, MethodTestSetExtMclk24576000_for_48000) {
  g_print("***** MethodTestSetExtMclk24576000_for_48000 - START *****\n");
  g_print("***** Deleting the Old Source. Wait for 10 Sec .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendDeleteSource());
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  g_print("***** Resetting to Internal Source. Wait for 10 Sec .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendSetSource("none","internal",0));
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  g_print("***** Create Pulse Audio Source .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendCreateSource("pcm",48000,"s24le","front-left,front-right"));
  g_print("***** Check if HDMI Source is Enumerated... Start Playback of 48KHz on VLC .. Waiting for 20 Sec.. *****\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));
  g_print("***** Set Source to Ext Clock .. *****\n");
  ASSERT_NE(nullptr,methods_handler);
  ASSERT_EQ(ADK_SI_OK, sendSetSource("hdmi-in","external",24576000));
  g_print("***** Sent SetSource to HDMI With 24576000 Clock Specified .. *****\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  checkAssertion("hdmi-in","external","Successful");
  g_print("***** Check the Playback... Waiting for 20Sec .. *****\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  g_print("***** MethodTestSetExtMclk24576000_for_48000 - END *****\n");
}

// Test to set external mclock with frequency 24576000 for playing at 96000Hz
TEST_F(AdkSystemInterfaceTest, MethodTestSetExtMclk24576000_for_96000) {
  g_print("***** MethodTestSetExtMclk24576000_for_96000 - START *****\n");
  g_print("***** Deleting the Old Source. Wait for 10 Sec .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendDeleteSource());
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  g_print("***** Resetting to Internal Source. Wait for 10 Sec ..*****\n");
  ASSERT_EQ(ADK_SI_OK, sendSetSource("none","internal",0));
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  g_print("***** Create Pulse Audio Source .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendCreateSource("pcm",96000,"s24le","front-left,front-right"));
  g_print("***** Check if HDMI Source is Enumerated... Start Playback of 96KHz on VLC .. Waiting for 20 Sec.. *****\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));
  g_print("***** Set Source to Ext Clock .. *****\n");
  ASSERT_NE(nullptr,methods_handler);
  ASSERT_EQ(ADK_SI_OK, sendSetSource("hdmi-in","external",24576000));
  g_print("***** Sent SetSource to HDMI With 24576000 Clock Specified .. *****\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  checkAssertion("hdmi-in","external","Successful");
  g_print("***** Check the Playback... Waiting for 20Sec .. *****\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  g_print("***** MethodTestSetExtMclk24576000_for_96000 - END *****\n");
}

// Test to set external mclock with frequency 12288000 for playing at 96000Hz
TEST_F(AdkSystemInterfaceTest, MethodTestSetExtMclk12288000_for_96000) {
  g_print("***** MethodTestSetExtMclk12288000_for_96000 - START *****\n");
  g_print("***** Deleting the Old Source. Wait for 10 Sec .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendDeleteSource());
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  g_print("***** Resetting to Internal Source. Wait for 10 Sec ..*****\n");
  ASSERT_EQ(ADK_SI_OK, sendSetSource("none","internal",0));
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  g_print("***** Create Pulse Audio Source .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendCreateSource("pcm",96000,"s24le","front-left,front-right"));
  g_print("***** Check if HDMI Source is Enumerated... Start Playback of 96KHz on VLC .. Waiting for 20 Sec.. *****\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));
  g_print("***** Set Source to Ext Clock .. *****\n");
  ASSERT_NE(nullptr,methods_handler);
  ASSERT_EQ(ADK_SI_OK, sendSetSource("hdmi-in","external",12288000));
  g_print("***** Sent SetSource to HDMI With 12288000 Clock Specified .. *****\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  checkAssertion("hdmi-in","external","Successful");
  g_print("***** Check the Playback... Waiting for 20Sec .. *****\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  g_print("***** MethodTestSetExtMclk12288000_for_96000 - END *****\n");
}

// Test to set external mclock with frequency 24576000 for playing at 192000Hz
TEST_F(AdkSystemInterfaceTest, MethodTestSetExtMclk24576000_for_192000) {
  g_print("***** MethodTestSetExtMclk24576000_for_192000 - START ****\n");
  g_print("***** Deleting the Old Source. Wait for 10 Sec .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendDeleteSource());
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  g_print("***** Resetting to Internal Source. Wait for 10 Sec ..*****\n");
  ASSERT_EQ(ADK_SI_OK, sendSetSource("none","internal",0));
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  g_print("***** Create Pulse Audio Source .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendCreateSource("pcm",192000,"s24le","front-left,front-right"));
  g_print("***** Check if HDMI Source is Enumerated... Start Playback of 192KHz on VLC .. Waiting for 20 Sec.. *****\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));
  g_print("***** Set Source to Ext Clock .. *****\n");
  ASSERT_NE(nullptr,methods_handler);
  ASSERT_EQ(ADK_SI_OK, sendSetSource("hdmi-in","external",24576000));
  g_print("***** Sent SetSource to HDMI With 24576000 Clock Specified .. *****\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  checkAssertion("hdmi-in","external","Successful");
  g_print("***** Check the Playback... Waiting for 20Sec .. *****\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  g_print("***** MethodTestSetExtMclk24576000_for_192000 - END *****\n");
}

// Test to set external mclock with frequency 22579200 for playing at 44100Hz
TEST_F(AdkSystemInterfaceTest, MethodTestSetExtMclk22579200_for_44100) {
  g_print("***** MethodTestSetExtMclk22579200_for_44100 - START *****\n");
  g_print("***** Deleting the Old Source. Wait for 10 Sec .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendDeleteSource());
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  g_print("***** Resetting to Internal Source. Wait for 10 Sec ..*****\n");
  ASSERT_EQ(ADK_SI_OK, sendSetSource("none","internal",0));
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  g_print("***** Create Pulse Audio Source .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendCreateSource("pcm",44100,"s24le","front-left,front-right"));
  g_print("***** Check if HDMI Source is Enumerated... Start Playback of 44.1KHz on VLC .. Waiting for 20 Sec.. *****\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));
  g_print("***** Set Source to Ext Clock .. *****\n");
  ASSERT_NE(nullptr,methods_handler);
  ASSERT_EQ(ADK_SI_OK, sendSetSource("hdmi-in","external",22579200));
  g_print("***** Sent SetSource to HDMI With 22579200 Clock Specified .. *****\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  checkAssertion("hdmi-in","external","Successful");
  g_print("***** Check the Playback... Waiting for 20Sec .. *****\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  g_print("***** MethodTestSetExtMclk22579200_for_44100 - END *****\n");
}

// Test to set external mclock with frequency 22579200 for playing at 176400Hz
TEST_F(AdkSystemInterfaceTest, MethodTestSetExtMclk22579200_for_176400) {
  g_print("***** MethodTestSetExtMclk22579200_for_176400 - START ******\n");
  g_print("***** Deleting the Old Source. Wait for 10 Sec .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendDeleteSource());
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  g_print("***** Resetting to Internal Source. Wait for 10 Sec ..*****\n");
  ASSERT_EQ(ADK_SI_OK, sendSetSource("none","internal",0));
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  g_print("***** Create Pulse Audio Source .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendCreateSource("pcm",176400,"s24le","front-left,front-right"));
  g_print("***** Check if HDMI Source is Enumerated... Start Playback of 176400KHz on VLC .. Waiting for 20 Sec.. *****\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));
  g_print("***** Set Source to Ext Clock .. *****\n");
  ASSERT_NE(nullptr,methods_handler);
  ASSERT_EQ(ADK_SI_OK, sendSetSource("hdmi-in","external",22579200));
  g_print("***** Sent SetSource to HDMI With 22579200 Clock Specified .. *****\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  checkAssertion("hdmi-in","external","Successful");
  g_print("***** Check the Playback... Waiting for 20Sec .. *****\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));
  g_print("***** MethodTestSetExtMclk22579200_for_176400 - END *****\n");
}

// Test to set external mclock with frequency 12288000 for playing at 48000Hz
TEST_F(AdkSystemInterfaceTest, MethodTestSetExtMclk12288000_for_48000) {
  g_print("***** MethodTestSetExtMclk12288000_for_48000 - START *****\n");
  g_print("***** Deleting the Old Source. Wait for 10 Sec .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendDeleteSource());
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  g_print("***** Resetting to Internal Source. Wait for 10 Sec ..*****\n");
  ASSERT_EQ(ADK_SI_OK, sendSetSource("none","internal",0));
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  g_print("***** Create Pulse Audio Source .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendCreateSource("pcm",48000,"s24le","front-left,front-right"));
  g_print("***** Check if HDMI Source is Enumerated... Start Playback of 48KHz on VLC .. Waiting for 20 Sec.. *****\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));
  g_print("***** Set Source to Ext Clock .. *****\n");
  ASSERT_NE(nullptr,methods_handler);
  ASSERT_EQ(ADK_SI_OK, sendSetSource("hdmi-in","external",12288000));
  g_print("***** Sent SetSource to HDMI With 12288000 Clock Specified .. *****\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  checkAssertion("hdmi-in","external","Successful");
  g_print("***** Check the Playback... Waiting for 20Sec .. *****\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));
  g_print("***** MethodTestSetExtMclk12288000_for_48000 - END *****\n");
}

// Test to set external mclock with frequency 12289600 for playing at 44100Hz
TEST_F(AdkSystemInterfaceTest, MethodTestSetExtMclk11289600_for_44100) {
  g_print("***** MethodTestSetExtMclk11289600_for_44100 - START *****\n");
  g_print("***** Deleting the Old Source. Wait for 10 Sec .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendDeleteSource());
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  g_print("***** Resetting to Internal Source. Wait for 10 Sec ..*****\n");
  ASSERT_EQ(ADK_SI_OK, sendSetSource("none","internal",0));
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  g_print("***** Create Pulse Audio Source .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendCreateSource("pcm",44100,"s24le","front-left,front-right"));
  g_print("***** Check if HDMI Source is Enumerated... Start Playback of 44.1KHz on VLC .. Waiting for 20 Sec.. *****\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));
  g_print("***** Set Source to Ext Clock .. *****\n");
  ASSERT_NE(nullptr,methods_handler);
  ASSERT_EQ(ADK_SI_OK, sendSetSource("hdmi-in","external",11289600));
  g_print("***** Sent SetSource to HDMI With 11289600 Clock Specified .. *****\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  checkAssertion("hdmi-in","external","Successful");
  g_print("***** Check the Playback... Waiting for 20Sec .. *****\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));
  g_print("***** MethodTestSetExtMclk11289600_for_44100 - END *****\n");
}

// Test to set external mclock with frequency 16384000 for playing at 32000Hz
TEST_F(AdkSystemInterfaceTest, MethodTestSetExtMclk16384000_for_32000) {
  g_print("***** MethodTestSetExtMclk16384000_for_32000 - START *****\n");
  g_print("***** Deleting the Old Source. Wait for 10 Sec .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendDeleteSource());
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  g_print("***** Resetting to Internal Source. Wait for 10 Sec ..*****\n");
  ASSERT_EQ(ADK_SI_OK, sendSetSource("none","internal",0));
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  g_print("***** Create Pulse Audio Source .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendCreateSource("pcm",32000,"s24le","front-left,front-right"));
  g_print("***** Check if HDMI Source is Enumerated... Start Playback of 32000KHz on VLC .. Waiting for 20 Sec.. *****\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));
  g_print("***** Set Source to Ext Clock .. *****\n");
  ASSERT_NE(nullptr,methods_handler);
  ASSERT_EQ(ADK_SI_OK, sendSetSource("hdmi-in","external",16384000));
  g_print("***** Sent SetSource to HDMI With 16384000 Clock Specified .. *****\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  checkAssertion("hdmi-in","external","Successful");
  g_print("***** Check the Playback... Waiting for 20Sec .. *****\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));
  g_print("***** MethodTestSetExtMclk16384000_for_32000 - END *****\n");
}

// Test to set external mclock with frequency 12288000 for playing at 32000Hz
TEST_F(AdkSystemInterfaceTest, MethodTestSetExtMclk12288000_for_32000) {
  g_print("***** MethodTestSetExtMclk12288000_for_32000 - START *****\n");
  g_print("***** Deleting the Old Source. Wait for 10 Sec .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendDeleteSource());
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  g_print("***** Resetting to Internal Source. Wait for 10 Sec ..*****\n");
  ASSERT_EQ(ADK_SI_OK, sendSetSource("none","internal",0));
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  g_print("***** Create Pulse Audio Source .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendCreateSource("pcm",32000,"s24le","front-left,front-right"));
  g_print("***** Check if HDMI Source is Enumerated... Start Playback of 32000KHz on VLC .. Waiting for 20 Sec.. *****\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));
  g_print("***** Set Source to Ext Clock .. *****\n");
  ASSERT_NE(nullptr,methods_handler);
  ASSERT_EQ(ADK_SI_OK, sendSetSource("hdmi-in","external",12288000));
  g_print("***** Sent SetSource to HDMI With 12288000 Clock Specified .. *****\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  checkAssertion("hdmi-in","external","Successful");
  g_print("***** Check the Playback... Waiting for 20Sec .. *****\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  g_print("***** MethodTestSetExtMclk12288000_for_32000 - END *****\n");
}

// Test to set internal clock
TEST_F(AdkSystemInterfaceTest, MethodTestSetInternal_Clock) {
  g_print("***** MethodTestSetInternal_Clock - START *****\n");

  //Delete the old source if any
  ASSERT_EQ(ADK_SI_OK, sendDeleteSource());

  //Reset to Internal Source
  ASSERT_EQ(ADK_SI_OK, sendSetSource("none","internal",0));

  //Creat HDMI input (Pulseaudio Source)
  ASSERT_EQ(ADK_SI_OK, sendCreateSource("pcm",48000,"s24le","front-left,front-right"));
  g_print("check if hdmi source is enumerated... waiting for 5 sec");
  std::this_thread::sleep_for(std::chrono::milliseconds(5000));

  //Set source to HDMI with internal clock
  ASSERT_NE(nullptr,methods_handler);
  ASSERT_EQ(ADK_SI_OK, sendSetSource("hdmi-in","internal",0));
  ASSERT_EQ(ADK_SI_OK, methods_handler->setSourceExtClk(SetSourceInput));
  g_print("Sent setsource to HDMI and with the clock specified");
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));

  //No check for clock-type change, as it remains to be internal
  ASSERT_EQ(true,_tc_ClockSourcePropChanged);
  ASSERT_STREQ("hdmi-in",_tc_ClockSourcePropChanged_string.c_str());

  ASSERT_EQ(true,_tc_SetSourceEvent);
  ASSERT_STREQ("Successful",_tc_SetSourceEvent_string.c_str());

  g_print("check if playback is happening fine.. waiting for 10 seconds");
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));

  g_print("***** MethodTestSetInternal_Clock - END *****\n");
}

// Test to set internal clock for playing at 44100Hz
TEST_F(AdkSystemInterfaceTest, MethodTestHDMI44100PlaybackUsingInternalClock) {
  g_print("***** MethodTestHDMI44100PlaybackUsingInternalClock - START");

  //Delete the old source if any
  ASSERT_EQ(ADK_SI_OK, sendDeleteSource());

  //Reset to Internal Source
  ASSERT_EQ(ADK_SI_OK, sendSetSource("none","internal",0));

  //Creat HDMI input (Pulseaudio Source)
  ASSERT_EQ(ADK_SI_OK, sendCreateSource("pcm",44100,"s24le","front-left,front-right"));
  g_print("check if hdmi source is enumerated... waiting for 5 sec");
  std::this_thread::sleep_for(std::chrono::milliseconds(5000));

  //Set source to HDMI with internal clock
  ASSERT_NE(nullptr,methods_handler);
  ASSERT_EQ(ADK_SI_OK, sendSetSource("hdmi-in","internal",0));
  ASSERT_EQ(ADK_SI_OK, methods_handler->setSourceExtClk(SetSourceInput));
  g_print("Sent setsource to HDMI and with the clock specified");
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));

  //No check for clock-type change, as it remains to be internal
  ASSERT_EQ(true,_tc_ClockSourcePropChanged);
  ASSERT_STREQ("hdmi-in",_tc_ClockSourcePropChanged_string.c_str());

  ASSERT_EQ(true,_tc_SetSourceEvent);
  ASSERT_STREQ("Successful",_tc_SetSourceEvent_string.c_str());

  g_print("check if playback is happening fine.. waiting for 10 seconds");
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));

  g_print("***** MethodTestHdmi44100PlaybackUsingInternalClock - END");
}

// Test to switch from external mclock 24576000 to internal paplay
TEST_F(AdkSystemInterfaceTest, MethodTestSetExtMclk24576000_switch_internal_paplay) {
  g_print("***** MethodTestSetExtMclk24576000_switch_internal_paplay - START *****\n");

  g_print("***** Deleting the Old Source. Wait for 10 Sec .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendDeleteSource());
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  g_print("***** Resetting to Internal Source. Wait for 10 Sec .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendSetSource("none","internal",0));
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  g_print("***** Create Pulse Audio Source .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendCreateSource("pcm",48000,"s24le","front-left,front-right"));
  g_print("check if hdmi source is enumerated and start playback of 48Khz on VLC .. waiting for 20 sec..\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  ASSERT_NE(nullptr,methods_handler);
  ASSERT_EQ(ADK_SI_OK, sendSetSource("hdmi-in","external",24576000));
  g_print("Sent Set Source to HDMI and with the Clock specified .. \n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  checkAssertion("hdmi-in","external","Successful");

  g_print("Check if Playback is happening fine.. Waiting for 20 Sec..\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  //Set Source to None Relinquish Local playback
  ASSERT_EQ(ADK_SI_OK, sendSetSource("none","internal",0));
  g_print("Sent setsource to HDMI and with the clock specified..\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));

  checkAssertion("none","internal","Successful");

  //Power down HDMI
  powerHDMI(false);

  std::this_thread::sleep_for(std::chrono::milliseconds(5000));
  g_print("Local playback from audio test source..\n");
  system("paplay /data/48000KHz.wav -d offload-low-latency");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));
  g_print("***** MethodTestSetExtMclk24576000_switch_internal_paplay - END *****\n");

    //Power down HDMI
  powerHDMI(true);
}

// Test to switch from internal paplay to external mclock 24576000
TEST_F(AdkSystemInterfaceTest, MethodTest_Internal_paplay_to_SetExtMclk24576000) {
  g_print("***** MethodTest_Internal_paplay_to_SetExtMclk24576000 - START *****\n");
  g_print("***** Deleting the Old Source. Wait for 10 Sec .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendDeleteSource());
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  g_print("***** Resetting to Internal Source. Wait for 10 Sec .. *****\n");
  //Power down HDMI
  //powerHDMI(false);
  //Set Source to None Relinquish Local playback
  ASSERT_EQ(ADK_SI_OK, sendSetSource("none","internal",0));
  g_print("Sent setsource to HDMI and with the clock specified");
  std::this_thread::sleep_for(std::chrono::milliseconds(5000));
  checkAssertion("none","internal","Successful");
  powerHDMI(false);
  std::this_thread::sleep_for(std::chrono::milliseconds(5000));
  g_print("Local playback from audio test source\n");
  system("paplay /data/48000KHz.wav -d offload-low-latency");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));
  //Power Up HDMI
  powerHDMI(true);
  g_print("Now start with HDMI streaming");
  //Creat HDMI input (Pulseaudio Source)
  ASSERT_EQ(ADK_SI_OK, sendCreateSource("pcm",48000,"s24le","front-left,front-right"));
  g_print("check if hdmi source is enumerated and start playback of 48Khz on VLC .. waiting for 5 sec \n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  //Set source to HDMI with external mclock
  ASSERT_NE(nullptr,methods_handler);
  ASSERT_EQ(ADK_SI_OK, sendSetSource("hdmi-in","external",24576000));
  g_print("Sent setsource to HDMI and with the clock specified \n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  checkAssertion("hdmi-in","external","Successful");

  g_print("check if playback is happening fine.. waiting for 20 seconds \n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  g_print("***** MethodTest_Internal_paplay_to_SetExtMclk24576000 - END ***** \n");
}

// Test to switch from external mclock 24576000 to network stream
TEST_F(AdkSystemInterfaceTest, MethodTestSetExtMclk24576000_to_NetworkStream) {
  g_print("***** MethodTestSetExtMclk24576000_to_NetworkStream(Allplay) - START *****\n");
  g_print("***** Deleting the Old Source. Wait for 10 Sec .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendDeleteSource());
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  g_print("***** Resetting to Internal Source. Wait for 10 Sec .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendSetSource("none","internal",0));
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  g_print("***** Create Pulse Audio Source .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendCreateSource("pcm",48000,"s24le","front-left,front-right"));
  g_print("check if hdmi source is enumerated and start playback of 48Khz on VLC .. waiting for 20 sec..\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  ASSERT_NE(nullptr,methods_handler);
  ASSERT_EQ(ADK_SI_OK, sendSetSource("hdmi-in","external",24576000));
  g_print("Sent Set Source to HDMI and with the Clock specified .. \n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  checkAssertion("hdmi-in","external","Successful");

  g_print("Check if Playback is happening fine.. Waiting for 20 Sec..\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  ResetTestEvents();

  g_print("Start allplay playback 'allplay play file:///data/yesterday_48KHz.wav' on a seperate terminal\n");
  g_print("Press any key and wait for 15 seconds");
  std::this_thread::sleep_for(std::chrono::milliseconds(15000));
  g_print("Check Audio-mananger logs to confirm player change\n");

  ASSERT_EQ(true,_tc_ClockSourcePropChanged);
  ASSERT_STREQ("none",_tc_ClockSourcePropChanged_string.c_str());

  ASSERT_EQ(true,_tc_ClockTypePropChanged);
  ASSERT_STREQ("internal",_tc_ClockTypePropChanged_string.c_str());

  //signal is sent only for stesource from MCU
  ASSERT_EQ(false,_tc_SetSourceEvent);

  g_print("And the old playback gets stopped and new playback starts from allplay\n");
  g_print("***** MethodTestSetExtMclk24576000_to_NetworkStream(Allplay) - END *****\n");
}

// Test to switch from external mclock 22579200 to network stream
TEST_F(AdkSystemInterfaceTest, MethodTestSetExtMclk22579200_to_NetworkStream) {
  g_print("***** MethodTestSetExtMclk22579200_to_NetworkStream(Allplay) - START");
  g_print("***** Deleting the Old Source. Wait for 10 Sec .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendDeleteSource());
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  g_print("***** Resetting to Internal Source. Wait for 10 Sec .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendSetSource("none","internal",0));
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  g_print("***** Create Pulse Audio Source .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendCreateSource("pcm",44100,"s24le","front-left,front-right"));
  g_print("check if hdmi source is enumerated and start playback of 44.1KHz on VLC .. waiting for 20 sec..\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  ASSERT_NE(nullptr,methods_handler);
  ASSERT_EQ(ADK_SI_OK, sendSetSource("hdmi-in","external",22579200));
  g_print("Sent Set Source to HDMI and with the Clock specified .. \n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  checkAssertion("hdmi-in","external","Successful");

  g_print("Check if Playback is happening fine.. Waiting for 20 Sec..\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  ResetTestEvents();

  g_print("Start allplay playback 'allplay play file:///data/44.1KHz.wav' on a seperate terminal\n");
  g_print("Press any key and wait for 15 seconds");
  std::this_thread::sleep_for(std::chrono::milliseconds(15000));
  g_print("Check Audio-mananger logs to confirm player change\n");

  ASSERT_EQ(true,_tc_ClockSourcePropChanged);
  ASSERT_STREQ("none",_tc_ClockSourcePropChanged_string.c_str());

  ASSERT_EQ(true,_tc_ClockTypePropChanged);
  ASSERT_STREQ("internal",_tc_ClockTypePropChanged_string.c_str());

  //signal is sent only for stesource from MCU
  ASSERT_EQ(false,_tc_SetSourceEvent);

  g_print("And the old playback gets stopped and new playback starts from allplay\n");
  g_print("***** MethodTestSetExtMclk22579200_to_NetworkStream(Allplay) - END *****\n");
}

// Test to switch from external mclock 12288000 to network stream
TEST_F(AdkSystemInterfaceTest, MethodTestSetExtMclk12288000_to_NetworkStream) {
  g_print("***** MethodTestSetExtMclk12288000_to_NetworkStream(Allplay) - START *****\n");
  g_print("***** Deleting the Old Source. Wait for 10 Sec .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendDeleteSource());
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  g_print("***** Resetting to Internal Source. Wait for 10 Sec .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendSetSource("none","internal",0));
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  g_print("***** Create Pulse Audio Source .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendCreateSource("pcm",48000,"s24le","front-left,front-right"));
  g_print("check if hdmi source is enumerated and start playback of 48Khz on VLC .. waiting for 20 sec..\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  ASSERT_NE(nullptr,methods_handler);
  ASSERT_EQ(ADK_SI_OK, sendSetSource("hdmi-in","external",12288000));
  g_print("Sent Set Source to HDMI and with the Clock specified .. \n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  checkAssertion("hdmi-in","external","Successful");

  g_print("Check if Playback is happening fine.. Waiting for 20 Sec..\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  ResetTestEvents();

  g_print("Start allplay playback 'allplay play file:///data/yesterday_48KHz.wav' on a seperate terminal\n");
  g_print("Press any key and wait for 15 seconds");
  std::this_thread::sleep_for(std::chrono::milliseconds(15000));
  g_print("Check Audio-mananger logs to confirm player change\n");

  ASSERT_EQ(true,_tc_ClockSourcePropChanged);
  ASSERT_STREQ("none",_tc_ClockSourcePropChanged_string.c_str());

  ASSERT_EQ(true,_tc_ClockTypePropChanged);
  ASSERT_STREQ("internal",_tc_ClockTypePropChanged_string.c_str());

  //signal is sent only for stesource from MCU
  ASSERT_EQ(false,_tc_SetSourceEvent);

  g_print("And the old playback gets stopped and new playback starts from allplay\n");
  g_print("***** MethodTestSetExtMclk12288000_to_NetworkStream(Allplay) - END *****\n");
}

// Test to switch from external mclock 12289600 to network stream
TEST_F(AdkSystemInterfaceTest, MethodTestSetExtMclk11289600_to_NetworkStream) {
  g_print("***** MethodTestSetExtMclk11289600_to_NetworkStream(Allplay) - START *****\n");
  g_print("***** Deleting the Old Source. Wait for 10 Sec .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendDeleteSource());
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  g_print("***** Resetting to Internal Source. Wait for 10 Sec .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendSetSource("none","internal",0));
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  g_print("***** Create Pulse Audio Source .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendCreateSource("pcm",44100,"s24le","front-left,front-right"));
  g_print("check if hdmi source is enumerated and start playback of 44.1KHz on VLC .. waiting for 20 sec..\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  ASSERT_NE(nullptr,methods_handler);
  ASSERT_EQ(ADK_SI_OK, sendSetSource("hdmi-in","external",12289600));
  g_print("Sent Set Source to HDMI and with the Clock specified .. \n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  checkAssertion("hdmi-in","external","Successful");

  g_print("Check if Playback is happening fine.. Waiting for 20 Sec..\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  ResetTestEvents();

  g_print("Start allplay playback 'allplay play file:///data/yesterday_48KHz.wav' on a seperate terminal\n");
  g_print("Press any key and wait for 15 seconds");
  std::this_thread::sleep_for(std::chrono::milliseconds(15000));
  g_print("Check Audio-mananger logs to confirm player change\n");

  ASSERT_EQ(true,_tc_ClockSourcePropChanged);
  ASSERT_STREQ("none",_tc_ClockSourcePropChanged_string.c_str());

  ASSERT_EQ(true,_tc_ClockTypePropChanged);
  ASSERT_STREQ("internal",_tc_ClockTypePropChanged_string.c_str());

  //signal is sent only for stesource from MCU
  ASSERT_EQ(false,_tc_SetSourceEvent);

  g_print("And the old playback gets stopped and new playback starts from allplay\n");
  g_print("***** MethodTestSetExtMclk11289600_to_NetworkStream(Allplay) - END *****\n");
}

// Test to switch from external mclock 16384000 to network stream
TEST_F(AdkSystemInterfaceTest, MethodTestSetExtMclk16384000_to_NetworkStream) {
  g_print("***** MethodTestSetExtMclk16384000_to_NetworkStream(Allplay) - START");
  g_print("***** Deleting the Old Source. Wait for 10 Sec .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendDeleteSource());
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  g_print("***** Resetting to Internal Source. Wait for 10 Sec .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendSetSource("none","internal",0));
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  g_print("***** Create Pulse Audio Source .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendCreateSource("pcm",32000,"s24le","front-left,front-right"));
  g_print("check if hdmi source is enumerated and start playback of 32KHz on VLC .. waiting for 20 sec..\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  ASSERT_NE(nullptr,methods_handler);
  ASSERT_EQ(ADK_SI_OK, sendSetSource("hdmi-in","external",16384000));
  g_print("Sent Set Source to HDMI and with the Clock specified .. \n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  checkAssertion("hdmi-in","external","Successful");

  g_print("Check if Playback is happening fine.. Waiting for 20 Sec..\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  ResetTestEvents();

  g_print("Start allplay playback 'allplay play file:///data/320000KHz.wav' on a seperate terminal\n");
  g_print("Press any key and wait for 15 seconds");
  std::this_thread::sleep_for(std::chrono::milliseconds(15000));
  g_print("Check Audio-mananger logs to confirm player change\n");

  ASSERT_EQ(true,_tc_ClockSourcePropChanged);
  ASSERT_STREQ("none",_tc_ClockSourcePropChanged_string.c_str());

  ASSERT_EQ(true,_tc_ClockTypePropChanged);
  ASSERT_STREQ("internal",_tc_ClockTypePropChanged_string.c_str());

  //signal is sent only for stesource from MCU
  ASSERT_EQ(false,_tc_SetSourceEvent);

  g_print("And the old playback gets stopped and new playback starts from allplay\n");
  g_print("***** MethodTestSetExtMclk16384000_to_NetworkStream(Allplay) - END *****\n");
}

// Test to switch from internal clock to network stream
TEST_F(AdkSystemInterfaceTest, MethodTestSetinternal_Clock_to_NetworkStream) {

  ASSERT_EQ(ADK_SI_OK, sendDeleteSource());
  ASSERT_EQ(ADK_SI_OK, sendSetSource("none","internal",0));
  ASSERT_EQ(ADK_SI_OK, sendCreateSource("pcm",48000,"s24le","front-left,front-right"));
  g_print("check If Hdmi Source is Enumerated... Waiting for 20 sec..\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  ASSERT_NE(nullptr,methods_handler);
  ASSERT_EQ(ADK_SI_OK, sendSetSource("hdmi-in","internal",0));
  ASSERT_EQ(ADK_SI_OK, methods_handler->setSourceExtClk(SetSourceInput));
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));

  ASSERT_EQ(true,_tc_ClockSourcePropChanged);
  ASSERT_STREQ("hdmi-in",_tc_ClockSourcePropChanged_string.c_str());

  ASSERT_EQ(true,_tc_SetSourceEvent);
  ASSERT_STREQ("Successful",_tc_SetSourceEvent_string.c_str());

  g_print("Check if Playback is Happening Fine.. Waiting for 10 Sec.. \n");
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));


  g_print("Start allplay playback 'allplay play file:///data/yesterday_48KHz.wav' on a seperate terminal\n");
  g_print("Press any key and wait for 15 seconds");
  std::this_thread::sleep_for(std::chrono::milliseconds(15000));
  g_print("Check Audio-mananger logs to confirm player change\n");
  //No check for clock-type change, as it remains to be internal
  ASSERT_EQ(true,_tc_ClockSourcePropChanged);
  ASSERT_STREQ("none",_tc_ClockSourcePropChanged_string.c_str());
  g_print("And the old playback gets stopped and new playback starts from allplay\n");
  g_print("***** MethodTestInternal_Clock_to_NetworkStream(Allplay) - END");
}

// Test to switch from network stream to external mclock 24575000
TEST_F(AdkSystemInterfaceTest, MethodTestNetworkStream_to_ExtMclk24576000) {
  g_print("***** MethodTestNetworkStream_to_ExtMclk24576000 - START");
  //Delete the old source if any
  ASSERT_EQ(ADK_SI_OK, sendDeleteSource());

  g_print("Start allplay playback 'allplay play file:///data/yesterday_48KHz.wav' on a seperate terminal\n");
  g_print("And press any key to continue\n");
  getchar();
  g_print("check if playback is happening fine.. waiting for 15 seconds");
  std::this_thread::sleep_for(std::chrono::milliseconds(15000));

  g_print("Now Start with HDMI streaming");
  ASSERT_EQ(ADK_SI_OK, sendDeleteSource());
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  g_print("***** Resetting to Internal Source. Wait for 10 Sec .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendSetSource("none","internal",0));
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  g_print("***** Create Pulse Audio Source .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendCreateSource("pcm",48000,"s24le","front-left,front-right"));
  g_print("check if hdmi source is enumerated and start playback of 48KHz on VLC .. waiting for 20 sec..\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  ASSERT_NE(nullptr,methods_handler);
  ASSERT_EQ(ADK_SI_OK, sendSetSource("hdmi-in","external",24575000 ));
  g_print("Sent Set Source to HDMI and with the Clock specified .. \n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  checkAssertion("hdmi-in","external","Successful");
  g_print("Check if Playback is happening fine.. Waiting for 20 Sec..\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  g_print("***** MethodTestNetworkStream_to_ExtMclk24576000 - END");
}

// Test to switch from network stream to external mclock 22579200
TEST_F(AdkSystemInterfaceTest, MethodTestNetworkStream_to_ExtMclk22579200) {
  g_print("***** MethodTestNetworkStream_to_ExtMclk22579200 - START");
  //Delete the old source if any
  ASSERT_EQ(ADK_SI_OK, sendDeleteSource());
  g_print("Start allplay playback 'allplay play file:///data/yesterday_44_1KHz.wav' on a seperate terminal\n");
  g_print("And press any key to continue\n");
  getchar();
  g_print("check if playback is happening fine.. waiting for 15 seconds");
  std::this_thread::sleep_for(std::chrono::milliseconds(15000));

  g_print("Now Start with HDMI streaming");
  ASSERT_EQ(ADK_SI_OK, sendDeleteSource());
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  g_print("***** Resetting to Internal Source. Wait for 10 Sec .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendSetSource("none","internal",0));
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  g_print("***** Create Pulse Audio Source .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendCreateSource("pcm",44100,"s24le","front-left,front-right"));
  g_print("check if hdmi source is enumerated and start playback of 44.1KHz on VLC .. waiting for 20 sec..\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  ASSERT_NE(nullptr,methods_handler);
  ASSERT_EQ(ADK_SI_OK, sendSetSource("hdmi-in","external", 22579200));
  g_print("Sent Set Source to HDMI and with the Clock specified .. \n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  checkAssertion("hdmi-in","external","Successful");

  g_print("Check if Playback is happening fine.. Waiting for 20 Sec..\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  g_print("***** MethodTestNetworkStream_to_ExtMclk22579200 - END");
}

// Test to switch from network stream to external mclock 12288000
TEST_F(AdkSystemInterfaceTest, MethodTestNetworkStream_to_ExtMclk12288000) {
  g_print("***** MethodTestNetworkStream_to_ExtMclk12288000 - START");
  //Delete the old source if any
  ASSERT_EQ(ADK_SI_OK, sendDeleteSource());

  g_print("Start allplay playback 'allplay play file:///data/yesterday_48KHz.wav' on a seperate terminal\n");
  g_print("And press any key to continue\n");
  getchar();
  g_print("check if playback is happening fine.. waiting for 5 seconds");
  std::this_thread::sleep_for(std::chrono::milliseconds(5000));

  g_print("Now Start with HDMI streaming");
  //Creat HDMI input (Pulseaudio Source)
  ASSERT_EQ(ADK_SI_OK, sendCreateSource("pcm",48000,"s24le","front-left,front-right"));
  g_print("check if hdmi source is enumerated and start playback of 48Khz on VLC .. waiting for 5 sec");
  std::this_thread::sleep_for(std::chrono::milliseconds(5000));

  //Set source to HDMI with external mclock
  ASSERT_NE(nullptr,methods_handler);
  ASSERT_EQ(ADK_SI_OK, sendSetSource("hdmi-in","external", 12288000));
  g_print("Set setsource to HDMI and with the clock specified");
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));

  checkAssertion("hdmi-in","external","Successful");

  g_print("The old playback gets stopped and new playback starts from HDMI using external mclock\n");
  g_print("check if playback is happening fine.. waiting for 5 seconds");
  std::this_thread::sleep_for(std::chrono::milliseconds(5000));

  g_print("***** MethodTestNetworkStream_to_ExtMclk12288000 - END");
}

// Test to switch from network stream to external mclock 12289600
TEST_F(AdkSystemInterfaceTest, MethodTestNetworkStream_to_ExtMclk11289600) {
  g_print("***** MethodTestNetworkStream_to_ExtMclk24576000 - START");
  //Delete the old source if any
  ASSERT_EQ(ADK_SI_OK, sendDeleteSource());

  g_print("Start allplay playback 'allplay play file:///data/yesterday_48KHz.wav' on a seperate terminal\n");
  g_print("And press any key to continue\n");
  getchar();
  g_print("check if playback is happening fine.. waiting for 5 seconds");
  std::this_thread::sleep_for(std::chrono::milliseconds(5000));

  g_print("Now Start with HDMI streaming");
  //Creat HDMI input (Pulseaudio Source)
  ASSERT_EQ(ADK_SI_OK, sendDeleteSource());
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  g_print("***** Resetting to Internal Source. Wait for 10 Sec .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendSetSource("none","internal",0));
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  g_print("***** Create Pulse Audio Source .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendCreateSource("pcm",44100,"s24le","front-left,front-right"));
  g_print("check if hdmi source is enumerated and start playback of 48KHz on VLC .. waiting for 20 sec..\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  ASSERT_NE(nullptr,methods_handler);
  ASSERT_EQ(ADK_SI_OK, sendSetSource("hdmi-in","external",12289600));
  g_print("Sent Set Source to HDMI and with the Clock specified .. \n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  checkAssertion("hdmi-in","external","Successful");

  g_print("Check if Playback is happening fine.. Waiting for 20 Sec..\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  g_print("***** MethodTestNetworkStream_to_ExtMclk11289600 - END");
}

// Test to switch from network stream to external mclock 16384000
TEST_F(AdkSystemInterfaceTest, MethodTestNetworkStream_to_ExtMclk16384000) {
  g_print("***** MethodTestNetworkStream_to_ExtMclk24576000 - START");
  //Delete the old source if any
  ASSERT_EQ(ADK_SI_OK, sendDeleteSource());

  g_print("Start allplay playback 'allplay play file:///data/yesterday_48KHz.wav' on a seperate terminal\n");
  g_print("And press any key to continue\n");
  getchar();
  g_print("check if playback is happening fine.. waiting for 5 seconds");
  std::this_thread::sleep_for(std::chrono::milliseconds(5000));

  g_print("Now Start with HDMI streaming");
  //Creat HDMI input (Pulseaudio Source)
  ASSERT_EQ(ADK_SI_OK, sendCreateSource("pcm",32000,"s24le","front-left,front-right"));
  g_print("check if hdmi source is enumerated and start playback of 48Khz on VLC .. waiting for 5 sec");
  std::this_thread::sleep_for(std::chrono::milliseconds(5000));

  //Set source to HDMI with external mclock
  ASSERT_NE(nullptr,methods_handler);
  ASSERT_EQ(ADK_SI_OK, sendSetSource("hdmi-in","external", 16384000));
  g_print("Set setsource to HDMI and with the clock specified");
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));

  checkAssertion("hdmi-in","external","Successful");

  g_print("The old playback gets stopped and new playback starts from HDMI using external mclock\n");
  g_print("check if playback is happening fine.. waiting for 5 seconds");
  std::this_thread::sleep_for(std::chrono::milliseconds(5000));

  g_print("***** MethodTestNetworkStream_to_ExtMclk16384000 - END");
}

// Test to switch from network stream to internal clock
TEST_F(AdkSystemInterfaceTest, MethodTestNetworkStream_to_Internal_Clock) {
  g_print("***** MethodTestNetworkStream_to_Internal_Clock - START");
  //Delete the old source if any
  ASSERT_EQ(ADK_SI_OK, sendDeleteSource());

  g_print("Start allplay playback 'allplay play file:///data/yesterday_48KHz.wav' on a seperate terminal\n");
  g_print("And press any key to continue\n");
  getchar();
  g_print("check if playback is happening fine.. waiting for 5 seconds");
  std::this_thread::sleep_for(std::chrono::milliseconds(5000));

  g_print("Now Start with HDMI streaming");
  //Creat HDMI input (Pulseaudio Source)
  ASSERT_EQ(ADK_SI_OK, sendCreateSource("pcm",48000,"s24le","front-left,front-right"));
  g_print("check if hdmi source is enumerated and start playback of 48Khz on VLC .. waiting for 5 sec");
  std::this_thread::sleep_for(std::chrono::milliseconds(5000));

  //Set source to HDMI with internal clock
  ASSERT_NE(nullptr,methods_handler);
  ASSERT_EQ(ADK_SI_OK, sendSetSource("hdmi-in","internal", 0));
  g_print("Set setsource to HDMI and with the clock specified");
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));

  //No check for clock-type change, as it remains to be internal
  ASSERT_EQ(true,_tc_ClockSourcePropChanged);
  ASSERT_STREQ("hdmi-in",_tc_ClockSourcePropChanged_string.c_str());


  g_print("The old playback gets stopped and new playback starts from HDMI using external mclock\n");
  g_print("check if playback is happening fine.. waiting for 5 seconds");
  std::this_thread::sleep_for(std::chrono::milliseconds(5000));

  g_print("***** MethodTestNetworkStream_to_Internal_Clock - END");
}

// Test to switch from external mclock to bluetooth
TEST_F(AdkSystemInterfaceTest, MethodTestExtMclk_to_Bluetooth) {
  g_print("***** MethodTestExtMClk_to_Bluetooth - START");
  g_print("***** Deleting the Old Source. Wait for 10 Sec .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendDeleteSource());
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  g_print("***** Resetting to Internal Source. Wait for 10 Sec .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendSetSource("none","internal",0));
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  g_print("***** Create Pulse Audio Source .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendCreateSource("pcm",48000,"s24le","front-left,front-right"));
  g_print("***** Check if HDMI Source is Enumerated... Start Playback of 48KHz on VLC .. Waiting for 20 Sec.. *****\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  g_print("***** Set Source to Ext Clock .. *****\n");
  ASSERT_NE(nullptr,methods_handler);
  ASSERT_EQ(ADK_SI_OK, sendSetSource("hdmi-in","external", 24576000));
  g_print("***** Sent SetSource to HDMI With 24576000 Clock Specified .. *****\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));

  checkAssertion("hdmi-in","external","Successful");
  g_print("***** Check the Playback... Waiting for 20Sec .. *****\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));

  g_print("Connect to a bluetooth sink device and start playing any audio\n");
  g_print("Now press any key to continue");
  getchar();
  std::this_thread::sleep_for(std::chrono::milliseconds(5000));
  ASSERT_NE(nullptr,methods_handler);
  ASSERT_EQ(ADK_SI_OK, sendSetSource("bluetooth","internal",0));
  g_print("Sent setsource to bluetooth and with the clock specified\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  ASSERT_EQ(true,_tc_ClockTypePropChanged);
  ASSERT_STREQ("internal",_tc_ClockTypePropChanged_string.c_str());
  g_print("Check Audio-mananger logs to confirm player change\n");
  g_print("check dmesg to see the clock switch to internal\n");
  g_print("And the old playback gets stopped and new playback starts from bluetooth sink device\n");
  g_print("***** MethodTestExt_Mclk_to_Bluetooth - END *****\n");
}

TEST_F(AdkSystemInterfaceTest, MethodTestInternal_Clock_to_Bluetooth) {
  g_print("***** MethodTestInternal_Clock_to_Bluetooth - START *****\n");
  ASSERT_EQ(ADK_SI_OK, sendDeleteSource());
  ASSERT_EQ(ADK_SI_OK, sendSetSource("none","internal",0));
  ASSERT_EQ(ADK_SI_OK, sendCreateSource("pcm",48000,"s24le","front-left,front-right"));
  g_print("check If Hdmi Source is Enumerated... Waiting for 20 sec..\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  ASSERT_NE(nullptr,methods_handler);
  ASSERT_EQ(ADK_SI_OK, sendSetSource("hdmi-in","internal", 0));
  ASSERT_EQ(ADK_SI_OK, methods_handler->setSourceExtClk(SetSourceInput));
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  ASSERT_EQ(true,_tc_ClockSourcePropChanged);
  ASSERT_STREQ("hdmi-in",_tc_ClockSourcePropChanged_string.c_str());
  ASSERT_EQ(true,_tc_SetSourceEvent);
  ASSERT_STREQ("Successful",_tc_SetSourceEvent_string.c_str());
  g_print("Check if Playback is Happening Fine.. Waiting for 10 Sec.. \n");
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));

  g_print("Connect to a bluetooth sink device and start playing any audio\n");
  g_print("Now press any key to continue\n");
  getchar();
  std::this_thread::sleep_for(std::chrono::milliseconds(5000));
  ASSERT_NE(nullptr,methods_handler);
  ASSERT_EQ(ADK_SI_OK, sendSetSource("bluetooth","internal",0));
  ASSERT_EQ(ADK_SI_OK, methods_handler->setSourceExtClk(SetSourceInput));
  g_print("Sent setsource to bluetooth and with the clock specified\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  ASSERT_EQ(true,_tc_ClockSourcePropChanged);
  ASSERT_STREQ("bluetooth",_tc_ClockSourcePropChanged_string.c_str());
  g_print("Check Audio-mananger logs to confirm player change\n");
  g_print("check dmesg to see the clock switch to internal\n");
  g_print("And the old playback gets stopped and new playback starts from bluetooth sink device\n");

  g_print("***** MethodTestInternal_Clock_to_Bluetooth - END *****\n");
}

// Test to switch from bluetooth to external mclock
TEST_F(AdkSystemInterfaceTest, MethodTestBluetooth_to_Ext_MClock) {
  g_print("***** MethodTestbluetooth_to_Ext_MClock - START *****\n");
  ASSERT_EQ(ADK_SI_OK, sendDeleteSource());
  ASSERT_NE(nullptr,methods_handler);
  ASSERT_EQ(ADK_SI_OK, sendSetSource("bluetooth","internal",0));
  ASSERT_EQ(ADK_SI_OK, methods_handler->setSourceExtClk(SetSourceInput));
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  g_print("Connect to a bluetooth sink device and start playing any audio\n");
  g_print("Now press any key to continue\n");
  getchar();
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  ASSERT_EQ(true,_tc_ClockSourcePropChanged);
  ASSERT_STREQ("bluetooth",_tc_ClockSourcePropChanged_string.c_str());
  g_print("Check Audio-mananger logs to confirm player change\n");
  g_print("check dmesg to see the clock switch to internal\n");
  g_print("And the old playback gets stopped and new playback starts from bluetooth sink device\n");
  g_print("***** Now Start with HDMI streaming *****\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));
  g_print("***** Deleting the Old Source. Wait for 10 Sec .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendDeleteSource());
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  g_print("***** Resetting to Internal Source. Wait for 10 Sec .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendSetSource("none","internal",0));
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  g_print("***** Create Pulse Audio Source .. *****\n");
  ASSERT_EQ(ADK_SI_OK, sendCreateSource("pcm",48000,"s24le","front-left,front-right"));
  g_print("***** Check if HDMI Source is Enumerated... Start Playback of 48KHz on VLC .. Waiting for 20 Sec.. *****\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));
  g_print("***** Set Source to Ext Clock .. *****\n");
  ASSERT_NE(nullptr,methods_handler);
  ASSERT_EQ(ADK_SI_OK, sendSetSource("hdmi-in","external", 24576000));
  g_print("***** Sent SetSource to HDMI With 24576000 Clock Specified .. *****\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  checkAssertion("hdmi-in","external","Successful");
  g_print("***** Check the Playback... Waiting for 20Sec .. *****\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  g_print("***** MethodTestBluetooth_to_External_MClock - END");
}

// Test to switch from bluetooth to internal clock
TEST_F(AdkSystemInterfaceTest, MethodTestBluetooth_to_Internal_Clock) {
  g_print("***** MethodTestbluetooth_to_Internal_Clock - START");
  ASSERT_EQ(ADK_SI_OK, sendDeleteSource());
  ASSERT_NE(nullptr,methods_handler);
  ASSERT_EQ(ADK_SI_OK, sendSetSource("bluetooth","internal",0));
  ASSERT_EQ(ADK_SI_OK, methods_handler->setSourceExtClk(SetSourceInput));
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  g_print("Connect to a bluetooth sink device and start playing any audio\n");
  g_print("Now press any key to continue\n");
  getchar();
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  ASSERT_EQ(true,_tc_ClockSourcePropChanged);
  ASSERT_STREQ("bluetooth",_tc_ClockSourcePropChanged_string.c_str());
  g_print("Check Audio-mananger logs to confirm player change\n");
  g_print("check dmesg to see the clock switch to internal\n");
  g_print("And the old playback gets stopped and new playback starts from bluetooth sink device\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  g_print("Now Start with HDMI streaming");
  ASSERT_EQ(ADK_SI_OK, sendDeleteSource());
  ASSERT_EQ(ADK_SI_OK, sendSetSource("none","internal",0));
  ASSERT_EQ(ADK_SI_OK, sendCreateSource("pcm",48000,"s24le","front-left,front-right"));
  g_print("check If Hdmi Source is Enumerated... Waiting for 20 sec..\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  ASSERT_NE(nullptr,methods_handler);
  ASSERT_EQ(ADK_SI_OK, sendSetSource("hdmi-in","internal", 0));
  ASSERT_EQ(ADK_SI_OK, methods_handler->setSourceExtClk(SetSourceInput));
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));

  ASSERT_EQ(true,_tc_ClockSourcePropChanged);
  ASSERT_STREQ("hdmi-in",_tc_ClockSourcePropChanged_string.c_str());

  ASSERT_EQ(true,_tc_SetSourceEvent);
  ASSERT_STREQ("Successful",_tc_SetSourceEvent_string.c_str());

  g_print("Check if Playback is Happening Fine.. Waiting for 10 Sec.. \n");
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));;
  g_print("***** MethodTestBluetooth_to_Internal_Clock - END");
}

// Test to switch from external mclock to Non_ADK_stream
TEST_F(AdkSystemInterfaceTest, MethodTestSetExtMclk24576000_to_Non_ADK_Stream) {
  g_print("***** MethodTestSetExtMclk24576000_to_Non-ADK_Stream - START");

  //Delete the old source if any
  ASSERT_EQ(ADK_SI_OK, sendDeleteSource());

  //Creat HDMI input (Pulseaudio Source)
  ASSERT_EQ(ADK_SI_OK, sendCreateSource("pcm",48000,"s24le","front-left,front-right"));
  g_print("check if hdmi source is enumerated and start playback of 48Khz on VLC .. waiting for 5 sec");
  std::this_thread::sleep_for(std::chrono::milliseconds(5000));

  //Set source to HDMI with external mclock
  ASSERT_NE(nullptr,methods_handler);
  ASSERT_EQ(ADK_SI_OK, sendSetSource("hdmi-in","external", 24576000));
  g_print("Sent setsource to HDMI and with the clock specified");
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));

  checkAssertion("hdmi-in","external","Successful");
  g_print("check if playback is happening fine.. waiting for 20 seconds");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  //Set Source to None Relinquish Local playback
  ASSERT_EQ(ADK_SI_OK, sendSetSource("none","internal", 0));
  g_print("Sent setsource to HDMI and with the clock specified");
  std::this_thread::sleep_for(std::chrono::milliseconds(5000));

  checkAssertion("none","internal","Successful");

  g_print("Non_ADK stream playback from audio test source .. Press ctrl-C to stop playback.. and return to test verdict\n");
  system("gst-launch-1.0 filesrc location= /data/yesterday_48KHz.wav ! wavparse ! audioconvert ! pulsesink");

  std::this_thread::sleep_for(std::chrono::milliseconds(2000));
  g_print("***** MethodTestSetExtMclk24576000_switch_Non-ADK_Stream - END");
}

// Test to switch from internal clock to Non_ADK_stream
TEST_F(AdkSystemInterfaceTest, MethodTestInternal_Clock_to_Non_ADK_Stream) {
  g_print("***** MethodTestInternal_Clock_to_Non-ADK_Stream - START");

  //Delete the old source if any
  ASSERT_EQ(ADK_SI_OK, sendDeleteSource());

  //Creat HDMI input (Pulseaudio Source)
  ASSERT_EQ(ADK_SI_OK, sendCreateSource("pcm",48000,"s24le","front-left,front-right"));
  g_print("check if hdmi source is enumerated and start playback of 48Khz on VLC .. waiting for 5 sec");
  std::this_thread::sleep_for(std::chrono::milliseconds(5000));

  //Set source to HDMI with internal clock
  ASSERT_NE(nullptr,methods_handler);
  ASSERT_EQ(ADK_SI_OK, sendSetSource("hdmi-in","internal", 0));
  g_print("Sent setsource to HDMI and with the clock specified");
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));

  //No check for clock-type change, as it remains to be internal
  ASSERT_EQ(true,_tc_ClockSourcePropChanged);
  ASSERT_STREQ("hdmi-in",_tc_ClockSourcePropChanged_string.c_str());

  ASSERT_EQ(true,_tc_SetSourceEvent);
  ASSERT_STREQ("Successful",_tc_SetSourceEvent_string.c_str());

  g_print("check if playback is happening fine.. waiting for 20 seconds");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  //Set Source to None Relinquish Local playback
  ASSERT_EQ(ADK_SI_OK, sendSetSource("none","internal", 0));
  g_print("Sent setsource to HDMI and with the clock specified");
  std::this_thread::sleep_for(std::chrono::milliseconds(5000));

  checkAssertion("none","internal","Successful");
  std::this_thread::sleep_for(std::chrono::milliseconds(2000));
  g_print("Non-ADK stream playback from audio test source\n");
  system("gst-launch-1.0 filesrc location= /data/yesterday_48KHz.wav ! wavparse ! audioconvert ! pulsesink");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));
  g_print("***** MethodTestInternal_Clock_to_Non-ADK_Stream - END");
}

// Test to switch from Non_ADK_stream to external mclock 24576000
TEST_F(AdkSystemInterfaceTest, MethodTest_Non_ADK_Stream_to_SetExtMclk24576000) {
  g_print("***** MethodTest_Non-ADK_Stream_to_SetExtMclk24576000 - START");

  //Delete the old source if any
  ASSERT_EQ(ADK_SI_OK, sendDeleteSource());

  std::this_thread::sleep_for(std::chrono::milliseconds(2000));
  g_print("Non-ADK stream playback from audio test source\n");
  system("gst-launch-1.0 filesrc location= /data/yesterday_48KHz.wav ! wavparse ! audioconvert ! pulsesink");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  g_print("Now start with HDMI streaming");
  //Creat HDMI input (Pulseaudio Source)
  ASSERT_EQ(ADK_SI_OK, sendCreateSource("pcm",48000,"s24le","front-left,front-right"));
  g_print("check if hdmi source is enumerated and start playback of 48Khz on VLC .. waiting for 5 sec");
  std::this_thread::sleep_for(std::chrono::milliseconds(5000));

  //Set source to HDMI with external mclock
  ASSERT_NE(nullptr,methods_handler);
  ASSERT_EQ(ADK_SI_OK, sendSetSource("hdmi-in","external", 24576000));
  g_print("Sent setsource to HDMI and with the clock specified");
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));

  checkAssertion("hdmi-in","external","Successful");

  g_print("check if playback is happening fine.. waiting for 20 seconds");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  g_print("***** MethodTest_Non-ADK_Stream_to_SetExtMclk24576000 - END");
}

// Test to switch from Non_ADK_stream to internal clock
TEST_F(AdkSystemInterfaceTest, MethodTest_Non_ADK_Stream_to_Internal_Clock) {
  g_print("***** MethodTest_Non-ADK_Stream_to_Internal_Clock - START");

  //Delete the old source if any
  ASSERT_EQ(ADK_SI_OK, sendDeleteSource());

  //Power down HDMI
  powerHDMI(false);

  //Set Source to None Relinquish Local playback
  ASSERT_EQ(ADK_SI_OK, sendSetSource("none","internal", 0));
  g_print("Sent setsource to none and with clock as internal");
  std::this_thread::sleep_for(std::chrono::milliseconds(5000));

  checkAssertion("none","internal","Successful");
  std::this_thread::sleep_for(std::chrono::milliseconds(2000));
  g_print("Non-ADK stream playback from audio test source\n");
  system("gst-launch-1.0 filesrc location= /data/yesterday_48KHz.wav ! wavparse ! audioconvert ! pulsesink");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  //Power Up HDMI
  powerHDMI(true);

  g_print("Now start with HDMI streaming");
  //Creat HDMI input (Pulseaudio Source)
  ASSERT_EQ(ADK_SI_OK, sendCreateSource("pcm",48000,"s24le","front-left,front-right"));
  g_print("check if hdmi source is enumerated and start playback of 48Khz on VLC .. waiting for 5 sec");
  std::this_thread::sleep_for(std::chrono::milliseconds(5000));

  //Set source to HDMI with external mclock
  ASSERT_NE(nullptr,methods_handler);
  ASSERT_EQ(ADK_SI_OK, sendSetSource("hdmi-in","external", 24576000));
  g_print("Sent setsource to HDMI and with the clock specified");
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));

  checkAssertion("hdmi-in","external","Successful");
  g_print("check if playback is happening fine.. waiting for 20 seconds");
  std::this_thread::sleep_for(std::chrono::milliseconds(20000));

  g_print("***** MethodTest_Non-ADK_Stream_to_Internal_Clock - END");
}

int main(int argc, char** argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}