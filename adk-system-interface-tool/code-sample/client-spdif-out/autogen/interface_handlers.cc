
/*
 * Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of The Linux Foundation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "interface_handlers.h"

namespace adk {

PropertiesHandler::PropertiesHandler(Dbus *dbus)
    : dbus_(dbus) {
    dbus_->setPropertiesChangedListener(
        std::bind(&PropertiesHandler::propertiesChangedCb, this,
            std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
}

void PropertiesHandler::propertiesChangedCb(const std::string &obj_path,
    const std::string &iface_name, GVariantIter *iter) {
    g_print("PropertiesHandler::propertiesChangedCb \n");
    if (iface_name == std::string(adk::dbus::kAudiomanagerDeviceControllerIface)) {
        PropertiesAttr changed_properties = processAudiomanagerDeviceControllerIfaceResponse(obj_path, iter);
        for (auto &f : properties_changed_listeners_) {
            f(changed_properties, obj_path);
        }
        return;
    }
}

PropertiesAttr PropertiesHandler::processAudiomanagerDeviceControllerIfaceResponse(
    const std::string &obj_path, GVariantIter *iter) {
    g_print("PropertiesHandler::processKAudiomanagerDeviceControllerIfaceResponse obj_path: %s \n", obj_path.c_str());
    char *key;
    GVariant *tmp = nullptr;
    PropertiesAttr changed_properties;
    bool obj_id_map_changed = false;

    std::lock_guard<std::mutex> lock(mutex_);
    changed_properties.clearChangedProperties();
    while (g_variant_iter_loop(iter, "{sv}", &key, &tmp)) {
        if (std::string(key) == "CurrentSinks") {
            GVariantIter property_iter;
            g_variant_iter_init(&property_iter, tmp);
            changed_properties.setCurrentSinks(dbus_->parseArrayOfString(property_iter));
            continue;
        }
    }

    if (tmp) {
        g_variant_unref(tmp);
    }
    if (obj_id_map_changed) {
        for (auto &f : obj_id_map_changed_listeners_) {
            f(obj_id_map_);
        }
    }
    return changed_properties;
}

std::tuple<ErrorCode, std::vector<std::string>> PropertiesHandler::readCurrentSinks() {
    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::vector<std::string> current_sinks;
    std::tie(result, response) = dbus_->propertyGet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kAudiomanagerDeviceControllerObjPath,
        g_variant_new("(ss)",
            adk::dbus::kAudiomanagerDeviceControllerIface,
            "CurrentSinks"));

    if (result == ADK_SI_OK && response) {
        GVariant *tmp;
        g_variant_get(response, "(v)", &tmp);
        GVariantIter property_iter;
        g_variant_iter_init(&property_iter, tmp);
        current_sinks = dbus_->parseArrayOfString(property_iter);
        g_variant_unref(tmp);
    }
    return std::make_tuple(result, current_sinks);
}

ErrorCode PropertiesHandler::updateCurrentSinks(
    std::vector<std::string> current_sinks) {
    GVariantBuilder current_sinks_builder;
    g_variant_builder_init(&current_sinks_builder, G_VARIANT_TYPE_ARRAY);
    for (auto &i : current_sinks) {
        g_variant_builder_add(&current_sinks_builder, "s", i.c_str());
    }

    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::tie(result, response) = dbus_->propertySet(
        adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kAudiomanagerDeviceControllerObjPath,
        g_variant_new("(ssv)",
            adk::dbus::kAudiomanagerDeviceControllerIface,
            "CurrentSinks",
            g_variant_new("as", &current_sinks_builder)));
    return result;
}

MethodsHandler::MethodsHandler(Dbus *dbus)
    : dbus_(dbus) {
    dbus_->setPropertiesChangedListener(
        std::bind(&MethodsHandler::interfacesAddedCb, this,
            std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
}

void MethodsHandler::interfacesAddedCb(const std::string &obj_path,
    const std::string &iface_name, GVariantIter * /*iter*/) {
    g_print("MethodsHandler::interfacesAddedCb \n");

    bool obj_id_map_changed = false;
    std::lock_guard<std::mutex> lock(mutex_);

    if (obj_id_map_changed) {
        for (auto &f : obj_id_map_changed_listeners_) {
            f(obj_id_map_);
        }
    }
}

std::tuple<ErrorCode, GetSinkInfoOut>
MethodsHandler::getSinkInfo(
    std::string in_arg) {
    GVariantBuilder input_builder;
    g_variant_builder_init(&input_builder, G_VARIANT_TYPE("(s)"));
    g_variant_builder_add(&input_builder, "s", in_arg.c_str());

    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::tie(result, response) = dbus_->gdbusMethodCall(adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kAudiomanagerDeviceControllerObjPath,
        adk::dbus::kAudiomanagerDeviceControllerIface,
        "GetSinkInfo",
        g_variant_builder_end(&input_builder));
    GetSinkInfoOut get_sink_info_out;
    if (result == ADK_SI_OK && response) {
        if (!g_variant_is_of_type(response, (const GVariantType *)"(sa{sv})")) {
            return std::make_tuple(ADK_SI_ERROR_FAILED, get_sink_info_out);
        }

        gchar *SinkName;
        GVariantIter *SinkInfo_iter;

        g_variant_get(response, "(sa{sv})", &SinkName, &SinkInfo_iter);

        get_sink_info_out.setSinkName(
            SinkName);
        get_sink_info_out.setSinkInfo(
            dbus_->parseDeviceControllerSinkInfo(*SinkInfo_iter));
    }
    return std::make_tuple(result, get_sink_info_out);
}
ErrorCode
MethodsHandler::setChBitMask(
    SetChBitMaskIn in_arg) {
    GVariantBuilder input_builder;
    g_variant_builder_init(&input_builder, G_VARIANT_TYPE("(say)"));
    std::string source_str = in_arg.getSource();
    g_variant_builder_add(&input_builder, "s", source_str.c_str());
    g_variant_builder_open(&input_builder, G_VARIANT_TYPE("ay"));
    for (auto &i : in_arg.getBitMask()) {
        g_variant_builder_add(&input_builder, "y", i);
    }
    g_variant_builder_close(&input_builder);

    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::tie(result, response) = dbus_->gdbusMethodCall(adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kAudiomanagerDeviceControllerObjPath,
        adk::dbus::kAudiomanagerDeviceControllerIface,
        "SetChBitMask",
        g_variant_builder_end(&input_builder));
    return result;
}
ErrorCode
MethodsHandler::setChStatusInfo(
    SetChStatusInfoIn in_arg) {
    GVariantBuilder input_builder;
    g_variant_builder_init(&input_builder, G_VARIANT_TYPE("(say)"));
    std::string sink_str = in_arg.getSink();
    g_variant_builder_add(&input_builder, "s", sink_str.c_str());
    g_variant_builder_open(&input_builder, G_VARIANT_TYPE("ay"));
    for (auto &i : in_arg.getChannelStatusInfo()) {
        g_variant_builder_add(&input_builder, "y", i);
    }
    g_variant_builder_close(&input_builder);

    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::tie(result, response) = dbus_->gdbusMethodCall(adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kAudiomanagerDeviceControllerObjPath,
        adk::dbus::kAudiomanagerDeviceControllerIface,
        "SetChStatusInfo",
        g_variant_builder_end(&input_builder));
    return result;
}
ErrorCode
MethodsHandler::setSinkMute(
    std::unordered_map<std::string, bool> in_arg) {
    GVariantBuilder input_builder;
    g_variant_builder_init(&input_builder, G_VARIANT_TYPE("(a{sb})"));
    g_variant_builder_open(&input_builder, G_VARIANT_TYPE("a{sb}"));
    for (auto &i : in_arg) {
        std::string key = i.first;
        bool value = i.second;
        g_variant_builder_open(&input_builder, G_VARIANT_TYPE("{sb}"));
        g_variant_builder_add(&input_builder, "s", key.c_str());
        g_variant_builder_add(&input_builder, "b", value);
        g_variant_builder_close(&input_builder);
    }
    g_variant_builder_close(&input_builder);

    ErrorCode result = ADK_SI_OK;
    GVariant *response = nullptr;
    std::tie(result, response) = dbus_->gdbusMethodCall(adk::dbus::kAdkAudiomanagerService,
        adk::dbus::kAudiomanagerDeviceControllerObjPath,
        adk::dbus::kAudiomanagerDeviceControllerIface,
        "SetSinkMute",
        g_variant_builder_end(&input_builder));
    return result;
}

SignalsHandler::SignalsHandler(Dbus *dbus)
    : dbus_(dbus) {
    dbus_->setSinkInfoUpdatedListener(
        std::bind(&SignalsHandler::callSinkInfoUpdatedListeners, this,
            std::placeholders::_1, std::placeholders::_2));
    dbus_->setChannelStatusUpdateListener(
        std::bind(&SignalsHandler::callChannelStatusUpdateListeners, this,
            std::placeholders::_1, std::placeholders::_2));
}

void SignalsHandler::callSinkInfoUpdatedListeners(const std::string &obj_path, GVariant *response) {
    g_print("SignalsHandler::callSinkInfoUpdatedListeners \n");
    if (!g_variant_is_of_type(response, (const GVariantType *)"(sa{sv})")) {
        return;
    }

    SinkInfoUpdatedSigOut sink_info_updated_out;

    gchar *SinkName;
    GVariantIter *SinkInfo_iter;

    g_variant_get(response, "(sa{sv})", &SinkName, &SinkInfo_iter);

    sink_info_updated_out.setSinkName(
        SinkName);
    sink_info_updated_out.setSinkInfo(
        dbus_->parseDeviceControllerSinkInfo(*SinkInfo_iter));
    for (auto &f : sink_info_updated_listeners_) {
        f(sink_info_updated_out, obj_path);
    }
}
void SignalsHandler::callChannelStatusUpdateListeners(const std::string &obj_path, GVariant *response) {
    g_print("SignalsHandler::callChannelStatusUpdateListeners \n");
    if (!g_variant_is_of_type(response, (const GVariantType *)"(say)")) {
        return;
    }

    ChannelStatusUpdateSigOut channel_status_update_out;

    gchar *Source;
    GVariantIter *ChannelStatus_iter;

    g_variant_get(response, "(say)", &Source, &ChannelStatus_iter);

    channel_status_update_out.setSource(
        Source);
    channel_status_update_out.setChannelStatus(
        dbus_->parseArrayOfUint8(*ChannelStatus_iter));
    for (auto &f : channel_status_update_listeners_) {
        f(channel_status_update_out, obj_path);
    }
}
}  // namespace adk
