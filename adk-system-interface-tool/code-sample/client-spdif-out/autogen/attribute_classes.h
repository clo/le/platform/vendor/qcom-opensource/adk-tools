/*
 * Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of The Linux Foundation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef AUTOGEN_ATTRIBUTE_CLASSES_H_
#define AUTOGEN_ATTRIBUTE_CLASSES_H_

#include <string>
#include <unordered_set>
#include <vector>

#include "attribute_strings.h"

namespace adk {

enum ErrorCode {
    ADK_SI_OK,
    ADK_SI_ERROR_FAILED,
    ADK_SI_ERROR_NO_SERVICE,
    ADK_SI_ERROR_NO_OBJECT,
    ADK_SI_ERROR_TIMEOUT
};

class DeviceControllerSinkInfo {
    /*
 Helper type to describe the Information of a Sink
*/

 public:
    DeviceControllerSinkInfo() = default;
    ~DeviceControllerSinkInfo() = default;

    bool getEnable() { return enable_; }

    void setEnable(const bool &enable) {
        enable_ = enable;
        changed_properties_.emplace(kEnable);
    }
    bool getMute() { return mute_; }

    void setMute(const bool &mute) {
        mute_ = mute;
        changed_properties_.emplace(kMute);
    }

 public:
    void clearChangedProperties() { changed_properties_.clear(); }
    std::unordered_set<std::string> getChangedProperties() { return changed_properties_; }

 private:
    bool enable_;
    bool mute_;

    std::unordered_set<std::string> changed_properties_;
};

class PropertiesAttr {
 public:
    PropertiesAttr() = default;
    ~PropertiesAttr() = default;

    std::vector<std::string> getCurrentSinks() {
        return current_sinks_;
    }
    void setCurrentSinks(std::vector<std::string> current_sinks) {
        current_sinks_ = current_sinks;
        changed_properties_.emplace(kCurrentSinks);
    }

    void clearChangedProperties() { changed_properties_.clear(); }
    std::unordered_set<std::string> getChangedProperties() { return changed_properties_; }

 private:
    std::vector<std::string> current_sinks_;

    std::unordered_set<std::string> changed_properties_;
};

class GetSinkInfoOut {
 public:
    GetSinkInfoOut() = default;
    ~GetSinkInfoOut() = default;

    std::string getSinkName() { return sink_name_; }
    void setSinkName(const std::string &sink_name) {
        sink_name_ = sink_name;
    }

    DeviceControllerSinkInfo getSinkInfo() { return sink_info_; }
    void setSinkInfo(const DeviceControllerSinkInfo &sink_info) {
        sink_info_ = sink_info;
    }

 private:
    std::string sink_name_;
    DeviceControllerSinkInfo sink_info_;
};
class SetChBitMaskIn {
 public:
    SetChBitMaskIn() = default;
    ~SetChBitMaskIn() = default;

    std::string getSource() { return source_; }
    void setSource(const std::string &source) {
        source_ = source;
    }

    std::vector<uint8_t> getBitMask() { return bit_mask_; }
    void setBitMask(const std::vector<uint8_t> &bit_mask) {
        bit_mask_ = bit_mask;
    }

 private:
    std::string source_;
    std::vector<uint8_t> bit_mask_;
};

class SetChStatusInfoIn {
 public:
    SetChStatusInfoIn() = default;
    ~SetChStatusInfoIn() = default;

    std::string getSink() { return sink_; }
    void setSink(const std::string &sink) {
        sink_ = sink;
    }

    std::vector<uint8_t> getChannelStatusInfo() { return channel_status_info_; }
    void setChannelStatusInfo(const std::vector<uint8_t> &channel_status_info) {
        channel_status_info_ = channel_status_info;
    }

 private:
    std::string sink_;
    std::vector<uint8_t> channel_status_info_;
};

class SinkInfoUpdatedSigOut {
 public:
    SinkInfoUpdatedSigOut() = default;
    ~SinkInfoUpdatedSigOut() = default;

    std::string getSinkName() { return sink_name_; }
    void setSinkName(const std::string &sink_name) {
        sink_name_ = sink_name;
    }
    DeviceControllerSinkInfo getSinkInfo() { return sink_info_; }
    void setSinkInfo(const DeviceControllerSinkInfo &sink_info) {
        sink_info_ = sink_info;
    }

 private:
    std::string sink_name_;
    DeviceControllerSinkInfo sink_info_;
};
class ChannelStatusUpdateSigOut {
 public:
    ChannelStatusUpdateSigOut() = default;
    ~ChannelStatusUpdateSigOut() = default;

    std::string getSource() { return source_; }
    void setSource(const std::string &source) {
        source_ = source;
    }
    std::vector<uint8_t> getChannelStatus() { return channel_status_; }
    void setChannelStatus(const std::vector<uint8_t> &channel_status) {
        channel_status_ = channel_status;
    }

 private:
    std::string source_;
    std::vector<uint8_t> channel_status_;
};

}  // namespace adk
#endif  //  AUTOGEN_ATTRIBUTE_CLASSES_H_
