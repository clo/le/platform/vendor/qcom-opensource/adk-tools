/*
 * Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of The Linux Foundation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <cassert>
#include <chrono>
#include <csignal>
#include <cstring>
#include <functional>
#include <iostream>
#include <string>
#include <thread>
#include <unordered_map>
#include <unordered_set>

#include "autogen/adk_system_interface.h"
#include "autogen/attribute_classes.h"
#include "autogen/attribute_strings.h"

using namespace adk;
using namespace std;

// helper function to print channel status information
void printcsi(std::vector<uint8_t> csi) {
    for (auto &it : csi) {
        g_print("%d ", it);
    }
    g_print("\n\n");
}

// helper function to print sink info
void printsinkinfo(DeviceControllerSinkInfo details) {
    std::unordered_set<std::string> changed_properties = details.getChangedProperties();

    if (changed_properties.empty()) {
        g_print("no sink info\n");
        return;
    }

    if (changed_properties.find(std::string(adk::kEnable)) != changed_properties.end()) {
        g_print("    enabled : %s \n", details.getEnable() ? "true" : "false");
    }
    if (changed_properties.find(std::string(adk::kMute)) != changed_properties.end()) {
        g_print("    mute status : %s \n", details.getMute() ? "true" : "false");
    }
}

// helper function to get sink info of a sink via getSinkInfo method call
void getsinkinfo(std::shared_ptr<MethodsHandler> methods_handler, std::string sink) {
    g_print("\n---> get sink info of sink: '%s' via getSinkInfo method call\n", sink.c_str());
    ErrorCode result = ADK_SI_OK;
    GetSinkInfoOut info;
    g_print("calling the method : getSinkInfo for info of - %s\n", sink.c_str());
    std::tie(result, info) = methods_handler->getSinkInfo(sink);
    if (result == ADK_SI_OK) {
        g_print("method call - getSinkInfo, success\n");
        g_print("    received info of : %s \n", info.getSinkName().c_str());
        printsinkinfo(info.getSinkInfo());
    } else {
        g_printerr("method call - getSinkInfo, failed : %d\n", result);
    }
}

bool startAdkSystemInterface() {
    g_print("startAdkSystemInterface\n");

    // ignore SIGPIPE
    struct sigaction action;
    action.sa_handler = SIG_IGN;
    sigemptyset(&action.sa_mask);
    action.sa_flags = 0;
    sigaction(SIGPIPE, &action, nullptr);

    // block signals we will want to "sigwait" on.
    // (needs to be set before we create any thread or the app will be killed
    // if we receive another a second signal while processing the first)
    sigset_t sig_set;
    sigemptyset(&sig_set);
    sigaddset(&sig_set, SIGHUP);
    sigaddset(&sig_set, SIGINT);
    sigaddset(&sig_set, SIGTERM);
    pthread_sigmask(SIG_BLOCK, &sig_set, nullptr);

    AdkSystemInterface &adk_si = AdkSystemInterface::getInstance();

    if (!adk_si.init("adk_system_interface_sample")) {
        g_print("adk si init failed\n");
        return false;
    }

    std::shared_ptr<PropertiesHandler> properties_handler = adk_si.getPropertiesHandler();

    std::shared_ptr<MethodsHandler> methods_handler = adk_si.getMethodsHandler();

    std::shared_ptr<SignalsHandler> signals_handler = adk_si.getSignalsHandler();

    ErrorCode result = ADK_SI_OK;
    std::string spdif_sink = "spdif-optical";
    std::string speaker_sink = "speaker";
    std::string spdif_source = "spdif-in";
    bool mute_value = false;

    g_print("\n\n");
    g_print("----------------- START ----------------");
    g_print("\n\n");
    g_print("registering listener for properties changed signal on property : CurrentSinks\n");
    properties_handler->setPropertiesChangedListener(
        [](PropertiesAttr prop_attr, const std::string &obj_path) -> void {
        std::unordered_set<std::string> changed_properties = prop_attr.getChangedProperties();
        if (changed_properties.find(std::string(adk::kCurrentSinks)) != changed_properties.end()) {
            g_print("\n--->received properties changed signal from %s "
                "obj_path \n", obj_path.c_str());
            std::vector<std::string> sinks = prop_attr.getCurrentSinks();
            g_print("    property : CurrentSinks, value are : \n");
            for (auto &it : sinks) {
                g_print("    %s\n", it.c_str());
            }
            g_print("\n");
        }
    });

    g_print("registering listener for SinkInfoUpdated signal on sink : '%s' and '%s'\n",
        spdif_sink.c_str(), speaker_sink.c_str());
    signals_handler->setSinkInfoUpdatedListener(
        [&spdif_sink, &speaker_sink, &methods_handler](
        SinkInfoUpdatedSigOut info, const std::string &obj_path) -> void {
        if (info.getSinkName() == spdif_sink || info.getSinkName() == speaker_sink) {
            g_print("\n--->received SinkInfoUpdated signal from %s obj_path \n",
                obj_path.c_str());
            g_print("    sink name : %s \n", info.getSinkName().c_str());
            printsinkinfo(info.getSinkInfo());
        }
    });
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));

    g_print("registering listener for ChannelStatusUpdate signal\n");
    signals_handler->setChannelStatusUpdateListener(
        [&spdif_sink, &methods_handler](
        ChannelStatusUpdateSigOut csi, const std::string &obj_path) -> void {
        g_print("\n--->received ChannelStatusUpdate signal from %s obj_path \n", obj_path.c_str());
        g_print("    Channel Status information of : '%s' is : ", csi.getSource().c_str());
        printcsi(csi.getChannelStatus());

        g_print("\n---->set the received channel status information to sink : '%s' via "
            "setChStatusInfo method\n", spdif_sink.c_str());
        ErrorCode result;
        SetChStatusInfoIn csitosink;
        csitosink.setSink(spdif_sink);
        csitosink.setChannelStatusInfo(csi.getChannelStatus());
        result = methods_handler->setChStatusInfo(csitosink);
        if (result == ADK_SI_OK) {
            g_print("setChStatusInfo method call success\n");
        } else {
            g_print("setChStatusInfo method call failed : %d\n", result);
        }
    });
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));

    g_print("\n--->read the current sinks via currentsinks property\n");
    std::vector<std::string> current_sinks;
    std::tie(result, current_sinks) = properties_handler->readCurrentSinks();
    if (result == ADK_SI_OK) {
        g_print("    current sinks are : \n");
        for (auto &it : current_sinks) {
            g_print("    %s\n", it.c_str());
        }
    } else {
        g_print("readCurrentSinks() , failed : %d\n", result);
        return false;
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));

    g_print("\n---->enable sink : '%s' and '%s' via currentsinks property\n",
        spdif_sink.c_str(), speaker_sink.c_str());
    std::vector<std::string> setsink = {spdif_sink, speaker_sink};
    result = properties_handler->updateCurrentSinks(setsink);
    if (result == ADK_SI_OK) {
        g_print("updateCurrentSinks() success\n");
    } else {
        g_print("updateCurrentSinks() , failed : %d\n", result);
        return false;
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));

    getsinkinfo(methods_handler, spdif_sink);
    getsinkinfo(methods_handler, speaker_sink);
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));

    g_print("\n--->set mute to : %s on sink : '%s' and '%s' via setSinkMute method\n",
        mute_value ? "true" : "false", spdif_sink.c_str(), speaker_sink.c_str());
    std::unordered_map<std::string, bool> mute =
        {{spdif_sink, mute_value}, {speaker_sink, mute_value}};
    result = methods_handler->setSinkMute(mute);
    if (result == ADK_SI_OK) {
        g_print("setSinkMute method call success\n");
    } else {
        g_printerr("setSinkMute method call failed : %d\n", result);
        return false;
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));

    getsinkinfo(methods_handler, spdif_sink);
    getsinkinfo(methods_handler, speaker_sink);
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));

    g_print("\n--->set channelstatus bit mask via setChBitMask method for source : '%s'\n",
        spdif_source.c_str());
    SetChBitMaskIn bitmask;
    bitmask.setSource(spdif_source);
    std::vector<uint8_t> mask = {0,0,255,255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,255,255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    g_print("    channelstatus bit mask  : ");
    for (auto &it : mask) {
        g_print("%d ", it);
    }
    g_print("\n");
    bitmask.setBitMask(mask);
    result = methods_handler->setChBitMask(bitmask);
    if (result == ADK_SI_OK) {
        //g_print("setChBitMask method call success\n");
    } else {
        g_printerr("setChBitMask method call failed : %d\n", result);
        return false;
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));

    /* Run mainloop */

    siginfo_t sig_info;
    int sig;
    do {
        int tries = 0;
        do {
            sig = sigwaitinfo(&sig_set, &sig_info);
            if (sig < 0) {
                if (errno == EINTR) {
                    // spurious wakeup, ignore
                    continue;
                }
                g_print("Error on sigwaitinfo errno %d %s, retrying \n", errno, strerror(errno));
                tries++;
                std::this_thread::sleep_for(std::chrono::seconds(1));
            } else {
                break;
            }
        } while (tries < 5);
        if (sig < 0) {
            g_print("Error waiting signal, exiting \n");
            return false;
        }
    } while ((sig != SIGTERM) && (sig != SIGINT) && (sig != SIGHUP));
    g_print("exit signal wait \n");

    return true;
}

int main() {
    return startAdkSystemInterface() ? 0 : 1;
}
